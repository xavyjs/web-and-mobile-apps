## Contains both an android app and several web applications in java and php developed for hackathons, internship and hobby  ##

**1. dash**

DASH is a new paradigm to build video streaming systems. This contains both the android client and server side utility code to run it up. 

Languages: Java (android), php, MySQL
OS: CentOS,SELinux

**2. inventory**

Inventory is my new hobby project which is in pipeline to launch as a SaaS for small scale industries in India for basic inventory management at a low cost. 

Technologies: Java, JSP, Struts, Hibernate, AngularJS, postgresSQL

**3. track_subjects**

One of my web applications developed during my internship it is live and successfully running. It helps research to keep track of their subjects for clinical trials in terms of their progress in the research. Internal messaging and mailing system are also available.

Technologies: PHP, Javascript, MySQL

**4. transport_portal**

An award winning project in the Indian National Level. Selected as top 15 projects out of several thousands for a web app development contest by M/s IBM India called the "Great Mind Challenge 2012". (TeamName: Lords, Place: Pondicherry)

[Weblink](https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/W1302f61f2e98_46e8_8f4b_649337b014b6/page/TGMC%20'12%20Final%20Round%20Results)

A transportation portal which facilitates customers to book bus tickets and track them. 

Languages: Java(JSP)
Tools: IBM websphere, IBM DB2