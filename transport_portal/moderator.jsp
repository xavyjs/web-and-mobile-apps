<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Smart Tours & Travels</title>
<link rel="stylesheet" href="ui.css">
<link rel="stylesheet" href="ui.progress-bar.css">
<link rel="stylesheet" href="style.css">
<script src="jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="progress.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
<div id = "wrapper">
<div id = "container">
<center>
<br><br>
<img src="logo1.png" alt="LOGO"></center>
<div id="progress_bar" class="ui-progress-bar ui-container">
            <div class="ui-progress" style="width: 79%;">
                <span class="ui-label" style="display:none;">Processing <b class="value">79%</b></span>
            </div><!-- .ui-progress -->
        </div><!-- #progress_bar --> 
        <div class="content" id="main_content" style="display: none;">
            <center>
            <h1 style="font-family: BPreplay">Moderator Login</h1>
            <form>
            Moderator ID :<input type="text" name="modid" style="margin: 0px 10px;">
            Moderator Password :<input type="password" name="modpass" style="margin: 0px 10px;">
            <input type="button" value="Login">
            </form>
            </center>
        </div><!-- #main_content -->
        <br><center>&copy;Copyright to Smart Tours & Travels 2013.</center><br>
        </div>
        </div>
</body>
</html>