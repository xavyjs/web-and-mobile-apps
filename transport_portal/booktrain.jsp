<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script language = "javascript">
      var XMLHttpRequestObject = false; 

      if (window.XMLHttpRequest) {
        XMLHttpRequestObject = new XMLHttpRequest();
      } else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }

      function getData(dataSource, divID) 
      { 
        if(XMLHttpRequestObject) {
          var obj = document.getElementById(divID); 
          XMLHttpRequestObject.open("GET", dataSource); 

          XMLHttpRequestObject.onreadystatechange = function() 
          { 
            if (XMLHttpRequestObject.readyState == 4 && 
              XMLHttpRequestObject.status == 200) { 
                obj.innerHTML = XMLHttpRequestObject.responseText; 
            } 
          } 

          XMLHttpRequestObject.send(null); 
        }
      }
    </script>
</head>
<body>
<br>TRAIN BOOKING INFO<br><br>
<form method="post" action="" id="bookform">
Source Station &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="text" name="username"><br><br>
Destination Station  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="text" name="username"><br><br>
Class &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <select name="class">
<option value="First Class AC">First Class AC</option>
<option value="Second AC">Second AC</option>
<option value="Third AC">Third AC</option>
<option value="3 AC Economy">3 AC Economy</option>
<option value="AC Chair CAR">AC Chair CAR</option>
<option value="First Class">First Class</option>
<option value="Sleeper Class">Sleeper Class</option>
<option value="Second Seating">Second Seating</option>
</select><br><br>
Journey Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="date" name="djourney"/><br><br>
Departure Time &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="time" name="depttime"/><br><br>
Arrival Time &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="time" name="arrvtime"/><br><br>
Train Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <select name="class">
<option value="Rajdhani/Duronto">Rajdhani/Duronto</option>
<option value="Shatabdi">Shatabdi</option>
<option value="SuperFast/Yuva">SuperFast/Yuva</option>
<option value="MailExpress">MailExpress</option>
<option value="Passenger/Ordinary">Passenger/Ordinary</option>
<option value="RailMotor">RailMotor</option>
<option value="DarjalingHimalayan">DarjalingHimalayan</option>
<option value="OverseasTrain">OverseasTrain</option>
<option value="Garibrath">Garibrath</option>
<option value="Composite">Composite</option>
</select><br><br>
&nbsp;&nbsp;&nbsp;<input type="radio" name="trip" onclick="getData('rt.jsp','rt')"> Return Journey &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="trip" onclick = "getData('dummy.jsp', 'rt')"> One Way<br><br>
<div id="rt">

</div>
<input type="button" value="SEARCH TRAIN">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="RESET">
</form>
</body>
</html>