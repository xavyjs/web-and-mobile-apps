<%@ page import="java.sql.*" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="style.css"/>
<link rel="stylesheet" type="text/css" href="menu.css"/>
<script type="text/javascript" src="lib/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
<link rel="stylesheet" href="source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
<script type="text/javascript" src="source/jquery.fancybox.pack.js?v=2.1.4"></script>
<link rel="stylesheet" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
<link rel="stylesheet" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript">
$(".fancybox").fancybox({
    openEffect  : 'none',
    closeEffect : 'none'
});
</script>
<script src="jquery-1.9.1.js"></script>
<script src="jquery-ui-1.10.1.custom.js"></script>
<link rel="stylesheet" href="jquery-ui-1.10.1.custom.css">
<script>
  $(function() {
    $( "#deptdate" ).datepicker({ minDate: 0, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatef", altFormat: "DD, d MM, yy"
     });
     $( "#arrvdate" ).datepicker({ minDate: 0, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatet", altFormat: "DD, d MM, yy"
     });
  });
  </script>
   <script type="text/javascript">
  function validate()
  {
  var lf = document.forms["bookform"]["deptplace"].value;
  var gt = document.forms["bookform"]["arrvplace"].value;
  var dd = document.forms["bookform"]["deptdate"].value;
  var ad = document.forms["bookform"]["arrvdate"].value;
  var ty = document.forms["bookform"]["type"].value;
  var tr = document.getElementsByName("trip");
  if(lf == "" || lf== null)
  {alert("Provide the departure place");return false;}
  else if(gt == ""|| gt==null)
  {alert("Provide the arrival place");return false;}
  else if(dd==""||gt==null)
  {alert("Provide the departure date");return false;}
  else if(ty==""||ty==null)
  {alert("Provide the car type");return false;}
  else 
  {
  var formValid = false;
        if (tr[0].checked) 
          formValid = true;      
    if (formValid)
    {
     if(ad==""||ad==null)
     {alert("Provide the arrival date");return false;}
    }
    var formValid1 = false;

    var i = 0;
    while (!formValid1 && i < tr.length) {
        if (tr[i].checked) formValid1 = true;
        i++;        
    }

    if (!formValid1) alert("Provide the Trip Type");
    return formValid1;
  }
  }
  </script>
</head>
<body>
<div id="signup" style="display:none;width:700px;">
<div id="checkavail" style="position: absolute;margin: 70px 350px"><input type="button" value="Check Availability" id="checkb"></div>
<div id="checkbox" style="position: absolute;margin: 70px  500px;width: 200px"></div>
<div id="sam" style="position: absolute;margin: 150px  355px;width: 200px">
<img src="sam.png">
</div>
<div id="regmain" style="margin: 0px 20px">
<center><h2 style="font-family: BPreplay">Member Registration Form</h2></center><br>
<form method="post" action="regprocess.jsp" name="regform" onSubmit="return validateForm()">
Username : <input type="text" name="username" id="usern" style="margin: 0px 60px"/><br><br>
Password : <input type="password" name="password" style="margin: 0px 63px"/><br><br>
Confirm Password : <input type="password" name="repassword" style="margin: 0px 5px"/><br><br>
First Name :<input type="text" name="fname" style="margin: 0px 57px"/><br><br>
Last Name  :<input type="text" name="lname" style="margin: 0px 59px"/><br><br>
Email      : <input type="text" name="email" style="margin: 0px 85px"/><br><br>
State      : <input type="text" name="state" style="margin: 0px 90px"/><br><br>
City       : <input type="text" name="city" style="margin: 0px 95px"/><br><br>
Country : <input type="text" name="country" style="margin: 0px 70px"/><br><br>
Mobile Number: <input type="text" name="mobile" style="margin: 0px 23px"/><br><br>
Sex : <select name="sex" style="margin: 0px 98px">
<option value="male">Male</option>
<option value="female">Female</option>
</select><br><br>
Pincode : <input type="text" name="pincode" style="margin: 0px 70px"/><br><br>
Date Of Birth :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="dob" id="dob">
<br><br>
Address : <textarea rows="" cols="50" name="address" style="margin: 0px 67px"></textarea><br><br>
Secret Question : <select name="secretq" id="secret" style="margin: 0px 20px;">
<option value=""></option>
<option value="What is your favorite Animal ?">What is your favorite Animal ?</option>
<option value="What is your mother's maiden name ?">What is your mother's maiden name ?</option>
<option value="Which is your favorite subject ?">Which is your favorite subject ?</option>
<option value="What is name of your High School ?">What is name of your High School ?</option>
</select><br><br>
Answer :<input type="text" id="secans" style="margin: 0px 75px" name="secreta"><br><br>
<input type="submit" value="Register" /><input type="reset" value="Reset" style="margin: 0px 30px"/><br><br>
</form>
</div>
</div>
<div id="signin" style="display:none;width:300px;">
<h3>Member Login</h3>
<form  name = "logform" method="post" action="logprocess.jsp" onsubmit="return validateForm2()">
Username : <input type="text" name="username" ><br><br>
Password : <input type="password" name="password" ><br><br><br>
<input type="submit" value="LOGIN">
<input type="reset" value="RESET" >&emsp;<a href="secretques.jsp">Forgot Password ?</a><br><br>
</form>
</div>
<div id="main" style="display:none;width:430px;padding: 5px 10px;">
<h3><font face="BPreplay">Modify Search</font></h3>
<form method="post" action="selectcab.jsp" onsubmit="return validate()" name="bookform">
 <div id="se" style="position: absolute;margin: 0px 175px;width: 200px">
    <label>Going To</label><br>
    <input type="text" name="arrvplace"><br><br>
    <input type="radio" name="trip" style="margin: 123px 0px" value="RoundTrip" onclick="arrvdate.disabled=false,alternatet.disabled=false">Round Trip&emsp;
    <input type="radio" name="trip" value="OneWay" onclick="arrvdate.disabled=true,alternatet.disabled=true">One Way
   </div>
   <div id="fi">
    <label>Leaving From</label><br>
    <input type="text" name="deptplace"><br><br>
    <label>Departure Date</label><br>
    <input type="text" name="deptdate" id="deptdate"><input type="text" id="alternatef" style="margin: 0px 21px" size="28"><br><br>
    <label>Arrival Date</label><br>
    <input type="text" name="arrvdate" id="arrvdate" disabled="disabled"><input type="text" id="alternatet" style="margin: 0px 21px" size="28" disabled="disabled"><br><br>
    <label>Car Type</label><br>
    <select style="width: 138px" name="type">
    <option value="">Select Type</option>
    <option value="AC">AC</option>
    <option value="NON-AC">NON-AC</option>
    </select>
    <br><br>
   </div>
   <div style="margin: 15px 160px;width: 300px">
   <input type="submit" value="Search Cab"><input type="reset" value="Cancel" style="margin: 0px 30px"> 
   </div>
   </form>
   </div>
   <div id="wrapper">
<div id="logomain">
<img src="logo1.png" alt="LOGO"></div>
<div id="titleimg">
<img src="guest_icon.png" width="50" height="50"></div>
<div id="title">
<b>Welcome!</b>&nbsp;&nbsp;<u>Guest
</u><br><a href="#signup" class="fancybox">Sign Up</a> | <a href="#signin" class="fancybox">Sign In</a>
</div>
<div id="banner">
<ul class="menuH decor1">
    <li style="margin-left: 10px;"><a href="index.jsp">Home</a></li>
    <li><a href="#2" class="arrow">Features</a>
        <ul>
            <li><a href="#">Explore Locations</a></li>
        </ul>
    </li>
    <li><a href="#2" class="arrow">My Account</a>
        <ul>
            <li><a href="profile.jsp">Edit Profile</a></li>
            <li><a href="#">Give Feedback</a></li>
            <li><a href="#">My Bookings</a></li>
        </ul>
    </li>
    <li><a href="#3">About Us</a></li>
    <li><a href="#3">Contact Us</a></li>
</ul>
</div>
<div id="container">
<h3 style="padding: 6px 38px;"><img src="carsmall.png"/>&nbsp;&nbsp;&nbsp;<u>CAB SEARCH DETAILS :</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#main" class="fancybox">MODIFY SEARCH</a></h3>
<div id="selectcab">
<%
String trip = request.getParameter("trip");
String deptplace = request.getParameter("deptplace");
String arrvplace = request.getParameter("arrvplace");
String type = request.getParameter("type");
String deptdate = request.getParameter("deptdate");
String arrvdate = request.getParameter("arrvdate");
System.out.println(trip);
System.out.println(deptplace);
System.out.println(arrvplace);
System.out.println(type);
System.out.println(deptdate);
System.out.println(arrvdate);
int counter =0;
Class.forName("com.ibm.db2.jcc.DB2Driver");
System.out.println("Driver Loaded Successfully");
Connection con = DriverManager.getConnection("jdbc:db2://localhost:50000/LORDS","senthilkumar","senthil");
System.out.println("Connection Made Successfully");
PreparedStatement stmt = con.prepareStatement("SELECT trip_id,deptplace,arrvplace,deptdate,arrvdate,depttime,car_model,car_type,status,trip from SENTHILKUMAR.CAR_SCHEDULE where trip = '"+trip+"' and deptplace = '"+deptplace+"' and arrvplace = '"+arrvplace+"' and deptdate = '"+deptdate+"' or arrvdate = '"+arrvdate+"' and car_type='"+type+"'");
ResultSet rs = stmt.executeQuery();
System.out.println("Results Executed");
out.println("<table width='100%'cellspacing=0' cellpadding='10'>");
        out.println("<tr style='border: 1px solid #9FDE18; background: #9FDE18 repeat-x; border-top-left-radius: 10px;border-top-right-radius: 4px;border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;color :white'>");
        out.println("<th>Trip_Type</th>");
        out.println("<th>Travel_ID</th>");
        out.println("<th>Source Address</th>");
        out.println("<th>Departure Address</th>");
        out.println("<th>Departure Date</th>");
        out.println("<th>Arival Date</th>");
        out.println("<th>Departure Time</th>");
        out.println("<th>Vehicle Name</th>");
        out.println("<th>Vehicle Type</th>");
        out.println("<th>Status</th>");
        out.println("</tr>");
while(rs.next())
{
counter++;
out.println("<tr>");
out.println("<td>");
out.println(rs.getString(10));
out.println("</td>");
out.println("<td>");
out.println(rs.getString(1));
out.println("</td>");
out.println("<td>");
out.println(rs.getString(2));
out.println("</td>");
out.println("<td>");
out.println(rs.getString(3));
out.println("</td>");
out.println("<td>");
out.println(rs.getString(4));
out.println("</td>");
out.println("<td>");
out.println(rs.getString(5));
out.println("</td>");
out.println("<td>");
out.println(rs.getString(6));
out.println("</td>");
out.println("<td>");
out.println(rs.getString(7));
out.println("</td>");
out.println("<td>");
out.println(rs.getString(8));
out.println("</td>");
out.println("<td>");
if(rs.getString(9).equals("Available"))
{
out.println("<form method='post' action='booknext.jsp?tripid="+rs.getString(1)+"'>");
out.println("<input type='submit' value = 'Book Now'/>");
out.println("</form>");
}
else
{
out.println("Booked");
}
out.println("</td>");
out.println("</tr>");
}
out.println("</table>");
out.println("<br>");
out.println("<br>");
out.println("<br>");
     %>
</div>
</div>
<div id="footer"><center><div id="carmenu">
<a href="uindex.jsp" style="padding: 0px 15px;color: white">Home</a>
<a href="" style="padding: 0px 15px;color: white">Terms & Conditions</a>
<a href="" style="padding: 0px 15px;color: white">About Us</a>
<a href="" style="padding: 0px 15px;color: white">Contact Us</a>
</div></center></div><br><center>&copy;Copyright to Smart Tours & Travels 2013.</center><br>
</div>
</body>
</html>