<%@ page import="java.sql.*" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Forgot Password</title>
<link rel="stylesheet" type="text/css" href="style.css"/>
<link rel="stylesheet" type="text/css" href="menu.css"/>
<script type="text/javascript" src="lib/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
<link rel="stylesheet" href="source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
<script type="text/javascript" src="source/jquery.fancybox.pack.js?v=2.1.4"></script>
<link rel="stylesheet" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
<link rel="stylesheet" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript">
$(".fancybox").fancybox({
    openEffect  : 'none',
    closeEffect : 'none'
});
</script>
<script type="text/javascript">
function validate()
{
var input = document.forms["security1"]["answer"].value;
if(input==""||input==null)
{
alert("Provide the answer for the question !!");
return false;
}
}
</script>
</head>
<body>
<div id="signup" style="display:none;width:700px;">
<div id="checkavail" style="position: absolute;margin: 70px 350px"><input type="button" value="Check Availability" id="checkb"></div>
<div id="checkbox" style="position: absolute;margin: 70px  500px;width: 200px"></div>
<div id="sam" style="position: absolute;margin: 150px  355px;width: 200px">
<img src="sam.png">
</div>
<div id="regmain" style="margin: 0px 20px">
<center><h2 style="font-family: BPreplay">Member Registration Form</h2></center><br>
<form method="post" action="regprocess.jsp" name="regform" onSubmit="return validateForm()">
Username : <input type="text" name="username" id="usern" style="margin: 0px 60px"/><br><br>
Password : <input type="password" name="password" style="margin: 0px 63px"/><br><br>
Confirm Password : <input type="password" name="repassword" style="margin: 0px 5px"/><br><br>
First Name :<input type="text" name="fname" style="margin: 0px 57px"/><br><br>
Last Name  :<input type="text" name="lname" style="margin: 0px 59px"/><br><br>
Email      : <input type="text" name="email" style="margin: 0px 85px"/><br><br>
State      : <input type="text" name="state" style="margin: 0px 90px"/><br><br>
City       : <input type="text" name="city" style="margin: 0px 95px"/><br><br>
Country : <input type="text" name="country" style="margin: 0px 70px"/><br><br>
Mobile Number: <input type="text" name="mobile" style="margin: 0px 23px"/><br><br>
Sex : <select name="sex" style="margin: 0px 98px">
<option value="male">Male</option>
<option value="female">Female</option>
</select><br><br>
Pincode : <input type="text" name="pincode" style="margin: 0px 70px"/><br><br>
Date Of Birth :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="dob" id="dob">
<br><br>
Address : <textarea rows="" cols="50" name="address" style="margin: 0px 67px"></textarea><br><br>
Secret Question : <select name="secretq" id="secret" style="margin: 0px 20px;">
<option value=""></option>
<option value="What is your favorite Animal ?">What is your favorite Animal ?</option>
<option value="What is your mother's maiden name ?">What is your mother's maiden name ?</option>
<option value="Which is your favorite subject ?">Which is your favorite subject ?</option>
<option value="What is name of your High School ?">What is name of your High School ?</option>
</select><br><br>
Answer :<input type="text" id="secans" style="margin: 0px 75px" name="secreta"><br><br>
<input type="submit" value="Register" /><input type="reset" value="Reset" style="margin: 0px 30px"/><br><br>
</form>
</div>
</div>
<div id="signin" style="display:none;width:300px;">
<h3>Member Login</h3>
<form  name = "logform" method="post" action="logprocess.jsp" onsubmit="return validateForm2()">
Username : <input type="text" name="username" ><br><br>
Password : <input type="password" name="password" ><br><br><br>
<input type="submit" value="LOGIN">
<input type="reset" value="RESET" >&emsp;<a href="secretques.jsp">Forgot Password ?</a><br><br>
</form>
</div>
<div id="wrapper">
<div id="titleimg">
<img src="guest_icon.png" width="50" height="50"></div>
<div id="logomain">
<img src="logo1.png" alt="LOGO"></div>
<div id="title">
<b>Welcome!</b>&nbsp;&nbsp;<u>Guest</u><br><a href="#signup" class="fancybox">Sign Up</a> | <a href="#signin" class="fancybox">Sign In</a>
</div>
<div id="banner">
<ul class="menuH decor1">
    <li style="margin-left: 10px;"><a href="index.jsp">Home</a></li>
    <li><a href="#2" class="arrow">Features</a>
        <ul>
            <li><a href="#">Explore Locations</a></li>
        </ul>
    </li>
    <li><a href="#3">About Us</a></li>
    <li><a href="#3">Contact Us</a></li>
</ul>
</div>
 <div id="container">
 <div style="position: relative;width: 300px;height: 180px;background : #e8eefa;border: 2px;border-style: solid;border-color: #c4daff;margin :15px 300px;padding: 10px 35px">
 <h3 style="font-family: BPreplay">Find Your Account<hr></h3>
 <%
String username = null;
int count = 0;
String input = request.getParameter("finduser");
System.out.println(input);
Class.forName("com.ibm.db2.jcc.DB2Driver");
Connection con = DriverManager.getConnection("jdbc:db2://localhost:50000/lords","senthilkumar","senthil");
PreparedStatement stmt = con.prepareStatement("select username from senthilkumar.user_info where username='"+input+"' or email='"+input+"'");
ResultSet rs = stmt.executeQuery();
while(rs.next())
{
count++;
username = rs.getString(1);
System.out.println(username);
}
if(count == 0)
{
out.println("No Such Username or Mail Address Exists");
}
else
{%>
<div style="position: absolute;margin: 0px 230px;">
<%
  try{ 
  int counter = 0;  
    Statement stmt1 = con.createStatement();
    System.out.println("Query Executed");
    String strQuery = "select username from senthilkumar.profile where username = '"+username+"'";
    ResultSet rs1 = stmt1.executeQuery(strQuery);
    System.out.println("Query Executed");
    while(rs1.next()){
    counter++;
       %>      
      <img src="image1.jsp?imgid=<%=rs1.getString(1)%>" width="50" height="50">
      <%
    }
    if(counter == 0)
    {
    %>
    <img src="user.png" width="50" height="50"/>
    <%
    }
    rs1.close();
  }
  catch(Exception e)
  {
    e.getMessage();
  }
  %>
  <br>
  <p style="margin: 5px 0px" align="center"><% out.println(username); %></p>
</div>
<div>Answer your secret question ?
 <form method="post" action="finrecover.jsp?username=<%=username%>" onsubmit="return validate()" name="security1">
<input type="text" name="answer" size="25" style="margin: 10px 0px"><br>
<input type="submit" value="Answer" style="margin: 10px 0px">&emsp;<a href="index.jsp"><input type="button" value="Cancel"></a>
</form>
</div>
<%
}
 %>
  </div>
 <br>
 </div>
 <div id="footer"><center><div id="carmenu">
<a href="uindex.jsp" style="padding: 0px 15px;color: white">Home</a>
<a href="" style="padding: 0px 15px;color: white">Terms & Conditions</a>
<a href="" style="padding: 0px 15px;color: white">About Us</a>
<a href="" style="padding: 0px 15px;color: white">Contact Us</a>
</div></center></div><br><center>&copy;Copyright to Smart Tours & Travels 2013.</center><br>
</div>
</body>
</html>