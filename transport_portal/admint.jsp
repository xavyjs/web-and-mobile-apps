<%@ page import="java.sql.*" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Administrator Console</title>
<script src="jquery-1.9.1.js"></script>
<script src="jquery-ui-1.10.1.custom.js"></script>
<link rel="stylesheet" href="jquery-ui-1.10.1.custom.css">
<script src="jquery.timeentry.js"></script>
<script src="jquery.timeentry.min.js"></script>
<link rel="stylesheet" type="text/css" href="jquery.timeentry.css">
<link rel="stylesheet" type="text/css" href="style.css"/>
<script>
$(function() {
$( "#tabs,#vehtype,#vehroute" ).tabs().addClass( "ui-tabs-horizontal" );
});
$(function() {
$( "#subpost,#submanagec,#submanageb,#routecar,#routebus" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
$( "#subpost,#submanagec,#submanageb,#routecar,#routebus li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
});
</script>
<style>
.ui-tabs-vertical { width: 100%; font-size: small;}
.ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 12em; }
.ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
.ui-tabs-vertical .ui-tabs-nav li a { display:block; }
.ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; border-right-width: 1px; }
.ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: left; width: 40em;}
</style>
<style>
.ui-tabs-horizontal {font-size: small;}
</style>
<script>
$(function(){
function addvehicle()
{
var cid = $("#cid").val();
var cnum = $("#cnum").val();
var cmod = $("#cmod").val();
var cname = $("#cname").val();
var ctype = $("#ctype").val();
if(cid==""||cid==null)
{alert("Enter the Car ID");}
else if(cnum==null||cnum=="")
{alert("Enter the Car Number");}
else if(cmod==null||cmod=="")
{alert("Enter the Car Model");}
else if(cname==""||cname==null)
{alert("Enter the Car Name");}
else if(ctype==""||ctype==null)
{alert("Enter the Car type");}
else
{
$.post("addveh.jsp", { cid: cid, cnum : cnum, cmod : cmod, cname : cname,ctype : ctype  },
			function(count){
					//show that the username is NOT available
					$('#result').html(count);
					//setTimeout();
		});
		}
}
$("#addv").click(function()
{
addvehicle();
});
$("#reset").click(function(){$("#result").load('dummy.jsp');});
$("#load").load('viewcab.jsp');
$("#load6").load('viewbus.jsp');
$("#load7").load('viewcroute.jsp');
$("#load8").load('viewbroute.jsp');
$("#refresh").click(function(){$("#load").load('viewcab.jsp');});
$("#refresh3").click(function(){$("#load6").load('viewbus.jsp');});
$("#refresh7").click(function(){$("#load7").load('viewcroute.jsp');});
$("#refresh8").click(function(){$("#load8").load('viewbroute.jsp');});
});
</script>
<script type="text/javascript">
$(function(){$("#fileffect").hide();$("#fileffect1").hide();$("#fileffect2").hide();$("#fileffect3").hide();$("#fileffect4").hide();});
function runeff()
{
var filter = $("#filter").val();
if(filter=="cartype")
{
$(function(){$("#fileffect1").hide();$("#fileffect2").hide();$("#fileffect3").hide();$("#fileffect4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#fileffect").show(selectedeff,option,500);});
return true;
}
if(filter=="carid")
{
$(function(){$("#fileffect").hide();$("#fileffect2").hide();$("#fileffect3").hide();$("#fileffect4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#fileffect1").show(selectedeff,option,500);});
return true;
}
if(filter=="carnumber")
{
$(function(){$("#fileffect").hide();$("#fileffect1").hide();$("#fileffect3").hide();$("#fileffect4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#fileffect2").show(selectedeff,option,500);});
return true;
}
if(filter=="carname")
{
$(function(){$("#fileffect").hide();$("#fileffect1").hide();$("#fileffect2").hide();$("#fileffect4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#fileffect3").show(selectedeff,option,500);});
return true;
}
if(filter=="carmodel")
{
$(function(){$("#fileffect").hide();$("#fileffect1").hide();$("#fileffect2").hide();$("#fileffect3").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#fileffect4").show(selectedeff,option,500);});
return true;
}
if(filter=="")
{
$(function(){$("#fileffect").hide();$("#fileffect1").hide();$("#fileffect2").hide();$("#fileffect3").hide();$("#fileffect4").hide();});
return true;
}
}
</script>
<script type="text/javascript">
$(function(){
function filtertype()
{
var filct = $("#ct").val();
if(filct==""||filct==null)
{alert("Enter the Car Type");}
else{
$("#load").load('filter.jsp?filct='+filct+'');
}
}
function filtertype1()
{
var filci = $("#ci").val();
if(filci==""||filci==null)
{alert("Enter the Car ID");}
else
{
$("#load").load('filter1.jsp?filci='+filci+'');
}
}
function filtertype2()
{
var filcnu = $("#cnu").val();
if(filcnu==""||filcnu==null)
{alert("Enter the Car Number");}
else
{
$("#load").load('filter2.jsp?filcnu='+filcnu+'');
}
}
function filtertype3()
{
var filcn = $("#cna").val();
if(filcn==""||filcn==null)
{alert("Enter the Car Name");}
else
{
$("#load").load('filter3.jsp?filcn='+filcn+'');
}
}
function filtertype4()
{
var filcm = $("#cmo").val();
if(filcm==""||filcm==null)
{alert("Enter the Car Model");}
else
{
$("#load").load('filter4.jsp?filcm='+filcm+'');
}
}
$("#filct").click(function(){
filtertype();
});
$("#filci").click(function(){
filtertype1();
});
$("#filcnu").click(function(){
filtertype2();
});
$("#filcn").click(function(){
filtertype3();
});
$("#filcm").click(function(){
filtertype4();
});
});
</script>
<script type="text/javascript">
$(function(){
function remove()
{
var cid = $("#remcar").val();
if(cid == ""||cid=="null")
{
alert("Please provide your Car ID");
}
else
{
$("#con").load('removecar.jsp?cid='+cid+'');
}
}
$("#refresh1").click(function(){
$("#con").load('dummy.jsp');
});
$("#remb").click(function(){
remove();
});
});
</script>
<!-- Manage Bus Script -->
<script>
$(function(){
function addvehicleb()
{
var bid = $("#bid").val();
var bnum = $("#bnum").val();
var bname = $("#bname").val();
var btype = $("#btype").val();
 if(bid==""||bid==null)
 {alert("Enter the Bus ID");}
 else if(bnum==null||bnum=="")
 {alert("Enter the Bus Number");}
 else if(bname==""||bname==null)
 {alert("Enter the Bus Name");}
 else if(btype==""||btype==null)
 {alert("Enter the Bus Type");}
 else
 {
$.post("addvehb.jsp", { bid: bid, bnum : bnum, bname : bname,btype : btype  },
			function(count){
					//show that the username is NOT available
					$('#resultb').html(count);
					//setTimeout();
		});
		}
}
function rem()
{
var busid = $("#removebus").val();
if(busid==""||busid==null)
{alert("Enter the Bus ID");}
else
{
$("#remco").load("rembus.jsp?busid="+busid+"");
}
}
$("#addvb").click(function()
{
addvehicleb();
});
$("#rembus").click(function()
{
rem();
});
$("#reset1").click(function(){$("#resultb").load('dummy.jsp');});
$("#refresh2").click(function(){$("#remco").load('dummy.jsp');});
});
</script>
<script type="text/javascript">
$(function(){$("#fileffectb").hide();$("#fileffectb1").hide();$("#fileffectb2").hide();$("#fileffectb3").hide();$("#fileffectb4").hide();});
function runeffb()
{
var filter = $("#filterb").val();
if(filter=="bustype")
{
$(function(){$("#fileffectb1").hide();$("#fileffectb2").hide();$("#fileffectb3").hide();$("#fileffectb4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#fileffectb").show(selectedeff,option,500);});
return true;
}
if(filter=="busid")
{
$(function(){$("#fileffectb").hide();$("#fileffectb2").hide();$("#fileffectb3").hide();$("#fileffectb4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#fileffectb1").show(selectedeff,option,500);});
return true;
}
if(filter=="busnumber")
{
$(function(){$("#fileffectb").hide();$("#fileffectb1").hide();$("#fileffectb3").hide();$("#fileffectb4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#fileffectb2").show(selectedeff,option,500);});
return true;
}
if(filter=="busname")
{
$(function(){$("#fileffectb").hide();$("#fileffectb1").hide();$("#fileffectb2").hide();$("#fileffectb4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#fileffectb3").show(selectedeff,option,500);});
return true;
}
if(filter=="")
{
$(function(){$("#fileffectb").hide();$("#fileffectb1").hide();$("#fileffectb2").hide();$("#fileffectb3").hide();$("#fileffectb4").hide();});
return true;
}
}
</script>
<script type="text/javascript">
$(function(){
function filtertypeb()
{
var filbt = $("#bt").val();
if(filbt==""||filbt==null)
{alert("Enter the Bus Type");}
else
{
$("#load6").load('filterb.jsp?filbt='+filbt+'');
}
}
function filtertypeb1()
{
var filbi = $("#bi").val();
if(filbi==""||filbi==null)
{alert("Enter the Bus ID");}
else
{
$("#load6").load('filterb1.jsp?filbi='+filbi+'');
}
}
function filtertypeb2()
{
var filbnu = $("#bnu").val();
if(filbnu==""||filbnu==null)
{alert("Enter the Bus Number");}
else
{
$("#load6").load('filterb2.jsp?filbnu='+filbnu+'');
}
}
function filtertypeb3()
{
var filbn = $("#bna").val();
if(filbn==""||filbn==null)
{alert("Enter the Bus Name");}
else
{
$("#load6").load('filterb3.jsp?filbn='+filbn+'');
}
}
$("#filbt").click(function(){
filtertypeb();
});
$("#filbi").click(function(){
filtertypeb1();
});
$("#filbnu").click(function(){
filtertypeb2();
});
$("#filbn").click(function(){
filtertypeb3();
});
});
</script>
<script>
  $(function() {
    $( "#deptdate" ).datepicker({ minDate: 0, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatef", altFormat: "DD, d MM, yy"
     });
     $( "#arrvdate" ).datepicker({ minDate: 0, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatet", altFormat: "DD, d MM, yy"
     });
     $( "#deptdateb" ).datepicker({ minDate: 0, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatebf", altFormat: "DD, d MM, yy"
     });
     $( "#arrvdateb" ).datepicker({ minDate: 0, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatebt", altFormat: "DD, d MM, yy"
     });
  });
  </script>
<script type="text/javascript">
$(function () {
	$('.timepic').timeEntry();
	$('.timepic1').timeEntry();
});
</script>
<!-- Adding Car Routes -->
<script type="text/javascript">
$(function(){
function addroute()
{
var triptyperc = $("#triptyperc").val();
var tripidrc = $("#tripidrc").val();
var deptplacerc = $("#deptplacerc").val();
var arrvplacerc = $("#arrvplacerc").val();
var deptdaterc = $("#deptdate").val();
var arrvdaterc = $("#arrvdate").val();
var carnumberrc = $("#carnumberrc").val();
var carmodelrc = $("#carmodelrc").val();
var cartyperc = $("#cartyperc").val();
var depttimerc=$("#depttimerc").val();
var arrvtimerc=$("#arrvtimerc").val();
 if(triptyperc==""||triptyperc==null)
 {alert("Provide Trip Type");}
 else if(tripidrc==""||tripidrc==null)
 {alert("Provide Trip ID");}
 else if(deptplacerc==""||deptplacerc==null)
 {alert("Provide Departure Place");}
 else if(arrvplacerc==""||arrvplacerc==null)
 {alert("Provide Arrival Place");}
 else if(deptdaterc==""||deptdaterc==null)
 {alert("Provide Departure Date");}
 else if(arrvdaterc==""||arrvdaterc==null)
 {alert("Provide Arrival Date");}
 else if(depttimerc==""||depttimerc==null)
 {alert("Provide Departure Time");}
 else if(arrvtimerc==""||arrvtimerc==null)
 {alert("Provide Arrival Time");}
 else if(carnumberrc==""||carnumberrc==null)
 {alert("Provide Car Number");}
 else if(carmodelrc==""||carmodelrc==null)
 {alert("Provide Car Model");}
 else if(cartyperc==""||cartyperc==null)
 {alert("Provide Car type");}
 else
 {
 alert("success");
$.post("addroutecar.jsp",{triptype : triptyperc , tripid : tripidrc , deptplace : deptplacerc , arrvplace : arrvplacerc , deptdate : deptdaterc , arrvdate : arrvdaterc , depttime : depttimerc , arrvtime : arrvtimerc , carnumber : carnumberrc , carmodel : carmodelrc , cartype : cartyperc} , function(res){
 $("#routeresc").html(res);
});
 }
}
$("#adroutec").click(function(){
addroute();
});
});
</script>
<!-- Trip Car Remove -->
<script type="text/javascript">
function removeroc()
{
var tripid = document.forms["remform"]["remroutec"].value;
if(tripid==""||tripid==null)
{alert("Provide the Trip ID");
return false;
}
}
</script>
<!-- Filter route script -->
<script type="text/javascript">
$(function(){$("#filterroute1").hide();$("#filterroute2").hide();$("#filterroute3").hide();$("#filterroute4").hide();});
function runeffrc()
{
var filterroute = $("#filterrc").val();
if(filterroute=="rctripid")
{
$(function(){$("#filterroute2").hide();$("#filterroute3").hide();$("#filterroute4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#filterroute1").show(selectedeff,option,500);});
return true;
}
if(filterroute=="rcdeptplace")
{
$(function(){$("#filterroute1").hide();$("#filterroute3").hide();$("#filterroute4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#filterroute2").show(selectedeff,option,500);});
return true;
}
if(filterroute=="rcarrvplace")
{
$(function(){$("#filterroute1").hide();$("#filterroute2").hide();$("#filterroute4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#filterroute3").show(selectedeff,option,500);});
return true;
}
if(filterroute=="rctriptype")
{
$(function(){$("#filterroute1").hide();$("#filterroute2").hide();$("#filterroute3").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#filterroute4").show(selectedeff,option,500);});
return true;
}
if(filterroute=="")
{
$(function(){$("#filterroute1").hide();$("#filterroute2").hide();$("#filterroute3").hide();$("#filterroute4").hide();});
return true;
}
}
</script>
<script type="text/javascript">
$(function(){
function filtertypebr1()
{
var filti = $("#trctripid").val();
if(filti==""||filti==null)
{alert("Enter the Trip ID");}
else
{
$("#load7").load('filterroute1.jsp?tripid='+filti+'');
}
}
function filtertypebr2()
{
var fildp = $("#trcdeptplace").val();
if(fildp==""||fildp==null)
{alert("Enter the Departure Place");}
else
{
$("#load7").load('filterroute2.jsp?deptplace='+fildp+'');
}
}
function filtertypebr3()
{
var filap = $("#trcarrvplace").val();
if(filap==""||filap==null)
{alert("Enter the Arrival Place");}
else
{
$("#load7").load('filterroute3.jsp?arrvplace='+filap+'');
}
}
function filtertypebr4()
{
var filtt = $("#trctriptype").val();
if(filtt==""||filtt==null)
{alert("Enter the Trip Type");}
else
{
$("#load7").load('filterroute4.jsp?triptype='+filtt+'');
}
}
$("#brctripid").click(function(){
filtertypebr1();
});
$("#brcdeptplace").click(function(){
filtertypebr2();
});
$("#brcarrvplace").click(function(){
filtertypebr3();
});
$("#brctriptype").click(function(){
filtertypebr4();
});
});
</script>
<!-- Adding Bus Routes -->
<script type="text/javascript">
$(function(){
function addrouteb()
{
var triptyperb = $("#triptyperb").val();
var tripidrb = $("#tripidrb").val();
var deptplacerb = $("#deptplacerb").val();
var arrvplacerb = $("#arrvplacerb").val();
var deptdaterb = $("#deptdateb").val();
var arrvdaterb = $("#arrvdateb").val();
var busidr = $("#busidr").val();
var busnamer = $("#busnamer").val();
var bustyper = $("#bustyper").val();
var depttimerb=$("#depttimerb").val();
var arrvtimerb=$("#arrvtimerb").val();
 if(triptyperb==""||triptyperb==null)
 {alert("Provide Trip Type");}
 else if(tripidrb==""||tripidrb==null)
 {alert("Provide Trip ID");}
 else if(deptplacerb==""||deptplacerb==null)
 {alert("Provide Departure Place");}
 else if(arrvplacerb==""||arrvplacerb==null)
 {alert("Provide Arrival Place");}
 else if(deptdaterb==""||deptdaterb==null)
 {alert("Provide Departure Date");}
 else if(arrvdaterb==""||arrvdaterb==null)
 {alert("Provide Arrival Date");}
 else if(depttimerb==""||depttimerb==null)
 {alert("Provide Departure Time");}
 else if(arrvtimerb==""||arrvtimerb==null)
 {alert("Provide Arrival Time");}
 else if(busidr==""||busidr==null)
 {alert("Provide Bus ID");}
 else if(busnamer==""||busnamer==null)
 {alert("Provide Bus Name");}
 else if(bustyper==""||bustyper==null)
 {alert("Provide Bus type");}
 else
 {
 alert("success");
$.post("addroutebus.jsp",{triptype : triptyperb , tripid : tripidrb , deptplace : deptplacerb , arrvplace : arrvplacerb , deptdate : deptdaterb , arrvdate : arrvdaterb , depttime : depttimerb , arrvtime : arrvtimerb , busid : busidr , busname : busnamer , bustype : bustyper} , function(res){
 $("#routeresb").html(res);
});
 }
}
$("#adrouteb").click(function(){
addrouteb();
});
});
</script>
<!-- Trip Car Remove -->
<script type="text/javascript">
function removerob()
{
var tripidb = document.forms["remformb"]["remrouteb"].value;
if(tripidb==""||tripidb==null)
{alert("Provide the Trip ID");
return false;
}
}
</script>
<!-- Filter Bus route -->
<script type="text/javascript">
$(function(){$("#filterrouteb1").hide();$("#filterrouteb2").hide();$("#filterrouteb3").hide();$("#filterrouteb4").hide();});
function runeffrb()
{
var filterrouteb = $("#filterrb").val();
if(filterrouteb=="rbtripid")
{
$(function(){$("#filterrouteb2").hide();$("#filterrouteb3").hide();$("#filterrouteb4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#filterrouteb1").show(selectedeff,option,500);});
return true;
}
if(filterrouteb=="rbdeptplace")
{
$(function(){$("#filterrouteb1").hide();$("#filterrouteb3").hide();$("#filterrouteb4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#filterrouteb2").show(selectedeff,option,500);});
return true;
}
if(filterrouteb=="rbarrvplace")
{
$(function(){$("#filterrouteb1").hide();$("#filterrouteb2").hide();$("#filterrouteb4").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#filterrouteb3").show(selectedeff,option,500);});
return true;
}
if(filterrouteb=="rbtriptype")
{
$(function(){$("#filterrouteb1").hide();$("#filterrouteb2").hide();$("#filterrouteb3").hide();});
$(function(){var selectedeff = "slide";
var option = {};
$("#filterrouteb4").show(selectedeff,option,500);});
return true;
}
if(filterrouteb=="")
{
$(function(){$("#filterrouteb1").hide();$("#filterrouteb2").hide();$("#filterrouteb3").hide();$("#filterrouteb4").hide();});
return true;
}
}
</script>
<script type="text/javascript">
$(function(){
function filtertypebr01()
{
var filtib = $("#trbtripid").val();
if(filtib==""||filtib==null)
{alert("Enter the Trip ID");}
else
{
$("#load8").load('filterroute01.jsp?tripid='+filtib+'');
}
}
function filtertypebr02()
{
var fildpb = $("#trbdeptplace").val();
if(fildpb==""||fildpb==null)
{alert("Enter the Departure Place");}
else
{
$("#load8").load('filterroute02.jsp?deptplace='+fildpb+'');
}
}
function filtertypebr03()
{
var filapb = $("#trbarrvplace").val();
if(filapb==""||filapb==null)
{alert("Enter the Arrival Place");}
else
{
$("#load8").load('filterroute03.jsp?arrvplace='+filapb+'');
}
}
function filtertypebr04()
{
var filttb = $("#trbtriptype").val();
if(filttb==""||filttb==null)
{alert("Enter the Trip Type");}
else
{
$("#load8").load('filterroute04.jsp?triptype='+filttb+'');
}
}
$("#brbtripid").click(function(){
filtertypebr01();
});
$("#brbdeptplace").click(function(){
filtertypebr02();
});
$("#brbarrvplace").click(function(){
filtertypebr03();
});
$("#brbtriptype").click(function(){
filtertypebr04();
});
});
</script>
</head>
<body>
<div id="wrapper">
<div id="titleimg">
<img src="admin.png" width="50px" height="50px"></div>
<div id="logomain">
<img src="logo1.png" alt="LOGO"></div>
<div id="title">
<b>Welcome!</b>&nbsp;&nbsp;<u>Administrator</u><br><a href="logout.jsp">Logout</a>
</div>
<div id="container" style="margin: 10px 0px;">
<div id="tabs">
<ul>
<li><a href="#home">Home</a></li>
<li><a href="#posts">Posts</a></li>
<li><a href="#comments">Comments</a></li>
<li><a href="#feedback">Feedback</a></li>
<li><a href="#modmanage">Manage Moderators</a></li>
<li><a href="#userinfo">User Info</a></li>
<li><a href="#vehiclem">Manage Vehicles</a></li>
<li><a href="#route">Manage Routes</a></li>
</ul>
<div id="home">
<center><img src="sam.png"></center>
</div>
<div id="posts">
 <div id="subpost">
 <ul>
<li><a href="#allpost">All Post</a></li>
<li><a href="#newpost">New Post</a></li>
</ul>
  <div id="allpost">All Post</div>
  <div id="newpost">New Post</div>
 </div>
</div>
<div id="comments">
comments
</div>
<div id="feedback">
feedback
</div>
<div id="modmanage">
mod manage
</div>
<div id="userinfo">
user info
</div>
<div id="vehiclem">
<div id="vehtype">
<ul>
<li><a href="#managecar">Manage Cars</a></li>
<li><a href="#managebus">Manage Buses</a></li>
</ul>
 <div id="managecar">
   <div id="submanagec">
  <ul>
<li><a href="#addveh">Add Cars</a></li>
<li><a href="#remveh">Remove Cars</a></li>
<li><a href="#viewveh">View Cars</a></li>
</ul>
  <div id="addveh" style="padding :0px 0px;font-size: small;margin: 0px 50px;">   
  <div id="image" style="position: absolute;margin: 175px 400px"><img src="interior.png" width="250" height="150"></div>
    <div id="result" style="position: absolute;margin: 40px 195px;width: 290px;"></div>
    <br>
  <h2  style="font-family: BPreplay;">Add Cars</h2>
  <div id="notify" style="width: 650px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 10px 10px;">
       Fill all the required fields !!
      </div>
      <br>
    <div style="background : #e8eefa;border: 2px;border-style: solid;border-color: #c4daff;width: 620px;padding: 0px 25px;height: 220px">
  <form action="">
  <div style="position: absolute;margin: 0px 200px;">
  <label>CAR NAME</label><br>
  <input type="text" name="cname" id="cname"/><br><br>
  <label>CAR TYPE</label><br>
 <select id="ctype" style="width: 137px;height: 25px">
 <option value="">Select Type</option>
 <option value="AC">AC</option>
 <option value="NON-AC">NON-AC</option>
 </select><br><br><br>
   <input type="button" value="Add Car" id="addv" style="margin: 0px; 150px;">
   <input type="reset" value="reset" id="reset">
  </div>
   <div style="margin: 20px 0px;">
  <label>CAR ID</label><br>
  <input type="text" name="cid" id="cid"/><br><br>
  <label>CAR NUMBER</label><br>
  <input type="text" name="cnum" id="cnum"/><br><br>
  <label>CAR MODEL</label><br>
  <input type="text" name="cmod" id="cmod"><br><br>
  </div>
   </form>
   </div>
   <br>
  </div>
  <div id="remveh">
  <h2 style="font-family: BPreplay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remove Cars</h2>
  <div id="load2" style="margin: 0px 35px;">
  Enter the car id <input type="text" id="remcar"/>&nbsp;<input type="button" id="remb" value="List Vehicle"/>&nbsp;<input type="button" id="refresh1" value="Clear List"/>
  </div>
  <div id="con" style="margin: 20px 0px;">
  </div>
  </div>
  <div id="viewveh">
  <h2 style="font-family: BPreplay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Available Car List</h2>
  <div id="load1" style="position: absolute;margin: 10px 540px;width: 215px;"><input type="button" value="Refresh List" id="refresh" style="margin: 0px 45px;"><br><br>
  &nbsp;&nbsp;Filter By <select id="filter" style="margin: 0px 28px;" onchange="return runeff()">
  <option value=""></option>
  <option value="cartype">Car Type</option>
  <option value="carid">Car Id</option>
  <option value="carnumber">Car Number</option>
  <option value="carname">Car Name</option>
  <option value="carmodel">Car Model</option>
  </select><br>
  </div>
  <div id="cartype" style="position: absolute;margin: 90px 540px;width: 265px;">
  <div id="fileffect">
  &nbsp;&nbsp;Car Type<select id="ct" style="margin: 0px 28px;">
  <option value="AC">AC</option>
  <option value="NON-AC">NON-AC</option>
  </select><br><br>
  &nbsp;&nbsp;<input type="button" value="Filter" id="filct"/>
  </div>
  <div id="fileffect1">
  &nbsp;&nbsp;Car ID<input type="text" id="ci" style="margin: 0px 43px;width: 90px" ><br><br>
  &nbsp;&nbsp;<input type="button" value="Filter" id="filci"/>
  </div>
  <div id="fileffect2">
  &nbsp;&nbsp;Car Number<input type="text" id="cnu" style="margin: 0px 10px;width: 90px"><br><br>
  &nbsp;&nbsp;<input type="button" value="Filter" id="filcnu"/>
  </div>
  <div id="fileffect3">
  &nbsp;&nbsp;Car Name<input type="text" id="cna" style="margin: 0px 23px;width: 90px"><br><br>
  &nbsp;&nbsp;<input type="button" value="Filter" id="filcn"/>
  </div>
  <div id="fileffect4">
  &nbsp;&nbsp;Car Model<input type="text" id="cmo" style="margin: 0px 22px;width: 90px"><br><br>
  &nbsp;&nbsp;<input type="button" value="Filter" id="filcm"/>
  </div>
  </div>
  <div id="load"></div>
  <br><br>
  <div id="carimg" style="position: relative;width: 530px;">
  <img border="0" src="vehicles/veh1.png" width="140"
	height="90"><img border="0" src="vehicles/veh6.png" width="140"
	height="90" style="margin: 0px 20px;"><img border="0" src="vehicles/veh5.png" width="160"
	height="90" style="margin: 0px 20px;">
  </div>
  </div>
 </div> 
 </div>
 <!--Manage Bus-->
 <div id = "managebus">
    <div id="submanageb">
       <ul>
<li><a href="#addvehb">Add Buses</a></li>
<li><a href="#remvehb">Remove Buses</a></li>
<li><a href="#viewvehb">View Buses</a></li>
</ul>
  <div id="addvehb" style="padding :0px 0px;font-size: small;margin: 0px 50px;">   
  <div id="imageb" style="position: absolute;margin: 165px 400px"><img src="interior.png" width="250" height="150"></div>
    <div id="resultb" style="position: absolute;margin: 40px 195px;width: 290px;"></div>
    <br>
  <h2  style="font-family: BPreplay;">Add Bus</h2>
    <div id="notify" style="width: 650px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 5px 10px;">
       Fill all the required fields !!
      </div>
      <br>
      <div style="background : #e8eefa;border: 2px;border-style: solid;border-color: #c4daff;width: 620px;padding: 0px 25px;height: 220px">
  <form action="">
  <div style="position: absolute;margin: 0px 200px;">
  <label>BUS NAME</label><br>
  <input type="text" name="bname" id="bname"/><br><br>
  <label>BUS TYPE</label><br>
 <select id="btype" style="width: 137px;height: 25px">
 <option value="">Select Type</option>
 <option value="AC,Sleeper">AC Sleeper</option>
 <option value="AC,Semi-Sleeper">AC Semi-Sleeper</option>
  <option value="NON-AC,Sleeper">NON-AC Sleeper</option>
    <option value="NON-AC,Semi-Sleeper">NON-AC Semi-Sleeper</option>
 </select><br><br><br>
   <input type="button" value="Add Bus" id="addvb" style="margin: 0px; 150px;">
   <input type="reset" value="reset" id="reset1">
  </div>
   <div style="margin: 30px 0px;">
  <label>BUS ID</label><br>
  <input type="text" name="bid" id="bid"/><br><br>
  <label>BUS NUMBER</label><br>
  <input type="text" name="bnum" id="bnum"/><br><br>
  </div>
   </form>
   </div>
   <br><br><br>
  </div>
  <div id="remvehb">
  <h2 style="font-family: BPreplay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remove Buses</h2>
  <div id="load3" style="margin: 0px 35px;">
  Enter the Bus id <input type="text" id="removebus"/>&nbsp;<input type="button" id="rembus" value="List Vehicle"/>&nbsp;<input type="button" id="refresh2" value="Clear List"/>
  </div>
  <div id="remco" style="margin: 20px 0px;"> 
  </div>
  </div>
    <div id="viewvehb">
  <h2 style="font-family: BPreplay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Available Bus List</h2>
  <div id="load4" style="position: absolute;margin: 10px 540px;width: 215px;"><input type="button" value="Refresh List" id="refresh3" style="margin: 0px 45px;"><br><br>
  &nbsp;&nbsp;Filter By <select id="filterb" style="margin: 0px 28px;" onchange="return runeffb()">
  <option value=""></option>
  <option value="bustype">Bus Type</option>
  <option value="busid">Bus Id</option>
  <option value="busnumber">Bus Number</option>
  <option value="busname">Bus Name</option>
  </select><br>
  </div>
  <div id="bustype" style="position: absolute;margin: 90px 540px;width: 265px;">
  <div id="fileffectb">
  &nbsp;&nbsp;Bus Type<select id="bt" style="margin: 0px 28px;width : 95px">
  <option value="AC,Sleeper">AC Sleeper</option>
  <option value="NON-AC,Sleeper">NON-AC Sleeper</option>
  <option value="AC,Semi-Sleeper">AC Semi-Sleeper</option>
  <option value="NON-AC,Semi-Sleeper">NON-AC Semi-Sleeper</option>
  </select><br><br>
  &nbsp;&nbsp;<input type="button" value="Filter" id="filbt"/>
  </div>
  <div id="fileffectb1">
  &nbsp;&nbsp;Bus ID<input type="text" id="bi" style="margin: 0px 43px;width: 90px" ><br><br>
  &nbsp;&nbsp;<input type="button" value="Filter" id="filbi"/>
  </div>
  <div id="fileffectb2">
  &nbsp;&nbsp;Bus Number<input type="text" id="bnu" style="margin: 0px 10px;width: 90px"><br><br>
  &nbsp;&nbsp;<input type="button" value="Filter" id="filbnu"/>
  </div>
  <div id="fileffectb3">
  &nbsp;&nbsp;Bus Name<input type="text" id="bna" style="margin: 0px 23px;width: 90px"><br><br>
  &nbsp;&nbsp;<input type="button" value="Filter" id="filbn"/>
  </div>
  </div>
  <div id="load6"></div>
  <br><br>
  <div id="busimg" style="position: relative;width: 530px;">
  <img border="0" src="vehicles/veh1.png" width="140"
	height="90"><img border="0" src="vehicles/veh6.png" width="140"
	height="90" style="margin: 0px 20px;"><img border="0" src="vehicles/veh5.png" width="160"
	height="90" style="margin: 0px 20px;">
  </div>
  </div>
 </div> 
 </div>
</div>
</div>
<!-- Managing Routes -->
<div id="route">
 <div id="vehroute">
 <ul>
  <li><a href="#managecarr">Manage Car Routes</a></li>
  <li><a href="#managebusr">Manage Bus Routes</a></li>
 </ul>
  <div id="managecarr">
    <div id="routecar">
     <ul>
       <li><a href="#routeadd">Add Car Route</a></li>
       <li><a href="#routerem">Remove Car Route</a></li>
       <li><a href="#routevew">View Car Route</a></li>
     </ul>
      <div id="routeadd">
      <div style="margin: 10px 20px">
     <h2><font  face="BPreplay">Add Car Routes</font></h2> 
      <div id="notify" style="width: 660px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 10px 10px;">
       <div id="routeresc" style="position: absolute;margin: 0px 380px;width : 300px;"></div>Fill all the required fields !!
      </div>
      <br>
      <div id="mainform" style="width: 640px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 20px 20px;">
        <div id="part2" style="margin: 0px 200px;position: absolute;width: 600px">
        <div style="margin :210px 270px;width: 200px;position: absolute;">
         <input type="button" value="Add Route" id="adroutec"><input type="reset" value="Cancel" style="margin: 0px 10px;">
         </div>
        <div style="position: absolute;margin: 0px 300px;width: 500px">
         <label>Arrival Time</label><br>
         <input type="text" class="timepic1" size="10" id="arrvtimerc">
         </div>
         <label>Trip ID</label>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;<label>Departure Time</label><br>
         <input type="text" id="tripidrc">&emsp;&emsp;&nbsp;&nbsp;<input type="text" id="depttimerc" class="timepic" size="10"><br><br>
         <label>Departure Date</label><br>
         <input type="text" id="deptdate"><input type="text" id="alternatef" style="margin: 0px 20px" size="30"><br><br>
         <label>Arrival Date</label><br>
         <input type="text" id="arrvdate"><input type="text" id="alternatet" style="margin: 0px 20px" size="30"><br><br>
         <label>Car Model</label>&emsp;&emsp;<label style="margin: 0px 88px">Car Type</label><br>
         <input type="text" id="carmodelrc">&emsp;&emsp;<select id="cartyperc" style="margin: 0px 10px"><option value="AC">AC</option><option value="NON-AC">NON-AC</option></select>
        </div>
        <div id="part1">
         <label>Trip Type</label><br>
         <select id="triptyperc">
         <option value=""></option>
         <option value="RoundTrip">Round Trip</option>
         <option value="OneWay">One Way</option>
         </select><br><br>
         <label>Departure Place</label><br>
         <input type="text" id="deptplacerc"><br><br>
         <label>Arrival Place</label><br>
         <input type="text" id="arrvplacerc"><br><br>
         <div id="cariden">
          <label>Car Number</label><br>
         <select id="carnumberrc">
         <option value=""></option>
           <%
          Class.forName("com.ibm.db2.jcc.DB2Driver");
          Connection con = DriverManager.getConnection("jdbc:db2://localhost:50000/lords","senthilkumar","senthil");
          PreparedStatement stmt = con.prepareStatement("select car_number from senthilkumar.car_list");
          PreparedStatement stmt1 = con.prepareStatement("select car_model,car_type from senthilkumar.car_list order by car_number asc");
          ResultSet rs = stmt.executeQuery();
          ResultSet rs1 = stmt1.executeQuery();
          while(rs.next()&&rs1.next())
          {
          out.println("<option value="+rs.getString(1)+" title='Car Model : "+rs1.getString(1)+" , Car Type : "+rs1.getString(2)+"'>"+rs.getString(1)+"</option>");
          }      
       %>
         </select></div><br>
        </div>
      </div>
     </div>
      </div>
      <div id="routerem">
      <div style="margin :10px 20px;">
       <h2><font  face="BPreplay">Remove Car Routes</font></h2>
             <div id="notify" style="width: 660px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 10px 10px;">
       Fill all the required fields !!
      </div>
            <div id="notify" style="width: 660px;margin : 10px 0px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 10px 10px;">
            <form target="_blank" action="removeroc.jsp" name="remform" onsubmit="return removeroc()">
       <label>TRIP ID</label><input type="text" name="remroutec" style="margin: 0px 10px;"><input type="submit" value="List Routes"><input type="reset" value="Clear">
       </form>
      </div>
      <div id="remloadc">

      </div>
      </div>
      </div>
      <div id="routevew">
                 <div style="margin :10px 20px;">
                  <div style="position :absolute;margin: 2px 310px;width: 180px;">Filter By <select id="filterrc" onchange="return runeffrc()">
                  <option value=""></option>
                  <option value="rctripid">Trip ID</option>
                  <option value="rcdeptplace">Departure Place</option>
                  <option value="rcarrvplace">Arrival Place</option>
                  <option value="rctriptype">Trip Type</option>
                  </select></div>
                  <div style="position :absolute;margin: 2px 490px;width: 200px;">
                  <div id="filterroute1">
                  <input type="text" id="trctripid" title="Provide Trip ID"><input type="button" id="brctripid" value="Filter">
                  </div>
                  <div id="filterroute2">
                  <input type="text" id="trcdeptplace" title="Provide Departure Place"><input type="button" id="brcdeptplace" value="Filter">
                  </div>
                  <div id="filterroute3">
                  <input type="text" id="trcarrvplace" title="Provide Arrival Place"><input type="button" id="brcarrvplace" value="Filter">
                  </div>
                  <div id="filterroute4">
                  <input type="text" id="trctriptype" title="Provide Trip Type"><input type="button" id="brctriptype" value="Filter">
                  </div>
                  </div>
        <h2><font  face="BPreplay">View the Available Route List</font></h2>
             <div id="notify" style="width: 660px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 10px 10px;">
       Available Route Lists <input type="button" id="refresh7" value="Refresh List" style="margin: 0px 170px">
      		</div>	
      </div>
      <div id="load7">
       
      </div>
      </div>
    </div>
  </div>
  <div id="managebusr">
         <div id="routebus">
     <ul>
       <li><a href="#routeaddb">Add Bus Route</a></li>
       <li><a href="#routeremb">Remove Bus Route</a></li>
       <li><a href="#routevewb">View Bus Route</a></li>
     </ul>
      <div id="routeaddb">
      <div style="margin: 10px 20px">
     <h2><font  face="BPreplay">Add Bus Routes</font></h2> 
      <div id="notify" style="width: 660px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 10px 10px;">
       <div id="routeresb" style="position: absolute;margin: 0px 380px;width : 300px;"></div>Fill all the required fields !!
      </div>
      <br>
      <div id="mainform" style="width: 640px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 20px 20px;">
        <div id="part2" style="margin: 0px 200px;position: absolute;width: 600px">
        <div style="margin :210px 350px;width: 200px;position: absolute;">
         <input type="button" value="Add Route" id="adrouteb">
         </div>
        <div style="position: absolute;margin: 0px 300px;width: 500px">
         <label>Arrival Time</label><br>
         <input type="text" class="timepic1" size="10" id="arrvtimerb">
         </div>
         <label>Trip ID</label>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;<label>Departure Time</label><br>
         <input type="text" id="tripidrb">&emsp;&emsp;&nbsp;&nbsp;<input type="text" id="depttimerb" class="timepic" size="10"><br><br>
         <label>Departure Date</label><br>
         <input type="text" id="deptdateb"><input type="text" id="alternatebf" style="margin: 0px 20px" size="30"><br><br>
         <label>Arrival Date</label><br>
         <input type="text" id="arrvdateb"><input type="text" id="alternatebt" style="margin: 0px 20px" size="30"><br><br>
         <label>Bus Name</label>&emsp;&emsp;<label style="margin: 0px 88px">Bus Type</label><br>
         <input type="text" id="busnamer">&emsp;&emsp;<select id="bustyper" style="margin: 0px 10px"><option value="AC,Sleeper">AC,Sleeper</option><option value="NON-AC,Sleeper">NON-AC,Sleeper</option><option value="AC,Semi-Sleeper">AC,Semi-Sleeper</option><option value="NON-AC,Semi-Sleeper">NON-AC,Semi-Sleeper</option></select>
        </div>
        <div id="part1">
         <label>Trip Type</label><br>
         <select id="triptyperb">
         <option value=""></option>
         <option value="RoundTrip">Round Trip</option>
         <option value="OneWay">One Way</option>
         </select><br><br>
         <label>Departure Place</label><br>
         <input type="text" id="deptplacerb"><br><br>
         <label>Arrival Place</label><br>
         <input type="text" id="arrvplacerb"><br><br>
         <div id="cariden">
          <label>Bus ID</label><br>
         <select id="busidr">
         <option value=""></option>
           <%
          Class.forName("com.ibm.db2.jcc.DB2Driver");
          PreparedStatement stmt2 = con.prepareStatement("select bus_id from senthilkumar.bus_list");
          PreparedStatement stmt3 = con.prepareStatement("select bus_name,bus_type from senthilkumar.bus_list order by bus_id asc");
          ResultSet rs3 = stmt2.executeQuery();
          ResultSet rs4 = stmt3.executeQuery();
          while(rs3.next()&&rs4.next())
          {
          out.println("<option value="+rs3.getString(1)+" title='Bus Name : "+rs4.getString(1)+" , Bus Type : "+rs4.getString(2)+"'>"+rs3.getString(1)+"</option>");
          }      
       %>
         </select></div><br>
        </div>
      </div>
     </div>
      </div>
      <div id="routeremb">
             <div style="margin :10px 20px;">
       <h2><font  face="BPreplay">Remove Bus Routes</font></h2>
             <div id="notify" style="width: 660px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 10px 10px;">
       Fill all the required fields !!
      </div>
            <div id="notify" style="width: 660px;margin : 10px 0px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 10px 10px;">
            <form target="_blank" action="removerob.jsp" name="remformb" onsubmit="return removerob()">
       <label>TRIP ID</label><input type="text" name="remrouteb" style="margin: 0px 10px;"><input type="submit" value="List Routes"><input type="reset" value="Clear">
       </form>
      </div>
      <div id="remloadb">

      </div>
      </div>
      </div>
      <div id="routevewb">
        <div style="margin :10px 20px;">
                  <div style="position :absolute;margin: 2px 310px;width: 180px;">Filter By <select id="filterrb" onchange="return runeffrb()">
                  <option value=""></option>
                  <option value="rbtripid">Trip ID</option>
                  <option value="rbdeptplace">Departure Place</option>
                  <option value="rbarrvplace">Arrival Place</option>
                  <option value="rbtriptype">Trip Type</option>
                  </select></div>
                  <div style="position :absolute;margin: 2px 490px;width: 200px;">
                  <div id="filterrouteb1">
                  <input type="text" id="trbtripid" title="Provide Trip ID"><input type="button" id="brbtripid" value="Filter">
                  </div>
                  <div id="filterrouteb2">
                  <input type="text" id="trbdeptplace" title="Provide Departure Place"><input type="button" id="brbdeptplace" value="Filter">
                  </div>
                  <div id="filterrouteb3">
                  <input type="text" id="trbarrvplace" title="Provide Arrival Place"><input type="button" id="brbarrvplace" value="Filter">
                  </div>
                  <div id="filterrouteb4">
                  <input type="text" id="trbtriptype" title="Provide Trip Type"><input type="button" id="brbtriptype" value="Filter">
                  </div>
                  </div>
        <h2><font  face="BPreplay">View the Available Route List</font></h2>
             <div id="notify" style="width: 660px;border: 2px;border-style: solid;border-color: #c4daff;background-color: #e8eefa;padding: 10px 10px;">
       Available Route Lists <input type="button" id="refresh8" value="Refresh List" style="margin: 0px 170px">
      		</div>	
      </div>
      <div id="load8">
       
      </div>          
      </div>
    </div>
  </div>
 </div>
</div>
</div>
<br>
<div id="footer"><center><div id="carmenu">
<center>&copy;Copyright to Smart Tours & Travels 2013.</center>
</div></center></div>
<br>
</div>
</div>
</body>
</html>