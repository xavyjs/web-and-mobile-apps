<%@ page import="java.sql.*" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Process</title>
<link rel="stylesheet" type="text/css" href="style.css"/>
<link rel="stylesheet" type="text/css" href="menu.css"/>
</head>
<body>
<div id="signup" style="display:none;width:700px;">
<h3>Member Registration Form</h3>
<form method="post" action="regprocess.jsp" name="regform" onSubmit="return validateForm()">
Username &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="text" name="username"/>
&nbsp;&nbsp;&nbsp;Password &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="password" name="password"/><br><br>
Confirm Password : <input type="password" name="repassword"/>
&nbsp;&nbsp;&nbsp;First Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<input type="text" name="fname"/><br><br>
Last Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<input type="text" name="lname"/>
&nbsp;&nbsp;&nbsp;Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<input type="text" name="email"/><br><br>
State &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="text" name="state"/>
&nbsp;&nbsp;&nbsp;City &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="text" name="city"/><br><br>
Country &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="text" name="country">
&nbsp;&nbsp;&nbsp;Mobile Number &nbsp;&nbsp;&nbsp;&nbsp;: <input type="text" name="mobile"/><br><br>
Sex &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <select name="sex">
<option value="male">Male</option>
<option value="female">Female</option>
</select>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pincode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="text" name="pincode" /><br>
Date Of Birth &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <select name="month" size="1">
    <option value=""></option> 
    <option value="Jan">Jan</option>
    <option value="Feb">Feb</option>
    <option value="Mar">Mar</option>
    <option value="Apr">Apr</option>
    <option value="May">May</option>
    <option value="Jun">Jun</option>
    <option value="Jul">Jul</option>
    <option value="Aug">Aug</option>
    <option value="Sep">Sep</option>
    <option value="Oct">Oct</option>
    <option value="Nov">Nov</option>
    <option value="Dec">Dec</option>
</select>

<!-- Day dropdown -->
<select name="day" size="1">
<option value=""></option>
    <option value="01">01</option>
    <option value="02">02</option>
    <option value="03">03</option>
    <option value="04">04</option>
    <option value="05">05</option>
    <option value="06">06</option>
    <option value="07">07</option>
    <option value="08">08</option>
    <option value="09">09</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option>
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    <option value="31">31</option>
</select>
<select name="year">
<option value=""></option>
<option value="2013">2013</option>
<option value="2012">2012</option>
<option value="2011">2011</option>
<option value="2010">2010</option>
<option value="2009">2009</option>
<option value="2008">2008</option>
<option value="2007">2007</option>
<option value="2006">2006</option>
<option value="2005">2005</option>
<option value="2004">2004</option>
<option value="2003">2003</option>
<option value="2002">2002</option>
<option value="2001">2001</option>
<option value="2000">2000</option>
<option value="1999">1999</option>
<option value="1998">1998</option>
<option value="1997">1997</option>
<option value="1996">1996</option>
<option value="1995">1995</option>
<option value="1994">1994</option>
<option value="1993">1993</option>
<option value="1992">1992</option>
<option value="1991">1991</option>
<option value="1990">1990</option>
<option value="1989">1989</option>
<option value="1988">1988</option>
<option value="1987">1987</option>
<option value="1986">1986</option>
<option value="1985">1985</option>
<option value="1984">1984</option>
<option value="1983">1983</option>
<option value="1982">1982</option>
<option value="1981">1981</option>
<option value="1980">1980</option>
<option value="1979">1979</option>
<option value="1978">1978</option>
<option value="1977">1977</option>
<option value="1976">1976</option>
<option value="1975">1975</option>
<option value="1974">1974</option>
<option value="1973">1973</option>
<option value="1972">1972</option>
<option value="1971">1971</option>
<option value="1970">1970</option>
<option value="1969">1969</option>
<option value="1968">1968</option>
<option value="1967">1967</option>
<option value="1966">1966</option>
<option value="1965">1965</option>
<option value="1964">1964</option>
<option value="1963">1963</option>
<option value="1962">1962</option>
<option value="1961">1961</option>
<option value="1960">1960</option>
<option value="1959">1959</option>
<option value="1958">1958</option>
<option value="1957">1957</option>
<option value="1956">1956</option>
<option value="1955">1955</option>
<option value="1954">1954</option>
<option value="1953">1953</option>
<option value="1952">1952</option>
<option value="1951">1951</option>
<option value="1950">1950</option>
</select>
Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <textarea rows="" cols="" name="address"></textarea><br><br>
<input type="submit" value="Register"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="Reset"/>
</form>
</div>
<div id="signin" style="display:none;width:300px;">
<h3>Member Login</h3>
<form  method="post" action="logprocess.jsp">
Username : <input type="text" name="username"><br><br>
Password : <input type="password" name="password"><br><br><br>
<input type="submit" value="LOGIN">
<input type="reset" value="RESET" >
</form>
</div>
<div id="wrapper">
<div id="titleimg">
<img src="guest_icon.png" width="50" height="50"></div>
<div id="logomain">
<img src="logo1.png" alt="LOGO"></div>
<div id="title">
<b>Welcome!</b>&nbsp;&nbsp;<u>Guest</u><br><font color="red">Login Error !!</font>
</div>
<div id="banner">
<ul class="menuH decor1">
    <li style="margin-left: 10px;"><a href="index.jsp">Home</a></li>
    <li><a href="#2" class="arrow">Features</a>
        <ul>
            <li><a href="#">Explore Locations</a></li>
        </ul>
    </li>
    <li><a href="#3">About Us</a></li>
    <li><a href="#3">Contact Us</a></li>
</ul>
</div>
<div id="container">
<div id="message">
<% 
String username= request.getParameter("username");
String password= request.getParameter("password");
System.out.println(username);
System.out.println(password);
int count=0;
String cname1 = null;
String cname2 = null;
String cname= null;
if(username.equals("")||password.equals(""))
{
out.println("<h1>Login Error!!</h1>");
out.println("<p>Sorry Try Again.</p>");
out.println("<p>Fill the Login Form with correct inforamtion.</p>");
out.println("<p>Fill all the mandatory Fields.</p>");
out.println("<p>Login Again ?.<a href='#signin' class='fancybox'>Click Here</a></p>");
}
else
{
Class.forName("com.ibm.db2.jcc.DB2Driver");
Connection con = DriverManager.getConnection("jdbc:db2://localhost:50000/LORDS","senthilkumar","senthil");
PreparedStatement stmt = con.prepareStatement("SELECT * FROM SENTHILKUMAR.USER_LOGIN WHERE USERNAME='"+username+"' AND PASSWORD='"+password+"'");
System.out.println("Query Executed Successfully");
ResultSet rs= stmt.executeQuery();
while(rs.next())
{
count++;
}
if(count==0)
{
out.println("<h1>Login Error !!</h1>");
out.println("<p>Sorry Try Again.</p>");
out.println("<p>Username & Password Combination does not exsists !!</p>");
out.println("<p>Fill the Login Form with correct inforamtion.</p>");
out.println("<p>Login Again ?.</p>");
}
else
{
PreparedStatement stmt1 = con.prepareStatement("select status from senthilkumar.user_info where username = '"+username+"'");
ResultSet rs1 = stmt1.executeQuery();
while(rs1.next())
{
String check = rs1.getString(1);
if(check.equals("Waiting"))
{
out.println("<h1>Login Error !!</h1>");
out.println("<p>Sorry Try Again.</p>");
out.println("<p>You have to authenticate check ur mail ID.</p>");
out.println("<p>Login Again ?.</p>");
}
else
{
rs = null;
stmt = con.prepareStatement("SELECT FIRST,LAST FROM SENTHILKUMAR.USER_INFO WHERE USERNAME IN (SELECT USERNAME FROM SENTHILKUMAR.USER_LOGIN WHERE PASSWORD='"+password+"')");
rs = stmt.executeQuery();
while(rs.next())
{
cname1=(rs.getString(1));
cname2=(rs.getString(2));
cname = cname1+cname2;
System.out.println(cname);
}
Cookie cookie = new Cookie("username1",username);
cookie.setMaxAge(90*24*60*60);
response.addCookie(cookie);
Cookie cookie1 = new Cookie("username",cname);
cookie1.setMaxAge(90*24*60*60);
response.addCookie(cookie1);
response.sendRedirect("uindex.jsp");
}
}
}
}
%>
</div>
<div id="logo">
<%
if(count ==0||count>0)
{
%>
<div style="margin: 25px 50px;">
<br>
<h3>Member Login</h3>
<form  name = "logform" method="post" action="logprocess.jsp" onsubmit="return validateForm2()">
Username : <input type="text" name="username" ><br><br>
Password : <input type="password" name="password" ><br><br><br>
<input type="submit" value="LOGIN">
<input type="reset" value="RESET" >
</form>
<script>
function validateForm2()
{
var username=document.forms["logform"]["username"].value;
var password = document.forms["logform"]["password"].value;
if (username==null || username=="")
  {
  alert("Username must be filled out");
  return false;
  }
  else if(password==""||password==null)
  {
  alert("Enter the Password");
  return false;
  }
}
</script>
</div>
<%
}
%>
</div>
</div>
<div id="footer"><center><div id="carmenu">
<a href="uindex.jsp" style="padding: 0px 15px;color: white">Home</a>
<a href="" style="padding: 0px 15px;color: white">Terms & Conditions</a>
<a href="" style="padding: 0px 15px;color: white">About Us</a>
<a href="" style="padding: 0px 15px;color: white">Contact Us</a>
</div></center></div><br><center>&copy;Copyright to Smart Tours & Travels 2013.</center><br>
</div>
</body>
</html>