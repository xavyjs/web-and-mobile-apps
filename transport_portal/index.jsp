<%@ page import="java.sql.*" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%  
response.setHeader("Pragma","no-cache");  
response.setHeader("Cache-Control","no-store");  
response.setHeader("Expires","0");  
response.setDateHeader("Expires",-1);  
%>
    <% 
                    	String cookieName = "username";
                    	Cookie cookies[] = request.getCookies();
                    	Cookie mycookie = null;
                    	if(cookies!=null)
                    	{
                        for(int i=0;i<cookies.length;i++)
                    	{
                    	if(cookies[i].getName().equals(cookieName))
                    	{
                    	response.sendRedirect("uindex.jsp");
                    	}
                    	}
                    	}
                    		
    	 %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Smart Tours And Travels</title>
<script type="text/javascript" src="lib/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
<link rel="stylesheet" href="source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
<script type="text/javascript" src="source/jquery.fancybox.pack.js?v=2.1.4"></script>
<link rel="stylesheet" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
<link rel="stylesheet" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript">
$(".fancybox").fancybox({
    openEffect  : 'none',
    closeEffect : 'none'
});
</script>
<script>
function validateForm()
{
var username=document.forms["regform"]["username"].value;
var password = document.forms["regform"]["password"].value;
var conpassword = document.forms["regform"]["repassword"].value;
var firstname = document.forms["regform"]["fname"].value;
var email = document.forms["regform"]["email"].value;
var state = document.forms["regform"]["state"].value;
var city = document.forms["regform"]["city"].value;
var country = document.forms["regform"]["country"].value;
var mobile = document.forms["regform"]["mobile"].value;
var dob = document.forms["regform"]["dob"].value;
var address = document.forms["regform"]["address"].value;
var pincode = document.forms["regform"]["pincode"].value;
var secretq = document.forms["regform"]["secretq"].value;
var secreta = document.forms["regform"]["secreta"].value;
if (username==null || username=="")
  {
  alert("Username must be filled out");
  return false;
  }
if(username.length > 15)
  {
  alert("Username should not execeed 15 characters!!");
  return false;
  }
if(password==""||password==null)
  {
  alert("Enter the Password");
  return false;
  }
  alert("username check finished");
if(password.length < 6 || password.length > 20)
  {
  alert("Password should be less than 20 characters and not less than 6 characters");
  return false;
  }
if(conpassword==""||conpassword==null)
  {
  alert("Enter the confirm password");
  return false;
  }
if(password != conpassword)
  {
  alert("Password does not match");
  return false;
  }
if(firstname==""||firstname==null)
  {
  alert("Enter the first name");
  return false;
  }
if(email==""||email==null)
  {
  alert("Enter the email address");
  return false;
  }
if(state==""||state==null)
  {
  alert("Enter the State");
  return false;
  }
if(city==""||city==null)
  {
  alert("Enter the City");
  return false;
  }
if(country==""||country==null)
  {
  alert("Enter the Country");
  return false;
  }
if(mobile==""||mobile==null)
  {
  alert("Enter the Mobile Number");
  return false;
  }
if(mobile.length >10 || mobile.length<10)
  {
  alert("Enter the valid Mobile Number");
  return false;
  }
if(dob==""||dob==null)
  {
  alert("Enter the Date of Birth");
  return false;
  }
if(pincode==""||pincode==null)
  {
  alert("Enter the Pincode");
  return false;
  }
if(secretq==""||secretq==null)
  {
  alert("Choose your Secret Question");
  return false;
  }
if(secreta==""||secreta==null)
  {
  alert("Provide Answer for Secret Question");
  return false;
  }
if(address==""||address==null)
  {
  alert("Enter the Address");
  return false;
  }
  var atpos=email.indexOf("@");
  var dotpos=email.lastIndexOf(".");
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
  {
  alert("Not a valid e-mail address");
  return false;
  }
}
</script>
<script>
function validateForm2()
{
var username=document.forms["logform"]["username"].value;
var password = document.forms["logform"]["password"].value;
if (username==null || username=="")
  {
  alert("Username must be filled out");
  return false;
  }
  else if(password==""||password==null)
  {
  alert("Enter the Password");
  return false;
  }
}
</script>
<link rel="stylesheet" type="text/css" href="style.css"/>
<link rel="stylesheet" type="text/css" href="menu.css"/>
    <script>
    $(document).ready(function() {
		//the min chars for username
		var min_chars = 3;
		//result texts
		var characters_error = 'Should not Exceed 15';
		var chck = 'Should not be null';
		var checking_html = 'Checking...';

		//when button is clicked
		$('#checkb').click(function(){
			//run the character number check
			if($('#usern').val().length > 15)
			{
			$('#checkbox').html(characters_error);
			}
			else if($('#usern').val() == "")
			{
			$('#checkbox').html(chck);
			}
			else if($('#usern').val() == null)
			{
			$('#checkbox').html(chck);
			}
			else
			{
				//else show the cheking_text and run the function to check
				$('#checkbox').html(checking_html);
				check_availability();
				}
		});

  });

//function to check username availability
function check_availability(){

		//get the username
		var username = $('#usern').val();

		//use ajax to run the check
		$.post("check_avail.jsp", { username: username },
			function(count){
					//show that the username is NOT available
					$('#checkbox').html(count);
		});

}
    </script>
    <script src="jquery-1.9.1.js"></script>
<script src="jquery-ui-1.10.1.custom.js"></script>
<link rel="stylesheet" href="jquery-ui.css">
<script type="text/javascript">
$(function() {
$( "#tabs" ).tabs().addClass( "ui-tabs-horizontal" );
});
</script>
<style>
.ui-tabs-horizontal {font-size: small;}
</style>
<script>
  $(function() {
     $( "#dob" ).datepicker({changeMonth: true, changeYear: true, maxDate: 0, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatet", altFormat: "DD, d MM, yy"
     });
  });
  </script>
  <script>
  $(function() {
    $( "#deptdate" ).datepicker({ minDate: -20, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatef", altFormat: "DD, d MM, yy"
     });
     $( "#arrvdate" ).datepicker({ minDate: -20, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatet", altFormat: "DD, d MM, yy"
     });
     $( "#deptdateb" ).datepicker({ minDate: 0, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatebf", altFormat: "DD, d MM, yy"
     });
     $( "#arrvdateb" ).datepicker({ minDate: 0, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatebt", altFormat: "DD, d MM, yy"
     });
  });
  </script>
  <script type="text/javascript">
  function validate()
  {
  var lf = document.forms["bookform"]["deptplace"].value;
  var gt = document.forms["bookform"]["arrvplace"].value;
  var dd = document.forms["bookform"]["deptdate"].value;
  var ad = document.forms["bookform"]["arrvdate"].value;
  var ty = document.forms["bookform"]["type"].value;
  var tr = document.getElementsByName("trip");
  if(lf == "" || lf== null)
  {alert("Provide the departure place");return false;}
  else if(gt == ""|| gt==null)
  {alert("Provide the arrival place");return false;}
  else if(dd==""||gt==null)
  {alert("Provide the departure date");return false;}
  else if(ad==""||ad==null)
  {alert("Provide the arrival date");return false;}
  else if(ty==""||ty==null)
  {alert("Provide the car type");return false;}
  else 
  {
  var formValid = false;

    var i = 0;
    while (!formValid && i < tr.length) {
        if (tr[i].checked) formValid = true;
        i++;        
    }

    if (!formValid) alert("Provide the Trip Type");
    return formValid;
  }
  }
  function validate1()
  {
    var lf = document.forms["bookform1"]["deptplaceb"].value;
  var gt = document.forms["bookform1"]["arrvplaceb"].value;
  var dd = document.forms["bookform1"]["deptdateb"].value;
  var ad = document.forms["bookform1"]["arrvdateb"].value;
  var ty = document.forms["bookform1"]["typeb"].value;
  var tr = document.getElementsByName("tripb");
  if(lf == "" || lf== null)
  {alert("Provide the departure place");return false;}
  else if(gt == ""|| gt==null)
  {alert("Provide the arrival place");return false;}
  else if(dd==""||gt==null)
  {alert("Provide the departure date");return false;}
  else if(ty==""||ty==null)
  {alert("Provide the car type");return false;}
  else 
  {
  var formValid = false;
        if (tr[0].checked) 
          formValid = true;      
    if (formValid)
    {
     if(ad==""||ad==null)
     {alert("Provide the arrival date");return false;}
    }
    var formValid1 = false;

    var i = 0;
    while (!formValid1 && i < tr.length) {
        if (tr[i].checked) formValid1 = true;
        i++;        
    }

    if (!formValid1) alert("Provide the Trip Type");
    return formValid1;
  }
  }
  </script>
</head>
<body>
<div id="signup" style="display:none;width:700px;">
<div id="checkavail" style="position: absolute;margin: 70px 350px"><input type="button" value="Check Availability" id="checkb"></div>
<div id="checkbox" style="position: absolute;margin: 70px  500px;width: 200px"></div>
<div id="sam" style="position: absolute;margin: 150px  355px;width: 200px">
<img src="sam.png">
</div>
<div id="regmain" style="margin: 0px 20px">
<center><h2 style="font-family: BPreplay">Member Registration Form</h2></center><br>
<form method="post" action="regprocess.jsp" name="regform" onSubmit="return validateForm()">
Username : <input type="text" name="username" id="usern" style="margin: 0px 60px"/><br><br>
Password : <input type="password" name="password" style="margin: 0px 63px"/><br><br>
Confirm Password : <input type="password" name="repassword" style="margin: 0px 5px"/><br><br>
First Name :<input type="text" name="fname" style="margin: 0px 57px"/><br><br>
Last Name  :<input type="text" name="lname" style="margin: 0px 59px"/><br><br>
Email      : <input type="text" name="email" style="margin: 0px 85px"/><br><br>
State      : <input type="text" name="state" style="margin: 0px 90px"/><br><br>
City       : <input type="text" name="city" style="margin: 0px 95px"/><br><br>
Country : <input type="text" name="country" style="margin: 0px 70px"/><br><br>
Mobile Number: <input type="text" name="mobile" style="margin: 0px 23px"/><br><br>
Sex : <select name="sex" style="margin: 0px 98px">
<option value="male">Male</option>
<option value="female">Female</option>
</select><br><br>
Pincode : <input type="text" name="pincode" style="margin: 0px 70px"/><br><br>
Date Of Birth :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="dob" id="dob">
<br><br>
Address : <textarea rows="" cols="50" name="address" style="margin: 0px 67px"></textarea><br><br>
Secret Question : <select name="secretq" id="secret" style="margin: 0px 20px;">
<option value=""></option>
<option value="What is your favorite Animal ?">What is your favorite Animal ?</option>
<option value="What is your mother's maiden name ?">What is your mother's maiden name ?</option>
<option value="Which is your favorite subject ?">Which is your favorite subject ?</option>
<option value="What is name of your High School ?">What is name of your High School ?</option>
</select><br><br>
Answer :<input type="text" id="secans" style="margin: 0px 75px" name="secreta"><br><br>
<input type="submit" value="Register" /><input type="reset" value="Reset" style="margin: 0px 30px"/><br><br>
</form>
</div>
</div>
<div id="signin" style="display:none;width:300px;">
<h3>Member Login</h3>
<form  name = "logform" method="post" action="logprocess.jsp" onsubmit="return validateForm2()">
Username : <input type="text" name="username" ><br><br>
Password : <input type="password" name="password" ><br><br><br>
<input type="submit" value="LOGIN">
<input type="reset" value="RESET" >&emsp;<a href="secretques.jsp">Forgot Password ?</a><br><br>
</form>
</div>
<div id="wrapper">
<div id="titleimg">
<img src="guest_icon.png" width="50" height="50"></div>
<div id="logomain">
<img src="logo1.png" alt="LOGO"></div>
<div id="title">
<b>Welcome!</b>&nbsp;&nbsp;<u>Guest</u><br><a href="#signup" class="fancybox">Sign Up</a> | <a href="#signin" class="fancybox">Sign In</a>
</div>
<div id="banner">
<ul class="menuH decor1">
    <li style="margin-left: 10px;"><a href="index.jsp">Home</a></li>
    <li><a href="#2" class="arrow">Features</a>
        <ul>
            <li><a href="#">Explore Locations</a></li>
        </ul>
    </li>
    <li><a href="#3">About Us</a></li>
    <li><a href="#3">Contact Us</a></li>
</ul>
</div>
<div id="container">
<div id="welcome">
<h3><font face="PoetsenOne"><b>Welcome To Smart Tours And Travels</b></font></h3>
<p align="justify">We make your trip enjoyable and pleasent with Smart Tours & Travels. We provide cab, train and bus bookings. It provides a best platform for booking travel and tour packages online. We provide 24 hours service and also no service charges for online booking. Make your trip enjoyable and worthful with Smart Tours and Travels.</p>
</div>
<div id="book">
<div id="tabs">
 <ul>
<li><a href="#bcab">Book Cab</a></li>
<li><a href="#bbus">Book Bus</a></li>
</ul>
 <div id="bcab" style="padding: 20px 32px;">
 <form method="post" action="selectcab.jsp" onsubmit="return validate()" name="bookform">
 <div id="se" style="position: absolute;margin: 0px 175px;width: 300px">
    <label>Going To</label><br>
    <input type="text" name="arrvplace"><br><br>
    <input type="radio" name="trip" style="margin: 123px 0px" value="RoundTrip" onclick="arrvdate.disabled=false,alternatet.disabled=false">Round Trip&emsp;
    <input type="radio" name="trip" value="OneWay" onclick="arrvdate.disabled=true,alternatet.disabled=true">One Way
   </div>
   <div id="fi">
    <label>Leaving From</label><br>
    <input type="text" name="deptplace"><br><br>
    <label>Departure Date</label><br>
    <input type="text" name="deptdate" id="deptdate"><input type="text" id="alternatef" style="margin: 0px 21px" size="28"><br><br>
    <label>Arrival Date</label><br>
    <input type="text" name="arrvdate" id="arrvdate" disabled="disabled"><input type="text" id="alternatet" style="margin: 0px 21px" size="28" disabled="disabled"><br><br>
    <label>Car Type</label><br>
    <select style="width: 138px" name="type">
    <option value="">Select Type</option>
    <option value="AC">AC</option>
    <option value="NON-AC">NON-AC</option>
    </select>
    <br><br>
   </div>
   <div style="margin: 15px 160px;width: 300px">
   <input type="submit" value="Search Cab"><input type="reset" value="Cancel" style="margin: 0px 30px"> 
   </div>
   </form>
 </div>
 <div id="bbus" style="padding: 20px 32px;">
  <form method="post" action="selectcabwl.jsp" onsubmit="return validate1()" name="bookform1">
  <div id="seb" style="position: absolute;margin: 0px 175px;width: 300px">
   <label>Going To</label><br>
   <input type="text" name="arrvplaceb"><br><br>
   <input type="radio" name="tripb" style="margin: 123px 0px" value="RoundTrip" onclick="arrvdateb.disabled=false,alternatebt.disabled=false">Round Trip&emsp;
    <input type="radio" name="tripb" value="OneWay" onclick="arrvdateb.disabled=true,alternatebt.disabled=true">One Way
  </div>
  <div id="fib">
  <label>Leaving From</label><br>
  <input type="text" name="deptplaceb"><br><br>
  <label>Departure Date</label><br>
    <input type="text" name="deptdateb" id="deptdateb"><input type="text" id="alternatebf" style="margin: 0px 21px" size="28"><br><br>
    <label>Arrival Date</label><br>
    <input type="text" name="arrvdateb" id="arrvdateb" disabled="disabled"><input type="text" id="alternatebt" style="margin: 0px 21px" size="28" disabled="disabled"><br><br>
    <label>Bus Type</label><br>
    <select style="width: 138px" name="typeb">
    <option value="">Select Type</option>
    <option value="AC,Sleeper">AC Sleeper</option>
    <option value="NON-AC,Sleeper">NON-AC Sleeper</option>
    <option value="AC,Semi-Sleeper">AC Semi-Sleeper</option>
    <option value="NON-AC,Semi-Sleeper">NON-AC Semi-Sleeper</option>
    </select>
    <br><br> 
  </div>
  <div style="margin: 15px 160px;width: 300px">
   <input type="submit" value="Search Bus"><input type="reset" value="Cancel" style="margin: 0px 30px"> 
   </div>
   </form>
 </div>
</div>
</div>
</div>
<div id="footer"><center><div id="carmenu">
<a href="uindex.jsp" style="padding: 0px 15px;color: white">Home</a>
<a href="" style="padding: 0px 15px;color: white">Terms & Conditions</a>
<a href="" style="padding: 0px 15px;color: white">About Us</a>
<a href="" style="padding: 0px 15px;color: white">Contact Us</a>
</div></center></div><br><center>&copy;Copyright to Smart Tours & Travels 2013.</center><br>
</div>
</body>
</html>