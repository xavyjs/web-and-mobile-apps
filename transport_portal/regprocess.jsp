<%@ page import="java.sql.*" import="java.util.Date" import="java.text.*" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import ="java.io.IOException" %>
    <%@ page import ="java.util.Properties" %>
     <%@ page import ="javax.mail.Message" %>
      <%@ page import ="javax.mail.MessagingException" %>
       <%@ page import ="javax.mail.Session" %>
       <%@ page import ="javax.mail.Transport" %>
       <%@ page import ="javax.mail.internet.AddressException" %>
       <%@ page import ="javax.mail.internet.InternetAddress" %>
       <%@ page import ="javax.mail.internet.MimeMessage" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration Process</title>
<link rel="stylesheet" type="text/css" href="menu.css"/>
      <script src="jquery-1.9.1.js"></script>
<script src="jquery-ui-1.10.1.custom.js"></script>
<link rel="stylesheet" href="jquery-ui.css">
<script>
  $(function() {
     $( "#dob" ).datepicker({changeMonth: true, changeYear: true, maxDate: 0, showOn: "button", buttonImage: "schedule.png",buttonImageOnly: true, altField: "#alternatet", altFormat: "DD, d MM, yy"
     });
  });
  </script>
      <script>
    $(document).ready(function() {
		//the min chars for username
		var min_chars = 3;
		//result texts
		var characters_error = 'Should not Exceed 15';
		var chck = 'Should not be null';
		var checking_html = 'Checking...';

		//when button is clicked
		$('#checkb').click(function(){
			//run the character number check
			if($('#usern').val().length > 15)
			{
			$('#checkbox').html(characters_error);
			}
			else if($('#usern').val() == "")
			{
			$('#checkbox').html(chck);
			}
			else if($('#usern').val() == null)
			{
			$('#checkbox').html(chck);
			}
			else
			{
				//else show the cheking_text and run the function to check
				$('#checkbox').html(checking_html);
				check_availability();
				}
		});

  });

//function to check username availability
function check_availability(){

		//get the username
		var username = $('#usern').val();

		//use ajax to run the check
		$.post("check_avail.jsp", { username: username },
			function(count){
					//show that the username is NOT available
					$('#checkbox').html(count);
		});

}
    </script>
<script type="text/javascript" src="lib/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
<link rel="stylesheet" href="source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
<script type="text/javascript" src="source/jquery.fancybox.pack.js?v=2.1.4"></script>
<link rel="stylesheet" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
<link rel="stylesheet" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript">
$(".fancybox").fancybox({
    openEffect  : 'none',
    closeEffect : 'none',
});
</script>
<script>
function validateForm()
{
var username=document.forms["regform"]["username"].value;
var password = document.forms["regform"]["password"].value;
var conpassword = document.forms["regform"]["repassword"].value;
var firstname = document.forms["regform"]["fname"].value;
var email = document.forms["regform"]["email"].value;
var state = document.forms["regform"]["state"].value;
var city = document.forms["regform"]["city"].value;
var country = document.forms["regform"]["country"].value;
var mobile = document.forms["regform"]["mobile"].value;
var dob = document.forms["regform"]["dob"].value;
var address = document.forms["regform"]["address"].value;
var pincode = document.forms["regform"]["pincode"].value;
var secretq = document.forms["regform"]["secretq"].value;
var secreta = document.forms["regform"]["secreta"].value;
if (username==null || username=="")
  {
  alert("Username must be filled out");
  return false;
  }
if(username.length > 15)
  {
  alert("Username should not execeed 15 characters!!");
  return false;
  }
if(password==""||password==null)
  {
  alert("Enter the Password");
  return false;
  }
  alert("username check finished");
if(password.length < 6 || password.length > 20)
  {
  alert("Password should be less than 20 characters and not less than 6 characters");
  return false;
  }
if(conpassword==""||conpassword==null)
  {
  alert("Enter the confirm password");
  return false;
  }
if(password != conpassword)
  {
  alert("Password does not match");
  return false;
  }
if(firstname==""||firstname==null)
  {
  alert("Enter the first name");
  return false;
  }
if(email==""||email==null)
  {
  alert("Enter the email address");
  return false;
  }
if(state==""||state==null)
  {
  alert("Enter the State");
  return false;
  }
if(city==""||city==null)
  {
  alert("Enter the City");
  return false;
  }
if(country==""||country==null)
  {
  alert("Enter the Country");
  return false;
  }
if(mobile==""||mobile==null)
  {
  alert("Enter the Mobile Number");
  return false;
  }
if(mobile.length >10 || mobile.length<10)
  {
  alert("Enter the valid Mobile Number");
  return false;
  }
if(dob==""||dob==null)
  {
  alert("Enter the Date of Birth");
  return false;
  }
if(pincode==""||pincode==null)
  {
  alert("Enter the Pincode");
  return false;
  }
if(secretq==""||secretq==null)
  {
  alert("Choose your Secret Question");
  return false;
  }
if(secreta==""||secreta==null)
  {
  alert("Provide Answer for Secret Question");
  return false;
  }
if(address==""||address==null)
  {
  alert("Enter the Address");
  return false;
  }
  var atpos=email.indexOf("@");
  var dotpos=email.lastIndexOf(".");
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
  {
  alert("Not a valid e-mail address");
  return false;
  }
}
</script>
<script>
function validateForm2()
{
var username=document.forms["logform"]["username"].value;
var password = document.forms["logform"]["password"].value;
if (username==null || username=="")
  {
  alert("Username must be filled out");
  return false;
  }
  else if(password==""||password==null)
  {
  alert("Enter the Password");
  return false;
  }
}
</script>
<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<div id="signup" style="display:none;width:700px;">
<div id="checkavail" style="position: absolute;margin: 70px 350px"><input type="button" value="Check Availability" id="checkb"></div>
<div id="checkbox" style="position: absolute;margin: 70px  500px;width: 200px"></div>
<div id="sam" style="position: absolute;margin: 150px  355px;width: 200px">
<img src="sam.png">
</div>
<div id="regmain" style="margin: 0px 20px">
<center><h2 style="font-family: BPreplay">Member Registration Form</h2></center><br>
<form method="post" action="regprocess.jsp" name="regform" onsubmit="return validateForm()">
Username : <input type="text" name="username" id="usern" style="margin: 0px 60px"/><br><br>
Password : <input type="password" name="password" style="margin: 0px 63px"/><br><br>
Confirm Password : <input type="password" name="repassword" style="margin: 0px 5px"/><br><br>
First Name :<input type="text" name="fname" style="margin: 0px 57px"/><br><br>
Last Name  :<input type="text" name="lname" style="margin: 0px 59px"/><br><br>
Email      : <input type="text" name="email" style="margin: 0px 85px"/><br><br>
State      : <input type="text" name="state" style="margin: 0px 90px"/><br><br>
City       : <input type="text" name="city" style="margin: 0px 95px"/><br><br>
Country : <input type="text" name="country" style="margin: 0px 70px"/><br><br>
Mobile Number: <input type="text" name="mobile" style="margin: 0px 23px"/><br><br>
Sex : <select name="sex" style="margin: 0px 98px">
<option value="male">Male</option>
<option value="female">Female</option>
</select><br><br>
Pincode : <input type="text" name="pincode" style="margin: 0px 70px"/><br><br>
Date Of Birth :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="dob" id="dob">
<br><br>
Address : <textarea rows="" cols="50" name="address" style="margin: 0px 67px"></textarea><br><br>
<input type="submit" value="Register" /><input type="reset" value="Reset" style="margin: 0px 30px"/><br><br>
</form>
</div>
</div>
<div id="signin" style="display:none;width:300px;">
<h3>Member Login</h3>
<form  method="post" action="logprocess.jsp" onSubmit="return validateForm2()">
Username : <input type="text" name="username" ><br><br>
Password : <input type="password" name="password" ><br><br><br>
<input type="submit" value="LOGIN">
<input type="reset" value="RESET" >
</form>
</div>
<div id="wrapper">
<div id="titleimg">
<img src="guest_icon.png" width="50" height="50"></div>
<div id="logomain">
<img src="logo1.png" alt="LOGO"></div>
<div id="title">
<b>Welcome!</b>&nbsp;&nbsp;<u>Guest</u><br><a href="#signup" class="fancybox">Sign Up</a> | <a href="#signin" class="fancybox">Sign In</a>
</div>
<div id="banner">
<ul class="menuH decor1">
    <li style="margin-left: 10px;"><a href="index.jsp">Home</a></li>
    <li><a href="#2" class="arrow">Features</a>
        <ul>
            <li><a href="#">Explore Locations</a></li>
        </ul>
    </li>
    <li><a href="#3">About Us</a></li>
    <li><a href="#3">Contact Us</a></li>
</ul>
</div>
<div id="container">
<div id="message">
<% 
String username = request.getParameter("username");
String password = request.getParameter("password");
String repassword = request.getParameter("repassword");
String first = request.getParameter("fname");
String last = request.getParameter("lname");
String email = request.getParameter("email");
String state = request.getParameter("state");
String city = request.getParameter("city");
String country = request.getParameter("country");
String sex = request.getParameter("sex");
String mobile = request.getParameter("mobile");
String address = request.getParameter("address");
String pincode = request.getParameter("pincode");
String dob = request.getParameter("dob");
String secretq = request.getParameter("secretq");
String secreta = request.getParameter("secreta");
System.out.println(dob);
int count=0;
try
{
if((username.length()==0)&&(password.length()==0))
{
count++;
out.println("<h1>Registration Error !!</h1>");
out.println("<p>Sorry Try Again.</p>");
out.println("<p>Fill the registration Form with correct inforamtion.</p>");
out.println("<p>Fill all the mandatory Fields.</p>");
out.println("<p>Register Again ?.<a href='#signup' class='fancybox'>Click Here</a></p>");
}
else if(!(password.equals(repassword)))
{
count++;
out.println("<h1>Registration Error !!</h1>");
out.println("<p>Sorry Try Again.</p>");
out.println("<p>Fill the registration Form with correct inforamtion.</p>");
out.println("<p>Fill all the mandatory Fields.</p>");
out.println("<p>Sorry Password does not Match !!</p>");
out.println("<p>Register Again ?.<a href='#signup' class='fancybox'>Click Here</a></p>");
}
else if(mobile.length()>10||mobile.length()<10)
{
count++;
out.println("<h1>Registration Error !!</h1>");
out.println("<p>Sorry Try Again.</p>");
out.println("<p>Fill the registration Form with correct inforamtion.</p>");
out.println("<p>Fill all the mandatory Fields.</p>");
out.println("<p>You Have Provided an invalid Mobile Number</p>");
out.println("<p>Register Again ?.<a href='#signup' class='fancybox'>Click Here</a></p>");
}
else
{
int count1=0,count2=0,count3=0;
Class.forName("com.ibm.db2.jcc.DB2Driver");
System.out.println("Driver Loaded Successfully");
Connection con = DriverManager.getConnection("jdbc:db2://localhost:50000/LORDS","senthilkumar","senthil");
System.out.println("Connection Made Successfully");
PreparedStatement stmt1 = con.prepareStatement("SELECT USERNAME FROM SENTHILKUMAR.USER_INFO WHERE USERNAME='"+username+"'");
ResultSet rs= stmt1.executeQuery();
while(rs.next())
{
count1++;
}
PreparedStatement stmt2 = con.prepareStatement("SELECT EMAIL FROM SENTHILKUMAR.USER_INFO WHERE EMAIL='"+email+"'");
ResultSet rs1= stmt2.executeQuery();
while(rs1.next())
{
count2++;
}
PreparedStatement stmt3 = con.prepareStatement("SELECT MOBILE FROM SENTHILKUMAR.USER_INFO WHERE MOBILE='"+mobile+"'");
ResultSet rs3= stmt3.executeQuery();
while(rs3.next())
{
count3++;
}
if(count1>0)
{
count++;
out.println("<h1>Registration Error !!</h1>");
out.println("<p>Sorry Try Again.</p>");
out.println("<p>Fill the registration Form with correct inforamtion.</p>");
out.println("<p>Fill all the mandatory Fields.</p>");
out.println("<p>Username already exsists,register with other username</p>");
out.println("<p>Register Again ?.<a href='#signup' class='fancybox'>Click Here</a></p>");
}
else if(count2>0)
{
count++;
out.println("<h1>Registration Error !!</h1>");
out.println("<p>Sorry Try Again.</p>");
out.println("<p>Fill the registration Form with correct inforamtion.</p>");
out.println("<p>Fill all the mandatory Fields.</p>");
out.println("<p>Email already exsists,register with other email address</p>");
out.println("<p>Register Again ?.<a href='#signup' class='fancybox'>Click Here</a></p>");
}
else if(count3>0)
{
count++;
out.println("<h1>Registration Error !!</h1>");
out.println("<p>Sorry Try Again.</p>");
out.println("<p>Fill the registration Form with correct inforamtion.</p>");
out.println("<p>Fill all the mandatory Fields.</p>");
out.println("<p>Mobile Number Already exsists,register with other Mobile Number</p>");
out.println("<p>Register Again ?.<a href='#signup' class='fancybox'>Click Here</a></p>");
}
else
{ 
        String to = email;
        String subject = "Registration Details of Smart Tours & Travels";
        String messageText = "Click the link to authenticate http://localhost:9080/lords/regmid.jsp?username="+username+"&authenticate=Approved Thank You for Registering";
        String host="smtp.gmail.com", user="senthilkumar.cse04@gmail.com", pass="senthilkumartrailerz36004";
		String SSL_FACTORY ="javax.net.ssl.SSLSocketFactory"; 
		boolean sessionDebug = true;
		Properties props = System.getProperties();
		props.put("mail.host", host);
		props.put("mail.transport.protocol.", "smtp");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
		Session mailSession = Session.getDefaultInstance(props,null);
		mailSession.setDebug(sessionDebug);
		Message msg =new MimeMessage(mailSession);
		//msg.setFrom(new InternetAddress(from));
		try
		{
		InternetAddress[] address1 = {new InternetAddress(to)};
		msg.setRecipients(Message.RecipientType.TO, address1);
		msg.setSubject(subject);
		msg.setContent(messageText,"text/plain"); // use setText if you want to send text
		Transport transport = mailSession.getTransport("smtp");
		System.out.println("Success");
		transport.connect(host, user, pass);
		System.out.println("Success");
		transport.sendMessage(msg, msg.getAllRecipients());//WasEmailSent = true; // assume it was sent
}
catch(Exception err) {

		//WasEmailSent = false; // assume it's a fail
		System.out.println(err); 
		//System.out.println("Error"+err.getMessage());
		}
PreparedStatement stmt = con.prepareStatement("INSERT INTO SENTHILKUMAR.USER_INFO VALUES ('"+username+"','"+first+"','"+last+"','"+email+"','"+state+"','"+city+"','"+country+"','"+sex+"','"+mobile+"','"+address+"','"+pincode+"','"+dob+"','Waiting','"+secretq+"','"+secreta+"')");
System.out.println("Statement executed Successfully");
stmt.executeUpdate();
stmt = con.prepareStatement("INSERT INTO SENTHILKUMAR.USER_LOGIN VALUES('"+username+"','"+password+"','"+password+"')");
stmt.executeUpdate();
System.out.println("Query Successfully");
out.println("<h1>Registration Successfull !!</h1>");
out.println("<br>");
out.println("<p>Congratulations</p>");
out.println("<p>You are now a member of smart tours and travels</p>");
out.println("<p>An email have been sent to your mail address for authentication.</p>");
out.println("<p>Enjoy your trip with Smart tours and travels</p>");
}
}
}
catch(NullPointerException ex)
{
out.println(ex);
}
%>
</div>
<%
if(count == 0)
{
%>
<div style="position :absolute;margin: 350px 350px;width: 300px"><center><font face="BPreplay">You are ready to go !!</font><a href="#signin" class="fancybox">Login Here</a></center></div>
<%
}
 %>
<div id="logo">
<%
 if(count == 0)
 {
 out.println("<img src='Facebook_like_thumb.png' width='300' height='300'>");
 }
 else
 {
 out.println("<img src='unlike.png' width='300' height='300'>");
 }
 %>
</div>
</div>
<div id="footer"><center><div id="carmenu">
<a href="uindex.jsp" style="padding: 0px 15px;color: white">Home</a>
<a href="" style="padding: 0px 15px;color: white">Terms & Conditions</a>
<a href="" style="padding: 0px 15px;color: white">About Us</a>
<a href="" style="padding: 0px 15px;color: white">Contact Us</a>
</div></center></div><br><center>&copy;Copyright to Smart Tours & Travels 2013.</center><br>
</div>
</body>
</html>