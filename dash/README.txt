// Contains details about team members, account used, files and descrition

Team Members

1. G SRI SHAILA  		 - A0070666B
2. SARANYA KANDAN 		 - A0132998M
3. XAVIER JAYARAJ SIDDARTH ASHOK - A0118982U

Username

1. SRI SHAILA G 		 - a0070666
2. SARANYA KANDAN 		 - a0132998
3. XAVIER JAYARAJ SIDDARTH ASHOK - a0118982

Pilatus Server Account 

XAVIER JAYARAJ SIDDARTH ASHOK - a0118982

Files

1. MyDPT12.apk 			- App installer, tested and works fine in the android tablet.
2. DPT12 folder			- Contains the cleaned android source code exported from eclipse.
3. server_scripts		- Contains all the php scripts used by our server implementation.