 <?php
    	$video_width_array = array(240,480,720);
    	$video_height_array = array(160,320,480);// low,med, high
    	$video_bandwidth_array = array(200000,768000,3092000);
    	$dir = $argv[1]."_output/";
    	$split = preg_split('\'trial\'', $argv[1]);
    	$folder_name =  preg_split('\'/\'',$split[1]);
    	$i = 0;
    	$output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    	$output .= "<MPD xmlns:xsi=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"urn:mpeg:mpegB:schema:DASH:MPD:DIS2011\" xsi:schemaLocation=\"urn:mpeg:mpegB:schema:DASH:MPD:DIS2011\" profiles=\"urn:mpeg:mpegB:profile:dash:isoff-basic-on-demand:cm\" type=\"OnDemand\" mediaPresentationDuration=\"PT0H0M22S\" minBufferTime=\"PT3.00S\">\n";
	$output .= "\t<Period  start=\"PT0S\" id=\"1\">\n";
	$output .= "\t\t<AdaptationSet group=\"1\" bitstreamSwitching=\"true\">\n";
	while($i < 3){
    		$dir = $argv[1]."_output".($i+1)."/";
		if(($dh = @opendir($dir)) !== false)
		{// representation tag holds all the segments belonging to the same quality
    		$output .= "\t\t\t<Representation id=\"".$i."\" codecs=\"avc1.4d401f\" mimeType=\"video/mp4\" width=\"".$video_width_array[1]."\" height=\"".$video_height_array[1]."\" bandwidth=\"".$video_bandwidth_array[$i]."\" startWithSAP=\"1\">\n";
	    	$output .= "\t\t\t\t<SegmentList duration=\"PT3.00S\">\n";
	    	$first_segment = 1;
	    	$files_in_dir = @scandir($dir);
            	natsort($files_in_dir);
            	while(list($key, $val) = each($files_in_dir))
            	{
			preg_match("/(.+?)(\.[^.]*$|$)/", $val, $matches);
                	$file_ext = $matches[2];// matches = {"sample.txt", "sample", ".txt"}
                	preg_match("/(.+?)(\_[^_]*$|$)/", $matches[1], $mat);//
			$path_for_url = preg_split('\'public_html\'', $dir);
			if($val != '.' && $val != '..' && $file_ext == ".mp4"){
					$output .= "\t\t\t\t\t<SegmentURL media= \"http://137.132.82.164/~a0118982".$path_for_url[1].$val."\"/>\n";	
			}
	    	}
            	$output .= "\t\t\t\t</SegmentList>\n";
	    	$output .= "\t\t\t</Representation>\n";
            	@closedir($dh);
        	}
        	else
        	{
            		die('unable to generate playlist');
       	 	}
        		$i += 1;
   	 }
    	$output .= "\t\t</AdaptationSet>\n\t</Period>\n</MPD>"; //closing tags
	   
    	$fp = fopen($argv[1]."main_playlist/".$folder_name[1].".mpd", "w");
    	fwrite($fp, $output);
    	fclose($fp);
?> 
