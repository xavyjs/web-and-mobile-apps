/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package edu.nus.MyDashServiceT12;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        public static final int padding_large=0x7f040002;
        public static final int padding_medium=0x7f040001;
        public static final int padding_small=0x7f040000;
    }
    public static final class drawable {
        public static final int ic_action_search=0x7f020000;
        public static final int ic_launcher=0x7f020001;
        public static final int icon_alert=0x7f020002;
    }
    public static final class id {
        public static final int button1=0x7f080005;
        public static final int button2=0x7f080004;
        public static final int editText1=0x7f080006;
        public static final int exit=0x7f080002;
        public static final int listView1=0x7f08000d;
        public static final int menu_settings=0x7f08000e;
        public static final int player=0x7f080000;
        public static final int preview=0x7f080009;
        public static final int recorder=0x7f080001;
        public static final int showupload=0x7f08000c;
        public static final int spinner1=0x7f080003;
        public static final int splitter=0x7f08000a;
        public static final int timer=0x7f080008;
        public static final int uploader=0x7f08000b;
        public static final int videoView=0x7f080007;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int activity_player=0x7f030001;
        public static final int activity_recorder=0x7f030002;
        public static final int activity_uploader=0x7f030003;
    }
    public static final class menu {
        public static final int activity_main=0x7f070000;
        public static final int activity_recorder=0x7f070001;
        public static final int activity_show_streamlets=0x7f070002;
        public static final int activity_uploader=0x7f070003;
    }
    public static final class string {
        public static final int app_name=0x7f050000;
        public static final int hello_world=0x7f050001;
        public static final int menu_settings=0x7f050002;
        public static final int title_activity_main=0x7f050003;
        public static final int title_activity_player=0x7f050006;
        public static final int title_activity_recorder=0x7f050004;
        public static final int title_activity_uploader=0x7f050005;
    }
    public static final class style {
        public static final int AppTheme=0x7f060000;
    }
}
