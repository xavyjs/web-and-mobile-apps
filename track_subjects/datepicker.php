
<html lang="en">
<head>
<meta charset="utf-8" />
<title>jQuery UI Datepicker - Icon trigger</title>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/ui-lightness/jquery-ui.css" />
<link rel="stylesheet" href="http://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="/resources/library/jquery-ui-timepicker-addon.js"></script>
<script>
$(function() {
	$('#selectedDateTime').datetimepicker({ dateFormat: 'dd-mm-yy', timeFormat: 'hh:mm:ss' });
});
</script>
<style>
.ui-timepicker-div .ui-widget-header{ margin-bottom: 8px; }
.ui-timepicker-div dl{ text-align: left; }
.ui-timepicker-div dl dt{ height: 25px; }
.ui-timepicker-div dl dd{ margin: -25px 0 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
</style>
</head>
<body>
	<p>Date: <input type="text" id="selectedDateTime" /></p>
</body>
</html>











