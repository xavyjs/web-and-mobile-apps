<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/DateTimePicker.php");  ?>

<?php //Retrieving Full Appointment Information
	$participant_aid = $_GET['participant_aid'];
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM participants WHERE Participant_AID=:participant_aid');
	$stmt->execute(array('participant_aid' => $participant_aid));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$participant_id = $row['Participant_ID'];
			$age = $row['Age'];
			$race_id = $row['Race_ID'];
			$gender_id = $row['Gender_ID'];
			$mobile = $row['Mobile'];
			$study_arm_id = $row['Study_Arm_ID'];
			$pstatus_id = $row['PStatus_ID'];
			$num_daily_doses = $row['Num_Daily_Doses'];
			$interval_1to2=($row['Interval_1to2']!=0 ? $row['Interval_1to2'] : ''); 
			$interval_2to3=($row['Interval_2to3']!=0 ? $row['Interval_2to3'] : ''); 
			$interval_3to4=($row['Interval_3to4']!=0 ? $row['Interval_3to4'] : ''); 
			$interval_4to5=($row['Interval_4to5']!=0 ? $row['Interval_4to5'] : ''); 
			$interval_5to6=($row['Interval_5to6']!=0 ? $row['Interval_5to6'] : ''); 
			$interval_6to7=($row['Interval_6to7']!=0 ? $row['Interval_6to7'] : ''); 
			$interval_7to8=($row['Interval_7to8']!=0 ? $row['Interval_7to8'] : ''); 
			$baseline_assessment_date=date('d M Y',strtotime($row['Baseline_Assessment_Date']));
			$month3_assessment_date=date('d M Y',strtotime($row['Month3_Assessment_Date']));
			$month6_assessment_date=date('d M Y',strtotime($row['Month6_Assessment_Date']));
			$healthcare_cost_mth13 = $row['Healthcare_Cost_Mth13'];
			$healthcare_cost_mth46 = $row['Healthcare_Cost_Mth46'];
			$pnote = $row['PNote']; 

		}		
	}						
	$dbo = null; //Close DB connection
?>

<?php //Racedropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	 
	$stmt = $dbo->prepare('SELECT * FROM ctbl_race');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no race yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Race_ID']==(isset($_POST['race_id']) ? $_POST['race_id'] : $race_id)) {
				$race_id_row = $race_id_row . "<option value=" . $row['Race_ID'] . " selected='selected'>" . $row['Race'] . "</option>";
			} else {
				$race_id_row = $race_id_row . "<option value=" . $row['Race_ID'] . ">" . $row['Race'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Gender dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	 
	$stmt = $dbo->prepare('SELECT * FROM ctbl_gender');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no gender yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Gender_ID']==(isset($_POST['gender_id']) ? $_POST['gender_id'] : $gender_id)) {
				$gender_id_row = $gender_id_row . "<option value=" . $row['Gender_ID'] . " selected='selected'>" . $row['Gender'] . "</option>";
			} else {
				$gender_id_row = $gender_id_row . "<option value=" . $row['Gender_ID'] . ">" . $row['Gender'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Study Arm dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	 
	$stmt = $dbo->prepare('SELECT * FROM ctbl_study_arm');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no study arm yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Study_Arm_ID']==(isset($_POST['study_arm_id']) ? $_POST['study_arm_id'] : $study_arm_id)) {
				$study_arm_id_row = $study_arm_id_row . "<option value=" . $row['Study_Arm_ID'] . " selected='selected'>" . $row['Study_Arm'] . "</option>";
			} else {
				$study_arm_id_row = $study_arm_id_row . "<option value=" . $row['Study_Arm_ID'] . ">" . $row['Study_Arm'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //PStatus dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	 
	$stmt = $dbo->prepare('SELECT * FROM ctbl_pstatus');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no status yet";
	} 
	else {
		$pstatus_id_row = $pstatus_id_row . "<option value='0'>Select one:</option>";
		foreach ($result as $row){
			if ($row['PStatus_ID']==(isset($_POST['pstatus_id']) ? $_POST['pstatus_id'] : $pstatus_id)) {
				$pstatus_id_row = $pstatus_id_row . "<option value=" . $row['PStatus_ID'] . " selected='selected'>" . $row['PStatus'] . "</option>";
			} else {
				$pstatus_id_row = $pstatus_id_row . "<option value=" . $row['PStatus_ID'] . ">" . $row['PStatus'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

	<h1 class='title'>Participant</h1>
	<p class='title'>Edit</p>
	<?php //echo $appointment_time_t; //To check variable ?>
	<form action="participant_edit_process.php" method="post">
		<table class='new'>
			<tr>
				<td align='right' width='30%'>Participant ID:</td>
				<td align='left' width='70%'>
					<input name="participant_id2" size="40" type="text" disabled="disabled" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $participant_id ?>"></input>
					<input name="participant_id" size="40" type="hidden" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $participant_id ?>"></input>
					<input name="participant_aid" size="40" type="hidden" value="<?php echo isset($_POST['participant_aid']) ? $_POST['participant_aid'] : $participant_aid ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Age:</td>
				<td align='left'>
					<input name="age" type="number" size="40" value="<?php echo isset($_POST['age']) ? $_POST['age'] : $age ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Race:</td>
				<td align='left'>			
					<select name="race_id">					
						<?php echo $race_id_row; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td align='right'>Gender:</td>
				<td align='left'>			
					<select name="gender_id">					
						<?php echo $gender_id_row; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td align='right'>Mobile No.:</td>
				<td align='left'>
					<input name="mobile" type="text" size="40" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : $mobile ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'></td>
				<td align='left'>
					<br>
				</td>
			</tr>
			<tr>
				<td align='right'>Study Arm:</td>
				<td align='left'>			
					<select name="study_arm_id">					
						<?php echo $study_arm_id_row; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td align='right'>Study Status:</td>
				<td align='left'>			
					<select name="pstatus_id">					
						<?php echo $pstatus_id_row; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td align='right'></td>
				<td align='left'>
					<br>
				</td>
			</tr>
			<tr>
				<td align='right'>No. of Daily Doses (1-8):</td>
				<td align='left'>
					<input name="num_daily_doses" type="number" size="40" value="<?php echo isset($_POST['num_daily_doses']) ? $_POST['num_daily_doses'] : $num_daily_doses ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'></td>
				<td align='left'>
					Interval between doses (in hours)
				</td>
			</tr>
			<tr>
				<td align='right'>Dose 1 - 2:</td>
				<td align='left'>
					<input name="interval_1to2" type="number" size="40" value="<?php echo isset($_POST['interval_1to2']) ? $_POST['interval_1to2'] : $interval_1to2 ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Dose 2 - 3:</td>
				<td align='left'>
					<input name="interval_2to3" type="number" size="40" value="<?php echo isset($_POST['interval_2to3']) ? $_POST['interval_2to3'] : $interval_2to3 ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Dose 3 - 4:</td>
				<td align='left'>
					<input name="interval_3to4" type="number" size="40" value="<?php echo isset($_POST['interval_3to4']) ? $_POST['interval_3to4'] : $interval_3to4 ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Dose 4 - 5:</td>
				<td align='left'>
					<input name="interval_4to5" type="number" size="40" value="<?php echo isset($_POST['interval_4to5']) ? $_POST['interval_4to5'] : $interval_4to5 ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Dose 5 - 6:</td>
				<td align='left'>
					<input name="interval_5to6" type="number" size="40" value="<?php echo isset($_POST['interval_5to6']) ? $_POST['interval_5to6'] : $interval_5to6 ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Dose 6 - 7:</td>
				<td align='left'>
					<input name="interval_6to7" type="number" size="40" value="<?php echo isset($_POST['interval_6to7']) ? $_POST['interval_6to7'] : $interval_6to7 ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Dose 7 - 8:</td>
				<td align='left'>
					<input name="interval_7to8" type="number" size="40" value="<?php echo isset($_POST['interval_7to8']) ? $_POST['interval_7to8'] : $interval_7to8 ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'></td>
				<td align='left'>
					<br>
				</td>
			</tr>
			<tr>
				<td align='right'>Date of Baseline Assessment:</td>
				<td align='left'>
					<input type="text" name="baseline_assessment_date" id="datepicker" class="datepicker" readonly="readonly" value="<?php echo isset($_POST['baseline_assessment_date']) ? $_POST['baseline_assessment_date'] : $baseline_assessment_date ?>"/>
				</td>
			</tr>
			<tr>
				<td align='right'>Date of Month 3 Assessment:</td>
				<td align='left'>
					<input type="text" name="month3_assessment_date" id="datepicker1" class="datepicker" readonly="readonly" value="<?php echo isset($_POST['month3_assessment_date']) ? $_POST['month3_assessment_date'] : $month3_assessment_date ?>"/>
				</td>
			</tr>
			<tr>
				<td align='right'>Date of Month 6 Assessment:</td>
				<td align='left'>
					<input type="text" name="month6_assessment_date" id="datepicker2" class="datepicker" readonly="readonly" value="<?php echo isset($_POST['month6_assessment_date']) ? $_POST['month6_assessment_date'] : $month6_assessment_date ?>"/>
				</td>
			</tr>
			<tr>
				<td align='right'></td>
				<td align='left'>
					<br>
				</td>
			</tr>
			<tr>
				<td align='right'>Healthcare Costs Month 1-3 ($):</td>
				<td align='left'>
					<input name="healthcare_cost_mth13" type="number" step="0.01" min=0 size="40" value="<?php echo isset($_POST['healthcare_cost_mth13']) ? $_POST['healthcare_cost_mth13'] : $healthcare_cost_mth13 ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Healthcare Costs Month 4-6 ($):</td>
				<td align='left'>
					<input name="healthcare_cost_mth46" type="number" step="0.01" min=0 size="40" value="<?php echo isset($_POST['healthcare_cost_mth46']) ? $_POST['healthcare_cost_mth46'] : $healthcare_cost_mth46 ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'></td>
				<td align='left'>
					<br>
				</td>
			</tr>
			<tr>
				<td align='right'>PNote:</td>
				<td align='left'>
					<textarea name='pnote' rows='5' cols='42'><?php echo isset($_POST['pnote']) ? $_POST['pnote'] : $pnote ?></textarea>
				</td>
			</tr>
			<tr>
				<td><br><br><br></td>
				<td align='right'>
					<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
					<input type="submit" value="Update"></input>
				</td>
			</tr>
		</table>
	</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
