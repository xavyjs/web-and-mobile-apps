<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="../css/style.css?v=2.14" type="text/css">
	<title>TAKSI</title>
</head>
<?php

	/**
	 * Example of retrieving an authentication token of the FitBit service
	 *
	 * PHP version 5.4
	 *
	 * @author     David Desberg <david@daviddesberg.com>
	 * @author     Pieter Hordijk <info@pieterhordijk.com>
	 * @copyright  Copyright (c) 2012 The authors
	 * @license    http://www.opensource.org/licenses/mit-license.html  MIT License
	 */

	use OAuth\OAuth1\Service\FitBit;
	use OAuth\Common\Storage\Session;
	use OAuth\Common\Consumer\Credentials;

	/**
	 * Bootstrap the example
	 */
	require_once __DIR__ . '/bootstrap.php';

	// Session storage
	$storage = new Session();

	// Setup the credentials for the requests
	$credentials = new Credentials(
		$servicesCredentials['fitbit']['key'],
		$servicesCredentials['fitbit']['secret'],
		$currentUri->getAbsoluteUri()
	);

	// Instantiate the FitBit service using the credentials, http client and storage mechanism for the token
	/** @var $fitbitService FitBit */
	$fitbitService = $serviceFactory->createService('FitBit', $credentials, $storage);
	
	if (!empty($_GET['oauth_token'])) {
		$token = $storage->retrieveAccessToken('FitBit');
		//print_r($token)."<br>";
		// This was a callback request from fitbit, get the token
		$fitbitService->requestAccessToken(
			$_GET['oauth_token'],
			$_GET['oauth_verifier'],
			$token->getRequestTokenSecret()
		);
		echo '<pre>';
		print_r($token);
		echo '</pre>';
		echo '<pre>';
		print_r($_SESSION);
		echo '</pre>';
		$result_userinfo = json_decode($fitbitService->request('user/-/profile.json'));
		$fitbit_user_id=$result_userinfo->user->encodedId;
		
					
			// $result_subscribeadd = json_decode($fitbitService->requestpost('user/-/apiSubscriptions/'.$_SESSION['fitbit_participant_id'].'.json'));
			// $result_subscribedel = json_decode($fitbitService->requestdelete('user/-/apiSubscriptions/'.$_SESSION['fitbit_participant_id'].'.json'));
			// $result_subscribelist = json_decode($fitbitService->request('user/-/apiSubscriptions.json'));
			// echo 'Subscription List: <pre>';
			// print_r($result_subscribelist);
			// echo '</pre>';	
			
			$result_activities = json_decode($fitbitService->request('user/'.$_SESSION['fitbit_user_id'].'/activities/steps/date/2013-09-01/today.json')); //More information here: https://wiki.fitbit.com/display/API/API-Get-Time-Series	
			// echo '<pre>';
			// print_r($result_subscribe);
			// echo '</pre>';
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database		
			$stmt = $dbo->prepare("DELETE FROM log_steps WHERE Fitbit_User_ID=?");
			$stmt->execute(array($fitbit_user_id));				
			// echo 'Acitivities result: <pre>' . print_r($result_activities, true) . '</pre>';
			//$array_activities = (array) $result_activities; //Convert objects to arrays
			//$array_activities = json_decode(json_encode($result_activities), true); //Convert objects to arrays; alternative way
			foreach ($result_activities->{'activities-steps'} as $key => $val) {						
				$stmt = $dbo->prepare("INSERT INTO log_steps(Fitbit_User_ID,Steps_Number,Steps_Date) VALUES(:fitbit_user_id,:steps_number,:steps_date)");
				$stmt->execute(array(':fitbit_user_id' => $fitbit_user_id, ':steps_number' => $val->value, ':steps_date' => date("Y-m-d", strtotime($val->dateTime))));			
				//echo $val->value . " steps on " . date("d M Y, D", strtotime($val->dateTime)) . "<br>";
			}	
			$dbo = null; //Close DB connection
			echo '<h2>Steps Data Downloaded Successfully</h2><p>Use the link above to go back to the Participant Information page.</p>';
		$_SESSION['fitbit_user_id'] = null;
	} elseif (!empty($_GET['go']) && $_GET['go'] == 'go') {
			$_SESSION['fitbit_user_id'] = isset($_GET['fitbit_user_id']) ? $_GET['fitbit_user_id'] : '-';
			$_SESSION['fitbit_participant_id'] = isset($_GET['participant_id']) ? $_GET['participant_id'] : '-';
			// extra request needed for oauth1 to request a request token :-)
			$token = $fitbitService->requestRequestToken();
			//print_r($token)."<br><br>";
			$url = $fitbitService->getAuthorizationUri(array('oauth_token' => $token->getRequestToken())); //getAuthorizationUri (requires user to authorize everything) or getAuthenticationUri (auto authorize if already logged into fitbit account)
			//print $url;
			header('Location: ' . $url);
	} else {
		$url = $currentUri->getRelativeUri() . '?go=go';
		echo "<a href='$url'>Login with FitBit!</a>";
		echo '<pre>';
		print_r($_SESSION);
		echo '</pre>';
	}