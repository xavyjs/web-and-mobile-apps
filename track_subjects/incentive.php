<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Readme
// Incentives are calculated when accessing incentive.php and participant_info.php with function resources/library/incentive.php
// Once calculated, the results will be inserted into log_incentive
// So any changes made to this page has to be made to resources/library/incentive.php
?>

<?php //Appointment Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$appointment_id=$_REQUEST['appointment_id'];
	
	$stmt = $dbo->prepare('SELECT a.Event_ID, e.Event, a.Fitbit_Start_Date, a.Appointment_Date, a.ApptStatus_ID, p.* FROM appointments AS a 
							INNER JOIN participants AS p ON a.Participant_ID = p.Participant_ID 
							INNER JOIN ctbl_event AS e ON a.Event_ID = e.Event_ID 
							WHERE a.Appointment_ID=:appointment_id');
	$stmt->execute(array('appointment_id' => $appointment_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "no appointment";
	} 
	else {
		foreach ($result as $row){
			$event_id = $row['Event_ID'];
			$event = $row['Event'];
			$fitbit_start_date=date('Y-m-d',strtotime($row['Fitbit_Start_Date']));
			$appointment_date=date('Y-m-d',strtotime($row['Appointment_Date']));
			$appointment_month = date("m", strtotime($row['Appointment_Date']));
			$appointment_year = date("Y", strtotime($row['Appointment_Date']));
			$apptstatus_id = $row['ApptStatus_ID'];
			$participant_id = $row['Participant_ID'];
			$fitbit_user_id = $row['Fitbit_User_ID'];
			$daily_rental = $row['Daily_Rental'];
	
			switch ($event_id)
			{
			case 3:
				$next_mth_prediction = $row['Prediction_Mth1'];
				break;
			case 4:
				$current_mth_prediction = $row['Prediction_Mth1'];	
				$next_mth_prediction = $row['Prediction_Mth2'];
				$incabsys = $row['IncabSys_Mth1'];
				break;
			case 5: 
				$current_mth_prediction = $row['Prediction_Mth2'];	
				$next_mth_prediction = $row['Prediction_Mth3'];
				$incabsys = $row['IncabSys_Mth2'];
				break;
			case 6:
				$current_mth_prediction = $row['Prediction_Mth3'];	
				$next_mth_prediction = $row['Prediction_Mth4'];
				$incabsys = $row['IncabSys_Mth3'];
				break;
			case 7:  
				$current_mth_prediction = $row['Prediction_Mth4'];	
				$incabsys = $row['IncabSys_Mth4'];
				break;
			case 8:  
				$current_mth_prediction = $row['Prediction_Mth4'];	
				$incabsys = $row['IncabSys_Mth5'];
				break;
			case 9:  
				$current_mth_prediction = $row['Prediction_Mth4'];	
				$incabsys = $row['IncabSys_Mth6'];
				break;
			case 10:  
				$current_mth_prediction = $row['Prediction_Mth4'];	
				$incabsys = $row['IncabSys_Mth7'];
				break;
			default:
				$current_mth_prediction = null;	
				$next_mth_prediction = null;
				$incabsys = null;
			}
			if ($current_mth_prediction==1) {
				$current_mth_prediction='Yes';
			} elseif ($current_mth_prediction==null) {
				$current_mth_prediction='';
			} else {
				$current_mth_prediction='No';
			}
			if ($next_mth_prediction==1) {
				$next_mth_prediction='Yes';
				$next_mth_prediction_row = "<option value=''>-</option>";
				$next_mth_prediction_row = $next_mth_prediction_row . "<option value='1' selected='selected'>Yes</option>";
				$next_mth_prediction_row = $next_mth_prediction_row . "<option value='0'>No</option>";
			} elseif ($next_mth_prediction==null) {
				$next_mth_prediction='';
				$next_mth_prediction_row = "<option value='' selected='selected'>-</option>";
				$next_mth_prediction_row = $next_mth_prediction_row . "<option value='1'>Yes</option>";
				$next_mth_prediction_row = $next_mth_prediction_row . "<option value='0'>No</option>";
			} else {
				$next_mth_prediction='No';
				$next_mth_prediction_row = "<option value=''>-</option>";
				$next_mth_prediction_row = $next_mth_prediction_row . "<option value='1'>Yes</option>";
				$next_mth_prediction_row = $next_mth_prediction_row . "<option value='0' selected='selected'>No</option>";
			}
			
			if ($incabsys==1) {
				$incabsys='Yes';
				$incabsys_row = "<option value=''>-</option>";
				$incabsys_row = $incabsys_row . "<option value='1' selected='selected'>Yes</option>";
				$incabsys_row = $incabsys_row . "<option value='0'>No</option>";
			} elseif ($incabsys==null) {
				$incabsys='';
				$incabsys_row = "<option value='' selected='selected'>-</option>";
				$incabsys_row = $incabsys_row . "<option value='1'>Yes</option>";
				$incabsys_row = $incabsys_row . "<option value='0'>No</option>";
			} else {
				$incabsys='No';
				$incabsys_row = "<option value=''>-</option>";
				$incabsys_row = $incabsys_row . "<option value='1'>Yes</option>";
				$incabsys_row = $incabsys_row . "<option value='0' selected='selected'>No</option>";
			}
			
		}		
	} 
	$dbo = null; //Close DB connection
?>

<?php //Steps Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM log_steps
				WHERE Steps_Date BETWEEN :date_from AND :date_to 
				AND Fitbit_User_ID=:fitbit_user_id
				ORDER BY Steps_Date ASC');
	$stmt->execute(array('date_from' => $fitbit_start_date,'date_to' => date('Y-m-d', strtotime($appointment_date .' -1 day')),'fitbit_user_id' => $fitbit_user_id));
	$row_count = $stmt->rowCount();
	$days_incentive = $row_count;
	$result = $stmt->fetchAll();

	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$steps_id = date("d", strtotime($row['Steps_Date']));
			$result_array[$steps_id]['steps_date'] = date("d M Y, D", strtotime($row['Steps_Date']));
			$result_array[$steps_id]['steps_number'] = $row['Steps_Number'];
			$row_steps_date = "<td align='left'>" . date("d M Y, D", strtotime($row['Steps_Date'])) . "</td>";
			$row_steps_number = "<td align='left'>" . $row['Steps_Number'] . "</td></tr>";
			$row_steps = $row_steps . $row_steps_date . $row_steps_number;
		}
	}						
	//Close DB connection
	$dbo = null;
?>

<?php //Calculate Number of days with steps_number
	$days_500=0;
	$days_7000=0;
	$steps_days=0;
	$steps_total=0;
	if (is_array($result_array)) {
		foreach ($result_array as $key => $value) {
			$steps_total=$steps_total+$result_array[$key]['steps_number'];
			$steps_days++;
			if ($result_array[$key]['steps_number']>=500) {
				$days_500++;
			}
			if ($result_array[$key]['steps_number']>=7000) {
				$days_7000++;
			}
		}
		$steps_average=$steps_total/$steps_days;
	}
?>

<?php // Get Lottery Incentive
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	$stmt = $dbo->prepare('SELECT * FROM lottery WHERE Participant_ID=? AND Event_ID=?');
	$stmt->execute(array($participant_id,$event_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();		
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$lottery_incentive=$lottery_incentive+500;
		}	
		$lottery_incentive_tr = '<tr>								
									<th>Lottery</th>
									<td>S$'.number_format($lottery_incentive,2).'</td>						
								</tr>';
	}
	//Close DB connection
	$dbo = null;
?>

<?php //Customized Display rows

	$list= array(4,5,6,7); // Only display prediction ddl for event_id 3,4,5,6
	if (in_array($event_id, $list)){
	
		//Calculate Total Incentive Awarded
		$total_incentive=0;
		if ($days_7000 >= ($days_incentive-8)) {
			$days_7000_eligibility='Yes';
			$total_incentive=$daily_rental;
		} else {
			$days_7000_eligibility='No';
		}
		
		if ($days_7000_eligibility==$current_mth_prediction) {
			$prediction_eligibility='Yes';
			$total_incentive=$total_incentive+10;
		} else {
			$prediction_eligibility='No';
		}
		
		$current_mth_prediction_tr = '	<tr>								
											<th>Current Month Prediction</th>
											<td>'.$current_mth_prediction.'</td>						
										</tr>';
		$days_7000_eligibility_tr = '	<tr>								
											<th>Step Incentive Eligibility (>= ' . ($days_incentive-8) . ' days with 7000 steps)</th>
											<td>'.$days_7000_eligibility.'</td>						
										</tr>';							
		$prediction_eligibility_tr = '	<tr>								
											<th>Prediction Task Incentive Eligibility</th>
											<td>'.$prediction_eligibility.'</td>						
										</tr>';
	}
	
	
	$list= array(3,4,5,6); // Only display prediction ddl for event_id 3,4,5,6
	if (in_array($event_id, $list)){
		$next_mth_prediction_tr = '	<tr>								
										<th>Next Month Prediction</th>
										<td>'.$next_mth_prediction.'</td>						
									</tr>	';
		$next_mth_prediction_status=isset($_GET['prediction_status']) ? '&#10004;' : '';							
		$next_mth_prediction_form='<table width="700px" align="left">
										<tr>			
											<form action="incentive_prediction_process.php" method="post">
											<td width="70%" style="font-weight:bold;" align="right">Predict if you will attain next month steps target?</td>
											<td width="20%" align="left">
												<input name="participant_id" size="40" type="hidden" value="'.$participant_id.'"></input>
												<input name="event_id" size="40" type="hidden" value="'.$event_id.'"></input>
												<input name="prev_url" size="40" type="hidden" value="'.$_SERVER["REQUEST_URI"].'"></input>
												<select name="next_mth_prediction">' . $next_mth_prediction_row . '</select>
												' . $next_mth_prediction_status . '
											</td>		
											<td width="10%" align="right"><input type="submit" value="Update"></input></td>
											</form>
										</tr>			
									</table>';
	}
	 
	if ($days_500 >= ($days_incentive-8) && $incabsys=='Yes') { // for Study Compliance Incentive Eligibility
		$days_500_eligibility='Yes';
		$total_incentive=$total_incentive+10;
	} else {
		$days_500_eligibility='No';
	}
	
	$list= array(4,5,6,7,8,9,10); // Only display prediction ddl for event_id 3,4,5,6
	if (in_array($event_id, $list)){
		$incabsys_status=isset($_GET['incabsys_status']) ? '&#10004;' : '';
		$incabsys_form='<table width="700px" align="left">
								<tr>			
									<form action="incentive_incabsys_process.php" method="post">
									<td width="70%" style="font-weight:bold;" align="right">Log in and out of in-cab driving system for >=' . ($days_incentive-8) . ' of ' . $days_incentive . ' days?</td>
									<td width="20%" align="left">
										<input name="participant_id" size="40" type="hidden" value="'.$participant_id.'"></input>
										<input name="event_id" size="40" type="hidden" value="'.$event_id.'"></input>
										<input name="prev_url" size="40" type="hidden" value="'.$_SERVER["REQUEST_URI"].'"></input>
										<select name="incabsys">' . $incabsys_row . '</select>
										' . $incabsys_status . '
									</td>		
									<td width="10%" align="right"><input type="submit" value="Update"></input></td>
									</form>
								</tr>			
							</table>';
		$total_incentive_tr = '<table width="760px" ><tr><td><br></td></tr></table><table class="wborder" width="700px" align="left">
									<tr>								
										<th width="70%" style="font-weight:bold;">Total Incentive Awarded</th>
										<td width="30%">S$' . number_format($total_incentive,2) . '</td>						
									</tr>
									'.$lottery_incentive_tr.'
								</table>';
		
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
		$stmt = $dbo->prepare('SELECT Incentive_Paid FROM log_incentive WHERE Participant_ID=? AND Event_ID=?');
		$stmt->execute(array($participant_id,$event_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		
		if ($row_count==0) {
		} 
		else {
			foreach ($result as $row){
				$incentive_paid=$row['Incentive_Paid'];
			}
		}							
		$dbo = null;
		if ($incentive_paid==null){
			$incentive_paid=0;
		}
		$incentive_paid_tr = '<table class="wborder" width="700px" align="left">
									<tr>								
										<th width="70%" style="font-weight:bold;">Incentive Paid</th>
										<td width="30%">S$' . number_format($incentive_paid,2) . '</td>						
									</tr>			
								</table>';
		$incentive_paid_form='<table width="700px" align="left">
										<tr>			
											<form action="incentive_paid_process.php" method="post">
											<td width="70%" style="font-weight:bold;" align="right">Amount to pay (S$):</td>
											<td width="20%" align="left">
												<input name="participant_id" size="40" type="hidden" value="'.$participant_id.'"></input>
												<input name="event_id" size="40" type="hidden" value="'.$event_id.'"></input>
												<input name="prev_url" size="40" type="hidden" value="'.$_SERVER["REQUEST_URI"].'"></input>
												<input name="incentive_paid" size="10" type="text" value="'.$_POST['incentive_paid'].'"></input>
											</td>		
											<td width="10%" align="right"><input type="submit" value="Pay"></input></td>
											</form>
										</tr>			
									</table>';
		
	}
?>

<?php //Insert into log_incentive
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$list= array(4,5,6,7,8,9,10); // Only insert into log_incentive if event=array
	if (in_array($event_id, $list)){
		//Insert into log_incentive
		
		if ($apptstatus_id==3){
			$stmt = $dbo->prepare('SELECT * FROM log_incentive WHERE Participant_ID=? AND Event_ID=?');
			$stmt->execute(array($participant_id,$event_id));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			
			if ($row_count==0) {
				$stmt = $dbo->prepare("INSERT INTO log_incentive(Participant_ID,Appointment_ID,Event_ID,Appointment_Date,Daily_Rental,Current_Mth_Prediction,Next_Mth_Prediction,Days_7000steps,Days_500steps,Avg_Steps,Step_Incentive_Eligibility,Prediction_Eligibility,Compliance_Incentive_Eligibility,Compliance_Lottery_Eligibility,Total_Incentive) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				$stmt->execute(array($participant_id,$appointment_id,$event_id,$appointment_date,$daily_rental,$current_mth_prediction,$next_mth_prediction,$days_7000,$days_500,$steps_average,$days_7000_eligibility,$prediction_eligibility,$days_500_eligibility,$days_500_eligibility,$total_incentive));
			} 
			else {
				$stmt = $dbo->prepare("UPDATE log_incentive SET Participant_ID=?, Appointment_ID=?, Event_ID=?, Appointment_Date=?, Daily_Rental=?, Current_Mth_Prediction=?, Next_Mth_Prediction=?, Days_7000steps=?, Days_500steps=?, Avg_Steps=?, Step_Incentive_Eligibility=?, Prediction_Eligibility=?, Compliance_Incentive_Eligibility=?, Compliance_Lottery_Eligibility=?, Total_Incentive=? WHERE Participant_ID=? AND Event_ID=?");
				$stmt->execute(array($participant_id,$appointment_id,$event_id,$appointment_date,$daily_rental,$current_mth_prediction,$next_mth_prediction,$days_7000,$days_500,$steps_average,$days_7000_eligibility,$prediction_eligibility,$days_500_eligibility,$days_500_eligibility,$total_incentive,$participant_id,$event_id));	
			}	
		} else {
			$stmt = $dbo->prepare("DELETE FROM log_incentive WHERE Participant_ID=? AND Event_ID=?");
			$stmt->execute(array($participant_id,$event_id));
		}	
	}
	//Close DB connection
	$dbo = null;
?>


			
			<h1 class='title'>Steps</h1>
			<p class='title'>Daily steps downloaded from Fitbit</p>
			
			<div style='float:left;width:760px;'>	
				<table class='wborder' width='700px' align='left'>
					<tr>								
						<th width='70%'>Participant ID</th>
						<td width='30%'><a href='participant_info.php?participant_id=<?php echo $participant_id; ?>'><?php echo $participant_id; ?></a></td>						
					</tr>	
					<tr>								
						<th width='70%'>Event</th>
						<td width='30%'><?php echo $event; ?></td>						
					</tr>
					<tr>								
						<th width='70%'>Fitbit Start Date</th>
						<td width='30%'><?php echo date("d M Y, D", strtotime($fitbit_start_date)); ?></td>						
					</tr>
					<tr>								
						<th width='70%'>Appointment Date</th>
						<td width='30%'><?php echo date("d M Y, D", strtotime($appointment_date)); ?></td>						
					</tr>
					<tr>								
						<th>Daily Rental</th>
						<td><?php echo isset($daily_rental) ? 'S$'.$daily_rental : '<a href="participant_edit.php?participant_id='.$participant_id.'&prev_url='.urlencode($_SERVER["REQUEST_URI"]).'">Update</a>'; ?></td>						
					</tr>	
					<tr>								
						<th>No. of days with steps > 7000</th>
						<td><?php echo $days_7000 . ' / ' . $days_incentive; ?></td>						
					</tr>
					<tr>								
						<th>No. of days with steps > 500</th>
						<td><?php echo $days_500 . ' / ' . $days_incentive;  ?></td>						
					</tr>	
					<?php echo $current_mth_prediction_tr; ?>
					<?php echo $next_mth_prediction_tr; ?>
				</table>
				<?php echo $next_mth_prediction_form; ?>	
				<?php echo $incabsys_form; ?>
				<table width='760px' ><tr><td><br></td></tr></table>
				<table class='wborder' width='700px' align='left'>
					<tr>								
						<th width='70%'>Average steps daily</th>
						<td width='30%'><?php echo number_format($steps_average, 2); ?></td>						
					</tr>	
					<?php echo $days_7000_eligibility_tr; ?>		
					<?php echo $prediction_eligibility_tr; ?>
					<tr>								
						<th>Step Compliance Incentive Eligibility (>= <?php echo ($days_incentive-8); ?> days with 500 steps & in-cab sys log-on)</th>
						<td><?php echo $days_500_eligibility; ?></td>						
					</tr>	
					<tr>								
						<th>Study Compliance Lottery Eligibility</th>
						<td><?php echo $days_500_eligibility; ?></td>						
					</tr>						
				</table>
				<?php echo $total_incentive_tr; ?>
				<table width='760px' ><tr><td><br></td></tr></table>
				<?php echo $incentive_paid_tr; ?>
				<?php echo $incentive_paid_form; ?>	
			</div>
			
			<div style='float:left;'>	
				<table class='wborder' width='180px'>
					<tr>								
						<th align='left' width='70%'>Date</th>
						<th align='left' width='30%'>Steps</th>						
					</tr>		
					<?php echo $row_steps; ?>
				</table>
				
			</div>
			<table width='180px' ><tr><td><br></td></tr></table>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
