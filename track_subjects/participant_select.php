<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //--------------Search Filters: Title Field
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_title');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "The database contains no title yet";
	} 
	else {
		$title_id_row= " <option value=''> </option>";
		foreach ($result as $row){
			if ($row['Title_ID']==$_GET['title_id']) {
				$title_id_row = $title_id_row . "<option value=" . $row['Title_ID'] . " selected='selected'>" . $row['Title_ID'] . ". " . $row['Title'] . "</option>";
			} else {
				$title_id_row = $title_id_row . "<option value=" . $row['Title_ID'] . ">" . $row['Title_ID'] . ". " . $row['Title'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$sql = 'SELECT p.Participant_ID, t.Title, p.Title_ID, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote, s1.PStatus 
						FROM participants AS p 
                        LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID
                        LEFT JOIN log_pstatus AS s1 on p.Participant_ID = s1.Participant_ID
                        LEFT OUTER JOIN log_pstatus AS s2 on (p.Participant_ID = s2.Participant_ID AND s1.PStatus_ID < s2.PStatus_ID)
			WHERE s2.PStatus_ID IS NULL
			ORDER BY p.Participant_ID';
	$stmt = $dbo->prepare($sql);								
	$stmt->execute();								
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		$appointment_row = "<tr><td colspan='10'>No participant</td></tr>";	
	} else {	
		foreach ($result as $row){			
			$select_row = "<tr><td><input name='selcheckbox[]' type='checkbox' id='selcheckbox[]' value=" . $row['Participant_ID'] . "></td>";
			$participant_id_row = "<td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "'>" . $row['Participant_ID'] . "</a></td>";		
			$title_row = "<td>" . $row['Title'] . "</td>";
			$lastname_row = "<td>" . htmlspecialchars($row['Lastname']) . "</td>";
			$firstname_row = "<td>" . htmlspecialchars($row['Firstname']) . "</td>";
			$mobile_row = "<td>" . $row['Mobile'] . "</td>";
			$email_row = "<td>" . htmlspecialchars($row['Email']) . "</td>";
			//$address_row = "<td>" . $row['Address1'] . "</td>";
			$postcode_row = "<td>" . $row['Postcode'] . "</td>";
			
			//Calculate Incentive
			$total_incentive=0;
			$incentive_paid=0;
			$incentive_balance=0;
			
			// Get Lottery Incentive
			$stmt2 = $dbo->prepare('SELECT * FROM lottery WHERE Participant_ID=?');
			$stmt2->execute(array($row['Participant_ID']));
			$row_count2 = $stmt2->rowCount();
			$result2 = $stmt2->fetchAll();		
			if ($row_count2==0) {
			} 
			else {
				foreach ($result2 as $row2){
					$total_incentive=$total_incentive+500;
				}	
			}
			
			$stmt2 = $dbo->prepare('SELECT * FROM log_incentive WHERE Participant_ID=?');
			$stmt2->execute(array($row['Participant_ID']));
			$row_count2 = $stmt2->rowCount();
			$result2 = $stmt2->fetchAll();
			
			if ($row_count2==0) {
				$incentive_balance='<td>' . '-' . '</td>';
			} 
			else {
				foreach ($result2 as $row2){
					$total_incentive=$total_incentive+$row2['Total_Incentive'];
					$incentive_paid=$incentive_paid+$row2['Incentive_Paid'];
				}
			}
			$incentive_balance='<td>S$' . number_format(($total_incentive-$incentive_paid),2) . '</td>';
									
			//For note field with hover tip
			if ($row['PNote'] != null) {
				$pnote_row = "<td><div style='cursor:help;clear:both;' title='" . str_replace( "'","&#39;",$row['PNote']) . "'>&#10004;</div></td>";	
			} else {
				$pnote_row = "<td></td>";
			}
			$pstatus_row = "<td>" . $row['PStatus'] . "</td>";
			$link_edit = "<td><a href='participant_edit.php?participant_id=" . $row['Participant_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a> ";
			
			if (!isset($_SESSION['accesslvl']) || $_SESSION['accesslvl'] >= 3) { //Display Delete function if user access level is 3 or higher
				$link_delete = "<a href='participant_delete.php?participant_id=" . $row['Participant_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Del</a>";
			} else {
				$link_delete = "";
			}
			
			$participant_row = $participant_row . $select_row . $participant_id_row . $title_row . $lastname_row . $firstname_row . $mobile_row . $email_row . $incentive_balance . $pnote_row . $pstatus_row . "</td></tr>";			
		}									
	}	
	$dbo = null; //Close DB connection						
?>

	<h1 class='title'>Participant</h1>
	<p class='title'>Batch add appointments (max 50 participants)</p>
		<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header-participant.php"); ?>
		<br><br>
	<form action='appointment_newbatch' method='post'>			
	<table class='wborder' width='960px'>
		<tr>
			<th width='6%'>Sel</th>		
			<th width='3%'>PID</th>									
			<th width='6%'>Title</th>
			<th>Last Name</th>		
			<th>First Name</th>	
			<th>Mobile</th>	
			<th>Email</th>	
			<th width='9%'>Balance</th>		
			<th width='5%'>PNote</th>
			<th>PStatus</th>		
		</tr>		
		<?php echo $participant_row; ?>
	</table>	
	<br>
	<table align='left'>
		<tr>
			<td colspan='10'>
				<input type='submit' value='Create appointment'></input>
			</td>
		</tr>
	</table>									
	</form>	
	<br><br>		
	

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>