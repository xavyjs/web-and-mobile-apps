<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //--------------Search Filters: Event Field
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_event');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "The database contains no event yet";
	} 
	else {
		$event_id_row= " <option value=''> </option>";
		foreach ($result as $row){
			if ($row['Event_ID']==$_GET['event_id']) {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . " selected='selected'>" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			} else {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . ">" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //--------------Search Filters: Location Field
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_location');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no location yet";
	} else {
		$location_id_row= " <option value=''> </option>";
		foreach ($result as $row){
			if ($row['Location_ID']==(isset($_GET['location_id']) ? $_GET['location_id'] : $location_id)) {
				$location_id_row = $location_id_row . "<option value=" . $row['Location_ID'] . " selected='selected'>" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
			} else {
				$location_id_row = $location_id_row . "<option value=" . $row['Location_ID'] . ">" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
			}
		}
	}
	$dbo = null; //Close DB connection
?>

<?php //--------------Search Filters: Appt Status Field
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_apptstatus');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no status yet";
	} else {
		$apptstatus_id_row= " <option value=''> </option>";
		foreach ($result as $row){
			if ($row['ApptStatus_ID']==(isset($_POST['apptstatus_id']) ? $_POST['apptstatus_id'] : $apptstatus_id)) {
				$apptstatus_id_row = $apptstatus_id_row . "<option value=" . $row['ApptStatus_ID'] . " selected='selected'>" . $row['ApptStatus_ID'] . ". " . $row['ApptStatus'] . "</option>";
			} else {
				$apptstatus_id_row = $apptstatus_id_row . "<option value=" . $row['ApptStatus_ID'] . ">" . $row['ApptStatus_ID'] . ". " . $row['ApptStatus'] . "</option>";
			}
		}
	}
	$dbo = null; //Close DB connection
?>

<?php //--------------Search Filters: From Day Field
	$i = 1;
	$date_from_d_row = "<option value=''> </option>";
	while ($i<=31) {
		if ($i==$_GET['date_from_d']) {
		$date_from_d_row = $date_from_d_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_from_d_row = $date_from_d_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php //--------------Search Filters: From Month Field
	$i = 1;
	$date_from_m_row = "<option value=''> </option>";
	while ($i<=12) {
		if ($i==$_GET['date_from_m']) {
		$date_from_m_row = $date_from_m_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_from_m_row = $date_from_m_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php //--------------Search Filters: From Year Field
	$i = date("Y")-1;
	$date_from_y_row = "<option value=''> </option>";
	while ($i<=date("Y")+5) {
		if ($i==$_GET['date_from_y']) {
		$date_from_y_row = $date_from_y_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_from_y_row = $date_from_y_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php //--------------Search Filters: To Day Field
	$i = 1;
	$date_to_d_row = "<option value=''> </option>";
	while ($i<=31) {
		if ($i==$_GET['date_to_d']) {
		$date_to_d_row = $date_to_d_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_to_d_row = $date_to_d_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php //--------------Search Filters: To Month Field
	$i = 1;
	$date_to_m_row = "<option value=''> </option>";
	while ($i<=12) {
		if ($i==$_GET['date_to_m']) {
		$date_to_m_row = $date_to_m_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_to_m_row = $date_to_m_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php //--------------Search Filters: To Year Field
	$i = date("Y")-1;
	$date_to_y_row = "<option value=''> </option>";
	while ($i<=date("Y")+5) {
		if ($i==$_GET['date_to_y']) {
		$date_to_y_row = $date_to_y_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_to_y_row = $date_to_y_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php
	// get class into the page
	// require_once('resources/library/calendar/tc_calendar.php');

	// instantiate class and set properties
	// $myCalendar = new tc_calendar("date1", true);
	// $myCalendar->setIcon("resources/library/calendar/images/iconCalendar.gif");
	// $myCalendar->setDate(01, 03, 1960);
	// $myCalendar->setPath("resources/library/calendar/");
	// $myCalendar->setYearInterval(1960, 2015);
	// $myCalendar->dateAllow('1960-01-01', '2015-03-01');
	// $myCalendar->setSpecificDate(array("2011-04-01", "2011-04-13", "2011-04-25"), 0, 'month');
	// $myCalendar->setOnChange("myChanged('test')");

	// output the calendar
	// $myCalendar->writeScript();	  
?>

<?php //Today's Appointment data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$event_id = (empty($_GET['event_id'])) ? '%' : $_GET['event_id']; 
	$apptstatus_id = (empty($_GET['apptstatus_id'])) ? '%' : $_GET['apptstatus_id']; 
	$location_id = (empty($_GET['location_id'])) ? '%' : $_GET['location_id']; 
	$appointment_date=$_GET['year'] . "-" . $_GET['month'] . "-" . $_GET['day'];
	if ($appointment_date=='--') {
		$appointment_date = null;
	}
	if (empty($appointment_date)) {
		$appointment_date = date('Y-m-d');
	}

	$rec_limit=(empty($_GET['rec_limit'])) ? '10' : $_GET['rec_limit']; 
	if (empty($_GET['page'])) {
		$offset=0;
	} else {
		$offset=($_GET['page']-1)*$rec_limit;
	}

	$sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, E.Event, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.ApptStatus_ID, S.ApptStatus, A.Note  
			FROM appointments AS A 
			LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
			LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
			LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID) AS A2
			WHERE (A2.ApptStatus_ID = 1 OR A2.ApptStatus_ID = 2) AND	
			A2.Appointment_Date = :appointment_date';					
	$sql .= ' AND A2.Participant_id LIKE :participant_id';
	$sql .= ' ORDER BY A2.Appointment_Date, A2.Appointment_Time, A2.Participant_id, A2.Event_ID';
	$sql .= ' LIMIT :offset, :rec_limit';
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':appointment_date', $appointment_date);
	$stmt->bindValue(':participant_id', $participant_id);
	$stmt->bindValue(':offset', (int) trim($offset), PDO::PARAM_INT);
	$stmt->bindValue(':rec_limit', (int) trim($rec_limit), PDO::PARAM_INT);										
	$stmt->execute();	
	$found_rows = $dbo->query('SELECT FOUND_ROWS()')->fetchColumn(0);									
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($participant_id=='%') {
		$participant_id="";
	}
	if ($location_id=='%') {
		$location_id="";
	}
	
	$todayappt_header = "Today's Appointments";
	
	if ($row_count==0) {
		$todayappt_row = "<tr><td colspan='6'>No appointment</td></tr>";	
	} else {	
		foreach ($result as $row){		
			
			$participant_id_row = "<tr><td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "'>" . $row['Participant_ID'] . "</a></td>";		
			$event_row = "<td>" . $row['Event'] . "</td>";
			if ($row['Appointment_Time'] != null) {
				$appointment_time_row = "<td>" . date("h:i A", strtotime($row['Appointment_Time'])) . "</td>";
			} else {
				$appointment_time_row = "<td></td>";
			}	
			$location_row = "<td>" . $row['Location'] . "</td>";					
			
			//--------------------------Add code to determine previous appointment status
			$apptstatus_loop=$row['ApptStatus'];
			if (!empty($row['ApptStatus'])) {
				$stmt2 = $dbo->prepare('SELECT A.Appointment_ID, A.Participant_id, A.Event_ID, A.Appointment_Date, A.Appointment_Time, A.ApptStatus_ID, S.ApptStatus
								FROM appointments AS A
								LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID 
								WHERE A.Participant_id= :participant_id AND
								A.Appointment_ID!= :appointment_id AND															
								A.Event_ID= :event_id AND
								(Appointment_Date < :appointment_date OR (Appointment_Date = :appointment_date AND Appointment_Time < :appointment_time)) AND
								(A.ApptStatus_ID IS NOT NULL AND A.ApptStatus_ID!=0)
								ORDER BY A.Event_ID, A.Appointment_Date DESC, A.Appointment_Time DESC');
				$stmt2->execute(array('participant_id' => $row['Participant_ID'], 'appointment_id' => $row['Appointment_ID'], 'event_id' => $row['Event_ID'], 'appointment_date' => $row['Appointment_Date'], 'appointment_time' => $row['Appointment_Time']));
				$row_count2 = $stmt2->rowCount();
				$result2 = $stmt2->fetchAll();	
				if ($row_count2==0) {
				} 
				else {	
					foreach ($result2 as $row2){	
						$apptstatus_loop=$row2['ApptStatus'][0] . "," . $apptstatus_loop;
					}
				}
			}		
			//--------------------------Add code to determine previous appointment status
							
			$apptstatus_loop_row = "<td>" . $apptstatus_loop . "</td>";
			$apptstatus_row= "<td>" . $row['ApptStatus'] . "</td>";
					
			//For note field with hover tip
			if ($row['Note'] != null) {
				$note_row = "<td><div style='cursor:help;clear:both;' title='" . $row['Note'] . "'>&#10004;</div></td>";	
			} else {
				$note_row = "<td></td>";
			}
			$link_edit = "<td><a href='appointment_edit.php?appointment_id=" . $row['Appointment_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a></td></tr>";
			$todayappt_row = $todayappt_row . $participant_id_row . $event_row . $appointment_time_row . $location_row . $apptstatus_row . $note_row . $link_edit;			
		}									
	}	
	
	//Pagination ------------------------------------------------
	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$event_id = (empty($_GET['event_id'])) ? '%' : $_GET['event_id']; 
	$apptstatus_id = (empty($_GET['apptstatus_id'])) ? '%' : $_GET['apptstatus_id']; 
	$location_id = (empty($_GET['location_id'])) ? '%' : $_GET['location_id']; 
	$appointment_date=$_GET['year'] . "-" . $_GET['month'] . "-" . $_GET['day'];
	
	$sql = 'SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, E.Event, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.ApptStatus_ID, S.ApptStatus, A.Note  
			FROM appointments AS A 
			LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
			LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
			LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID
			WHERE (A.ApptStatus_ID = 1 OR A.ApptStatus_ID = 2) AND	
			A.Appointment_Date = :appointment_date';					
		$sql .= ' AND A.Participant_id LIKE :participant_id';
		$sql .= ' ORDER BY A.Appointment_Date, A.Appointment_Time, A.Participant_id, A.Event_ID';
		$stmt = $dbo->prepare($sql);
		$stmt->bindValue(':appointment_date', $appointment_date);
		$stmt->bindValue(':participant_id', $participant_id);						
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
		$missedappt_pagination = "";
	} 
	else {	
		$total_pages = ceil($row_count/$rec_limit);
		for ($i=1; $i<=$total_pages; $i++) { 
			if ($i==1 && empty($_GET['page'])) {
				$todayappt_pagination = $todayappt_pagination . "<strong>".$i."</strong>&nbsp;"; 
			}
			elseif ($i==$_GET['page']) {
				$todayappt_pagination = $todayappt_pagination . "<strong>".$i."</strong>&nbsp;"; 
			}
			else {
				$todayappt_pagination = $todayappt_pagination . "<a href='dashboard.php?&year=".$_GET['year'].
									"&month=".$_GET['month'].
									"&day=".$_GET['day'].
									"&page=".$i."&rec_limit=".$rec_limit."'>".$i."</a>&nbsp;"; 
			}
		}
	}
	//Pagination ------------------------------------------------			
	$dbo = null; //Close DB connection						
?>

<?php //Missed Appointment data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$event_id = (empty($_GET['event_id'])) ? '%' : $_GET['event_id']; 
	$apptstatus_id = (empty($_GET['apptstatus_id'])) ? '%' : $_GET['apptstatus_id']; 
	$location_id = (empty($_GET['location_id'])) ? '%' : $_GET['location_id']; 
	$appointment_date=$_GET['year'] . "-" . $_GET['month'] . "-" . $_GET['day'];
	if ($appointment_date=='--') {
		$appointment_date = null;
	}
	if (empty($appointment_date)) {
		$appointment_date = date('Y-m-d');
	}

	$rec_limit=(empty($_GET['rec_limit'])) ? '5' : $_GET['rec_limit']; 
	if (empty($_GET['page'])) {
		$offset=0;
	} else {
		$offset=($_GET['page']-1)*$rec_limit;
	}

	$sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, E.Event, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.ApptStatus_ID, S.ApptStatus, A.Note  
			FROM appointments AS A 
			LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
			LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
			LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID) AS A2
			WHERE (A2.ApptStatus_ID = 1 OR A2.ApptStatus_ID = 2) AND	
			A2.Appointment_Date < :appointment_date';					
	$sql .= ' AND A2.Participant_id LIKE :participant_id';
	$sql .= ' ORDER BY A2.Appointment_Date, A2.Appointment_Time, A2.Participant_id, A2.Event_ID';
	$sql .= ' LIMIT :offset, :rec_limit';
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':appointment_date', $appointment_date);
	$stmt->bindValue(':participant_id', $participant_id);
	$stmt->bindValue(':offset', (int) trim($offset), PDO::PARAM_INT);
	$stmt->bindValue(':rec_limit', (int) trim($rec_limit), PDO::PARAM_INT);										
	$stmt->execute();	
	$found_rows = $dbo->query('SELECT FOUND_ROWS()')->fetchColumn(0);									
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($participant_id=='%') {
		$participant_id="";
	}
	if ($location_id=='%') {
		$location_id="";
	}
	
	$missedappt_header = "Missed Appointments";
	
	if ($row_count==0) {
		$missedappt_row = "<tr><td colspan='7'>No appointment</td></tr>";	
	} else {	
		foreach ($result as $row){		
			
			$participant_id_row = "<tr><td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "'>" . $row['Participant_ID'] . "</a></td>";		
			$event_row = "<td>" . $row['Event'] . "</td>";
			if ($row['Appointment_Date'] != null) {
				$appointment_date_row = "<td>" . date("d-m-Y", strtotime($row['Appointment_Date'])) . " ";
			} else {
				$appointment_date_row = "<td>";
			}
			if ($row['Appointment_Time'] != null) {
				$appointment_time_row = date("H:i", strtotime($row['Appointment_Time'])) . "</td>";
			} else {
				$appointment_time_row = "</td>";
			}	
			$location_row = "<td>" . $row['Location'] . "</td>";					
			
			//--------------------------Add code to determine previous appointment status
			$apptstatus_loop=$row['ApptStatus'];
			if (!empty($row['ApptStatus'])) {
				$stmt2 = $dbo->prepare('SELECT A.Appointment_ID, A.Participant_id, A.Event_ID, A.Appointment_Date, A.Appointment_Time, A.ApptStatus_ID, S.ApptStatus
								FROM appointments AS A
								LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID 
								WHERE A.Participant_id= :participant_id AND
								A.Appointment_ID!= :appointment_id AND															
								A.Event_ID= :event_id AND
								(Appointment_Date < :appointment_date OR (Appointment_Date = :appointment_date AND Appointment_Time < :appointment_time)) AND
								(A.ApptStatus_ID IS NOT NULL AND A.ApptStatus_ID!=0)
								ORDER BY A.Event_ID, A.Appointment_Date DESC, A.Appointment_Time DESC');
				$stmt2->execute(array('participant_id' => $row['Participant_ID'], 'appointment_id' => $row['Appointment_ID'], 'event_id' => $row['Event_ID'], 'appointment_date' => $row['Appointment_Date'], 'appointment_time' => $row['Appointment_Time']));
				$row_count2 = $stmt2->rowCount();
				$result2 = $stmt2->fetchAll();	
				if ($row_count2==0) {
				} 
				else {	
					foreach ($result2 as $row2){	
						$apptstatus_loop=$row2['ApptStatus'][0] . "," . $apptstatus_loop;
					}
				}
			}		
			//--------------------------Add code to determine previous appointment status
							
			$apptstatus_row = "<td>" . $apptstatus_loop . "</td>";
					
			//For note field with hover tip
			if ($row['Note'] != null) {
				$note_row = "<td><div style='cursor:help;clear:both;' title='" . $row['Note'] . "'>&#10004;</div></td>";	
			} else {
				$note_row = "<td></td>";
			}
			$link_edit = "<td><a href='appointment_edit.php?appointment_id=" . $row['Appointment_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a></td></tr>";
			$missedappt_row = $missedappt_row . $participant_id_row . $event_row . $appointment_date_row . $appointment_time_row . $location_row . $note_row . $link_edit;			
		}									
	}	
	
	//Pagination ------------------------------------------------
	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$event_id = (empty($_GET['event_id'])) ? '%' : $_GET['event_id']; 
	$apptstatus_id = (empty($_GET['apptstatus_id'])) ? '%' : $_GET['apptstatus_id']; 
	$location_id = (empty($_GET['location_id'])) ? '%' : $_GET['location_id']; 
	$appointment_date=$_GET['year'] . "-" . $_GET['month'] . "-" . $_GET['day'];
	
	$sql = 'SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, E.Event, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.ApptStatus_ID, S.ApptStatus, A.Note  
			FROM appointments AS A 
			LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
			LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
			LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID
			WHERE (A.ApptStatus_ID = 1 OR A.ApptStatus_ID = 2) AND	
			A.Appointment_Date < :appointment_date';					
		$sql .= ' AND A.Participant_id LIKE :participant_id';
		$sql .= ' ORDER BY A.Appointment_Date, A.Appointment_Time, A.Participant_id, A.Event_ID';
		$stmt = $dbo->prepare($sql);
		$stmt->bindValue(':appointment_date', $appointment_date);
		$stmt->bindValue(':participant_id', $participant_id);						
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
		$missedappt_pagination = "";
	} 
	else {	
		$total_pages = ceil($row_count/$rec_limit);
		for ($i=1; $i<=$total_pages; $i++) { 
			if ($i==1 && empty($_GET['page'])) {
				$missedappt_pagination = $missedappt_pagination . "<strong>".$i."</strong>&nbsp;"; 
			}
			elseif ($i==$_GET['page']) {
				$missedappt_pagination = $missedappt_pagination . "<strong>".$i."</strong>&nbsp;"; 
			}
			else {
				$missedappt_pagination = $missedappt_pagination . "<a href='dashboard.php?&year=".$_GET['year'].
									"&month=".$_GET['month'].
									"&day=".$_GET['day'].
									"&page=".$i."&rec_limit=".$rec_limit."'>".$i."</a>&nbsp;"; 
			}
		}
	}
	//Pagination ------------------------------------------------			
	$dbo = null; //Close DB connection						
?>

<?php //SMS Reply data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	

	$rec_limit=(empty($_GET['rec_limit'])) ? '10' : $_GET['rec_limit']; 
	if (empty($_GET['page'])) {
		$offset=0;
	} else {
		$offset=($_GET['page']-1)*$rec_limit;
	}

	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT * FROM log_sms_reply) AS r
			WHERE r.Dismissed <> 'y'";					
	$sql .= " ORDER BY r.Timestamp DESC";
	$sql .= " LIMIT :offset, :rec_limit";
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':offset', (int) trim($offset), PDO::PARAM_INT);
	$stmt->bindValue(':rec_limit', (int) trim($rec_limit), PDO::PARAM_INT);										
	$stmt->execute();	
	$found_rows = $dbo->query('SELECT FOUND_ROWS()')->fetchColumn(0);									
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
		
	$smsreply_header = "SMS Received";
	
	if ($row_count==0) {
		$smsreply_row = "<tr><td colspan='7'>No SMS</td></tr>";	
	} else {	
		foreach ($result as $row){			
			$participant_id_row = "<tr><td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "'>" . $row['Participant_ID'] . "</a></td>";		
			$appointment_id_row = "<td>" . $row['Appointment_ID'] . "</td>";
			$message = strlen($row['Message'])>37 ? substr($row['Message'],0,34).'...' : $row['Message'];
			$message_row = "<td>" . $message . "</td>";					
			$timestamp_row = "<td>" . date("d-m-Y H:i", strtotime($row['Timestamp'])) . "</td>";	
			$link_dismiss = "<td><a href='sms_dismiss.php?log_sms_reply_id=" . $row['log_SMS_Reply_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Dismiss</a></td></tr>";
			$smsreply_row = $smsreply_row . $participant_id_row . $appointment_id_row . $message_row . $timestamp_row . $link_dismiss;			
		}									
	}	
	
	//Pagination ------------------------------------------------
	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	
	$sql = "SELECT * FROM log_sms_reply AS r 
			WHERE r.Dismissed <> 'y'";					
	$sql .= " ORDER BY r.Timestamp DESC";
	$stmt = $dbo->prepare($sql);					
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
		$smsreply_pagination = "";
	} 
	else {	
		$total_pages = ceil($row_count/$rec_limit);
		for ($i=1; $i<=$total_pages; $i++) { 
			if ($i==1 && empty($_GET['page'])) {
				$smsreply_pagination = $smsreply_pagination . "<strong>".$i."</strong>&nbsp;"; 
			}
			elseif ($i==$_GET['page']) {
				$smsreply_pagination = $smsreply_pagination . "<strong>".$i."</strong>&nbsp;"; 
			}
			else {
				$smsreply_pagination = $smsreply_pagination . "<a href='dashboard.php?&year=".$_GET['year'].
									"&month=".$_GET['month'].
									"&day=".$_GET['day'].
									"&page=".$i."&rec_limit=".$rec_limit."'>".$i."</a>&nbsp;"; 
			}
		}
	}
	//Pagination ------------------------------------------------			
	$dbo = null; //Close DB connection						
?>

	<h1 class='title'>Dashboard</h1>
	<p class='title'><?php echo date("d M Y, D", strtotime($appointment_date)); ?><p>
	<p class='title'><?php echo "Today: " . date("d M Y, D, h:i:s A"); ?><p>	
	 <table class='dashboard_main'>
		<tr>
			<td width=50%>
				<table class='dashboard_left'>	
					<tr>
						<th colspan='7'>
							<?php echo $todayappt_header ?>
						</th></tr>
					<tr>
						<th id='th_header'>PID</th>
						<th id='th_header'>Event</th>
						<th id='th_header'>Time</th>
						<th id='th_header'>Location</th>
						<th id='th_header'>Status</th>
						<th id='th_header'>Note</th>
						<th id='th_header'> </th>
					</tr>
					<?php echo $todayappt_row; ?> 
				</table>
				<table width='460px'>
					<tr>
						<td align='left'>	
							<?php echo $todayappt_pagination; ?> 
						</td>
						<td align='right'><a href='dashboard.php?participant_id=<?php echo $participant_id; ?>&event_id=<?php echo $event_id; ?>&apptstatus_id=<?php echo $apptstatus_id; ?>&location_id=<?php echo $location_id; ?>&date_from_d=<?php echo $_GET['date_from_d']; ?>&date_from_m=<?php echo $_GET['date_from_m']; ?>&date_from_y=<?php echo $_GET['date_from_y']; ?>&date_to_d=<?php echo $_GET['date_to_d']; ?>&date_to_m=<?php echo $_GET['date_to_m']; ?>&date_to_y=<?php echo $_GET['date_to_y']; ?>&filter=1&page=1&rec_limit=99999'>Show All</a>
						</td>
					</tr>
				</table>
			</td>
			<td width=50% align='right'>
				<table class='dashboard_right'>	
					<tr>
						<th colspan='6'>
							<?php echo $missedappt_header ?>
						</th></tr>
					<tr>
						<th id='th_header'>PID</th>
						<th id='th_header'>Event</th>
						<th id='th_header'>Date/Time</th>
						<th id='th_header'>Location</th>
						<th id='th_header'>Note</th>
						<th id='th_header'> </th>
					</tr>
					<?php echo $missedappt_row; ?> 
				</table>
				<table width='460px'>
					<tr>
						<td align='left'>	
							<?php echo $missedappt_pagination; ?> 
						</td>
						<td align='right'><a href='dashboard.php?participant_id=<?php echo $participant_id; ?>&event_id=<?php echo $event_id; ?>&apptstatus_id=<?php echo $apptstatus_id; ?>&location_id=<?php echo $location_id; ?>&date_from_d=<?php echo $_GET['date_from_d']; ?>&date_from_m=<?php echo $_GET['date_from_m']; ?>&date_from_y=<?php echo $_GET['date_from_y']; ?>&date_to_d=<?php echo $_GET['date_to_d']; ?>&date_to_m=<?php echo $_GET['date_to_m']; ?>&date_to_y=<?php echo $_GET['date_to_y']; ?>&filter=1&page=1&rec_limit=99999'>Show All</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td align='right'>
				<table class='dashboard_right'>	
					<tr>
						<th colspan='6'>
							<?php echo $smsreply_header ?>
						</th></tr>
					<tr>
						<th id='th_header'>PID</th>
						<th id='th_header'>AID</th>
						<th id='th_header'>Message</th>
						<th id='th_header'>Timestamp</th>
						<th id='th_header'><a href='sms_dismissall.php?prev_url=<?php print urlencode($_SERVER["REQUEST_URI"]) ?>'>Dism All</a></th>
					</tr>
					<?php echo $smsreply_row; ?> 
				</table>
				<table width='460px'>
					<tr>
						<td align='left'>	
							<?php echo $smsreply_pagination; ?> 
						</td>
						<td align='right'><a href='dashboard.php?participant_id=<?php echo $participant_id; ?>&event_id=<?php echo $event_id; ?>&apptstatus_id=<?php echo $apptstatus_id; ?>&location_id=<?php echo $location_id; ?>&date_from_d=<?php echo $_GET['date_from_d']; ?>&date_from_m=<?php echo $_GET['date_from_m']; ?>&date_from_y=<?php echo $_GET['date_from_y']; ?>&date_to_d=<?php echo $_GET['date_to_d']; ?>&date_to_m=<?php echo $_GET['date_to_m']; ?>&date_to_y=<?php echo $_GET['date_to_y']; ?>&filter=1&page=1&rec_limit=99999'>Show All</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
				
		<br><br>


<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>