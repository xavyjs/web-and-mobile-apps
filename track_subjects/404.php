<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>
		<br><br>
		<h1>404: Page not Found</h1>
		<br>
		Sorry, but the page you are looking for has not been found. <br>
		Try checking the URL for errors, then hit the refresh button on your browser.
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>