<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

			<h1 class='title'>SMS</h1>
			<p class='title'>Send SMS template to participant</p>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$error = false;						
			$participant_aid=$_POST['participant_aid'];
			$mobile=$_POST['mobile'];
			$sms_type=$_POST['sms_type'];
			$sms_text=$_POST['sms_text'];
			$prev_url=$_POST['prev_url'];
			
			if ($mobile=="") {
				$error = true;
				$errormsg = $errormsg . "Mobile No cannot be empty<br>";
			} 
			
			if (is_numeric($_POST['mobile'])===false) {
				$error = true;
				$errormsg = $errormsg . "Mobile No must be an integer<br>";
			} 
			
			if ($sms_text=="") {
				$error = true;
				$errormsg = $errormsg . "Message cannot be empty<br>";
			} 
			
			if ($error === true) {
				echo $errormsg;
				echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
						<form action='send_sms' method='post'>
						<input name='participant_aid' type='hidden' size='20' value='" . $participant_aid . "'></input>
						<input name='mobile' type='hidden' size='20' value='" . $mobile . "'></input>
						<input name='sms_text' type='hidden' size='20' value='" . $sms_text . "'></input>
						<input name='sms_type' type='hidden' size='20' value='" . $sms_type . "'></input>
						<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
						<input type='submit' value='Back'></input></form>";
			} else {				
				include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/SendSMS.php");
				$sms_result=gw_send_sms($participant_aid,$sms_type,$sms_text);	
				print $sms_result;
				print "<br><br><a href='" . $prev_url . "'>Back to where you came from</a>";	
			}
		} else {
			header("location:send_sms.php");		
		}		
	?>		
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 