<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

	<h1 class='title'>Dashboard</h1>
	<p class='title'><?php echo "Specified Date: " . date("d M Y, D", strtotime($_GET['year']."/".$_GET['month']."/".$_GET['day'])) . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time Now: " . date("d M Y, D, h:i:s A"); ?><p>
	 <table class='dashboard_main'>
		<tr >
			<td width=50%>			
				<iframe src="dashboard_todayappointments?year=<?php echo $_GET['year']; ?>&month=<?php echo $_GET['month']; ?>&day=<?php echo $_GET['day']; ?>" id="dashboard_iframe" scrolling="no"></iframe>
			</td>
			<td width=50% align='right'>
				<iframe src="dashboard_missedappointments?year=<?php echo $_GET['year']; ?>&month=<?php echo $_GET['month']; ?>&day=<?php echo $_GET['day']; ?>" id="dashboard_iframe" scrolling="no"></iframe>
			</td>
		</tr>
		<tr>
			<td>
				<iframe src="dashboard_smsreply?year=<?php echo $_GET['year']; ?>&month=<?php echo $_GET['month']; ?>&day=<?php echo $_GET['day']; ?>" id="dashboard_iframe" scrolling="no"></iframe>
			</td>
			<td align='right'>
				<iframe src="dashboard_todo?year=<?php echo $_GET['year']; ?>&month=<?php echo $_GET['month']; ?>&day=<?php echo $_GET['day']; ?>" id="dashboard_iframe" scrolling="no"></iframe>
			</td>
		</tr>
	</table>
				
		<br><br>
		
		<?php 
			// echo '<pre>';
				// var_dump($_SESSION);
			// echo '</pre>'; 
		?>


<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>