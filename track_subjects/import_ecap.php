<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	if ($_FILES[csv][size] > 0) {

		//get the csv file
		$file = $_FILES[csv][tmp_name];
		$handle = fopen($file,"r");
		
		$stmt = $dbo->prepare("DELETE FROM import_ecap");
		$stmt->execute();
		
		//remove header from 1st line
		$column_headers = fgetcsv($handle);
		
		//loop through the csv file and insert into database
		do {
			if ($data[4]) {
				if ($data[1]) { //update $participant_id if participant_id is present
					$participant_id=$data[1]; 
				}
				if ($data[2]) { //update $ecap_id if ecap_id is present
					$ecap_id=$data[2]; 
				}
			
				$stmt = $dbo->prepare("INSERT INTO import_ecap (Participant_Id, eCAP_Id, Dose, Dose_Timestamp, Dose_Group, Dose_Label) VALUES
					(
						?, ?, ?, ?, ?, ?
					)
				");
				$stmt->execute(array($participant_id,$ecap_id,$data[3],$data[4],$data[6],$data[7]));
			}
		} 
		
		while ($data = fgetcsv($handle,1000,",","'"));
		//
		
		$message='File uploaded successfully on: ' . date("d M Y, D, h:i:s A") . '<br><a href="/export_ecap.php">Download imported data</a><br><br>';

		//redirect
		//header('Location: import_ecap.php?success=1'); die;

	}
	
	$dbo = null; //Close DB connection
?>

<?php //Get last imported date
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT Timestamp FROM import_ecap ORDER BY Timestamp DESC LIMIT 1');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no eCAP data yet";
	} 
	else {
		foreach ($result as $row){
			$last_imported_date = date("d M Y, D, h:i:s A", strtotime($row['Timestamp']));
		}
	}						
	$dbo = null; //Close DB connection
?>

		
	<h1 class='title'>Import a CSV File with PHP & MySQL</h1>
	<p class='title'>Add New</p>
	<form action="" method="post" enctype="multipart/form-data" name="form1" id="form1">			
		<table class='new'>
		<tr>
			<td></td>
			<td align='left' style='color:#ff0000;'>
				<?php echo isset($message) ? $message : '' ?>
			</td>
		</tr>
		<tr>
			<td align='right' width='30%'>Choose your file:</td>
			<td align='left' width='70%'>			
				<input name="csv" type="file" id="csv" accept=".csv" size="140"/>	
				<!-- <input name="csv" type="file" id="csv" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" size="140"/>	-->
				<!-- read more on accept format at http://stackoverflow.com/questions/11832930/html-input-file-accept-attribute-file-type-csv -->
			</td>
		</tr>
		<tr>
			<td><br><br><br></td>
			<td align='right'>
				<input type="submit" name="Submit" value="Submit" />
			</td>
		</tr>
		</table>
	</form>
	<div align='center'>
		<table class='outcomebox'>
			<tr>
				<th>
					Last imported on: <?php print $last_imported_date; ?>
				</th>
			</tr>
		</table>
	</div>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
