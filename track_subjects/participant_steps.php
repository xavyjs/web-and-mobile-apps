<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Steps Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$sql="SELECT * FROM log_steps
			WHERE Fitbit_User_ID = (SELECT Fitbit_User_ID FROM participants WHERE Participant_ID=:participant_id)
			ORDER BY Steps_Date DESC";
	$stmt = $dbo->prepare($sql);
	$stmt->execute(array('participant_id' => $_GET['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$steps_date = "<td align='left'>" . date("d M Y, D", strtotime($row['Steps_Date'])) . "</td>";
			$steps_number = "<td align='left'>" . $row['Steps_Number'] . "</td></tr>";
			$list_row = $list_row . $steps_date . $steps_number;
		}
	}						
	//Close DB connection
	$dbo = null;
?>
			
			<h1 class='title'>Steps</h1>
			<p class='title'>Daily steps downloaded from Fitbit</p>
						
			<table class='participantinfo'>
				<tr>								
					<th align='left' width='50%'>Date</th>
					<th align='left' width='50%'>No. of Steps</th>						
				</tr>		
				<?php echo $list_row; ?>
			</table>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
