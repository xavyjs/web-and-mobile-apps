<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/comfort_header.php"); ?>

	<h1 class='title'>Create new participant</h1>
	<p class='title'>Adding new driver to screener list</p>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;
		$title_id=$_POST['title_id'];
		$lastname=$_POST['lastname'];
		$firstname=$_POST['firstname'];
		$mobile=$_POST['mobile'];
		$email=$_POST['email'];
		$address=$_POST['address'];
		$postcode=$_POST['postcode'];
		
		// Check for title
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
		$stmt = $dbo->prepare('SELECT Title FROM ctbl_title WHERE Title_ID=:title_id');
			$stmt->execute(array('title_id' => $title_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		if ($row_count==0) {
		} 
		else {
			foreach ($result as $row){														
				$title=$row['Title'];	
			}
		}					
		$dbo = null; //Close DB connection
		
		// Check for existing participant
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
		$stmt = $dbo->prepare('SELECT * FROM participants WHERE Lastname=:lastname AND Firstname=:firstname AND Mobile=:mobile');
			$stmt->execute(array('lastname' => $lastname,'firstname' => $firstname,'mobile' => $mobile));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		if ($row_count==0) {
		} 
		else {
			foreach ($result as $row){														
				$participant_id=$row['Participant_ID'];	
				$error = true;
				$errormsg = $errormsg . "Participant (ID: " . $participant_id . ") already exists.<br>";
			}
		}					
		$dbo = null; //Close DB connection
		
		if ($lastname=="") {
			$error = true;
			$errormsg = $errormsg . "Last Name cannot be empty<br>";
		} 
		
		if ($firstname=="") {
			$error = true;
			$errormsg = $errormsg . "First Name cannot be empty<br>";
		} 
		
		if ($mobile=="") {
			$error = true;
			$errormsg = $errormsg . "Mobile No cannot be empty<br>";
		} 
		
		if (is_numeric($mobile)===false || strlen($mobile)!=8) {
			$error = true;
			$errormsg = $errormsg . "Mobile No must be 8 numbers<br>";
		} 
		
		if ($email=="" && $address=="") {
			$error = true;
			$errormsg = $errormsg . "One of Email or Residential Address cannot be empty<br>";
		} 
		
		if ($address<>"" && $postcode=="") {
			$error = true;
			$errormsg = $errormsg . "Postal Code cannot be empty<br>";
		} 
		
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='comfort_participant_new' method='post'>
					<input name='title_id' type='hidden' size='20' value='" . $title_id . "'></input>
					<input name='lastname' type='hidden' size='20' value='" . $lastname . "'></input>
					<input name='firstname' type='hidden' size='20' value='" . $firstname . "'></input>
					<input name='mobile' type='hidden' size='20' value='" . $mobile . "'></input>
					<input name='email' type='hidden' size='20' value='" . $email . "'></input>
					<input name='address' type='hidden' size='20' value='" . $address . "'></input>
					<input name='postcode' type='hidden' size='20' value='" . $postcode . "'></input>
					<div align='right'><input type='submit' value='Back'></input></div></form>";
		} else {					
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database								
			$stmt = $dbo->prepare("INSERT INTO participants(Title_ID,Lastname,Firstname,Mobile,Email,Address1,Postcode) VALUES(:title_id,:lastname,:firstname,:mobile,:email,:address,:postcode)");
			$stmt->execute(array(':title_id' => $title_id, ':lastname' => $lastname, ':firstname' => $firstname, ':mobile' => $mobile, ':email' => $email, ':address' => $address, ':postcode' => $postcode));
			print "<div align='center'>";
			print "<table class='outcomebox'><tr><th>Assigned participant ID for " . $title . " " . $lastname . ":</th></tr><tr><td><h4>" . $dbo->lastInsertId() . "</h4></td></tr></table>"; 
			print "<a href='comfort_screener_new.php?participant_id=" . $dbo->lastInsertId() . "'>Proceed to screener questionnaire</a></div>";
			
			$dbo = null; //Close DB connection
			
			// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/EmailAppointment.php");
			// $email_id = 1; //Template 1: For newly appointment
			// EmailAppointment($participant_id,$appt_event,$appt_date,$appt_time,$appt_location,$email_id);
			
			// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/SendSMS.php");
			// $sms_id = 1; //Template 1: For newly appointment
			// print gw_send_sms($participant_id,$appt_event,$appt_date,$appt_time,$appt_location,$sms_id);	
							
			//header("location:" . $prev_url);							
		}
			
	} else {
		header("location:comfort_participant_new.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 