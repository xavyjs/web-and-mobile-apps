<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Personal Details Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT t.Title, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote FROM participants AS p LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID WHERE p.Participant_ID=:participant_id');
	$stmt->execute(array('participant_id' => $_REQUEST['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$title = $row['Title'];
			$lastname = $row['Lastname'];
			$firstname = $row['Firstname'];
			$mobile = $row['Mobile'];
			$email = $row['Email'];
			$address1 = $row['Address1'];
			$postcode = $row['Postcode'];
			$pnote = $row['PNote'];
		}
	}						
	//Close DB connection
	$dbo = null;
?>

<?php //Sealed Fitbit Pick-up (Event 1) Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT s.ApptStatus, a.* FROM appointments AS a INNER JOIN ctbl_apptstatus AS s ON a.apptstatus_id = s.ApptStatus_ID WHERE a.Participant_ID=:participant_id AND a.Event_ID=:event_id ORDER BY Appointment_ID');
	$stmt->execute(array('participant_id' => $_REQUEST['participant_id'], 'event_id' => "1"));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		$event1_date = "<a href='appointment_new.php?participant_id=1&event_id=1'>Make appointment</a>";
	} 
	else {
		foreach ($result as $row){
			$event1_date = date("d M Y, D", strtotime($row['Appointment_Date']));
			$event1_status = $row['apptstatus_id'];
		}
	}		
	$dbo = null; //Close DB connection
	
	//--------------Appt Status Field Start	
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	$event1_statusrow = "<select name='event1_status'>";
	$stmt = $dbo->prepare('SELECT * FROM ctbl_apptstatus');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {	
	} 
	else {
		$event1_statusrow = $event1_statusrow . "<option value=''> </option>";
		foreach ($result as $row){
			if ($row['ApptStatus_ID']==$event1_status) {
				$event1_statusrow = $event1_statusrow . "<option value=" . $row['ApptStatus_ID'] . " selected='selected'>" . $row['ApptStatus_ID'] . ". " . $row['ApptStatus'] . "</option>";
			}
			else {
					$event1_statusrow = $event1_statusrow . "<option value=" . $row['ApptStatus_ID'] . ">" . $row['ApptStatus_ID'] . ". " . $row['ApptStatus'] . "</option>";
			}			
		}
	}
	$event1_statusrow =  $event1_statusrow . "</select>";								
	//--------------Appt Status Field End
		
	$dbo = null; //Close DB connection
?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;						
		$event1_status = $_POST['event1_status'];
		$event1_id = $_GET['appointment_id'];

		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
								
		$stmt = $dbo->prepare("UPDATE appointments SET ApptStatus_id=? WHERE Appointment_ID=?");
		$stmt->execute(array($event1_status, $event1_id));						
				
		$dbo = null; //Close DB connection
		
		//if $_POST['prev_url'] is empty, redirect to appointment.php page
		$prev_url = (empty($_POST['prev_url'])) ? 'participant_info.php?participant_id=' . $participant_id : $_POST['prev_url']; 	
		header("location: participant_info.php?participant_id=" . $_GET['participant_id']);								
	}
?>
			
			<h1 class='title'>Participant Information</h1>
			<p class='title'>Eligibility, Appointments, Incentives etc</p>
						
			<table class='participantinfo'>
				<tr>
					<th align='left' valign='top' bgcolor="#cdcdcd" colspan='3'>Personal Details</th>
					<td align='right' bgcolor="#cdcdcd" style='border:none;'><a href="participant_edit.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>">Edit</a></td>
				</tr>
				<tr>
					<td align='right' width='12%' bgcolor="#cdcdcd">Participant ID:</td>
					<td align='left' width='18%'>
						<?php echo isset($_REQUEST['participant_id']) ? $_REQUEST['participant_id'] : '' ?>
						<input name="participant_id" size="15" type="hidden" value="<?php echo isset($_REQUEST['participant_id']) ? $_REQUEST['participant_id'] : '' ?>"></input>
					</td>
					<td align='right' width='12%' bgcolor="#cdcdcd">Name:</td>
					<td align='left' width='63%'>							
						<?php echo $title . ' ' . $lastname . ' ' . $firstname; ?>
					</td>
				</tr>
				<tr>
					<td align='right' bgcolor="#cdcdcd">Mobile No.:</td>
					<td align='left'>
						<?php echo $mobile; ?>
					</td>
					<td align='right' bgcolor="#cdcdcd">Email:</td>
					<td align='left'>							
						<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
					</td>
				</tr>
				<tr>
					<td align='right' bgcolor="#cdcdcd">Postal Code:</td>
					<td align='left'>
						<?php echo $postcode; ?>
					</td>
					<td align='right' bgcolor="#cdcdcd">Address:</td>
					<td align='left'>							
						<?php echo $address1; ?>
					</td>
				</tr>
				<tr>
					<td align='right' valign='top' bgcolor="#cdcdcd">PNote:</td>
					<td align='left' colspan='3'>
						<?php echo $pnote; ?>
					</td>
				</tr>	
			</table>
			<br><br>
			
		<form name="event1_edit"<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
			<table class='participantinfo'>
				<tr>
					<th align='left' valign='top' bgcolor="#cdcdcd" colspan='5'>Sealed Fitbit Pick-up</th>
					<td align='right' bgcolor="#cdcdcd" style='border:none;'></td>
				</tr>
				<tr>
					<td align='right' width='12%' bgcolor="#cdcdcd">Appt Date:</td>
					<td align='left' width='22%'>
						<?php echo $event1_date; ?>
					</td>
					<td align='right' width='12%' bgcolor="#cdcdcd">Appt Time:</td>
					<td align='left' width='22%'>							
						<?php echo $event1_time; ?>
					</td>
					<td align='right' width='12%' bgcolor="#cdcdcd">Location:</td>
					<td align='left' width='20%'>							
						<?php echo $event1_location; ?>
					</td>
				</tr>
				<tr>
					<td align='right' bgcolor="#cdcdcd">SMS Reminder:</td>
					<td align='left'>							
						<?php echo $event1_sms; ?>
					</td>					
					<td align='right' width='10%' bgcolor="#cdcdcd">Reply?:</td>
					<td align='left'>							
						<?php echo $event1_reply; ?>
					</td>
					<td align='right' bgcolor="#cdcdcd">Appt Status:</td>
					<td align='left'>
						<?php echo $event1_statusrow; ?>
					</td>
				</tr>
				<tr>
					<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
					<td align='left' colspan='5'>
						<?php echo $event1_note; ?>
					</td>
				</tr>
			</table>
			<div align='right'><br><input type="submit" value="Save Changes"></input></div>
		</form>
			

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
