<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/comfort_header.php"); ?>

<link rel="stylesheet" type="text/css" href="/resources/library/jquery-autocomplete/jquery.autocomplete.css" />
<script type="text/javascript" src="/resources/library/jquery-autocomplete/lib/jquery.js"></script>
<script type="text/javascript" src="/resources/library/jquery-autocomplete/jquery.autocomplete.js"></script>
<script>
 $(document).ready(function(){
  $("#firstname").autocomplete("autocomplete_firstname.php", {
        selectFirst: true
  });
  $("#lastname").autocomplete("autocomplete_lastname.php", {
        selectFirst: true
  });
  $("#mobile").autocomplete("autocomplete_mobile.php", {
        selectFirst: true
  });
  $("#email").autocomplete("autocomplete_email.php", {
        selectFirst: true
  });
 });
</script>

<?php //--------------Search Filters: Title Field
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_title');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "The database contains no title yet";
	} 
	else {
		$title_id_row= " <option value=''> </option>";
		foreach ($result as $row){
			if ($row['Title_ID']==$_GET['title_id']) {
				$title_id_row = $title_id_row . "<option value=" . $row['Title_ID'] . " selected='selected'>" . $row['Title_ID'] . ". " . $row['Title'] . "</option>";
			} else {
				$title_id_row = $title_id_row . "<option value=" . $row['Title_ID'] . ">" . $row['Title_ID'] . ". " . $row['Title'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php
	// get class into the page
	// require_once('resources/library/calendar/tc_calendar.php');

	// instantiate class and set properties
	// $myCalendar = new tc_calendar("date1", true);
	// $myCalendar->setIcon("resources/library/calendar/images/iconCalendar.gif");
	// $myCalendar->setDate(01, 03, 1960);
	// $myCalendar->setPath("resources/library/calendar/");
	// $myCalendar->setYearInterval(1960, 2015);
	// $myCalendar->dateAllow('1960-01-01', '2015-03-01');
	// $myCalendar->setSpecificDate(array("2011-04-01", "2011-04-13", "2011-04-25"), 0, 'month');
	// $myCalendar->setOnChange("myChanged('test')");

	// output the calendar
	// $myCalendar->writeScript();	  
?>

<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$title_id = (empty($_GET['title_id'])) ? '%' : $_GET['title_id']; 
	$firstname = (empty($_GET['firstname'])) ? '%' : $_GET['firstname']; 
	$lastname = (empty($_GET['lastname'])) ? '%' : $_GET['lastname']; 
	$mobile = (empty($_GET['mobile'])) ? '%' : $_GET['mobile']; 
	$email = (empty($_GET['email'])) ? '%' : $_GET['email']; 


	$rec_limit=(empty($_GET['rec_limit'])) ? '30' : $_GET['rec_limit']; 
	if (empty($_GET['page'])) {
		$offset=0;
	} else {
		$offset=($_GET['page']-1)*$rec_limit;
	}

	$sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT p.Participant_ID, t.Title, p.Title_ID, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote, s1.PStatus 
						FROM participants AS p 
                        LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID
                        LEFT JOIN log_pstatus AS s1 on p.Participant_ID = s1.Participant_ID
                        LEFT OUTER JOIN log_pstatus AS s2 on (p.Participant_ID = s2.Participant_ID AND s1.PStatus_ID < s2.PStatus_ID)
			WHERE s2.PStatus_ID IS NULL) AS p2
			WHERE';					
	$sql .= ' p2.Participant_id LIKE :participant_id';
	$sql .= ' AND p2.Title_ID LIKE :title_id';
	$sql .= ' AND p2.Lastname LIKE :lastname';
	$sql .= ' AND p2.Firstname LIKE :firstname';
	$sql .= ' AND p2.Mobile LIKE :mobile';
	$sql .= ' AND p2.Email LIKE :email';
	$sql .= ' ORDER BY p2.Participant_ID';
	$sql .= ' LIMIT :offset, :rec_limit';
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':participant_id', $participant_id);
	$stmt->bindValue(':title_id', $title_id);
	$stmt->bindValue(':lastname', '%' . $lastname . '%');
	$stmt->bindValue(':firstname', '%' . $firstname . '%');
	$stmt->bindValue(':mobile', '%' . $mobile . '%');
	$stmt->bindValue(':email', '%' . $email . '%');			
	$stmt->bindValue(':offset', (int) trim($offset), PDO::PARAM_INT);
	$stmt->bindValue(':rec_limit', (int) trim($rec_limit), PDO::PARAM_INT);										
	$stmt->execute();	
	$found_rows = $dbo->query('SELECT FOUND_ROWS()')->fetchColumn(0);									
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($participant_id=='%') {
		$participant_id="";
	}
	if ($title_id=='%') {
		$title_id="";
	}
	if ($lastname=='%') {
		$lastname="";
	}
	if ($firstname=='%') {
		$firstname="";
	}
	if ($mobile=='%') {
		$mobile="";
	}
	if ($email=='%') {
		$email="";
	}
	
	$filter_row = "Filter (Participant ID=" . $participant_id . ", Title=" . $event_id . ", Last Name=" . $lastname . ", First Name=" . $firstname . ", Mobile=" . $mobile . ", Email=" . $email . ")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Record/s: " . $found_rows;
	
	if ($row_count==0) {
		$appointment_row = "<tr><td colspan='10'>No participant</td></tr>";	
	} else {	
		foreach ($result as $row){				
			//$participant_id_row = "<tr><td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "'>" . $row['Participant_ID'] . "</a></td>";		
			$participant_id_row = "<tr><td>" . $row['Participant_ID'] . "</td>";	
			$title_row = "<td>" . $row['Title'] . "</td>";
			$lastname_row = "<td>" . htmlspecialchars($row['Lastname']) . "</td>";
			$firstname_row = "<td>" . htmlspecialchars($row['Firstname']) . "</td>";
			$mobile_row = "<td>" . $row['Mobile'] . "</td>";
			$email_row = "<td>" . htmlspecialchars($row['Email']) . "</td>";
			//$address_row = "<td>" . $row['Address1'] . "</td>";
			$postcode_row = "<td>" . $row['Postcode'] . "</td>";
			
			//Calculate Incentive
			$total_incentive=0;
			$incentive_paid=0;
			$incentive_balance=0;
			
			// Get Lottery Incentive
			$stmt2 = $dbo->prepare('SELECT * FROM lottery WHERE Participant_ID=?');
			$stmt2->execute(array($row['Participant_ID']));
			$row_count2 = $stmt2->rowCount();
			$result2 = $stmt2->fetchAll();		
			if ($row_count2==0) {
			} 
			else {
				foreach ($result2 as $row2){
					$total_incentive=$total_incentive+500;
				}	
			}
			
			$stmt2 = $dbo->prepare('SELECT * FROM log_incentive WHERE Participant_ID=?');
			$stmt2->execute(array($row['Participant_ID']));
			$row_count2 = $stmt2->rowCount();
			$result2 = $stmt2->fetchAll();
			
			if ($row_count2==0) {
				$incentive_balance='<td>' . '-' . '</td>';
			} 
			else {
				foreach ($result2 as $row2){
					$total_incentive=$total_incentive+$row2['Total_Incentive'];
					$incentive_paid=$incentive_paid+$row2['Incentive_Paid'];
				}
			}
			$incentive_balance='<td>S$' . number_format(($total_incentive-$incentive_paid),2) . '</td>';
									
			//For note field with hover tip
			if ($row['PNote'] != null) {
				$pnote_row = "<td><div style='cursor:help;clear:both;' title='" . str_replace( "'","&#39;",$row['PNote']) . "'>&#10004;</div></td>";	
			} else {
				$pnote_row = "<td></td>";
			}
			$pstatus_row = "<td>" . $row['PStatus'] . "</td>";
			$link_edit = "<td><a href='comfort_participant_edit.php?participant_id=" . $row['Participant_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a> ";
			
			if (!isset($_SESSION['accesslvl']) || $_SESSION['accesslvl'] >= 3) { //Display Delete function if user access level is 3 or higher
				$link_delete = "<a href='participant_delete.php?participant_id=" . $row['Participant_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Del</a>";
			} else {
				$link_delete = "";
			}
			
			$participant_row = $participant_row . $participant_id_row . $title_row . $lastname_row . $firstname_row . $mobile_row . $email_row . $incentive_balance . $pnote_row . $pstatus_row . $link_edit . $link_delete . "</td></tr>";			
		}									
	}	
	
	//Pagination ------------------------------------------------
	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$title_id = (empty($_GET['title_id'])) ? '%' : $_GET['title_id']; 
	$firstname = (empty($_GET['firstname'])) ? '%' : $_GET['firstname']; 
	$lastname = (empty($_GET['lastname'])) ? '%' : $_GET['lastname']; 
	$mobile = (empty($_GET['mobile'])) ? '%' : $_GET['mobile']; 
	$email = (empty($_GET['email'])) ? '%' : $_GET['email']; 
	
	$sql = 'SELECT p.Participant_ID, t.Title, p.Title_ID, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote, s1.PStatus 
			FROM participants AS p 
                        LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID
                        LEFT JOIN log_pstatus AS s1 on p.Participant_ID = s1.Participant_ID
                        LEFT OUTER JOIN log_pstatus AS s2 on (p.Participant_ID = s2.Participant_ID AND s1.PStatus_ID < s2.PStatus_ID)
			WHERE s2.PStatus_ID IS NULL';					
	$sql .= ' AND p.Participant_id LIKE :participant_id';
	$sql .= ' AND p.Title_ID LIKE :title_id';
	$sql .= ' AND p.Lastname LIKE :lastname';
	$sql .= ' AND p.Firstname LIKE :firstname';
	$sql .= ' AND p.Mobile LIKE :mobile';
	$sql .= ' AND p.Email LIKE :email';
	$sql .= ' ORDER BY p.Participant_ID';
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':participant_id', $participant_id);
	$stmt->bindValue(':title_id', $title_id);
	$stmt->bindValue(':lastname', '%' . $lastname . '%');
	$stmt->bindValue(':firstname', '%' . $firstname . '%');
	$stmt->bindValue(':mobile', '%' . $mobile . '%');
	$stmt->bindValue(':email', '%' . $email . '%');												
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
		$pagination_row = "";
	} 
	else {	
		$total_pages = ceil($row_count/$rec_limit);
		for ($i=1; $i<=$total_pages; $i++) { 
			if ($i==1 && empty($_GET['page'])) {
				$pagination_row = $pagination_row . "<strong>".$i."</strong>&nbsp;"; 
			}
			elseif ($i==$_GET['page']) {
				$pagination_row = $pagination_row . "<strong>".$i."</strong>&nbsp;"; 
			}
			else {
				$pagination_row = $pagination_row . "<a href='participant_list.php?participant_id=".$participant_id.
									"&title_id=".$title_id.
									"&lastname=".$lastname.
									"&firstname=".$firstname.
									"&mobile=".$mobile.
									"&email=".$email.
									"&page=".$i."&rec_limit=".$rec_limit."'>".$i."</a>&nbsp;"; 
			}
		}
	}
	//Pagination ------------------------------------------------			
	$dbo = null; //Close DB connection						
?>

	<h1 class='title'>Participant</h1>
	<p class='title'>List</p>

		<table class='searchfilters'>
			<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="get">
			<tr><th align='left' colspan=8>
				Search Filters
			</td></tr>
			<tr>				
				<td align="right" width='11%'>Participant ID:</td>
				<td align="left" width='18%'>
					<input name="participant_id" type="text" size="10" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $_GET['participant_id'] ?>"></input>
				</td>
				<td align='right' width='7%'>Title:</td>
				<td align='left' width='15%'>
					<select name="title_id"><?php echo $title_id_row; ?></select>
				</td>
				<td align="right" width='11%'>Last Name:</td>
				<td align="left" width='18%'>
					<input name="lastname" type="text" id="lastname" size="20" value="<?php echo isset($_POST['lastname']) ? $_POST['lastname'] : $_GET['lastname'] ?>"></input>
				</td>
				<td align="right" width='11%'>First Name:</td>
				<td align="left" width='18%'>
					<input name="firstname" type="text" id="firstname" size="20" value="<?php echo isset($_POST['firstname']) ? $_POST['firstname'] : $_GET['firstname'] ?>"></input>
				</td>
			</tr>
			<tr>					
				<td align="right">Mobile:</td>
				<td align="left">
					<input name="mobile" type="text" id="mobile" size="10" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : $_GET['mobile'] ?>"></input>
				</td>
				<td align="right">Email:</td>
				<td align="left">
					<input name="email" type="text" id="email" size="20" value="<?php echo isset($_POST['email']) ? $_POST['email'] : $_GET['email'] ?>"></input>
				</td>
				<td align="right" colspan=3>
				</td>
				<td align="right">
					<table>
						<tr>
							<td>
								<input name="filter" type="hidden" value="1"></input>
								<input type="submit" value="Filter"></input>
								</form>
							</td>
							<td>
								<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
								<input name="clearall" type="hidden" value="1"></input>
								<input type="submit" value="Clear All"></input>
							</form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table><br>
				
	<table class='wborder' width='960px'>
		<tr>
			<th colspan='10'>
				<?php echo $filter_row ?>
			</th></tr>
		<tr>
			<th width='3%'>PID</th>									
			<th width='6%'>Title</th>
			<th>Last Name</th>		
			<th>First Name</th>	
			<th>Mobile</th>	
			<th>Email</th>	
			<th width='9%'>Balance</th>		
			<th width='5%'>PNote</th>
			<th>PStatus</th>	
			<th width='6%'></th>		
		</tr>		
		<?php echo $participant_row; ?>
	</table>				
	<table width='960px'>
		<tr>
			<td align='left'>	
				<?php echo $pagination_row; ?> 
			</td>
			<td align='right'><a href='comfort_participant_list.php?participant_id=<?php echo $participant_id; ?>&title_id=<?php echo $title_id; ?>&lastname=<?php echo $lastname; ?>&firstname=<?php echo $firstname; ?>&mobile=<?php echo $mobile; ?>&email=<?php echo $email; ?>&filter=1&page=1&rec_limit=99999'>Show All</a>
			</td>
		</tr>
	</table>		
	<br><br>		
	

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>