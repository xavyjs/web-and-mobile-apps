<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;
		$selcheckbox = $_POST['selcheckbox'];
		$event_id=$_POST['event_id'];
		$fitbit_start_date=date('Y-m-d',strtotime($_POST['fitbit_start_date']));
		if ($_POST['fitbit_start_date']=="") {
			$fitbit_start_date = NULL;
		} 
		$appointment_date=date('Y-m-d',strtotime($_POST['appointment_date']));
		$location_id=$_POST['location_id'];
		$apptstatus_id=1; //Status as Booked
		$numofparticipant=$_POST['numofparticipant'];
		
		foreach($selcheckbox as $value) {
		  $errormsg_selcheckbox=$errormsg_selcheckbox.'<input type="hidden" name="selcheckbox[]" value="'. $value. '">';
		}
		
		if ($_POST['appointment_date']=="") {
			$error = true;
			$errormsg = $errormsg . "Appointment Date cannot be empty<br>";
		} 
		
		if ($_POST['fitbit_start_date']>$_POST['appointment_date']) {
			$error = true;
			$errormsg = $errormsg . "Fitbit Start Date cannot be later than Appointment Date<br>";
		} 
		
		if ($_POST['numofparticipant']>50) {
			$error = true;
			$errormsg = $errormsg . "The limit for batch create appointments is 50 participants<br>";
		} 
		
		$i=1; 
		while($i<=$numofparticipant) {
			$appointment_time=date('H:i:s',strtotime($_POST['appointment_time']));
			${'appointment_time'.$i}=date('H:i:s',strtotime($_POST['appointment_time'.$i]));
			//echo "Time: " . ${'appointment_time'.$i} . "<br>";
			
			$errormsg_appointment_time=$errormsg_appointment_time."<input name='appointment_time".$i."' type='hidden' size='20' value='" . $_POST['appointment_time'.$i] . "'></input>";
			
			if ($_POST['appointment_time'.$i]=="") {
				$error = true;
				$errormsg = $errormsg . "Appointment Time #" . $i . " cannot be empty<br>";
			} 
			$i++;
		} 
		
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='appointment_newbatch' method='post'>
					".$errormsg_selcheckbox."
					<input name='event_id' type='hidden' size='20' value='" . $event_id . "'></input>
					<input name='fitbit_start_date' type='hidden' size='20' value='" . $_POST['fitbit_start_date'] . "'></input>
					<input name='appointment_date' type='hidden' size='20' value='" . $_POST['appointment_date'] . "'></input>
					".$errormsg_appointment_time."
					<input name='location_id' type='hidden' size='20' value='" . $location_id . "'></input>
					<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
					<div align='right'><input type='submit' value='Back'></input></div></form>";
		} else {					
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database	
			
			$j=0;
			foreach ($selcheckbox as $participant_id) {
				$j++;
				$stmt = $dbo->prepare('SELECT MAX(Appointment_ID) AS Appointment_ID, ApptStatus_ID FROM appointments WHERE Participant_ID=:participant_id AND event_id=:event_id HAVING Appointment_ID IS NOT NULL'); //Update prev appointment status to Rescheduled if status = Booked or Confirmed
				$stmt->execute(array('participant_id' => $participant_id, 'event_id' => $event_id));
				$row_count = $stmt->rowCount();
				$result = $stmt->fetchAll();

				if ($row_count==0) {
					if ($event_id==2) {
						$stmt2 = $dbo->prepare("INSERT INTO appointments(Participant_ID,Event_ID,Appointment_Date,Appointment_Time,Location_ID,ApptStatus_ID) 					VALUES(:participant_id,:event_id,:appointment_date,:appointment_time,:location_id,:apptstatus_id)");
						$stmt2->execute(array(':participant_id' => $participant_id, ':event_id' => 1, ':appointment_date' => date('Y-m-d',strtotime($appointment_date . ' -3 Weekdays')), ':appointment_time' => ${'appointment_time'.$j}, ':location_id' => 2, ':apptstatus_id' => 1));
					}
				} 
				else {
					foreach ($result as $row){
						if ($row['ApptStatus_ID'] == 1 || $row['ApptStatus_ID'] == 2) {
							$stmt2 = $dbo->prepare("UPDATE appointments SET ApptStatus_ID=5 WHERE Appointment_ID=?");
							$stmt2->execute(array($row['Appointment_ID']));		
						}
					}
				}
				
				$stmt = $dbo->prepare("INSERT INTO appointments(Participant_ID,Event_ID,Fitbit_Start_Date,Appointment_Date,Appointment_Time,Location_ID,ApptStatus_ID) 					VALUES(:participant_id,:event_id,:fitbit_start_date,:appointment_date,:appointment_time,:location_id,:apptstatus_id)");
				$stmt->execute(array(':participant_id' => $participant_id, ':event_id' => $event_id, ':fitbit_start_date' => $fitbit_start_date, ':appointment_date' => $appointment_date, ':appointment_time' => ${'appointment_time'.$j}, ':location_id' => $location_id, ':apptstatus_id' => $apptstatus_id));
				print "Appointment (ID: " . $dbo->lastInsertId() . ") created<br><br>";
			}				
				print "<a href='participant_select.php'>Back to Participant Batch page</a>";							

			$dbo = null; //Close DB connection
		}						
	} else {
		header("location:participant_select.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 