<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Personal Details Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT t.Title, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote FROM participants AS p LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID WHERE p.Participant_ID=:participant_id');
	$stmt->execute(array('participant_id' => $_REQUEST['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "<input name='name' size='80' type='text' disabled='disabled' value='No participant selected'></input>";
	} 
	else {
		foreach ($result as $row){
			$title = $row['Title'];
			$lastname = $row['Lastname'];
			$firstname = $row['Firstname'];
			$mobile = $row['Mobile'];
			$email = $row['Email'];
			$address1 = $row['Address1'];
			$postcode = $row['Postcode'];
			$pnote = $row['PNote'];
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Screener Questionnaire Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM screener WHERE Participant_ID=:participant_id ORDER BY Screener_ID');
	$stmt->execute(array('participant_id' => $_REQUEST['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$screener_id = $row['Screener_ID'];
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/Eligibility.php");
			$eligibility = cal_eligibility($row['Screener_ID']);
			$eligibility_outcome = $eligibility[0];
			$eligibility_reason = $eligibility[1];
			$eligibility_timestamp = date("d-m-Y H:i", strtotime($row['Timestamp']));
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Sealed Fitbit Pick-up (Event 1) Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	$stmt = $dbo->prepare('SELECT l.Location, s.ApptStatus, a.* FROM appointments AS a 
							LEFT JOIN ctbl_apptstatus AS s ON a.ApptStatus_id = s.ApptStatus_ID 
							LEFT JOIN ctbl_location AS l on a.Location_ID = l.Location_ID 
							WHERE a.Participant_ID=:participant_id AND a.Event_ID=:event_id 
							ORDER BY Appointment_ID');
	$stmt->execute(array('participant_id' => $_REQUEST['participant_id'], 'event_id' => "1"));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		//$event1_date = "<a href='appointment_new.php?participant_id=" . $_REQUEST['participant_id'] . "&event_id=1'>Make appointment</a>";
	} 
	else {
		foreach ($result as $row){
			$event1_id = $row['Appointment_ID'];
			$event1_date = date("d M Y, D", strtotime($row['Appointment_Date']));
			$event1_time = date("h:i A", strtotime($row['Appointment_Time']));
			$event1_location = $row['Location'];
			$event1_status = $row['ApptStatus'];
			$event1_reply = $row['Reply_chkbox'];
			$event1_note = $row['Note'];
		}
	}
	if ($event1_id <> "" || $event1_id <> NULL) {
		$event1_edit = "&nbsp;<a href='appointment_edit.php?appointment_id=" . $event1_id . "'>Edit</a>";
	}
	if ($event1_reply==1) {
		$event1_reply_chkbox = "&#10004;"; 
	}	
	$dbo = null; //Close DB connection
	
	//SMS Log records
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	$stmt = $dbo->prepare('SELECT * FROM log_sms WHERE Appointment_ID=:event1_id ORDER BY log_SMS_ID');
	$stmt->execute(array('event1_id' => $event1_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$event1_sms = $event1_sms . date("dmYHi", strtotime($row['Timestamp'])) . " ";
		}
	}
	$event1_sms = rtrim($event1_sms,' '); //Remove comma and space
	$dbo = null; //Close DB connection
?>
			
	<h1 class='title'>Participant Information</h1>
	<p class='title'>Eligibility, Appointments, Incentives etc</p>
	<?php //echo $event1_sms; //To test variable ?>
	<table class='participantinfo'>
		<tr>
			<th align='left' bgcolor="#cdcdcd" colspan='3'>Personal Details</th>
			<td align='right' bgcolor="#cdcdcd" style='border:none;'><a href="participant_edit.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>">Edit</a></td>
		</tr>
		<tr>
			<td align='right' width='12%' bgcolor="#cdcdcd">Participant ID:</td>
			<td align='left' width='18%'>
				<?php echo isset($_REQUEST['participant_id']) ? $_REQUEST['participant_id'] : '' ?>
				<input name="participant_id" size="15" type="hidden" value="<?php echo isset($_REQUEST['participant_id']) ? $_REQUEST['participant_id'] : '' ?>"></input>
			</td>
			<td align='right' width='12%' bgcolor="#cdcdcd">Name:</td>
			<td align='left' width='63%'>							
				<?php echo $title . ' ' . $lastname . ' ' . $firstname; ?>
			</td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">Mobile No.:</td>
			<td align='left'>
				<?php echo $mobile; ?>
			</td>
			<td align='right' bgcolor="#cdcdcd">Email:</td>
			<td align='left'>							
				<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
			</td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">Postal Code:</td>
			<td align='left'>
				<?php echo $postcode; ?>
			</td>
			<td align='right' bgcolor="#cdcdcd">Address:</td>
			<td align='left'>							
				<?php echo $address1; ?>
			</td>
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">PNote:</td>
			<td align='left' colspan='3'>
				<?php echo $pnote; ?>
			</td>
		</tr>	
	</table>
	<br><br>
	<table class='participantinfo'>
		<tr>
			<th align='left' bgcolor="#cdcdcd" colspan='6'>Most Recent Screener Questionnaire</th>
			<td align='right' bgcolor="#cdcdcd" style='border:none;'>
				<a href="screener_history.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>">View history</a>
				&nbsp;<a href="screener_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='5%' bgcolor="#cdcdcd">ID:</td>
			<td align='left' width='5%'>
				<?php echo $screener_id; ?>
			</td>
			<td align='left' width='17%'>							
				<?php echo $eligibility_outcome; ?>
			</td>
			<td align='right' width='10%' bgcolor="#cdcdcd">Reason/s:</td>
			<td align='left' width='36%'>							
				<?php echo $eligibility_reason; ?>
			</td>
			<td align='right' width='10%' bgcolor="#cdcdcd">Timestamp:</td>
			<td align='left' width='17%'>							
				<?php echo $eligibility_timestamp; ?>
			</td>
		</tr>	
	</table>
	<br><br>
	<table class='participantinfo'>
		<tr>
			<th align='left' bgcolor="#cdcdcd" colspan='3'>Sealed Fitbit Pick-up</th>
			<td align='right' bgcolor="#cdcdcd" style='border:none;' colspan='5'>
				<a href="appointment_history.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=1">View History</a>
				&nbsp;<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=1">New</a>
				<?php echo $event1_edit; ?></td>
		</tr>
		<tr>
			<td align='right' width='10%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='15%'>
				<?php echo $event1_date; ?>
			</td>
			<td align='right' width='10%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='15%'>							
				<?php echo $event1_time; ?>
			</td>
			<td align='right' width='10%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='15%'>							
				<?php echo $event1_location; ?>
			</td>
			<td align='right' width='10%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='15%'>
				<?php echo $event1_status; ?>
			</td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS:</td>
			<td align='left' colspan='5'>							
				<?php echo $event1_sms; ?>
			</td>					
			<td align='right' width='10%' bgcolor="#cdcdcd">Reply?:</td>
			<td align='left'>							
				<?php echo $event1_reply_chkbox; ?>
			</td>
			
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='7'>
				<?php echo $event1_note; ?>
			</td>
		</tr>
	</table>
			

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
