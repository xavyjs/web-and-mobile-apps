<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>

<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT p.Participant_ID, s.Steps_Number, s.Steps_Date FROM log_steps AS s INNER JOIN participants AS p ON s.Fitbit_User_ID = p.Fitbit_User_ID ORDER BY Participant_ID, Steps_Date');
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$dbo = null; //Close DB connection

	include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/OutputCSV.php");
	download_send_headers("export_steps_" . date("Ymd-His") . ".csv");
	echo outputcsv($result);
	die();
?>		

<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 