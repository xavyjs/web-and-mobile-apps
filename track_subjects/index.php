<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

			<h1 class='title'>Calendar</h1>
			<p class='title'>Upcoming appointments<p>
				<a href='appointment_toupdate.php' class="classname">Appointment to be Updated</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href='appointment_upcoming.php' class="classname">Upcoming Appointment</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href='appointment_toschedule.php' class="classname">Appointment to be Scheduled</a>
				<br><br><br>
				<?php header("location:dashboard.php?year=".date('Y')."&month=".date('m')."&day=".date('d')); ?>
				

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 