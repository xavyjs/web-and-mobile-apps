<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<script type="text/javascript">
function save_check() {
    if(document.forms["screener_new"]["weight"].value == "" || document.forms["screener_new"]["bodyfat"].value == "")
        var r=confirm("Weight or Body Fat field is empty. Continue?");
		if (r==true) {
		  return true;
		}
		else {
		  return false;
		}
}
</script>

<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	$stmt = $dbo->prepare('SELECT t.Title, p.Lastname, p.Firstname FROM participants AS p LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID WHERE p.Participant_ID=:participant_id');
	$stmt->execute(array('participant_id' => $_GET['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		$name = "<input name='name' size='80' type='text' disabled='disabled' value='No participant selected'></input>";
	} 
	else {
		foreach ($result as $row){
			$name = "<input name='name' size='80' type='text' disabled='disabled' value='" . $row['Title'] . " " . $row['Lastname'] . " " . $row['Firstname'] . "'></input>";
		}
	}		
	
	$stmt = $dbo->prepare('SELECT * FROM screener WHERE Screener_ID=:screener_id');
	$stmt->execute(array('screener_id' => $_GET['screener_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$q2 = $row['Q2'];
			$q3 = $row['Q3'];
			$q4 = $row['Q4'];
			$q5 = $row['Q5'];
			$q6 = $row['Q6'];
			$q7 = $row['Q7'];
			$q8_1 = $row['Q8_1'];
			$q8_2 = $row['Q8_2'];
			$q8_3 = $row['Q8_3'];
			$q8_4 = $row['Q8_4'];			
			$q9 = $row['Q9'];
			$q10 = $row['Q10'];
			$q11 = $row['Q11'];
			$q12 = $row['Q12'];
			$q13 = $row['Q13'];
		}
	}							
	$dbo = null; //Close DB connection
?>
			
			<h1 class='title'>Screener Questionnaire</h1>
			<p class='title'>Add New (All questions are mandatory)<p>

			<table cellpadding='3px'>
				<tr>
					<td align='left' width='3%'>1.</td>
					<td align='right' width='12%'>Participant ID:</td>
					<td align='left' width='10%'>
						<input name="participant_id2" size="5" type="text" disabled="disabled" value="<?php echo isset($_REQUEST['participant_id']) ? $_REQUEST['participant_id'] : '' ?>"></input>
						<input name="participant_id" size="5" type="hidden" value="<?php echo isset($_REQUEST['participant_id']) ? $_REQUEST['participant_id'] : '' ?>"></input>
					</td>
					<td align='right' width='10%'>Name:</td>
					<td align='left' width='65%'>							
						<?php echo $name; ?>	
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>2.</td>
					<td align='left' colspan='4'>How old were you at your last birthday?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'><input name="Q2" size="5" type="text" disabled="disabled" value="<?php echo isset($q2) ? $q2 : '' ?>"></input> years old</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>3.</td>
					<td align='left' colspan='4'>Do you drive at least 5 days a week, and at least 8 hours each day?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="radio" disabled="disabled" name="Q3" value="1" <?php echo $q3 == '1' ? "checked" : '' ?>>Yes<br>
						<input type="radio" disabled="disabled" name="Q3" value="2" <?php echo $q3 == '2' ? "checked" : '' ?>>No
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>4.</td>
					<td align='left' colspan='4'>Which best describes your driving status?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="radio" disabled="disabled" name="Q4" value="1" <?php echo $q4 == '1' ? "checked" : '' ?>>Main driver with no <b><u>relief</u></b> driver<br>
						<input type="radio" disabled="disabled" name="Q4" value="2" <?php echo $q4 == '2' ? "checked" : '' ?>>Main driver with one(1) <b><u>relief</u></b> driver<br>
						<input type="radio" disabled="disabled" name="Q4" value="2" <?php echo $q4 == '3' ? "checked" : '' ?>>Main driver with more than one (>1) <b><u>relief</u></b> driver<br>
						<input type="radio" disabled="disabled" name="Q4" value="3" <?php echo $q4 == '4' ? "checked" : '' ?>>Relief driver
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>5.</td>
					<td align='left' colspan='4'>Are you able to walk for 10 minutes without stopping?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="radio" disabled="disabled" name="Q5" value="1" <?php echo $q5 == '1' ? "checked" : '' ?>>Yes<br>
						<input type="radio" disabled="disabled" name="Q5" value="2" <?php echo $q5 == '2' ? "checked" : '' ?>>No
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>6.</td>
					<td align='left' colspan='4'>Are you able to walk up 10 stairs without stopping?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="radio" disabled="disabled" name="Q6" value="1" <?php echo $q6 == '1' ? "checked" : '' ?>>Yes<br>
						<input type="radio" disabled="disabled" name="Q6" value="2" <?php echo $q6 == '2' ? "checked" : '' ?>>No
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>7.</td>
					<td align='left' colspan='4'>Has anyone in your immediate family (mother, father, sister or brother) had a heart attack or died suddenly of a heart related disorder before age 55(men) or 65(women)?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="radio" disabled="disabled" name="Q7" value="1" <?php echo $q7 == '1' ? "checked" : '' ?>>Yes<br>
						<input type="radio" disabled="disabled" name="Q7" value="2" <?php echo $q7 == '2' ? "checked" : '' ?>>No
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>8.</td>
					<td align='left' colspan='4'>Has your doctor informed you that you have any of these conditions? <i>(check all that apply)</i></td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="checkbox" disabled="disabled" name="Q8[1]" value="1" <?php echo $q8_1 == '1' ? "checked" : '' ?>>Heart condition or disease (also include any type of heart surgery)<br>
						<input type="checkbox" disabled="disabled" name="Q8[2]" value="2" <?php echo $q8_2 == '1' ? "checked" : '' ?>>Stroke<br>
						<input type="checkbox" disabled="disabled" name="Q8[3]" value="3" <?php echo $q8_3 == '1' ? "checked" : '' ?>>Lung Disease (e.g. chronic obstructive pulmonary disease/COPD or asthma)<br>
						<input type="checkbox" disabled="disabled" name="Q8[4]" value="4" <?php echo $q8_4 == '1' ? "checked" : '' ?>>Diabetes
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>9.</td>
					<td align='left' colspan='4'>In the past 1 year, have you had chest pain when you engage in physical activity or when at rest?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="radio" disabled="disabled" name="Q9" value="1" <?php echo $q9 == '1' ? "checked" : '' ?>>Yes<br>
						<input type="radio" disabled="disabled" name="Q9" value="2" <?php echo $q9 == '2' ? "checked" : '' ?>>No
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>10.</td>
					<td align='left' colspan='4'>Do you ever experience dizziness or even lose consciousness?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="radio" disabled="disabled" name="Q10" value="1" <?php echo $q10 == '1' ? "checked" : '' ?>>Yes<br>
						<input type="radio" disabled="disabled" name="Q10" value="2" <?php echo $q10 == '2' ? "checked" : '' ?>>No
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>11.</td>
					<td align='left' colspan='4'>Do you have any bone, joint or muscle problem (e.g. back, knee, hip, shoulder or ankle) that could be made worse by participating in exercise?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="radio" disabled="disabled" name="Q11" value="1" <?php echo $q11 == '1' ? "checked" : '' ?>>Yes<br>
						<input type="radio" disabled="disabled" name="Q11" value="2" <?php echo $q11 == '2' ? "checked" : '' ?>>No
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>12.</td>
					<td align='left' colspan='4'>Do you take tablets for high blood pressure and either continue to have poorly controlled high blood pressure, or do you not follow up with a doctor on a regular basis?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="radio" disabled="disabled" name="Q12" value="1" <?php echo $q12 == '1' ? "checked" : '' ?>>Yes<br>
						<input type="radio" disabled="disabled" name="Q12" value="2" <?php echo $q12 == '2' ? "checked" : '' ?>>No
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td align='left'>13.</td>
					<td align='left' colspan='4'>Do you know of any reason why participating in this HPB exercise programme or any other physical activity might be harmful to your health?</td>
				<tr>
					<td align='left'></td>
					<td align='left' colspan='4'>
						<input type="radio" disabled="disabled" name="Q13" value="1" <?php echo $Q13 == '1' ? "checked" : '' ?>>Yes<br>
						<input type="radio" disabled="disabled" name="Q13" value="2" <?php echo $Q13 == '2' ? "checked" : '' ?>>No
					</td>
				</tr>
				<tr><td><br></td></tr>
			</table>
			
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
