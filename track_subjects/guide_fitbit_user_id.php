<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<h1 class='title'>Guide</h1>	
<p class='title'>How to get Fitbit User ID</p>

<u>Step 1:</u><br>
Log in @ <a href="http://www.fitbit.com/" target="_blank">Fitbit Official Website</a>.<br><br>

<u>Step 2:</u><br>
<img src="images/guide_how_to_get_fitbit_user_id_001.png" width="640px"><br><br>

<u>Step 3:</u><br>
Copy the Fitbit User ID: <br>
<img src="images/guide_how_to_get_fitbit_user_id_002.png" width="640px"><br><br>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>