<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>



<?php //Reply from unknown number that system has not send a message to will not be captured

	//test url: http://taksi-study.com/sms_reply?mobile=6597375968&message=Yes&mtid=1309110030845
	
	// file_put_contents("log/SMS.log",date("dmYHi")."| reply url: ".$_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI]."\r\n", FILE_APPEND);
	

	$error = false;			
	$mobile=ltrim($_GET['mobile'],'65');
	$message=$_GET['message'];
	$mt_id=$_GET['mtid'];	
	$timestamp=date("Y-m-d H:i:s");
	
	if ($message=="") {
		$error = true;
	} 
	
	if ($error === true) {
	} else {					
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
		
		$stmt = $dbo->prepare('SELECT Participant_ID, Appointment_ID, SMS_ID FROM log_sms WHERE MT_ID=:mt_id');
		$stmt->execute(array('mt_id' => $mt_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		if ($row_count==0) {
		} 
		else {
			foreach ($result as $row){
				$participant_id=$row['Participant_ID'];
				$appointment_id=$row['Appointment_ID'];
				$sms_id=$row['SMS_ID'];
			}
		}
		
		if ($participant_id) {
			// Write SMS Reply information to log_sms_reply	
			$stmt = $dbo->prepare("INSERT INTO log_sms_reply(MT_ID,Participant_ID,Appointment_ID,Mobile,Message,Timestamp) VALUES (:mt_id,:participant_id,:appointment_id,:mobile,:message,:timestamp)");
			$stmt->execute(array(':mt_id' => $mt_id, ':participant_id' => $participant_id, ':appointment_id' => $appointment_id, ':mobile' => $mobile, ':message' => $message, ':timestamp' => $timestamp));
			$sms_id_row = $sms_id_row . $dbo->lastInsertId() . ", ";	
			
			// file_put_contents("log/SMS.log",date("dmYHi")."| existing mt id: ".$mobile."\r\n", FILE_APPEND);
		} 
		else {
		
			$stmt = $dbo->prepare('SELECT Participant_AID FROM participants WHERE Mobile=:mobile');
			$stmt->execute(array('mobile' => $mobile));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			
			if ($row_count==0) {
				// Write SMS Reply information to log_sms_reply	
				$stmt = $dbo->prepare("INSERT INTO log_sms_reply(MT_ID,Mobile,Message,Timestamp) VALUES (:mt_id,:mobile,:message,:timestamp)");
				$stmt->execute(array(':mt_id' => $mt_id, ':mobile' => $mobile, ':message' => $message, ':timestamp' => $timestamp));
				$sms_id_row = $sms_id_row . $dbo->lastInsertId() . ", ";
				
				// file_put_contents("log/SMS.log",date("dmYHi")."| new number: ".$mobile."\r\n", FILE_APPEND);
			} 
			else {
				foreach ($result as $row){
					$participant_id=$row['Participant_AID'];
					// Write SMS Reply information to log_sms_reply	
					$stmt = $dbo->prepare("INSERT INTO log_sms_reply(MT_ID,Participant_ID,Appointment_ID,Mobile,Message,Timestamp) VALUES (:mt_id,:participant_id,:appointment_id,:mobile,:message,:timestamp)");
					$stmt->execute(array(':mt_id' => $mt_id, ':participant_id' => $participant_id, ':appointment_id' => $appointment_id, ':mobile' => $mobile, ':message' => $message, ':timestamp' => $timestamp));
					$sms_id_row = $sms_id_row . $dbo->lastInsertId() . ", ";
					
					// file_put_contents("log/SMS.log",date("dmYHi")."| existing number: ".$mobile."\r\n", FILE_APPEND);
				}
			}
		}

		// If (strtolower($message)=="yes" && $sms_id==15) {//If reply = "Yes" and SMS_ID=15, participant confirmed to remain in study
			// $apptstatus_id=2;
			// $stmt2 = $dbo->prepare("UPDATE log_sms SET sms_id=? WHERE MT_ID=?"); //Update sms_id to 150 so that cron job will not update status to Dropped out
			// $stmt2->execute(array(151,$mt_id));		
			// $pstatus="Termination SMS sent. Participant confirmed to remain in study";	
			// $todo="Reschedule appointment";
		// } elseif (strtolower($message)=="yes" && ($sms_id==1 || $sms_id==3)) { //If reply = "Yes" and SMS_ID=1 or 3, Update appointment Status to "Confirmed"
			// $apptstatus_id=2;
			// $stmt2 = $dbo->prepare("UPDATE appointments SET ApptStatus_ID=? WHERE Appointment_ID=? AND ApptStatus_ID=1"); //Only update if Status is Booked
			// $stmt2->execute(array($apptstatus_id,$appointment_id));	
			// $stmt3 = $dbo->prepare("UPDATE log_todo SET Dismissed='y' WHERE Appointment_ID=?"); //Only update if Status is Booked
			// $stmt3->execute(array($appointment_id));
		// }
		
		// if (isset($pstatus)) { //if pstatus is not blank, call function and insert pstatus
				// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/PStatus.php");
				// PStatus_Add($participant_id,$pstatus);
			// }
			// if (isset($todo)) { //if todo is not blank, call function and insert todo
				// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/ToDo.php");
				// ToDo_Add($participant_id,$appointment_id,$todo);
		// }	
	}
	Print "SMS (ID: " . rtrim($sms_id_row,', ') . ") saved into log";
	$dbo = null; //Close DB connection		
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 