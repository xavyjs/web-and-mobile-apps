<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php PowerUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

			<h1 class='title'>Edit SMS Template</h1>
			<p class='title'>Edit the content of the SMS to participant<p>
			
			<p style='color:red;'><b>Warning!</b><br>Making changes may have serious consequences to the system process.<br>Proceed with caution.<br><br></p>
			
								
				<?php
				
					include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
					dbConnect(); // Connect to Database
					
					$stmt = $dbo->prepare("SELECT *
												FROM ctbl_sms");
						$stmt->execute();
						$row_count = $stmt->rowCount();
						$result = $stmt->fetchAll();
						
						if ($row_count==0) {
							echo "No SMS template available.";
						} 
						else {		
							echo "<table><tr><td>Select Template: </td>
									<td>
										<form action=" . htmlentities($_SERVER['PHP_SELF']) . " method='get'>
											<select name='sms_id'>";
							foreach ($result as $row){	
								if ($row['SMS_ID']==$_GET['sms_id']) {
									echo "<option value=" . $row['SMS_ID'] . " selected='selected'>" . $row['SMS_ID']  . ". " . htmlspecialchars($row['Template_Name']) . "</option>";	
								}
								else {
									echo "<option value=" . $row['SMS_ID'] . ">" . $row['SMS_ID']  . ". " . htmlspecialchars($row['Template_Name']) . "</option>";	
								}
							}
						echo "</select>&nbsp;&nbsp;&nbsp;<input type='submit' value='Edit'></input></form></td></tr></table><br><br><br>";		
						}							
						
					if ($_SERVER["REQUEST_METHOD"] == "GET") {
						include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
						dbConnect(); // Connect to Database
						
						$sms_id=$_GET['sms_id'];
						
						$stmt = $dbo->prepare("SELECT *
												FROM ctbl_sms 
												WHERE SMS_ID =?");
						$stmt->execute(array($sms_id));
						$row_count = $stmt->rowCount();
						$result = $stmt->fetchAll();
						
						if ($row_count==0) {
						} 
						else {									
							foreach ($result as $row){		
							
								$template_name=$row['Template_Name'];
								$sms_subject=$row['SMS_Subject'];
								
								echo "<form action=" . htmlentities($_SERVER['PHP_SELF']) . " method='post'>
										<table><tr><td valign='top' align='right'>Template: </td><td align='left'>
											<input name='template_name' size='80' type='text' value='" . htmlspecialchars($template_name) . "'></input><br><br>
											</td></tr>";
								echo "<tr><td valign='top' align='right'>Content: </td><td align='left'>
										<textarea name='sms_subject' rows='15' cols='80'>" . htmlspecialchars($sms_subject) . "</textarea><br><br>
									</td></tr><tr><td></td><td align='right'><input name='sms_id' type='hidden' size='20' value='" . $sms_id . "'><input type='submit' value='Save' name='button_post'></input></td></tr></table></form>";
							}
						}
					}	

					if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['button_post'] == "Save") {
						include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
						dbConnect(); // Connect to Database					
						$sms_id=$_POST['sms_id'];
						$template_name=$_POST['template_name'];
						$sms_subject=$_POST['sms_subject'];						
						$stmt = $dbo->prepare("UPDATE ctbl_sms SET Template_Name=?, SMS_Subject=? WHERE SMS_ID=?");
								$stmt->execute(array($template_name,$sms_subject, $sms_id));					
						echo "SMS template updated";						
					}
										
					//Close DB connection
					$dbo = null;					
				?>	

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
