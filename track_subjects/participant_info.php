<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Personal Details Data
	$participant_aid=$_GET['participant_aid'];
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT p.Fitbit_User_ID, p.Fitbit_Device, p.access_token, p.access_token_secret, p.Participant_ID, p.Age, r.Race, g.Gender, p.Mobile, p.Study_Arm_ID, sa.Study_Arm, ps.PStatus, p.Num_Daily_Doses, p.Interval_1to2, p.Interval_2to3, p.Interval_3to4, p.Interval_4to5, p.Interval_5to6, p.Interval_6to7, p.Interval_7to8, p.Baseline_Assessment_Date, p.Month3_Assessment_Date, p.Month6_Assessment_Date, p.Healthcare_Cost_Mth13, p.Healthcare_Cost_Mth46, p.PNote FROM participants AS p 
								LEFT JOIN ctbl_race AS r ON p.Race_ID = r.Race_ID
								LEFT JOIN ctbl_gender AS g ON p.Gender_ID = g.Gender_ID
								LEFT JOIN ctbl_study_arm AS sa ON p.Study_Arm_ID = sa.Study_Arm_ID
								LEFT JOIN ctbl_pstatus AS ps ON p.PStatus_ID = ps.PStatus_ID 
								WHERE p.Participant_AID=:participant_aid');
	$stmt->execute(array('participant_aid' => $participant_aid));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "<input name='name' size='80' type='text' disabled='disabled' value='No participant selected'></input>";
	} 
	else {
		foreach ($result as $row){
			$fitbit_user_id = $row['Fitbit_User_ID'];
			$fitbit_device = $row['Fitbit_Device'];
			$access_token = $row['access_token'];
			$access_token_secret = $row['access_token_secret'];
			$participant_id=$row['Participant_ID'];
			$age = $row['Age'];
			$race = $row['Race'];
			$gender = $row['Gender'];
			$mobile = $row['Mobile'];
			$study_arm_id = $row['Study_Arm_ID'];
			$study_arm = $row['Study_Arm'];
			$pstatus = $row['PStatus'];
			$num_daily_doses = $row['Num_Daily_Doses'];
			$interval_1to2 = $row['Interval_1to2'];
			$interval_2to3 = $row['Interval_2to3'];
			$interval_3to4 = $row['Interval_3to4'];
			$interval_4to5 = $row['Interval_4to5'];
			$interval_5to6 = $row['Interval_5to6'];
			$interval_6to7 = $row['Interval_6to7'];
			$interval_7to8 = $row['Interval_7to8'];
			$baseline_assessment_date = date("d M Y, D", strtotime($row['Baseline_Assessment_Date']));
			$month3_assessment_date = date("d M Y, D", strtotime($row['Month3_Assessment_Date']));
			$month6_assessment_date = date("d M Y, D", strtotime($row['Month6_Assessment_Date']));
			$healthcare_cost_mth13 = $row['Healthcare_Cost_Mth13'];
			$healthcare_cost_mth46 = $row['Healthcare_Cost_Mth46'];
			$pnote = $row['PNote'];
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php 
// Calculate Subsidy
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/Cal_Adherence.php");
	list($mth13_adherence_days, $mth13_adherence_percent, $row_mth13_data, $mth46_adherence_days, $mth46_adherence_percent, $row_mth13_data) = cal_adherence($participant_aid);
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/Cal_Adherence.php");
	list($mth13_subsidy_percent,$mth46_subsidy_percent) = cal_subsidy_percent($participant_aid,$mth13_adherence_percent,$mth46_adherence_percent);
	
?>

<?php //Get Incentive Paid
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT Incentive_Paid FROM log_incentive WHERE Participant_ID=?');
	$stmt->execute(array($_REQUEST['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){	
			$incentive_paid=$incentive_paid+$row['Incentive_Paid'];
		}
	}
	$dbo = null; //Close DB connection
?>


			
	<h1 class='title'>Participant</h1>
	<p class='title'>Information</p>
	<?php //echo $event1_sms; //To test variable ?>
	<table class='participantinfo'>
		<tr>
			<th align='left' colspan='4'>Personal Details</th>
			<th align='right' colspan='4'>
				<a href="sms_list.php?participant_aid=<?php echo $_GET['participant_aid']; ?>&mobile=<?php echo $mobile; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">SMS History</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="send_sms.php?participant_aid=<?php echo $_GET['participant_aid']; ?>&sms_type=auto&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">Send SMS</a>&nbsp;
				<a href="participant_edit.php?participant_aid=<?php echo $_GET['participant_aid']; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">Edit</a>
			</th>
		</tr>
		<tr>
			<td align='right' width='12%' bgcolor="#e9e9e9">Participant ID:</td>
			<td align='left' width='13%'>
				<?php echo $participant_id; ?>
				<input name="participant_aid" size="15" type="hidden" value="<?php echo isset($_GET['participant_aid']) ? $_GET['participant_aid'] : '' ?>"></input>
			</td>
			<td align='right' width='12%' bgcolor="#e9e9e9">Age:</td>
			<td align='left' width='13%'>							
				<?php echo $age; ?>
			</td>		
			<td align='right' width='12%' bgcolor="#e9e9e9">Race:</td>
			<td align='left' width='13%'>							
				<?php echo $race; ?>
			</td>
			<td align='right' width='12%' bgcolor="#e9e9e9">Mobile No.:</td>
			<td align='left' width='13%'>
				<?php echo $mobile; ?>			
			</td>
		</tr>
		<tr>		
			<td align='right' bgcolor="#e9e9e9">Study Arm:</td>
			<td align='left'>							
				<?php echo $study_arm; ?>
			</td>
			<td align='right' bgcolor="#e9e9e9">PStatus:</td>
			<td align='left'>							
				<?php echo $pstatus; ?>
			</td>
			<td align='right' bgcolor="#e9e9e9">D.Doses:</td>
			<td align='left'>					
				<?php echo $num_daily_doses; ?>
			</td>
			<td align='right' bgcolor="#e9e9e9">Intervals:</td>
			<td align='left'>					
				<?php echo $interval_1to2 . ' ' . $interval_2to3 . ' ' . $interval_3to4 . ' ' . $interval_4to5 . ' ' . $interval_5to6 . ' ' . $interval_6to7 . ' ' . $interval_7to8; ?>
			</td>
		</tr>
		<tr>	
			<td align='right' bgcolor="#e9e9e9">Baseline A.Date:</td>
			<td align='left'>							
				<?php echo $baseline_assessment_date; ?>
			</td>
			<td align='right' bgcolor="#e9e9e9">Mth 3 A.Date:</td>
			<td align='left'>							
				<?php echo $month3_assessment_date; ?>
			</td>
			<td align='right' bgcolor="#e9e9e9">Mth 6 A.Date:</td>
			<td align='left'>							
				<?php echo $month6_assessment_date; ?>
			</td>
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#e9e9e9">PNote:</td>
			<td align='left' colspan='7'>
				<?php echo htmlspecialchars($pnote); ?>
			</td>
		</tr>	
	</table>
	<br>
	<table class='participantinfo'>
		<tr>
			<th align='left' colspan='4'>Adherence (Days Monitored = 85)</th>
			<th align='right' colspan='4'>
				<a href="adherence_calculation.php?participant_id=<?php echo $participant_id; ?>&participant_aid=<?php echo $_GET['participant_aid']; ?>&prev_url=<?php echo urlencode($_SERVER['REQUEST_URI']); ?>">Adherence Calculation</a>&nbsp;
				<a href='participant_ecap.php?participant_id=<?php echo $participant_id; ?>&participant_aid=<?php echo $_GET['participant_aid']; ?>&prev_url=<?php echo urlencode($_SERVER['REQUEST_URI']); ?>'>eCAP Doses</a>
			</th>
		</tr>
		<tr>
			<td align='right' width='17%' bgcolor="#e9e9e9">Mth 1-3 Adherent Days:</td>
			<td align='left' width='8%'>
				<?php echo isset($mth13_adherence_days) ? $mth13_adherence_days : ''; ?>
			</td>
			<td align='right' width='17%' bgcolor="#e9e9e9">Mth 1-3 Adherent %:</td>
			<td align='left' width='8%'>							
				<?php echo isset($mth13_adherence_percent) ? $mth13_adherence_percent*100 : ''; ?>
			</td>
			<td align='right' width='17%' bgcolor="#e9e9e9">Mth 4-6 Adherent Days:</td>
			<td align='left' width='8%'>
				<?php echo isset($mth46_adherence_days) ? $mth46_adherence_days : ''; ?>
			</td>
			<td align='right' width='17%' bgcolor="#e9e9e9">Mth 4-6 Adherent %:</td>
			<td align='left' width='8%'>							
				<?php echo isset($mth46_adherence_percent) ? $mth46_adherence_percent*100 : ''; ?>
			</td>
		</tr>	
	</table>
	<br>
	<table class='participantinfo'>
		<tr>
			<th align='left' colspan='4'>Incentives</th>
			<th align='right' colspan='4'>			
			</th>
		</tr>
		<tr>
			<td align='right' width='23%' bgcolor="#e9e9e9">Mth 1-3 Healthcare Cost:</td>
			<td align='left' width='10%'>
				<?php echo isset($healthcare_cost_mth13) ? 'S$'.number_format($healthcare_cost_mth13,2) : ''; ?>
			</td>
			<td align='right' width='23%' bgcolor="#e9e9e9">Mth 1-3 Subsidy %:</td>
			<td align='left' width='10%'>							
				<?php echo $mth13_subsidy_percent*100; ?>
			</td>
			<td align='right' width='23%' bgcolor="#e9e9e9">HMth 1-3 Subsidy Amt:</td>
			<td align='left' width='10%'>							
				<?php echo $mth13_subsidy_percent ? 'S$'.number_format($healthcare_cost_mth13,2)*$mth13_subsidy_percent : ''; ?>
			</td>
		</tr>	
		<tr>
			<td align='right' width='23%' bgcolor="#e9e9e9">Mth 4-6 Healthcare Cost:</td>
			<td align='left' width='10%'>
				<?php echo isset($healthcare_cost_mth46) ? 'S$'.number_format($healthcare_cost_mth46,2) : ''; ?>
			</td>
			<td align='right' width='23%' bgcolor="#e9e9e9">Mth 4-6 Subsidy %:</td>
			<td align='left' width='10%'>							
				<?php echo $mth46_subsidy_percent*100; ?>
			</td>
			<td align='right' width='23%' bgcolor="#e9e9e9">HMth 4-6 Subsidy Amt:</td>
			<td align='left' width='10%'>							
				<?php echo $mth13_subsidy_percent ? 'S$'.number_format($healthcare_cost_mth46,2)*$mth46_subsidy_percent : ''; ?>
			</td>
		</tr>	
	</table>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
