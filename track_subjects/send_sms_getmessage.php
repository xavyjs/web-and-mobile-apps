<?php //Retrieving SMS Templates

	$sms_id = $_GET['sms_id'];

	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT SMS_Subject FROM ctbl_sms WHERE SMS_ID=:sms_id');
	$stmt->execute(array('sms_id' => $sms_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			echo htmlentities($row['SMS_Subject']);
		}
	}						
	$dbo = null; //Close DB connection
?>