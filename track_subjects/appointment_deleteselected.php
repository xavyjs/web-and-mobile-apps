<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php PowerUserCheck(); //Check legitimate session ?>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

			<form action="appointment_deleteselected_process.php" method="post">
						
			<h1 class='title'>Delete selected appointments</h1>
			<p class='title'>Are you sure you want to delete these selected appointments of Participant ID: <?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : ''; ?>?</p>
			
			<table id="tbl_registration">
			<tr>
				<td>
					<?php 
						if(isset($_POST['selcheckbox'])) {
							// foreach(array_keys($_POST['selcheckbox']) as $i) {
								// $del_id = $del_id . $_POST['selcheckbox'][$i].",";
							// }	
							// $del_id = substr($del_id, 0, -1); 
							
							//implode array
							$inQuery = implode(',', array_fill(0, count($_POST['selcheckbox']), '?'));

							include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
							dbConnect(); // Connect to Database
							
							$stmt = $dbo->prepare('SELECT A.Appointment_ID, A.Participant_UID, A.Event_ID, E.Event, E.Event_Week, A.Appointment_Date_Recommended, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.Appointment_Status, S.ApptStatus, A.Weight, A.Weight_chkbox, A.BodyFat, A.BodyFat_chkbox, A.Note
														FROM appointments AS A 
														LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
														LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
														LEFT JOIN ctbl_apptstatus AS S ON A.Appointment_Status = S.ApptStatus_ID 
														WHERE A.Appointment_ID in (' . $inQuery . ')
														ORDER BY A.Event_ID, A.Appointment_Date, A.Appointment_Time');
								// bindvalue is 1-indexed, so $k+1
								$stmt->execute($_POST['selcheckbox']);
								$row_count = $stmt->rowCount();

								$result = $stmt->fetchAll();
								 
								if ($row_count==0) {
								} 
								else {	

									echo "<table class='wborder'>
										<tr style='background-color: #cdcdcd; font-weight: normal;'>";
									echo "<th>Week</th>
											<th>Event</th>
											<th>Recommended Date (dd-mm-yyyy)</th>
											<th>Booked Date (dd-mm-yyyy)</th>
											<th>Time</th>
											<th>Location</th>
											<th>Appt Status</th>
											<th>Weight (kg)</th>
											<th>NM</th>
											<th>Body Fat (%)</th>
											<th>NM</th>
											<th>Note</th>
										</tr>";
								
									foreach ($result as $row){
															
										$appt_id=$row['Appointment_ID'];	
										$appt_event_id=$row['Event_ID'];								
										$appt_event=$row['Event'];
										$appt_event_week=$row['Event_Week'];
										$appt_date_auto=$row['Appointment_Date_Recommended'];																		
										$appt_date=$row['Appointment_Date'];								
										$appt_time=$row['Appointment_Time'];
										$appt_location_id=$row['Location_ID'];
										$appt_location=$row['Location'];
										$Appointment_Status=$row['Appointment_Status'];
										$appt_status=$row['ApptStatus'];
										$weight=$row['Weight'];
										$weight_chkbox=$row['Weight_chkbox'];
										$bodyfat=$row['BodyFat'];
										$bodyfat_chkbox=$row['BodyFat_chkbox'];		
										$note=$row['Note'];

										//--------------------------Add code to determine previous appointment status
										$appt_status2 = "";
										if (!empty($Appointment_Status)) {
											$stmt2 = $dbo->prepare('SELECT A.Appointment_ID, A.Participant_UID, A.Event_ID, A.Appointment_Date, A.Appointment_Time, A.Appointment_Status, S.ApptStatus
															FROM appointments AS A
															LEFT JOIN ctbl_apptstatus AS S ON A.Appointment_Status = S.ApptStatus_ID 
															WHERE A.Participant_UID= :participant_id AND
															A.Appointment_ID!= :appt_id AND															
															A.Event_ID= :appt_event_id AND
															(Appointment_Date < :appt_date OR (Appointment_Date = :appt_date AND Appointment_Time < :appt_time)) AND
															(A.Appointment_Status IS NOT NULL AND A.Appointment_Status!=0)
															ORDER BY A.Event_ID, A.Appointment_Date DESC, A.Appointment_Time DESC');
											$stmt2->execute(array('participant_id' => $participant_id, 'appt_id' => $appt_id, 'appt_event_id' => $appt_event_id, 'appt_date' => $appt_date, 'appt_time' => $appt_time));
											$row_count2 = $stmt2->rowCount();
											$result2 = $stmt2->fetchAll();
											
											if ($row_count2==0) {
											} 
											else {	
												foreach ($result2 as $row2){
													
													$appt_status2=$row2['ApptStatus'];
													$appt_status= $appt_status2[0] . "," . $appt_status;
												}
											}
										}
										//--------------------------Add code to determine previous appointment status
																																								
										if ($appt_date . " " . $appt_time < date('Y-m-d H:i:s') && strpos($appt_status,'M,Missed') !== false) {
											echo "<tr style='background-color: #d95252;'>";
										} 
										elseif ($appt_date < date('Y-m-d')) {
											echo "<tr style='color:#999'>";
										}
										elseif ($appt_date > date('Y-m-d') && $appt_date <= date('Y-m-d', strtotime("+3 days"))) {
											echo "<tr style='background-color: #ffdf85;'>";
										} 
										elseif ($appt_date == date('Y-m-d')) {
											echo "<tr style='background-color: #f29966;'>";
										} 
										else {
											echo "<tr>";
										}
										
										if ($appt_date_auto != null) {
											$appt_date_auto=date("d-m-Y", strtotime($appt_date_auto)); 
										}
										
										if ($appt_date != null) {
											$appt_date=date("d-m-Y", strtotime($appt_date)); 
										}
										
										if ($appt_time != null) {
											$appt_time=date("h:i A", strtotime($appt_time)); 
										}
										
										echo "<td><input name='selcheckbox[]' type='hidden' id='selcheckbox[]' value=" . $appt_id . " checked></input>" . $appt_event_week; 
										echo "</td><td>" . $appt_event; 
										echo "</td><td>" . $appt_date_auto; 
										echo "</td><td>" . $appt_date; 
										echo "</td><td>" . $appt_time; 
										echo "</td><td>" . $appt_location; 
										echo "</td><td>" . $appt_status; 
										echo "</td><td>" . $weight; 
										if ($weight_chkbox==1) {
											echo "</td><td><input name='weight_chkbox' type='checkbox' disabled=disabled' size='20' value='" . $weight_chkbox . "' checked></input>"; 
										} else {
											echo "</td><td><input name='weight_chkbox' type='checkbox' disabled=disabled' size='20' value='" . $weight_chkbox . "'></input>"; 
										}
										echo "</td><td>" . $bodyfat;
										if ($bodyfat_chkbox==1) {
											echo "</td><td><input name='bodyfat_chkbox' type='checkbox' disabled=disabled' size='20' value='" . $bodyfat_chkbox . "' checked></input>"; 
										} else {
											echo "</td><td><input name='bodyfat_chkbox' type='checkbox' disabled=disabled' size='20' value='" . $bodyfat_chkbox . "'></input>"; 
										}
										
										//For note field with hover tip
										echo "</td><td>";
										if ($note != null) {
											echo "<div style='cursor: help;' title='" . $note . "'>&#10004;</div>"; 
										}
										echo "</td></tr>";
							
									}
									echo "</table>";	
								}	
							echo $del_id;
						}
						else {
							echo "No appointment selected";
						}
					?>
				</td>
			</tr>
			<tr>	
				<td>					
					<input name="participant_id" size="20" type="hidden" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : '' ?>"></input><br>
				</td>
			</tr>
			<tr>
				<td>
					<?php 
						if(isset($_POST['selcheckbox'])) {
							echo "<input type='submit' value='Delete'></input>&nbsp;&nbsp;&nbsp;";
						}
					?>		
					<a href="appointment_select.php?participant_id=<?php echo $_POST['participant_id'] ?>">Back to appointment page</a>
				</td>
			</tr>
			</table>
			
			</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>