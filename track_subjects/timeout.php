<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>

<?php 
session_start();
session_destroy();
header("refresh:3;url=login.php?prev_url=" . urlencode($_GET['prev_url'])); //redirect after x time (seconds)
?> 

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header_nomenu.php"); ?>
	<br><br><br>
	You have been logged out because your session has expired.<br><br>
	<a href='login.php?prev_url=<?php echo urlencode($_GET['prev_url']); ?>'>Click here if you are not redirected to the homepage</a>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>		

<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 