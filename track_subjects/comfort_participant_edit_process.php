<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/comfort_header.php"); ?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;						
		$participant_id=$_POST['participant_id'];
		$title_id=$_POST['title_id'];
		$lastname=$_POST['lastname'];
		$firstname=$_POST['firstname'];
		$mobile=$_POST['mobile'];
		$email=$_POST['email'];
		$address1=$_POST['address1'];
		$postcode=$_POST['postcode'];
		$pnote=$_POST['pnote'];
		$daily_rental=$_POST['daily_rental'];
		$prev_url=$_POST['prev_url'];

		if ($lastname=="") {
			$error = true;
			$errormsg = $errormsg . "Last Name cannot be empty<br>";
		} 
		
		if ($firstname=="") {
			$error = true;
			$errormsg = $errormsg . "First Name cannot be empty<br>";
		} 
		
		if ($mobile=="") {
			$error = true;
			$errormsg = $errormsg . "Mobile No cannot be empty<br>";
		} 
		
		if (is_numeric($mobile)===false || strlen($mobile)!=8) {
						$error = true;
						$errormsg = $errormsg . "Mobile No must be 8 numbers<br>";
		} 
		
		// if ($daily_rental=="") {
			// $error = true;
			// $errormsg = $errormsg . "Daily Rental cannot be empty<br>";
		// } 
		
		if ($daily_rental<>"" && is_numeric($daily_rental)===false) {
						$error = true;
						$errormsg = $errormsg . "Daily Rental must be numbers<br>";
		} 
		
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='comfort_participant_edit' method='post'>
					<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
					<input name='title_id' type='hidden' size='20' value='" . $title_id . "'></input>
					<input name='lastname' type='hidden' size='20' value='" . $lastname . "'></input>
					<input name='firstname' type='hidden' size='20' value='" . $firstname . "'></input>
					<input name='mobile' type='hidden' size='20' value='" . $mobile . "'></input>
					<input name='email' type='hidden' size='20' value='" . $email . "'></input>
					<input name='address1' type='hidden' size='20' value='" . $address1 . "'></input>
					<input name='postcode' type='hidden' size='20' value='" . $postcode . "'></input>
					<input name='pnote' type='hidden' size='20' value='" . $pnote . "'></input>
					<input name='daily_rental' type='hidden' size='20' value='" . $daily_rental . "'></input>
					<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
					<input type='submit' value='Back'></input></form>";
		} else {					
		
			// if ($fitbit_user_id=="") {
				// $fitbit_user_id = null;
			// } 
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database		
											
			$stmt = $dbo->prepare("UPDATE participants SET Title_ID=?, Firstname=?, Lastname=?, Mobile=?, Email=?, Address1=?, Postcode=?, PNote=?, Daily_Rental=? WHERE Participant_ID=?");
			$stmt->execute(array($title_id, $firstname, $lastname, $mobile, $email, $address1, $postcode, $pnote, $daily_rental, $participant_id));						
			
			// If ($apptstatus_id==1||$apptstatus_id==4) {
				// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/EmailAppointment.php");
				// $email_id = 3; //Template 3: For updated appointment
				// EmailAppointment($participant_id,$event_id,$appointment_date,$appointment_time,$location_id,$email_id);
			// }	
			
			$dbo = null; //Close DB connection			
			header("location:" . $prev_url);							
		}
	} else {
		header("location:comfort_participant_edit.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 