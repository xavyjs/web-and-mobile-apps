<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

		<?php
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
					dbConnect(); // Connect to Database
					
					$appt_id=$_POST['appt_id'];
					$participant_id=$_POST['participant_id'];
					
					print('<pre>');
					print_r($_POST);
					print('</pre>');
					
					$stmt = $dbo->prepare("SELECT Email
													FROM participants 
													WHERE Participant_UID= :participant_id AND (Email IS NOT NULL OR Email <> '')");
							$stmt->execute(array('participant_id' => $participant_id));
							$row_count = $stmt->rowCount();
							$result = $stmt->fetchAll();
							
							if ($row_count==0) {
							} 
							else {									
								foreach ($result as $row){		
							
									//send email
									$email_add = $row['Email'];
									$email = "trio@duke-nus.edu.sg";
									$subject = $_POST['subject'];
								  
									$boundary = "nextPart";
									
									$headers = "MIME-Version: 1.0\r\n";
									$headers = "From: " . $email . "\r\n";
									$headers .= "BCC: ". $email . "\r\n"; //$headers .= "BCC: edmund.shen@duke-nus.edu.sg,". $email . "\r\n";
									$headers .= "Reply-To: ". $email . "\r\n";
									$headers .= "Content-Type: multipart/alternative; boundary = $boundary\r\n";

									//text version
									$headers .= "\n--$boundary\n"; // beginning \n added to separate previous content
									$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
									$headers .= "This is the plain version";

									//html version
									$headers .= "\n--$boundary\n";
									$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
									$body = "<html><body><p>";
									$body .= $_POST['body'];
									$body .= "</p></body></html>";

									mail($email_add, $subject,$body,$headers);
								    echo "Thank you for using our mail form";	
								}	
							}
				}
				
				//Close DB connection
				$dbo = null;
				
				header("location:appointment.php?participant_id=" . $participant_id);
		?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
