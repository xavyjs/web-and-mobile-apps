<?php session_start(); ?>

<?php

	if ($_FILES['updates']['tmp_name']) {
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		

		$xml = simplexml_load_file($_FILES['updates']['tmp_name']);
		file_put_contents("log/fitbit_post.log",date("dmYHi")."|".print_r($xml,true)."\r\n", FILE_APPEND);
		
		// $fitbit_user_id = '24NJF5';
		$fitbit_user_id = $xml->updatedResource->ownerId;

		$stmt = $dbo->prepare("INSERT INTO log_fitbit(Message,Timestamp) VALUES (:message,:timestamp)");
		$stmt->execute(array(':message' => $fitbit_user_id,':timestamp' => date("Y-m-d H:i:s")));
		
		$stmt = $dbo->prepare('SELECT access_token, access_token_secret FROM participants WHERE Fitbit_User_ID=:fitbit_user_id');
		$stmt->execute(array('fitbit_user_id' => $fitbit_user_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		if ($row_count==0) {
		} 
		else {
			foreach ($result as $row){ // Append access token and secret to $_SESSION
				$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['value']=$row['access_token'];	
				$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['secret']=$row['access_token_secret'];	
				$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['authorized']=true;	
			}
		}				
		
		file_put_contents("log/session.log",print_r($_SESSION,true)."\r\n\r\n",FILE_APPEND);
		
		if (isset($_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['value'])&&isset($_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['secret'])&&isset($_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['authorized'])) {
			/*
			 * login_with_fitbit.php
			 *
			 * @(#) $Id: login_with_fitbit.php,v 1.2 2013/07/31 11:48:04 mlemos Exp $
			 *
			 */

			/*
			 *  Get the http.php file from http://www.phpclasses.org/httpclient
			 */
			require('http.php');
			require('oauth_client.php');

			$client = new oauth_client_class;
			$client->debug = 1;
			$client->debug_http = 1;
			$client->server = 'Fitbit';
			$client->redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].
				dirname(strtok($_SERVER['REQUEST_URI'],'?')).'/login_with_fitbit.php';

			$client->client_id = '2e5313cfc3d44c3e9c20d1d724fdac7a'; $application_line = __LINE__; // Fitbit Consumer Key from dev.fitbit.com | Account: edmund.shen@duke-nus.edu.sg
			$client->client_secret = '4ab38cf55bdb43e9b4cb61be02217421'; // Fitbit Consumer Secret
		
			if(strlen($client->client_id) == 0
			|| strlen($client->client_secret) == 0)
				die('Please go to Fitbit application registration page https://dev.fitbit.com/apps/new , '.
					'create an application, and in the line '.$application_line.
					' set the client_id to Consumer key and client_secret with Consumer secret. '.
					'The Callback URL must be '.$client->redirect_uri).' Make sure this URL is '.
					'not in a private network and accessible to the Fitbit site.';

			if(($success = $client->Initialize()))
			{
				if(($success = $client->Process()))
				{
					if(strlen($client->access_token))
					{
						$success = $client->CallAPI(
							'https://api.fitbit.com/1/user/-/activities/steps/date/2013-09-01/today.json',
							'GET', array(), array('FailOnAccessError'=>true), $user);			
					}
				}
				$success = $client->Finalize($success);
			}
			if($client->exit)
				exit;
			if($success) {	
				file_put_contents("log/fitbit_pull.log",date("dmYHi")."|".$fitbit_user_id."\r\n",FILE_APPEND);
				$stmt = $dbo->prepare("DELETE FROM log_steps WHERE Fitbit_User_ID=?");
				$stmt->execute(array($fitbit_user_id));				

				foreach ($user->{'activities-steps'} as $key => $val) {						
					$stmt = $dbo->prepare("INSERT INTO log_steps(Fitbit_User_ID,Steps_Number,Steps_Date) VALUES(:fitbit_user_id,:steps_number,:steps_date)");
					$stmt->execute(array(':fitbit_user_id' => $fitbit_user_id, ':steps_number' => $val->value, ':steps_date' => date("Y-m-d", strtotime($val->dateTime))));			
					//echo $val->value . " steps on " . date("d M Y, D", strtotime($val->dateTime)) . "<br>";
				}	
			}
		}
		$dbo = null; //Close DB connection	
	}
?>