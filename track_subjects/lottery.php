<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Personal Details Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT DISTINCT(Participant_ID) FROM appointments WHERE ApptStatus_ID=3 AND Event_ID=:event_id');
	$stmt->execute(array('event_id' => $event_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$participant_id = $row['Participant_ID'];
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Get Event Name into dynamic variable
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT Event FROM ctbl_event ORDER BY Event_ID');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result_event = $stmt->fetchAll();
	$dbo = null; //Close DB connection
?>

<?php //Get Event Name into dynamic variable
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT Event_ID, Event FROM ctbl_event ORDER BY Event_ID');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
	} 
	else {
		$list= array(4,5,6,7,8,9,10); 
		foreach ($result as $row){
			$event_id=$row['Event_ID'];
			$result_array[$event_id]['button_newlottery']='<a href="lottery_draw_process.php?event_id=' . $event_id . '&prev_url=' . urlencode($_SERVER["REQUEST_URI"]) . '">Draw</a>';
			
			$result_array[$event_id]['Event'] = $row['Event'];
			if (in_array($event_id, $list)){			
				$stmt2 = $dbo->prepare('SELECT * FROM lottery WHERE EVENT_ID=:event_id ORDER BY Auto_Timestamp');
				$stmt2->execute(array('event_id' => $event_id));
				$row_count2 = $stmt2->rowCount();
				$result2 = $stmt2->fetchAll();		
				if ($row_count2==0) {
				} 
				else {
					$i=1;
					foreach ($result2 as $row2){
						$result_array[$event_id]['participant_id'][$i] = '<a href="participant_info.php?participant_id='.$row2['Participant_ID'].'">' . $row2['Participant_ID'] . '</a>';	
						$i++;
						if ($row_count2>=3) {
							$result_array[$event_id]['button_newlottery']='';
						} 
					}			
					$result_array[$event_id]['button_clearall']='<a href="lottery_clearall.php?event_id=' . $event_id . '&prev_url=' . urlencode($_SERVER["REQUEST_URI"]) . '">Clear All</a>';	
				}
			
				
			}
		}
	}
	// echo '<pre>';
	// print_r($result_array);
	// echo '</pre>';
	$dbo = null; //Close DB connection
?>

			
	<h1 class='title'>Lottery</h1>
	<p class='title'>Information</p>


	<table class='participantinfo'> 
		<tr>
			<th align='left' colspan='2'>Lottery Winners</th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<th align='right' colspan='4'></th>
		</tr>		
		<tr>
			<td align='left' bgcolor='#e9e9e9' style='font-weight:bold;' width='20%'>Event</td>
			<td align='left' bgcolor='#e9e9e9' style='font-weight:bold;' width='20%'>Participant ID</td>
			<td align='left' bgcolor='#e9e9e9' style='font-weight:bold;' width='20%'>Participant ID</td>
			<td align='left' bgcolor='#e9e9e9' style='font-weight:bold;' width='20%'>Participant ID</td>
			<td align='left' bgcolor='#e9e9e9' style='font-weight:bold;' width='10%'></td>
			<td align='left' bgcolor='#e9e9e9' style='font-weight:bold;' width='10%'></td>
		</tr> 
		<?php $event_num=3; ?>
		<?php $event_num++; ?> <!-- Change Event_ID here to cascade down -->
		<tr bgcolor='<?php echo $result_array[$event_num]['color']; ?>'>
			<td align='left'><?php echo $result_array[$event_num]['Event']; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][1]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][2]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][3]; ?></td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_newlottery']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_clearall']; ?>
			</td>
		</tr> <?php $event_num++; ?> <!-- Change Event_ID here to cascade down -->
		<tr bgcolor='<?php echo $result_array[$event_num]['color']; ?>'>
			<td align='left'><?php echo $result_array[$event_num]['Event']; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][1]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][2]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][3]; ?></td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_newlottery']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_clearall']; ?>
			</td>
		</tr> 
		<?php $event_num++; ?> <!-- Change Event_ID here to cascade down -->
		<tr bgcolor='<?php echo $result_array[$event_num]['color']; ?>'>
			<td align='left'><?php echo $result_array[$event_num]['Event']; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][1]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][2]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][3]; ?></td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_newlottery']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_clearall']; ?>
			</td>
		</tr> 
		<?php $event_num++; ?> <!-- Change Event_ID here to cascade down -->
		<tr bgcolor='<?php echo $result_array[$event_num]['color']; ?>'>
			<td align='left'><?php echo $result_array[$event_num]['Event']; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][1]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][2]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][3]; ?></td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_newlottery']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_clearall']; ?>
			</td>
		</tr> 
		<?php $event_num++; ?> <!-- Change Event_ID here to cascade down -->
		<tr bgcolor='<?php echo $result_array[$event_num]['color']; ?>'>
			<td align='left'><?php echo $result_array[$event_num]['Event']; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][1]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][2]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][3]; ?></td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_newlottery']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_clearall']; ?>
			</td>
		</tr> 
		<?php $event_num++; ?> <!-- Change Event_ID here to cascade down -->
		<tr bgcolor='<?php echo $result_array[$event_num]['color']; ?>'>
			<td align='left'><?php echo $result_array[$event_num]['Event']; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][1]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][2]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][3]; ?></td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_newlottery']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_clearall']; ?>
			</td>
		</tr> 
		<?php $event_num++; ?> <!-- Change Event_ID here to cascade down -->
		<tr bgcolor='<?php echo $result_array[$event_num]['color']; ?>'>
			<td align='left'><?php echo $result_array[$event_num]['Event']; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][1]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][2]; ?></td>
			<td align='left'><?php echo $result_array[$event_num]['participant_id'][3]; ?></td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_newlottery']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td align='right'>
				<?php echo $result_array[$event_num]['button_clearall']; ?>
			</td>
		</tr> 
		
	</table>
	<br>
	<?php 
		//how many lines?
		$linecount=4;

		//what's a typical line length?
		$length=100;

		//which file?
		$file="log/lottery.log";

		//we double the offset factor on each iteration
		//if our first guess at the file offset doesn't
		//yield $linecount lines
		$offset_factor=1;


		$bytes=filesize($file);

		$fp = fopen($file, "r") or die("Can't open $file");


		$complete=false;
		while (!$complete)
		{
			//seek to a position close to end of file
			$offset = $linecount * $length * $offset_factor;
			fseek($fp, -$offset, SEEK_END);


			//we might seek mid-line, so read partial line
			//if our offset means we're reading the whole file, 
			//we don't skip...
			if ($offset<$bytes)
				fgets($fp);

			//read all following lines, store last x
			$lines=array();
			while(!feof($fp))
			{
				$line = fgets($fp);
				array_push($lines, $line);
				if (count($lines)>$linecount)
				{
					array_shift($lines);
					$complete=true;
				}
			}

			//if we read the whole file, we're done, even if we
			//don't have enough lines
			if ($offset>=$bytes)
				$complete=true;
			else
				$offset_factor*=2; //otherwise let's seek even further back

		}
		fclose($fp);

		print '<br><br><br>Last 3 error log:<br><br>';
		print $lines[2] . '<br>';
		print $lines[1] . '<br>';
		print $lines[0];
	?>
	
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
