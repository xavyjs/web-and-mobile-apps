<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	if ($_GET['event_id'] || $_GET['prev_url']) {
		$event_id=$_GET['event_id'];
		$prev_url=$_GET['prev_url'];
		
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database	

			$stmt = $dbo->prepare('SELECT Event FROM ctbl_event WHERE Event_ID=:event_id');
			$stmt->execute(array('event_id' => $event_id));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			if ($row_count==0) {
			} 
			else {
				foreach ($result as $row){
					$event=$row['Event'];
				}
			}
	
			$stmt = $dbo->prepare('SELECT DISTINCT(Participant_ID) FROM log_incentive WHERE Compliance_Lottery_Eligibility="Yes" AND Event_ID=:event_id AND Participant_ID NOT IN (SELECT Participant_ID FROM lottery WHERE Event_ID=:event_id)');
			$stmt->execute(array('event_id' => $event_id));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			
				file_put_contents("log/lottery.log",date("dmYHi")."|No participant with Compliance_Lottery_Eligibility=Yes for Event_ID=".$event_id."\r\n", FILE_APPEND);
			if ($row_count==0) {
			} 
			else {
				$a=array();
				foreach ($result as $row){
					$participant_id = $row['Participant_ID'];		
					array_push($a,$participant_id);
				}
							
				// $random_keys=array_rand($a,3); // Randomizing array for more than 1 results
				// echo $a[$random_keys[0]]."<br>";
				// echo $a[$random_keys[1]]."<br>";
				// echo $a[$random_keys[2]];
				
				$random_keys=array_rand($a,1); // Randomizing array for only 1 result

				$stmt = $dbo->prepare('SELECT * FROM lottery WHERE Event_ID=:event_id');
				$stmt->execute(array('event_id' => $event_id));
				$row_count = $stmt->rowCount();
				$result = $stmt->fetchAll();
				if ($row_count<3) {
					$stmt2 = $dbo->prepare("INSERT INTO lottery(Event_ID,Participant_ID) VALUES(:event_id,:participant_id)");
					$stmt2->execute(array(':event_id' => $event_id, ':participant_id' => $a[$random_keys]));
					include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/ToDo.php");
					$todo = $event . ' lottery winner';
					ToDo_Add($a[$random_keys],0,$todo); //Participant ID, Appointment ID, To Do message
				} 
				else {	
					file_put_contents("log/lottery.log",date("dmYHi")."|There are already 3 Participant IDs for Event_ID=".$event_id."\r\n", FILE_APPEND);
				}											
		}
		$dbo = null; //Close DB connection			
		header("location:" . $prev_url);
	} else {
		header("location:lottery.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 