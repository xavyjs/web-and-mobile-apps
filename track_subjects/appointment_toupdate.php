<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //--------------Search Filters: Event Field
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_event');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "The database contains no event yet";
	} 
	else {
		$event_id_row= " <option value=''> </option>";
		foreach ($result as $row){
			if ($row['Event_ID']==$_GET['event_id']) {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . " selected='selected'>" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			} else {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . ">" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //--------------Search Filters: Location Field
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_location');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no location yet";
	} else {
		$location_id_row= " <option value=''> </option>";
		foreach ($result as $row){
			if ($row['Location_ID']==(isset($_GET['location_id']) ? $_GET['location_id'] : $location_id)) {
				$location_id_row = $location_id_row . "<option value=" . $row['Location_ID'] . " selected='selected'>" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
			} else {
				$location_id_row = $location_id_row . "<option value=" . $row['Location_ID'] . ">" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
			}
		}
	}
	$dbo = null; //Close DB connection
?>

<?php //--------------Search Filters: Appt Status Field
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_apptstatus');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no status yet";
	} else {
		$apptstatus_id_row= " <option value=''> </option>";
		foreach ($result as $row){
			if ($row['ApptStatus_ID']==(isset($_POST['apptstatus_id']) ? $_POST['apptstatus_id'] : $apptstatus_id)) {
				$apptstatus_id_row = $apptstatus_id_row . "<option value=" . $row['ApptStatus_ID'] . " selected='selected'>" . $row['ApptStatus_ID'] . ". " . $row['ApptStatus'] . "</option>";
			} else {
				$apptstatus_id_row = $apptstatus_id_row . "<option value=" . $row['ApptStatus_ID'] . ">" . $row['ApptStatus_ID'] . ". " . $row['ApptStatus'] . "</option>";
			}
		}
	}
	$dbo = null; //Close DB connection
?>

<?php //--------------Search Filters: From Day Field
	$i = 1;
	$date_from_d_row = "<option value=''> </option>";
	while ($i<=31) {
		if ($i==$_GET['date_from_d']) {
		$date_from_d_row = $date_from_d_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_from_d_row = $date_from_d_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php //--------------Search Filters: From Month Field
	$i = 1;
	$date_from_m_row = "<option value=''> </option>";
	while ($i<=12) {
		if ($i==$_GET['date_from_m']) {
		$date_from_m_row = $date_from_m_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_from_m_row = $date_from_m_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php //--------------Search Filters: From Year Field
	$i = date("Y")-1;
	$date_from_y_row = "<option value=''> </option>";
	while ($i<=date("Y")+5) {
		if ($i==$_GET['date_from_y']) {
		$date_from_y_row = $date_from_y_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_from_y_row = $date_from_y_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php //--------------Search Filters: To Day Field
	$i = 1;
	$date_to_d_row = "<option value=''> </option>";
	while ($i<=31) {
		if ($i==$_GET['date_to_d']) {
		$date_to_d_row = $date_to_d_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_to_d_row = $date_to_d_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php //--------------Search Filters: To Month Field
	$i = 1;
	$date_to_m_row = "<option value=''> </option>";
	while ($i<=12) {
		if ($i==$_GET['date_to_m']) {
		$date_to_m_row = $date_to_m_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_to_m_row = $date_to_m_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php //--------------Search Filters: To Year Field
	$i = date("Y")-1;
	$date_to_y_row = "<option value=''> </option>";
	while ($i<=date("Y")+5) {
		if ($i==$_GET['date_to_y']) {
		$date_to_y_row = $date_to_y_row . "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
		}
		else {
			$date_to_y_row = $date_to_y_row . "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
		}
		$i++;
	}
?>

<?php
	// get class into the page
	// require_once('resources/library/calendar/tc_calendar.php');

	// instantiate class and set properties
	// $myCalendar = new tc_calendar("date1", true);
	// $myCalendar->setIcon("resources/library/calendar/images/iconCalendar.gif");
	// $myCalendar->setDate(01, 03, 1960);
	// $myCalendar->setPath("resources/library/calendar/");
	// $myCalendar->setYearInterval(1960, 2015);
	// $myCalendar->dateAllow('1960-01-01', '2015-03-01');
	// $myCalendar->setSpecificDate(array("2011-04-01", "2011-04-13", "2011-04-25"), 0, 'month');
	// $myCalendar->setOnChange("myChanged('test')");

	// output the calendar
	// $myCalendar->writeScript();	  
?>

<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$event_id = (empty($_GET['event_id'])) ? '%' : $_GET['event_id']; 
	$apptstatus_id = (empty($_GET['apptstatus_id'])) ? '%' : $_GET['apptstatus_id']; 
	$location_id = (empty($_GET['location_id'])) ? '%' : $_GET['location_id']; 
	$date_from=$_GET['date_from_y'] . "-" . $_GET['date_from_m'] . "-" . $_GET['date_from_d'];
	$date_to=$_GET['date_to_y'] . "-" . $_GET['date_to_m'] . "-" . $_GET['date_to_d'];
	if ($date_from=='--') {
		$date_from = null;
	}
	if ($date_to=='--') {
		$date_to = null;
	}
	if (empty($date_from)) {
		$date_from = "1900-01-01";
	}
	if (empty($date_to)) {
		$date_to = "3000-01-01";
	}

	$rec_limit=(empty($_GET['rec_limit'])) ? '30' : $_GET['rec_limit']; 
	if (empty($_GET['page'])) {
		$offset=0;
	} else {
		$offset=($_GET['page']-1)*$rec_limit;
	}

	$sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, E.Event, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.ApptStatus_ID, S.ApptStatus, A.Note  
			FROM appointments AS A 
			LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
			LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
			LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID) AS A2
			WHERE A2.ApptStatus_ID = 1 AND	
			A2.Appointment_Date < :appointment_datetime';					
	$sql .= ' AND A2.Participant_id LIKE :participant_id';
	$sql .= ' AND A2.Event_ID LIKE :event_id';
	$sql .= ' AND A2.ApptStatus_ID LIKE :apptstatus_id';
	$sql .= ' AND A2.Location_ID LIKE :location_id';
	$sql .= ' AND A2.Appointment_Date BETWEEN :date_from AND :date_to';
	$sql .= ' ORDER BY A2.Appointment_Date, A2.Appointment_Time, A2.Participant_id, A2.Event_ID';
	$sql .= ' LIMIT :offset, :rec_limit';
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':appointment_datetime', date('Y-m-d'));
	$stmt->bindValue(':participant_id', $participant_id);
	$stmt->bindValue(':event_id', $event_id);
	$stmt->bindValue(':apptstatus_id', $apptstatus_id);
	$stmt->bindValue(':location_id', $location_id);
	$stmt->bindValue(':date_from', $date_from);
	$stmt->bindValue(':date_to', $date_to);			
	$stmt->bindValue(':offset', (int) trim($offset), PDO::PARAM_INT);
	$stmt->bindValue(':rec_limit', (int) trim($rec_limit), PDO::PARAM_INT);										
	$stmt->execute();	
	$found_rows = $dbo->query('SELECT FOUND_ROWS()')->fetchColumn(0);									
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($participant_id=='%') {
		$participant_id="";
	}
	if ($event_id=='%') {
		$event_id="";
	}
	if ($location_id=='%') {
		$location_id="";
	}
	if ($apptstatus_id=='%') {
		$apptstatus_id="";
	}
	$date_from = date("d-m-Y", strtotime($date_from));
	if ($date_from=='01-01-1900') {
		$date_from="";
	}
	$date_to = date("d-m-Y", strtotime($date_to));
	if ($date_to=='01-01-3000') {
		$date_to="";
	}
	
	$filter_row = "Filter (Participant ID=" . $participant_id . ", Event=" . $event_id . ", Location=" . $location_id . ", Status=" . $apptstatus_id . ", Date From=" . $date_from . ", Date To=" . $date_to . ")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Record/s: " . $found_rows;
	
	if ($row_count==0) {
		$appointment_row = "<tr><td colspan='14'>No appointment</td></tr>";	
	} else {	
		foreach ($result as $row){	
			
			if ($row['Appointment_Date'] < date('Y-m-d')) { //Determine row color based on appointment date
				$appointment_tr = "<tr style='color:#999'>";
			}
			elseif ($row['Appointment_Date'] > date('Y-m-d') && $row['Appointment_Date'] <= date('Y-m-d', strtotime("+3 days"))) {
				$appointment_tr = "<tr style='background-color: #ffdf85;'>";
			} 
			elseif ($row['Appointment_Date'] == date('Y-m-d')) {
				$appointment_tr = "<tr style='background-color: #f29966;'>";
			} 
			else {
				$appointment_tr = "<tr>";
			}			
			
			$participant_id_row = "<td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "'>" . $row['Participant_ID'] . "</a></td>";		
			$event_row = "<td>" . $row['Event'] . "</td>";
			if ($row['Appointment_Date'] != null) {
				$appointment_date_row = "<td>" . date("d M Y, D", strtotime($row['Appointment_Date'])) . "</td>";
			} else {
				$appointment_date_row = "<td></td>";
			}		
			if ($row['Appointment_Time'] != null) {
				$appointment_time_row = "<td>" . date("h:i A", strtotime($row['Appointment_Time'])) . "</td>";
			} else {
				$appointment_time_row = "<td></td>";
			}	
			$location_row = "<td>" . $row['Location'] . "</td>";					
			
			//--------------------------Add code to determine previous appointment status
			$apptstatus_loop=$row['ApptStatus'];
			if (!empty($row['ApptStatus'])) {
				$stmt2 = $dbo->prepare('SELECT A.Appointment_ID, A.Participant_id, A.Event_ID, A.Appointment_Date, A.Appointment_Time, A.ApptStatus_ID, S.ApptStatus
								FROM appointments AS A
								LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID 
								WHERE A.Participant_id= :participant_id AND
								A.Appointment_ID!= :appointment_id AND															
								A.Event_ID= :event_id AND
								(Appointment_Date < :appointment_date OR (Appointment_Date = :appointment_date AND Appointment_Time < :appointment_time)) AND
								(A.ApptStatus_ID IS NOT NULL AND A.ApptStatus_ID!=0)
								ORDER BY A.Event_ID, A.Appointment_Date DESC, A.Appointment_Time DESC');
				$stmt2->execute(array('participant_id' => $row['Participant_ID'], 'appointment_id' => $row['Appointment_ID'], 'event_id' => $row['Event_ID'], 'appointment_date' => $row['Appointment_Date'], 'appointment_time' => $row['Appointment_Time']));
				$row_count2 = $stmt2->rowCount();
				$result2 = $stmt2->fetchAll();	
				if ($row_count2==0) {
				} 
				else {	
					foreach ($result2 as $row2){	
						$apptstatus_loop=$row2['ApptStatus'][0] . "," . $apptstatus_loop;
					}
				}
			}		
			//--------------------------Add code to determine previous appointment status
							
			$apptstatus_row = "<td>" . $apptstatus_loop . "</td>";
					
			//For note field with hover tip
			if ($row['Note'] != null) {
				$note_row = "<td><div style='cursor:help;clear:both;' title='" . $row['Note'] . "'>&#10004;</div></td>";	
			} else {
				$note_row = "<td></td>";
			}
			$link_edit = "<td><a href='appointment_edit.php?appointment_id=" . $row['Appointment_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a></td></tr>";
			$appointment_row = $appointment_row . $appointment_tr . $participant_id_row . $event_row . $appointment_date_row . $appointment_time_row . $location_row . $apptstatus_row . $note_row . $link_edit;			
		}									
	}	
	
	//Pagination ------------------------------------------------
	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$event_id = (empty($_GET['event_id'])) ? '%' : $_GET['event_id']; 
	$apptstatus_id = (empty($_GET['apptstatus_id'])) ? '%' : $_GET['apptstatus_id']; 
	$location_id = (empty($_GET['location_id'])) ? '%' : $_GET['location_id']; 										
	$date_from=$_GET['date_from_y'] . "-" . $_GET['date_from_m'] . "-" . $_GET['date_from_d'];
	$date_to=$_GET['date_to_y'] . "-" . $_GET['date_to_m'] . "-" . $_GET['date_to_d'];											
	if ($date_from=='--') {
		$date_from = null;
	}
	if ($date_to=='--') {
		$date_to = null;
	}
	if (empty($date_from)) {
		$date_from = "1900-01-01";
	}
	if (empty($date_to)) {
		$date_to = "3000-01-01";
	}										
	$sql = 'SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, E.Event, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.ApptStatus_ID, S.ApptStatus, A.Note  
			FROM appointments AS A 
			LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
			LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
			LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID
			WHERE A.ApptStatus_ID = 1 AND	
			A.Appointment_Date < :appointment_datetime';					
		$sql .= ' AND A.Participant_id LIKE :participant_id';
		$sql .= ' AND A.Event_ID LIKE :event_id';
		$sql .= ' AND A.ApptStatus_ID LIKE :apptstatus_id';
		$sql .= ' AND A.Location_ID LIKE :location_id';
		$sql .= ' AND A.Appointment_Date BETWEEN :date_from AND :date_to';
		$sql .= ' ORDER BY A.Appointment_Date, A.Appointment_Time, A.Participant_id, A.Event_ID';
		$stmt = $dbo->prepare($sql);
		$stmt->bindValue(':appointment_datetime', date('Y-m-d'));
		$stmt->bindValue(':participant_id', $participant_id);
		$stmt->bindValue(':event_id', $event_id);
		$stmt->bindValue(':apptstatus_id', $apptstatus_id);
		$stmt->bindValue(':location_id', $location_id);
		$stmt->bindValue(':date_from', $date_from);
		$stmt->bindValue(':date_to', $date_to);									
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
		$pagination_row = "";
	} 
	else {	
		$total_pages = ceil($row_count/$rec_limit);
		for ($i=1; $i<=$total_pages; $i++) { 
			if ($i==1 && empty($_GET['page'])) {
				$pagination_row = $pagination_row . "<strong>".$i."</strong>&nbsp;"; 
			}
			elseif ($i==$_GET['page']) {
				$pagination_row = $pagination_row . "<strong>".$i."</strong>&nbsp;"; 
			}
			else {
				$pagination_row = $pagination_row . "<a href='appointment_toupdate.php?participant_id=".$participant_id.
									"&event_id=".$event_id.
									"&apptstatus_id=".$apptstatus_id.
									"&location_id=".$location_id.
									"&date_from_d=".$_GET['date_from_d'].
									"&date_from_m=".$_GET['date_from_m'].
									"&date_from_y=".$_GET['date_from_y'].
									"&date_to_d=".$_GET['date_to_d'].
									"&date_to_m=".$_GET['date_to_m'].
									"&date_to_y=".$_GET['date_to_y'].
									"&page=".$i."&rec_limit=".$rec_limit."'>".$i."</a>&nbsp;"; 
			}
		}
	}
	//Pagination ------------------------------------------------			
	$dbo = null; //Close DB connection						
?>

	<h1 class='title'>Appointment</h1>
	<p class='title'>To Update<p>
		<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header-appointment.php"); ?>
		<br><br>
		<table class='searchfilters'>
			<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="get">
			<tr><th align='left' colspan=8>
				Search Filters
			</td></tr>
			<tr>				
				<td align="right" width='11%'>Participant ID:</td>
				<td align="left" width='18%'>
					<input name="participant_id" type="text" size="10" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $_GET['participant_id'] ?>"></input>
				</td>
				<td align='right' width='7%'>Event:</td>
				<td align='left' width='15%'>
					<select name="event_id"><?php echo $event_id_row; ?></select>
				</td>
				<td align='right' width='9%'>Location:</td>
				<td align='left' width='15%'>
					<select name="location_id"><?php echo $location_id_row; ?></select>
				</td>
				<td align='right' width='7%'>Status:</td>
				<td align='left' width='16%'>
					<select name="apptstatus_id"><?php echo $apptstatus_id_row; ?></select>
				</td>
			</tr>
			<tr>					
				<td align="right">
					Date From: 
				</td>
				<td align="left">
					<select name="date_from_d"><?php echo $date_from_d_row ?></select>
					<select name="date_from_m"><?php echo $date_from_m_row ?></select>
					<select name="date_from_y"><?php echo $date_from_y_row ?></select>	
				</td>
				<td align="right">
					Date To: 
				</td>
				<td align="left">
					<select name="date_to_d"><?php echo $date_to_d_row ?></select>
					<select name="date_to_m"><?php echo $date_to_m_row ?></select>
					<select name="date_to_y"><?php echo $date_to_y_row ?></select>
				</td>
				<td align="right" colspan=3>
				</td>
				<td align="right">
					<table>
						<tr>
							<td>
								<input name="filter" type="hidden" value="1"></input>
								<input type="submit" value="Filter"></input>
								</form>
							</td>
							<td>
								<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
								<input name="clearall" type="hidden" value="1"></input>
								<input type="submit" value="Clear All"></input>
							</form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table><br>
	 
		<table class='wborder' width='960px'>	
			<tr>
				<th colspan='14'>
					<?php echo $filter_row ?>
				</th></tr>
			<tr>
				<th>PID</th>
				<th>Event</th>
				<th>Date</th>
				<th>Time</th>
				<th>Location</th>
				<th>Status</th>
				<th>Note</th>
				<th> </th>
			</tr>
			<?php echo $appointment_row; ?> 
		</table>
		<table width='960px'>
			<tr>
				<td align='left'>	
					<?php echo $pagination_row; ?> 
				</td>
				<td align='right'><a href='appointment_toupdate.php?participant_id=<?php echo $participant_id; ?>&event_id=<?php echo $event_id; ?>&apptstatus_id=<?php echo $apptstatus_id; ?>&location_id=<?php echo $location_id; ?>&date_from_d=<?php echo $_GET['date_from_d']; ?>&date_from_m=<?php echo $_GET['date_from_m']; ?>&date_from_y=<?php echo $_GET['date_from_y']; ?>&date_to_d=<?php echo $_GET['date_to_d']; ?>&date_to_m=<?php echo $_GET['date_to_m']; ?>&date_to_y=<?php echo $_GET['date_to_y']; ?>&filter=1&page=1&rec_limit=99999'>Show All</a>
				</td>
			</tr>
		</table>		
		<br><br>
		<table align='right'>
		<tr style='font-weight:bold;'><td colspan='2'>Appointment Legend</td></tr>	
		<tr><td style='background-color: #d95252;' width='20px'></td><td>2 consecutive misses</td></tr>
		<tr><td style='background-color: #f29966;' width='20px'></td><td>Today</td></tr>
		<tr><td style='background-color: #ffdf85;' width='20px'></td><td>Next 3 days</td></tr>		
		</table>
		<div style='clear: both'></div>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>