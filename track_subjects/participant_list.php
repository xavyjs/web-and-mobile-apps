<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<link rel="stylesheet" type="text/css" href="/resources/library/jquery-autocomplete/jquery.autocomplete.css" />
<script type="text/javascript" src="/resources/library/jquery-autocomplete/lib/jquery.js"></script>
<script type="text/javascript" src="/resources/library/jquery-autocomplete/jquery.autocomplete.js"></script>
<script>
 $(document).ready(function(){
  $("#participant_id").autocomplete("autocomplete_participant_id.php", {
        selectFirst: true
  });
  $("#mobile").autocomplete("autocomplete_mobile.php", {
        selectFirst: true
  });
 });
</script>

<?php //--------------Search Filters: PStatus Field
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_pstatus');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "The database contains no pstatus yet";
	} 
	else {
		$pstatus_id_row= " <option value=''> </option>";
		foreach ($result as $row){
			if ($row['PStatus_ID']==$_GET['pstatus_id']) {
				$pstatus_id_row = $pstatus_id_row . "<option value=" . $row['PStatus_ID'] . " selected='selected'>" . $row['PStatus_ID'] . ". " . $row['PStatus'] . "</option>";
			} else {
				$pstatus_id_row = $pstatus_id_row . "<option value=" . $row['PStatus_ID'] . ">" . $row['PStatus_ID'] . ". " . $row['PStatus'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>


<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$mobile = (empty($_GET['mobile'])) ? '%' : $_GET['mobile']; 
	$pstatus_id = (empty($_GET['pstatus_id'])) ? '%' : $_GET['pstatus_id']; 

	$rec_limit=(empty($_GET['rec_limit'])) ? '30' : $_GET['rec_limit']; 
	if (empty($_GET['page'])) {
		$offset=0;
	} else {
		$offset=($_GET['page']-1)*$rec_limit;
	}

	$sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT p.Participant_AID, p.Participant_ID, p.Age, r.Race, p.Race_ID, g.Gender, p.Gender_ID, p.Mobile, sa.Study_Arm, p.Study_Arm_ID, ps.PStatus, p.PStatus_ID, p.Num_Daily_Doses, p.Baseline_Assessment_Date, p.Month3_Assessment_Date, p.Month6_Assessment_Date, p.PNote
						FROM participants AS p 
                        LEFT JOIN ctbl_race AS r ON p.Race_ID = r.Race_ID
						LEFT JOIN ctbl_gender AS g ON p.Gender_ID = g.Gender_ID
						LEFT JOIN ctbl_study_arm AS sa ON p.Study_Arm_ID = sa.Study_Arm_ID
						LEFT JOIN ctbl_pstatus AS ps ON p.PStatus_ID = ps.PStatus_ID) AS p2
			WHERE';					
	$sql .= ' p2.Participant_id LIKE :participant_id';
	$sql .= ' AND p2.Mobile LIKE :mobile';
	$sql .= ' AND p2.PStatus_ID LIKE :pstatus_id';
	$sql .= ' ORDER BY p2.Participant_ID';
	$sql .= ' LIMIT :offset, :rec_limit';
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':participant_id', $participant_id);
	$stmt->bindValue(':mobile', '%' . $mobile . '%');	
	$stmt->bindValue(':pstatus_id', $pstatus_id);
	$stmt->bindValue(':offset', (int) trim($offset), PDO::PARAM_INT);
	$stmt->bindValue(':rec_limit', (int) trim($rec_limit), PDO::PARAM_INT);										
	$stmt->execute();	
	$found_rows = $dbo->query('SELECT FOUND_ROWS()')->fetchColumn(0);									
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($participant_id=='%') {
		$participant_id="";
	}
	if ($mobile=='%') {
		$mobile="";
	}
	if ($pstatus_id=='%') {
		$pstatus_id="";
	}
	
	$filter_row = "Filter (Participant ID=" . $participant_id . ", Mobile=" . $mobile . ", PStatus=" . $pstatus_id . ")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Record/s: " . $found_rows;
	
	if ($row_count==0) {
		$appointment_row = "<tr><td colspan='10'>No participant</td></tr>";	
	} else {	
		foreach ($result as $row){				
			$participant_id_row = "<tr><td><a href='participant_info.php?participant_aid=" . $row['Participant_AID'] . "'>" . htmlspecialchars($row['Participant_ID']) . "</a></td>";		
			$age_row = "<td>" . $row['Age'] . "</td>";
			$race_row = "<td>" . $row['Race'] . "</td>";
			$gender_row = "<td>" . $row['Gender'] . "</td>";
			$mobile_row = "<td>" . $row['Mobile'] . "</td>";
			$study_arm_row = "<td>" . $row['Study_Arm'] . "</td>";
			$pstatus_row = "<td>" . $row['PStatus'] . "</td>";
			$num_daily_doses_row = "<td>" . $row['Num_Daily_Doses'] . "</td>";
			if ($row['Baseline_Assessment_Date'] != null) {
				$baseline_assessment_date_row = "<td>" . date("d M Y, D", strtotime($row['Baseline_Assessment_Date'])) . "</td>";
			} else {
				$baseline_assessment_date_row = "<td></td>";
			}	
			if ($row['Month3_Assessment_Date'] != null) {
				$month3_assessment_date_row = "<td>" . date("d M Y, D", strtotime($row['Month3_Assessment_Date'])) . "</td>";
			} else {
				$month3_assessment_date_row = "<td></td>";
			}	
			if ($row['Month6_Assessment_Date'] != null) {
				$month6_assessment_date_row = "<td>" . date("d M Y, D", strtotime($row['Month6_Assessment_Date'])) . "</td>";
			} else {
				$month6_assessment_date_row = "<td></td>";
			}				
			
			//Calculate Incentive
			$total_incentive=0;
			$incentive_paid=0;
			$incentive_balance=0;
			
			// Get Lottery Incentive
			$stmt2 = $dbo->prepare('SELECT * FROM lottery WHERE Participant_ID=?');
			$stmt2->execute(array($row['Participant_AID']));
			$row_count2 = $stmt2->rowCount();
			$result2 = $stmt2->fetchAll();		
			if ($row_count2==0) {
			} 
			else {
				foreach ($result2 as $row2){
					$total_incentive=$total_incentive+500;
				}	
			}
			
			$stmt2 = $dbo->prepare('SELECT * FROM log_incentive WHERE Participant_ID=?');
			$stmt2->execute(array($row['Participant_AID']));
			$row_count2 = $stmt2->rowCount();
			$result2 = $stmt2->fetchAll();
			
			if ($row_count2==0) {
				$incentive_balance='<td>' . '-' . '</td>';
			} 
			else {
				foreach ($result2 as $row2){
					$total_incentive=$total_incentive+$row2['Total_Incentive'];
					$incentive_paid=$incentive_paid+$row2['Incentive_Paid'];
				}
			}
			$incentive_balance='<td>S$' . number_format(($total_incentive-$incentive_paid),2) . '</td>';
									
			//For note field with hover tip
			if ($row['PNote'] != null) {
				$pnote_row = "<td><div style='cursor:help;clear:both;' title='" . str_replace( "'","&#39;",$row['PNote']) . "'>&#10004;</div></td>";	
			} else {
				$pnote_row = "<td></td>";
			}
			$pstatus_row = "<td>" . $row['PStatus'] . "</td>";
			$link_edit = "<td><a href='participant_edit.php?participant_aid=" . $row['Participant_AID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a> ";
			
			if (!isset($_SESSION['accesslvl']) || $_SESSION['accesslvl'] >= 3) { //Display Delete function if user access level is 3 or higher
				$link_delete = "<a href='participant_delete.php?participant_aid=" . $row['Participant_AID'] . "&participant_id=" . $row['Participant_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Del</a>";
			} else {
				$link_delete = "";
			}
			
			$participant_row = $participant_row . $participant_id_row . $age_row . $race_row . $gender_row . $mobile_row . $num_daily_doses_row . $baseline_assessment_date_row . $month3_assessment_date_row . $month6_assessment_date_row . $pstatus_row . $pnote_row . $link_edit . $link_delete . "</td></tr>";			
		}									
	}	
	
	//Pagination ------------------------------------------------
	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$mobile = (empty($_GET['mobile'])) ? '%' : $_GET['mobile']; 
	$pstatus_id = (empty($_GET['pstatus_id'])) ? '%' : $_GET['pstatus_id']; 
	
	$sql = 'SELECT p.Participant_AID, p.Participant_ID, p.Age, r.Race, p.Race_ID, g.Gender, p.Gender_ID, p.Mobile, sa.Study_Arm, p.Study_Arm_ID, ps.PStatus, p.PStatus_ID, p.Num_Daily_Doses, p.Baseline_Assessment_Date, p.Month3_Assessment_Date, p.Month6_Assessment_Date, p.PNote 
			FROM participants AS p 
                        LEFT JOIN ctbl_race AS r ON p.Race_ID = r.Race_ID
						LEFT JOIN ctbl_gender AS g ON p.Gender_ID = g.Gender_ID
						LEFT JOIN ctbl_study_arm AS sa ON p.Study_Arm_ID = sa.Study_Arm_ID
						LEFT JOIN ctbl_pstatus AS ps ON p.PStatus_ID = ps.PStatus_ID
			WHERE';		
	$sql .= ' p.Participant_id LIKE :participant_id';
	$sql .= ' AND p.Mobile LIKE :mobile';
	$sql .= ' AND p.PStatus_ID LIKE :pstatus_id';
	$sql .= ' ORDER BY p.Participant_ID';
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':participant_id', $participant_id);
	$stmt->bindValue(':mobile', '%' . $mobile . '%');	
	$stmt->bindValue(':pstatus_id', $pstatus_id);												
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
		$pagination_row = "";
	} 
	else {	
		$total_pages = ceil($row_count/$rec_limit);
		for ($i=1; $i<=$total_pages; $i++) { 
			if ($i==1 && empty($_GET['page'])) {
				$pagination_row = $pagination_row . "<strong>".$i."</strong>&nbsp;"; 
			}
			elseif ($i==$_GET['page']) {
				$pagination_row = $pagination_row . "<strong>".$i."</strong>&nbsp;"; 
			}
			else {
				$pagination_row = $pagination_row . "<a href='participant_list.php?participant_id=".$participant_id.
									"&mobile=".$mobile.
									"&pstatus_id=".$pstatus_id.
									"&page=".$i."&rec_limit=".$rec_limit."'>".$i."</a>&nbsp;"; 
			}
		}
	}
	//Pagination ------------------------------------------------			
	$dbo = null; //Close DB connection						
?>

	<h1 class='title'>Participant</h1>
	<p class='title'>List</p>
		<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header-participant.php"); ?>
		<br><br>
		<table class='searchfilters'>
			<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="get">
			<tr><th align='left' colspan=8>
				Search Filters
			</td></tr>
			<tr>				
				<td align="right" width='11%'>Participant ID:</td>
				<td align="left" width='22%'>
					<input name="participant_id" type="text" size="10" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $_GET['participant_id'] ?>"></input>
				</td>
				<td align="right" width='11%'>Mobile:</td>
				<td align="left" width='22%'>
					<input name="mobile" type="text" id="mobile" size="10" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : $_GET['mobile'] ?>"></input>
				</td>
				<td align='right' width='1%'>PStatus:</td>
				<td align='left' width='22%'>
					<select name="pstatus_id"><?php echo $pstatus_id_row; ?></select>
				</td>
			</tr>
			<tr>					
				<td align="right" colspan=5>
				</td>
				<td align="right">
					<table>
						<tr>
							<td>
								<input name="filter" type="hidden" value="1"></input>
								<input type="submit" value="Filter"></input>
								</form>
							</td>
							<td>
								<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
								<input name="clearall" type="hidden" value="1"></input>
								<input type="submit" value="Clear All"></input>
							</form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table><br>
				
	<table class='wborder' width='960px'>
		<tr>
			<th colspan='10'>
				<?php echo $filter_row ?>
			</th></tr>
		<tr>
			<th>PID</th>									
			<th width='4%'>Age</th>
			<th>Race</th>		
			<th>Gender</th>	
			<th>Mobile</th>		
			<th width='9%'>Doses</th>		
			<th>Baseline Date</th>
			<th>Month 3 Date</th>	
			<th>Month 6 Date</th>
			<th>PStatus</th>
			<th>PNote</th>
			<th width='6%'></th>		
		</tr>		
		<?php echo $participant_row; ?>
	</table>				
	<table width='960px'>
		<tr>
			<td align='left'>	
				<?php echo $pagination_row; ?> 
			</td>
			<td align='right'><a href='participant_list.php?participant_id=<?php echo $participant_id; ?>&mobile=<?php echo $mobile; ?>&pstatus_id=<?php echo $pstatus_id; ?>&filter=1&page=1&rec_limit=99999'>Show All</a>
			</td>
		</tr>
	</table>		
	<br><br>		
	

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>