<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>
	
		<form action="appointment_email_process.php" method="post">
		
			<h1 class='title'>Email appointment</h1>
			<p class='title'>Are you sure you want to send an email for this appointment?</p>
			
			<table id="tbl_registration">
			
			<?php
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
					dbConnect(); // Connect to Database
					
					$appt_id=$_POST['appt_id'];
					$participant_id=$_POST['participant_id'];
					$appt_event=$_POST['appt_event'];
					$appt_date=$_POST['appt_date'];
					$appt_time=$_POST['appt_time'];
					$appt_location=$_POST['appt_location'];
					$email_id=1;
					
					$stmt = $dbo->prepare("SELECT *
											FROM ctbl_emails 
											WHERE Email_ID = :email_id");
					$stmt->execute(array('email_id' => $email_id));
					$row_count = $stmt->rowCount();
					$result = $stmt->fetchAll();
					
					if ($row_count==0) {
					} 
					else {									
						foreach ($result as $row){		
						
							$email_subject=$row['Email_Subject'];
							$email_body=$row['Email_Body'];
							
							$email_body=str_replace("<Participant_UID>",$participant_id,$email_body);
							$email_body=str_replace("<Event>",$appt_event,$email_body);
							$email_body=str_replace("<Appointment_Date>",date("d F Y, l", strtotime($appt_date)),$email_body);
							$email_body=str_replace("<Appointment_Time>",date("h:i A", strtotime($appt_time)),$email_body);
							$email_body=str_replace("<Location>",$appt_location,$email_body);
					
							echo "<tr><td valign='top' align='right'>Subject: </td><td align='left'>
									<input name='subject' size='80' type='text' value='" . $email_subject . "'></input><br><br>
								</td></tr>";
							echo "<tr><td valign='top' align='right'>Body: </td><td align='left'>
									<textarea name='body' rows='15' cols='80'>" . $email_body . "</textarea><br><br>
								</td></tr>";
						}
					}
				}
			?>
			<tr>
				<td></td>
				<td>
				<td>
					<input name="appt_id" size="20" type="hidden" value="<?php echo isset($_POST['appt_id']) ? $_POST['appt_id'] : '' ?>"></input>
					<input name="participant_id" size="20" type="hidden" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : '' ?>"></input>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td align='left'>
					<input type="submit" value="Send"></input>
					<a href="appointment.php?participant_id=<?php echo $_POST['participant_id'] ?>">Back to appointment page</a>
				</td>
			</tr>
			</table>
			
			</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>