<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //--------------Search Filters: Title Field
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_title');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "The database contains no title yet";
	} 
	else {
		$title_id_row= " <option value=''> </option>";
		foreach ($result as $row){
			if ($row['Title_ID']==$_GET['title_id']) {
				$title_id_row = $title_id_row . "<option value=" . $row['Title_ID'] . " selected='selected'>" . $row['Title_ID'] . ". " . $row['Title'] . "</option>";
			} else {
				$title_id_row = $title_id_row . "<option value=" . $row['Title_ID'] . ">" . $row['Title_ID'] . ". " . $row['Title'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php
	// get class into the page
	// require_once('resources/library/calendar/tc_calendar.php');

	// instantiate class and set properties
	// $myCalendar = new tc_calendar("date1", true);
	// $myCalendar->setIcon("resources/library/calendar/images/iconCalendar.gif");
	// $myCalendar->setDate(01, 03, 1960);
	// $myCalendar->setPath("resources/library/calendar/");
	// $myCalendar->setYearInterval(1960, 2015);
	// $myCalendar->dateAllow('1960-01-01', '2015-03-01');
	// $myCalendar->setSpecificDate(array("2011-04-01", "2011-04-13", "2011-04-25"), 0, 'month');
	// $myCalendar->setOnChange("myChanged('test')");

	// output the calendar
	// $myCalendar->writeScript();	  
?>
				
<?php //Appointment Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	$stmt = $dbo->prepare('SELECT p.Participant_ID, t.Title, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote FROM participants AS p LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID ORDER BY Participant_ID');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		//$event1_date = "<a href='appointment_new.php?participant_id=" . $_REQUEST['participant_id'] . "&event_id=1'>Make appointment</a>";
	} 
	else {
		foreach ($result as $row){
			$participant_id = "<tr><td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "'>" . $row['Participant_ID'] . "</a></td>";
			$title = "<td>" . $row['Title'] . "</td>";
			$lastname = "<td>" . $row['Lastname'] . "</td>";
			$firstname = "<td>" . $row['Firstname'] . "</td>";
			$mobile = "<td>" . $row['Mobile'] . "</td>";
			$email = "<td><a href='mailto:" . $row['Email'] . "'>" . $row['Email'] . "</a></td>";
			$address1 = "<td>" . $row['Address1'] . "</td>";
			$postcode = "<td>" . $row['Postcode'] . "</td>";
			$pnote = $row['PNote'];
				//For note field with hover tip
				if ($pnote != null) {
					$pnote = "<td><div style='cursor:help;clear:both;' title='" . $pnote . "'>&#10004;</div></td>";	
				} else {
					$pnote = "<td></td>";
				}
			$timestamp = "<td>" . date("d-m-Y H:i", strtotime($row['Timestamp'])) . "</td>";
			$link_edit = "<td><a href='participant_edit.php?participant_id=" . $row['Participant_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a></td></tr>";
			$participant_row = $participant_row . $participant_id . $title . $lastname . $firstname . $mobile . $email . $address1 . $postcode . $pnote . $link_edit;
		}
	}
		
	$dbo = null; //Close DB connection
?>

<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$title_id = (empty($_GET['title_id'])) ? '%' : $_GET['title_id']; 
	$firstname = (empty($_GET['firstname'])) ? '%' : $_GET['firstname']; 
	$lastname = (empty($_GET['lastname'])) ? '%' : $_GET['lastname']; 
	$mobile = (empty($_GET['mobile'])) ? '%' : $_GET['mobile']; 
	$email = (empty($_GET['email'])) ? '%' : $_GET['email']; 


	$rec_limit=(empty($_GET['rec_limit'])) ? '30' : $_GET['rec_limit']; 
	if (empty($_GET['page'])) {
		$offset=0;
	} else {
		$offset=($_GET['page']-1)*$rec_limit;
	}

	$sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT p.Participant_ID, t.Title, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote 
			FROM participants AS p 
			LEFT JOIN ctbl_title AS t 
			ON p.Title_ID = t.Title_ID) AS p2
			WHERE';					
	$sql .= ' p2.Participant_id LIKE :participant_id';
	$sql .= ' AND p2.Title_ID LIKE :title_id';
	$sql .= ' AND p2.Lastname LIKE :lastname';
	$sql .= ' AND p2.Firstname LIKE :firstname';
	$sql .= ' AND p2.Mobile LIKE :mobile';
	$sql .= ' AND p2.Email LIKE :email';
	$sql .= ' ORDER BY p2.Participant_ID';
	$sql .= ' LIMIT :offset, :rec_limit';
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':participant_id', $participant_id);
	$stmt->bindValue(':title_id', $title_id);
	$stmt->bindValue(':Lastname', $Lastname);
	$stmt->bindValue(':Firstname', $Firstname);
	$stmt->bindValue(':Mobile', $Mobile);
	$stmt->bindValue(':Email', $Email);			
	$stmt->bindValue(':offset', (int) trim($offset), PDO::PARAM_INT);
	$stmt->bindValue(':rec_limit', (int) trim($rec_limit), PDO::PARAM_INT);										
	$stmt->execute();	
	$found_rows = $dbo->query('SELECT FOUND_ROWS()')->fetchColumn(0);									
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($participant_id=='%') {
		$participant_id="";
	}
	if ($title_id=='%') {
		$title_id="";
	}
	if ($lastname=='%') {
		$lastname="";
	}
	if ($firstname=='%') {
		$firstname="";
	}
	if ($mobile=='%') {
		$mobile="";
	}
	if ($email=='%') {
		$email="";
	}
	
	$filter_row = "Filter (Participant ID=" . $participant_id . ", Title=" . $event_id . ", Last Name=" . $lastname . ", First Name=" . $firstname . ", Mobile=" . $mobile . ", Email=" . $email . ")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Record/s: " . $found_rows;
	
	if ($row_count==0) {
		$appointment_row = "<tr><td colspan='10'>No participant</td></tr>";	
	} else {	
		foreach ($result as $row){				
			$participant_id_row = "<tr><td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "'>" . $row['Participant_ID'] . "</a></td>";		
			$title_row = "<td>" . $row['Title'] . "</td>";
			$lastname_row = "<td>" . $row['Lastname'] . "</td>";
			$firstname_row = "<td>" . $row['Firstname'] . "</td>";
			$mobile_row = "<td>" . $row['Mobile'] . "</td>";
			$email_row = "<td>" . $row['Email'] . "</td>";
			$address_row = "<td>" . $row['Address1'] . "</td>";
			$postcode_row = "<td>" . $row['Postcode'] . "</td>";
									
			//For note field with hover tip
			if ($row['Note'] != null) {
				$note_row = "<td><div style='cursor:help;clear:both;' title='" . $note . "'>&#10004;</div></td>";	
			} else {
				$note_row = "<td></td>";
			}
			$link_edit = "<td><a href='participant_edit.php?participant_id=" . $row['Participant_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a></td></tr>";
			$participant_row = $participant_row . $participant_id_row . $title_row . $lastname_row . $firstname_row . $mobile_row . $email_row . $address_row . $postcode_row . $note_row . $link_edit;			
		}									
	}	
	
	//Pagination ------------------------------------------------
	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$event_id = (empty($_GET['event_id'])) ? '%' : $_GET['event_id']; 
	$apptstatus_id = (empty($_GET['apptstatus_id'])) ? '%' : $_GET['apptstatus_id']; 
	$location_id = (empty($_GET['location_id'])) ? '%' : $_GET['location_id']; 										
	$date_from=$_GET['date_from_y'] . "-" . $_GET['date_from_m'] . "-" . $_GET['date_from_d'];
	$date_to=$_GET['date_to_y'] . "-" . $_GET['date_to_m'] . "-" . $_GET['date_to_d'];											
	if ($date_from=='--') {
		$date_from = null;
	}
	if ($date_to=='--') {
		$date_to = null;
	}
	if (empty($date_from)) {
		$date_from = "1900-01-01";
	}
	if (empty($date_to)) {
		$date_to = "3000-01-01";
	}										
	$sql = 'SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, E.Event, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.ApptStatus_ID, S.ApptStatus, A.Note  
			FROM appointments AS A 
			LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
			LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
			LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID
			WHERE A.ApptStatus_ID = 1 AND	
			A.Appointment_Date >= :appointment_datetime';					
		$sql .= ' AND A.Participant_id LIKE :participant_id';
		$sql .= ' AND A.Event_ID LIKE :event_id';
		$sql .= ' AND A.ApptStatus_ID LIKE :apptstatus_id';
		$sql .= ' AND A.Location_ID LIKE :location_id';
		$sql .= ' AND A.Appointment_Date BETWEEN :date_from AND :date_to';
		$sql .= ' ORDER BY A.Appointment_Date, A.Appointment_Time, A.Participant_id, A.Event_ID';
		$stmt = $dbo->prepare($sql);
		$stmt->bindValue(':appointment_datetime', date('Y-m-d'));
		$stmt->bindValue(':participant_id', $participant_id);
		$stmt->bindValue(':event_id', $event_id);
		$stmt->bindValue(':apptstatus_id', $apptstatus_id);
		$stmt->bindValue(':location_id', $location_id);
		$stmt->bindValue(':date_from', $date_from);
		$stmt->bindValue(':date_to', $date_to);									
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
		$pagination_row = "No appointment. Please contact administrator.";
	} 
	else {	
		$total_pages = ceil($row_count/$rec_limit);
		for ($i=1; $i<=$total_pages; $i++) { 
			if ($i==1 && empty($_GET['page'])) {
				$pagination_row = $pagination_row . "<strong>".$i."</strong>&nbsp;"; 
			}
			elseif ($i==$_GET['page']) {
				$pagination_row = $pagination_row . "<strong>".$i."</strong>&nbsp;"; 
			}
			else {
				$pagination_row = $pagination_row . "<a href='appointment_upcoming.php?participant_id=".$participant_id.
									"&event_id=".$event_id.
									"&apptstatus_id=".$apptstatus_id.
									"&location_id=".$location_id.
									"&date_from_d=".$_GET['date_from_d'].
									"&date_from_m=".$_GET['date_from_m'].
									"&date_from_y=".$_GET['date_from_y'].
									"&date_to_d=".$_GET['date_to_d'].
									"&date_to_m=".$_GET['date_to_m'].
									"&date_to_y=".$_GET['date_to_y'].
									"&page=".$i."&rec_limit=".$rec_limit."'>".$i."</a>&nbsp;"; 
			}
		}
	}
	//Pagination ------------------------------------------------			
	$dbo = null; //Close DB connection						
?>

	<h1 class='title'>Participant List</h1>
	<p class='title'>A list of all participants</p>
		<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header-participant.php"); ?>
		<br><br>
		<table class='searchfilters'>
			<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="get">
			<tr><th align='left' colspan=8>
				Search Filters
			</td></tr>
			<tr>				
				<td align="right" width='11%'>Participant ID:</td>
				<td align="left" width='18%'>
					<input name="participant_id" type="text" size="10" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : '' ?>"></input>
				</td>
				<td align='right' width='7%'>Title:</td>
				<td align='left' width='15%'>
					<select name="title_id"><?php echo $title_id_row; ?></select>
				</td>
				<td align="right" width='11%'>Last Name:</td>
				<td align="left" width='18%'>
					<input name="lastname" type="text" size="20" value="<?php echo isset($_POST['lastname']) ? $_POST['lastname'] : '' ?>"></input>
				</td>
				<td align="right" width='11%'>First Name:</td>
				<td align="left" width='18%'>
					<input name="firstname" type="text" size="20" value="<?php echo isset($_POST['firstname']) ? $_POST['firstname'] : '' ?>"></input>
				</td>
			</tr>
			<tr>					
				<td align="right">Mobile:</td>
				<td align="left">
					<input name="mobile" type="text" size="10" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : '' ?>"></input>
				</td>
				<td align="right">Email:</td>
				<td align="left">
					<input name="email" type="text" size="20" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>"></input>
				</td>
				<td align="right" colspan=3>
					</form>
				</td>
				<td align="right">
					<table>
						<tr>
							<td>
								<input name="filter" type="hidden" value="1"></input>
								<input type="submit" value="Filter"></input>
								</form>
							</td>
							<td>
								<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
								<input name="clearall" type="hidden" value="1"></input>
								<input type="submit" value="Clear All"></input>
							</form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table><br>
				
	<table class='participantinfo'>
		<tr>
			<th colspan='10'>
				<?php echo $filter_row ?>
			</th></tr>
		<tr>
			<td align='left' valign='top' bgcolor="#cdcdcd">PID</td>									
			<td align='left' valign='top' bgcolor="#cdcdcd">Title</td>
			<td align='left' valign='top' bgcolor="#cdcdcd">Last Name</td>		
			<td align='left' valign='top' bgcolor="#cdcdcd">First Name</td>	
			<td align='left' valign='top' bgcolor="#cdcdcd">Mobile</td>	
			<td align='left' valign='top' bgcolor="#cdcdcd">Email</td>	
			<td align='left' valign='top' bgcolor="#cdcdcd">Address</td>	
			<td align='left' valign='top' bgcolor="#cdcdcd">Postal Code</td>		
			<td align='left' valign='top' bgcolor="#cdcdcd">PNote</td>
			<td align='left' valign='top' bgcolor="#cdcdcd"></td>		
		</tr>		
		<?php echo $participant_row; ?>
	</table>				
	<table width='960px'>
		<tr>
			<td align='left'>	
				<?php echo $pagination_row; ?> 
			</td>
			<td align='right'><a href='participant_list.php?participant_id=<?php echo $participant_id; ?>&event_id=<?php echo $event_id; ?>&apptstatus_id=<?php echo $apptstatus_id; ?>&location_id=<?php echo $location_id; ?>&date_from_d=<?php echo $_GET['date_from_d']; ?>&date_from_m=<?php echo $_GET['date_from_m']; ?>&date_from_y=<?php echo $_GET['date_from_y']; ?>&date_to_d=<?php echo $_GET['date_to_d']; ?>&date_to_m=<?php echo $_GET['date_to_m']; ?>&date_to_y=<?php echo $_GET['date_to_y']; ?>&filter=1&page=1&rec_limit=99999'>Show All</a>
			</td>
		</tr>
	</table>		
	<br><br>		
	

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>