<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

			<h1 class='title'>Send Email</h1>
			<p class='title'>Manually send email to participant</p>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$error = false;						
			$participant_id=$_POST['participant_id'];
			$appointment_id=$_POST['appointment_id'];
			$email=$_POST['email'];
			$email_type=$_POST['email_type'];
			$email_subject=$_POST['email_subject'];
			$email_text=$_POST['email_text'];
			$prev_url=$_POST['prev_url'];
			
			if ($email=="") {
				$error = true;
				$errormsg = $errormsg . "Mobile No cannot be empty<br>";
			} 
			
			if ($email_subject=="") {
				$error = true;
				$errormsg = $errormsg . "Email Subject cannot be empty<br>";
			} 
			
			if ($email_text=="") {
				$error = true;
				$errormsg = $errormsg . "Email Body cannot be empty<br>";
			} 
			
			if ($error === true) {
				echo $errormsg;
				echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
						<form action='send_email' method='post'>
						<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
						<input name='email' type='hidden' size='20' value='" . $email . "'></input>
						<input name='email_type' type='hidden' size='20' value='" . $email_type . "'></input>
						<input name='email_subject' type='hidden' size='20' value='" . $email_subject . "'></input>
						<input name='email_text' type='hidden' size='20' value='" . $email_text . "'></input>
						<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
						<input type='submit' value='Back'></input></form>";
			} else {	
				include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/SendEmail.php");
				$participant_id=$_POST['participant_id'];
				$email_result=gw_send_email($participant_id,$appointment_id,$email_type,$email_subject,$email_text);	
				print $email_result;
				print "<br><br><a href='" . $prev_url . "'>Back to where you came from</a>";	
				
			}
		} else {
			header("location:send_email.php");		
		}		
	?>		
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 