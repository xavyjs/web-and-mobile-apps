<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;						
		$participant_id=$_POST['participant_id'];
		$screener_id=$_POST['screener_id'];
		$scrstatus_id=$_POST['scrstatus_id'];
		$prev_url=$_POST['prev_url'];
		
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='participant_edit' method='post'>
					<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
					<input name='screener_id' type='hidden' size='20' value='" . $screener_id . "'></input>
					<input name='scrstatus_id' type='hidden' size='20' value='" . $scrstatus_id . "'></input>
					<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
					<input type='submit' value='Back'></input></form>";
		} else {					
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database		
											
			$stmt = $dbo->prepare("UPDATE screener SET ScrStatus_ID=? WHERE Screener_ID=?");
			$stmt->execute(array($scrstatus_id, $screener_id));						
			
			// If ($apptstatus_id==1||$apptstatus_id==4) {
				// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/EmailAppointment.php");
				// $email_id = 3; //Template 3: For updated appointment
				// EmailAppointment($participant_id,$event_id,$appointment_date,$appointment_time,$location_id,$email_id);
			// }	
			
			$stmt = $dbo->prepare('SELECT ScrStatus FROM ctbl_scrstatus WHERE ScrStatus_ID=:scrstatus_id'); //Create PStatus if Appointment Status = Attended
			$stmt->execute(array('scrstatus_id' => $scrstatus_id));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			
			if ($row_count==0) {
			} 
			else {
				foreach ($result as $row){
					$scrstatus=$row['ScrStatus'];
				}
			}
			$pstatus=$scrstatus;	
			$stmt = $dbo->prepare("INSERT INTO log_pstatus(Participant_ID,PStatus,Timestamp) VALUES(:participant_id,:pstatus,:timestamp)");
			$stmt->execute(array(':participant_id' => $participant_id,':pstatus' => $pstatus,':timestamp' => date("Y-m-d H:i:s")));

			
			$dbo = null; //Close DB connection			
			header("location:" . $prev_url);							
		}
	} else {
		header("location:screener_edit.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 