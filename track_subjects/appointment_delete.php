<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php PowerUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>
		
	<h1 class='title'>Appointment</h1>
	<p class='title'>Delete</p>
	
		<form action="appointment_delete_process.php" method="post">	
			<div align='center'>
				<table class="outcomebox">
				<tr>
					<th>
						Appointment (ID: <?php echo $_GET['appointment_id']; ?>) will be deleted.<br>Are you sure you want to proceed?
					</th>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td>
						<input name="appointment_id" size="20" type="hidden" value="<?php echo isset($_GET['appointment_id']) ? $_GET['appointment_id'] : '' ?>"></input>
						<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
						<input type="submit" value="Proceed"></input>
					</td>
				</table>
				<a href="<?php echo $_GET['prev_url']; ?>">Back to previous page</a>
			</div>
		</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>