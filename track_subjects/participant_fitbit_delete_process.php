<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php PowerUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>
		
	<h1 class='title'>Delete appointment</h1>
	<p class='title'>Deletion is permanent</p>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database
			
			$participant_id=$_POST['participant_id'];
			$prev_url=$_POST['prev_url'];
			
			$stmt = $dbo->prepare("UPDATE participants SET Fitbit_User_ID=?, access_token=?, access_token_secret=? WHERE Participant_ID=?");
			$stmt->execute(array(null, null, null, $participant_id));		
			
			$dbo = null; //Close DB connection
			print "Fitbit credentials deleted from Participant ID " . $participant_id . "<br><br>";
			print "<a href='" . $_POST['prev_url'] . "'>Back to previous page</a>";
		} else {
			header("location:appointment_delete.php");		
		}
	?>
	
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 