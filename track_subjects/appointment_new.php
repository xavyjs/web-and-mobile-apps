<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/library/DateTimePicker.php");  ?>

<?php //Retrieving Full Appointment Information
	$fitbit_start_date=date("d M Y");
	$appointment_date=date("d M Y");
	$appointment_time=date("h:i A");
	$appointment_id = $_GET['appointment_id'];
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM appointments 
							WHERE Appointment_ID=:appointment_id');
	$stmt->execute(array('appointment_id' => $appointment_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$participant_id = $row['Participant_ID'];
			$event_id = $row['Event_ID'];
			$fitbit_start_date = $row['Fitbit_Start_Date'];
			$appointment_date=$row['Appointment_Date'];
			$appointment_time = date("h:i A", strtotime($row['Appointment_Time']));
			$location_id = $row['Location_ID'];
			$apptstatus_id = $row['ApptStatus_ID'];
			$note = $row['Note'];
		}		
		if ($_GET['appt_type']=='today') {
			$appointment_date=date('d M Y',strtotime($appointment_date. ' +1 Month'));
			$event_id=$event_id+1;
		} else {
			$appointment_date=date('d M Y',strtotime($appointment_date));
		}	
	}						
	$dbo = null; //Close DB connection
?>

<?php //Retrieving Full Participant Information
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	if (!isset($participant_id)) {
		$participant_id = $_GET['participant_id'];
	}
	
	$stmt = $dbo->prepare('SELECT t.Title, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote FROM participants AS p LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID WHERE p.Participant_ID=:participant_id');
	$stmt->execute(array('participant_id' => $participant_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "Participant does not exist";
	} 
	else {
		foreach ($result as $row){
			$title = $row['Title'];
			$lastname = $row['Lastname'];
			$firstname = $row['Firstname'];
		}		
	}						
	$dbo = null; //Close DB connection
?>

<?php //Event dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_event');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "The database contains no event yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Event_ID']==$_REQUEST['event_id']) {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . " selected='selected'>" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			} elseif ($row['Event_ID']==$event_id) {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . " selected='selected'>" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			}
			else {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . ">" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			}
		}
	}
	$dbo = null; //Close DB connection
?>

<?php //Location dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_location');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no location yet";
	} else {
		foreach ($result as $row){
			if ($row['Location_ID']==$_POST['location_id']) {
				$location_id_row = $location_id_row . "<option value=" . $row['Location_ID'] . " selected='selected'>" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
			} else {
				$location_id_row = $location_id_row . "<option value=" . $row['Location_ID'] . ">" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
			}
		}
	}
	$dbo = null; //Close DB connection
?>
	
	<h1 class='title'>Appointment</h1>
	<p class='title'>New</p>
	
	<form action="appointment_new_process.php" method="post">
		<table  class='new'>
			<tr>
				<td align='right' width='30%'>Participant:</td>
				<td align='left' width='70%'>
					<?php echo "(<a href='participant_info.php?participant_id=" . $participant_id . "'>ID: " . $participant_id . "</a>) " .$title . " " . $lastname . " " . $firstname; ?> 
					<input name="participant_id" size="40" type="hidden" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $participant_id ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Event:</td>
				<td align='left'>			
					<select name="event_id">					
						<?php echo $event_id_row; ?>	
					</select>
				</td>
			</tr>
			<tr>
				<td align='right'>Fitbit Start Date:</td>
				<td align='left'>
					<input type="text" name="fitbit_start_date" id="datepicker" class="datepicker" readonly="readonly" value="<?php echo isset($_POST['fitbit_start_date']) ? $_POST['fitbit_start_date'] : '' ?><?php echo isset($fitbit_start_date) ? $fitbit_start_date : '' ?>"/>
				</td>
			</tr>
			<tr>
				<td align='right'>Appointment Date:</td>
				<td align='left'>
					<input type="text" name="appointment_date" id="datepicker2" class="datepicker" readonly="readonly" value="<?php echo isset($_POST['appointment_date']) ? $_POST['appointment_date'] : '' ?><?php echo isset($appointment_date) ? $appointment_date : '' ?>"/>
				</td>
			</tr>
			<tr>
				<td align='right'>Appointment Time:</td>
				<td align='left'>
					<input type="text" name="appointment_time" id="timepicker" readonly="readonly" value="<?php echo isset($_POST['appointment_time']) ? $_POST['appointment_time'] : '' ?><?php echo isset($appointment_time) ? $appointment_time : '' ?>"/>
				</td>
			</tr>
			<tr>
				<td align='right'>Location:</td>
				<td align='left'>			
					<select name="location_id">					
						<?php echo $location_id_row; ?>	
					</select>
				</td>
			</tr>
			<tr>
				<td><br><br><br></td>
				<td align='right'>
					<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
					<input type="submit" value="Create Appointment"></input>
				</td>
			</tr>
		</table>
	</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
