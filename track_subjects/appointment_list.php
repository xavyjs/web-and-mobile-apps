<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Get Event Name
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	$stmt = $dbo->prepare('SELECT Event FROM ctbl_event 
							WHERE Event_ID=:event_id');
	$stmt->execute(array('event_id' => $_REQUEST['event_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$event = $row['Event'];
		}
	}	
	$dbo = null; //Close DB connection
?>

<?php //Appointment Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	$stmt = $dbo->prepare('SELECT e.Event, l.Location, s.ApptStatus, a.* FROM appointments AS a 
							LEFT JOIN ctbl_event AS e ON a.Event_ID = e.Event_ID 
							LEFT JOIN ctbl_apptstatus AS s ON a.ApptStatus_id = s.ApptStatus_ID 
							LEFT JOIN ctbl_location AS l on a.Location_ID = l.Location_ID 
							WHERE a.Participant_ID=:participant_id AND a.Event_ID=:event_id 
							ORDER BY a.Appointment_ID');
	$stmt->execute(array('participant_id' => $_REQUEST['participant_id'], 'event_id' => $_REQUEST['event_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		//$event1_date = "<a href='appointment_new.php?participant_id=" . $_REQUEST['participant_id'] . "&event_id=1'>Make appointment</a>";
		echo "no appointment";
	} 
	else {
		foreach ($result as $row){
			$appointment_id = "<tr><td>" . $row['Appointment_ID'] . "</td>";
			$event = $row['Event'];
			$appointment_date = "<td>" . date("d M Y, D", strtotime($row['Appointment_Date'])) . "</td>";
			$appointment_time = "<td>" . date("h:i A", strtotime($row['Appointment_Time'])) . "</td>";
			$location = "<td>" . $row['Location'] . "</td>";
			$apptstatus = "<td>" . $row['ApptStatus'] . "</td>";
			$reply_chkbox = $row['Reply_chkbox'];
				//For Reply checkbox
				if ($reply_chkbox==1) {
					$reply_chkbox = "<td>&#10004;</td>";
				} else {
					$reply_chkbox = "<td></td>";
				}
				//For note field with hover tip
				if ($row['Note'] != null) {
					$note = "<td><div style='cursor:help;clear:both;' title='" . str_replace( "'","&#39;",$row['Note']) . "'>&#10004;</div></td>";	
				} else {
					$note = "<td></td>";
				}
			$timestamp = "<td>" . date("d-m-Y H:i", strtotime($row['Timestamp'])) . "</td>";
			$link_edit = "<td><a href='appointment_edit.php?appointment_id=" . $row['Appointment_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a> ";
			if (!isset($_SESSION['accesslvl']) || $_SESSION['accesslvl'] >= 3) { //Display Delete function if user access level is 3 or higher
				$link_delete = "<a href='appointment_delete.php?appointment_id=" . $row['Appointment_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Del</a>";
			} else {
				$link_delete = "";
			}
			$appointment_row = $appointment_row . $appointment_id . $appointment_date . $appointment_time . $location . $apptstatus . $reply_chkbox . $note . $timestamp . $link_edit . $link_delete . "</td></tr>";
		}
	}
		
	$dbo = null; //Close DB connection
?>
			
			<h1 class='title'>Appointment</h1>
			<p class='title'>List</p>
						
			<table class='wborder'>
				<tr>
					<th colspan=9><?php echo $event; ?></th>
				</tr>
				<tr>
					<td bgcolor='#e9e9e9' style='font-weight:bold;' width='10%'>Appt ID</td>
					<td bgcolor='#e9e9e9' style='font-weight:bold;' width='15%'>Appt Date</td>
					<td bgcolor='#e9e9e9' style='font-weight:bold;' width='10%'>Appt Time</td>
					<td bgcolor='#e9e9e9' style='font-weight:bold;' width='15%'>Location</td>
					<td bgcolor='#e9e9e9' style='font-weight:bold;' width='15%'>Status</td>
					<td bgcolor='#e9e9e9' style='font-weight:bold;' width='5%'>Reply?</td>
					<td bgcolor='#e9e9e9' style='font-weight:bold;' width='5%'>Note</td>
					<td bgcolor='#e9e9e9' style='font-weight:bold;' width='15%'>Timestamp</td>
					<td bgcolor='#e9e9e9' style='font-weight:bold;' width='10%'></td>	
				</tr>		
				<?php echo $appointment_row; ?>
			</table>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
