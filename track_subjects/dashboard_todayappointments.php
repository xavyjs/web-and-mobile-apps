<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="../css/style.css?v=2.14" type="text/css">
	<title>TAKSI</title>
</head>
<?php //Today's Appointment data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$event_id = (empty($_GET['event_id'])) ? '%' : $_GET['event_id']; 
	$apptstatus_id = (empty($_GET['apptstatus_id'])) ? '%' : $_GET['apptstatus_id']; 
	$location_id = (empty($_GET['location_id'])) ? '%' : $_GET['location_id']; 
	$appointment_date=$_GET['year'] . "-" . $_GET['month'] . "-" . $_GET['day'];
	if ($appointment_date=='--') {
		$appointment_date = null;
	}
	if (empty($appointment_date)) {
		$appointment_date = date('Y-m-d');
	}

	$rec_limit=(empty($_GET['rec_limit'])) ? '10' : $_GET['rec_limit']; 
	if (empty($_GET['page'])) {
		$offset=0;
	} else {
		$offset=($_GET['page']-1)*$rec_limit;
	}

	$sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, E.Event, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.ApptStatus_ID, S.ApptStatus, A.Note  
			FROM appointments AS A 
			LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
			LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
			LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID) AS A2
			WHERE (A2.ApptStatus_ID = 1 OR A2.ApptStatus_ID = 2) AND	
			A2.Appointment_Date = :appointment_date';					
	$sql .= ' AND A2.Participant_id LIKE :participant_id';
	$sql .= ' ORDER BY A2.Appointment_Date, A2.Appointment_Time, A2.Participant_id, A2.Event_ID';
	$sql .= ' LIMIT :offset, :rec_limit';
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':appointment_date', $appointment_date);
	$stmt->bindValue(':participant_id', $participant_id);
	$stmt->bindValue(':offset', (int) trim($offset), PDO::PARAM_INT);
	$stmt->bindValue(':rec_limit', (int) trim($rec_limit), PDO::PARAM_INT);										
	$stmt->execute();	
	$found_rows = $dbo->query('SELECT FOUND_ROWS()')->fetchColumn(0);									
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($participant_id=='%') {
		$participant_id="";
	}
	if ($location_id=='%') {
		$location_id="";
	}
	
	$todayappt_header = "<img src='/css/images/todayappointments.png'>Today's Appointments";
	
	if ($row_count==0) {
		$todayappt_row = "<tr><td colspan='7'>No appointment</td></tr>";	
	} else {	
		foreach ($result as $row){		
			
			$participant_id_row = "<tr><td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "' target='_parent'>" . $row['Participant_ID'] . "</a></td>";		
			$event_row = "<td>" . $row['Event'] . "</td>";
			if ($row['Appointment_Time'] != null) {
				$appointment_time_row = "<td>" . date("h:i A", strtotime($row['Appointment_Time'])) . "</td>";
			} else {
				$appointment_time_row = "<td></td>";
			}	
			$location_row = "<td>" . $row['Location'] . "</td>";					
			
			//--------------------------Add code to determine previous appointment status
			$apptstatus_loop=$row['ApptStatus'];
			if (!empty($row['ApptStatus'])) {
				$stmt2 = $dbo->prepare('SELECT A.Appointment_ID, A.Participant_id, A.Event_ID, A.Appointment_Date, A.Appointment_Time, A.ApptStatus_ID, S.ApptStatus
								FROM appointments AS A
								LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID 
								WHERE A.Participant_id= :participant_id AND
								A.Appointment_ID!= :appointment_id AND															
								A.Event_ID= :event_id AND
								(Appointment_Date < :appointment_date OR (Appointment_Date = :appointment_date AND Appointment_Time < :appointment_time)) AND
								(A.ApptStatus_ID IS NOT NULL AND A.ApptStatus_ID!=0)
								ORDER BY A.Event_ID, A.Appointment_Date DESC, A.Appointment_Time DESC');
				$stmt2->execute(array('participant_id' => $row['Participant_ID'], 'appointment_id' => $row['Appointment_ID'], 'event_id' => $row['Event_ID'], 'appointment_date' => $row['Appointment_Date'], 'appointment_time' => $row['Appointment_Time']));
				$row_count2 = $stmt2->rowCount();
				$result2 = $stmt2->fetchAll();	
				if ($row_count2==0) {
				} 
				else {	
					foreach ($result2 as $row2){	
						$apptstatus_loop=$row2['ApptStatus'][0] . "," . $apptstatus_loop;
					}
				}
			}		
			//--------------------------Add code to determine previous appointment status
							
			$apptstatus_loop_row = "<td>" . $apptstatus_loop . "</td>";
			$apptstatus_row= "<td>" . $row['ApptStatus'] . "</td>";
					
			//For note field with hover tip 
			if ($row['Note'] != null) {
				$note_row = "<td><div style='cursor:help;clear:both;' title='" . str_replace( "'","&#39;",$row['Note']) . "'>&#10004;</div></td>";	
			} else {
				$note_row = "<td></td>";
			}
			$link_edit = "<td><a href='appointment_update.php?appointment_id=" . $row['Appointment_ID'] . "&appt_type=today&prev_url=" . urlencode('/index') . "' target='_parent'>Edit</a></td></tr>";
			$todayappt_row = $todayappt_row . $participant_id_row . $event_row . $appointment_time_row . $location_row . $apptstatus_row . $note_row . $link_edit;			
		}									
	}	
	
	//Pagination ------------------------------------------------
	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	$event_id = (empty($_GET['event_id'])) ? '%' : $_GET['event_id']; 
	$apptstatus_id = (empty($_GET['apptstatus_id'])) ? '%' : $_GET['apptstatus_id']; 
	$location_id = (empty($_GET['location_id'])) ? '%' : $_GET['location_id']; 
	$appointment_date=$_GET['year'] . "-" . $_GET['month'] . "-" . $_GET['day'];
	
	$sql = 'SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, E.Event, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.ApptStatus_ID, S.ApptStatus, A.Note  
			FROM appointments AS A 
			LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
			LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
			LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID
			WHERE (A.ApptStatus_ID = 1 OR A.ApptStatus_ID = 2) AND	
			A.Appointment_Date = :appointment_date';					
		$sql .= ' AND A.Participant_id LIKE :participant_id';
		$sql .= ' ORDER BY A.Appointment_Date, A.Appointment_Time, A.Participant_id, A.Event_ID';
		$stmt = $dbo->prepare($sql);
		$stmt->bindValue(':appointment_date', $appointment_date);
		$stmt->bindValue(':participant_id', $participant_id);						
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
		$missedappt_pagination = "";
	} 
	else {	
		$total_pages = ceil($row_count/$rec_limit);
		for ($i=1; $i<=$total_pages; $i++) { 
			if ($i==1 && empty($_GET['page'])) {
				$todayappt_pagination = $todayappt_pagination . "<strong>".$i."</strong>&nbsp;"; 
			}
			elseif ($i==$_GET['page']) {
				$todayappt_pagination = $todayappt_pagination . "<strong>".$i."</strong>&nbsp;"; 
			}
			else {
				$todayappt_pagination = $todayappt_pagination . "<a href='dashboard_todayappointments.php?&year=".$_GET['year'].
									"&month=".$_GET['month'].
									"&day=".$_GET['day'].
									"&page=".$i."&rec_limit=".$rec_limit."'>".$i."</a>&nbsp;"; 
			}
		}
	}
	//Pagination ------------------------------------------------			
	$dbo = null; //Close DB connection						
?>
<body id='iframe'>
	<table class='dashboard_left'>	
		<tr>
			<th class='th_header' colspan='7'>
				<?php echo $todayappt_header ?>
			</th>
		</tr>
		<tr>
			<th width='7%'>PID</th>
			<th width='37%'>Event</th>
			<th width='16%'>Time</th>
			<th width='18%'>Location</th>
			<th width='14%'>Status</th>
			<th width='4%'>Note</th>
			<th width='4%'> </th>
		</tr>
		<?php echo $todayappt_row; ?> 
	</table>
	<table width='460px'>
		<tr>
			<td align='left'>	
				<?php echo $todayappt_pagination; ?> 
			</td>
			<td align='right'>
			</td>
		</tr>
	</table>
</body>