<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;						
		$participant_id=$_POST['participant_id'];
		$fitbit_device=$_POST['fitbit_device'];
		$prev_url=$_POST['prev_url'];
		
		if ($fitbit_device==''){
			$fitbit_device=null;
		} else {
		
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database		
			
			$stmt = $dbo->prepare('SELECT Participant_ID FROM participants WHERE Fitbit_Device=?');
			$stmt->execute(array($fitbit_device));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			if ($row_count==0) {
			} 
			else {
				foreach ($result as $row){
					$errormsg= $errormsg . 'Duplicated fitbit device with Participant ID: <a href="participant_info.php?participant_id=' . $row['Participant_ID'] . '">' . $row['Participant_ID'] . '</a><br>';
				}
				$error = true;		
			}
			$dbo = null; //Close DB connection	
		
		}
		
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='participant_fitbit_device' method='post'>
					<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
					<input name='fitbit_device' type='hidden' size='20' value='" . $fitbit_device . "'></input>
					<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
					<input type='submit' value='Back'></input></form>";
		} else {					
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database		
											
			$stmt = $dbo->prepare("UPDATE participants SET Fitbit_Device=? WHERE Participant_ID=?");
			$stmt->execute(array($fitbit_device, $participant_id));						

			$dbo = null; //Close DB connection			
			header("location:" . $prev_url);							
		}
	} else {
		header("location:participant_fitbit_device.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 