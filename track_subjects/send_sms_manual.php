<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Retrieving Full Appointment Information
	$participant_aid = $_GET['participant_aid'];
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM participants WHERE Participant_AID=:participant_aid');
	$stmt->execute(array('participant_aid' => $participant_aid));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "Appointment does not exist";
	} 
	else {
		foreach ($result as $row){
			$participant_id = $row['Participant_ID'];
			$event_id = $row['Event_ID'];
			$title_id = $row['Title_ID'];
			$lastname = $row['Lastname'];
			$firstname = $row['Firstname'];
			$mobile = $row['Mobile'];
			$email = $row['Email'];
			$address1 = $row['Address1'];
			$postcode = $row['Postcode'];
			$pnote = $row['PNote'];
		}		
	}						
	$dbo = null; //Close DB connection
?>

	<h1 class='title'>SMS</h1>
	<p class='title'>Send Manual SMS to participant</p>
	<?php //echo $appointment_time_t; //To check variable ?>
	<form action="send_sms_manual_process.php" method="post">
		<table class='new'>
			<tr>
				<td align='right' width='30%'>Participant ID:</td>
				<td align='left' width='70%'>
					<input name="participant_aid2" size="40" type="text" disabled="disabled" value=<?php echo $participant_id; ?>></input>
					<input name="participant_aid" size="40" type="hidden" value="<?php echo isset($_POST['participant_aid']) ? $_POST['participant_aid'] : $_GET['participant_aid'] ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Mobile No.:</td>
				<td align='left'>
					<input name="mobile" type="text" size="40" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : $mobile ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Message:</td>
				<td align='left'>
					<textarea name='sms_text' rows='5' cols='42'><?php echo isset($_POST['sms_text']) ? $_POST['sms_text'] : '' ?></textarea>
				</td>
			</tr>
			<tr>
				<td><br><br><br></td>
				<td align='right'>
					<a href="send_sms.php?participant_aid=<?php echo $_GET['participant_aid']; ?>&sms_type=auto&prev_url=<?php echo urlencode($_GET['prev_url']); ?>">Send Template SMS</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input name="sms_type" size="40" type="hidden" value="manual"></input>
					<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
					<input type="submit" value="Send"></input>
				</td>
			</tr>
		</table>
	</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
