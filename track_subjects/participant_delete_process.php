<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php PowerUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>
		
	<h1 class='title'>Delete participant</h1>
	<p class='title'>Deletion is permanent</p>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database
			
			$participant_aid=$_POST['participant_aid'];
			$participant_id=$_POST['participant_id'];
			$prev_url=$_POST['prev_url'];
			
			$stmt = $dbo->prepare("DELETE FROM participants WHERE Participant_AID=?");
			$stmt->execute(array($participant_aid));
			
			$dbo = null; //Close DB connection
			print "Participant (ID: " . $participant_id . ") deleted<br><br>";
			print "<a href='" . $_POST['prev_url'] . "'>Back to previous page</a>";
		} else {
			header("location:participant_delete.php");		
		}
	?>
	
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 