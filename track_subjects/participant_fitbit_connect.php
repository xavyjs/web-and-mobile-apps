<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php 
	$_SESSION['fitbit_participant_id'] = $_GET['participant_id'];
	$_SESSION['fitbit_user_id'] = $_GET['fitbit_user_id'];
	$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['value']=null;	
	$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['secret']=null;	
	$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['authorized']=null;	
?>

	<h1 class='title'>Connecting to Fitbit</h1>
	<p class='title'>Establishing connection with www.fitbit.com<p>
	 <table class='dashboard_main'>
		<tr>
			<td width=100%>		
				<?php echo '<a href="participant_info?participant_id='.$_GET['participant_id'].'">Back to Participant Information page</a><br>'; ?>
				<?php echo 'Please wait while the page is loading: <br><br><br><br>'; ?>
				<iframe src="fitbit_connect.php?participant_id=<?php print $_GET['participant_id']; ?>&fitbit_user_id=<?php print $_GET['fitbit_user_id']; ?>&go=go" id="fitbit_iframe" scrolling="vertical"></iframe>
			</td>
		</tr>
	</table>
	
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>