<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>

<?php //Populate Adherence Data into table
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$days_monitored = 85;
	
	$stmt = $dbo->prepare("DELETE FROM log_adherence");
	$stmt->execute();
	
	$stmt = $dbo->prepare('SELECT * FROM participants WHERE Participant_ID IN (SELECT DISTINCT(Participant_Id) FROM import_ecap) ORDER BY Participant_ID');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$participant_aid=$row['Participant_AID'];
			$healthcare_cost_mth13 = $row['Healthcare_Cost_Mth13'];
			$healthcare_cost_mth46 = $row['Healthcare_Cost_Mth46'];
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/Cal_Adherence.php");
			list($mth13_adherence_days, $mth13_adherence_percent, $row_mth13_data, $mth46_adherence_days, $mth46_adherence_percent, $row_mth13_data) = cal_adherence($participant_aid);
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/Cal_Adherence.php");
			list($mth13_subsidy_percent,$mth46_subsidy_percent) = cal_subsidy_percent($participant_aid,$mth13_adherence_percent,$mth46_adherence_percent);
			
			$mth13_subsidy_amt = number_format($healthcare_cost_mth13,2)*$mth13_subsidy_percent;
			$mth46_subsidy_amt = number_format($healthcare_cost_mth46,2)*$mth46_subsidy_percent;
			
			$stmt2 = $dbo->prepare("INSERT INTO log_adherence(Participant_AID,Days_Monitored,Mth13_Adherence_Days,Mth13_Adherence_Percent,Mth13_Subsidy_Percent,Mth13_Subsidy_Amt,Mth46_Adherence_Days,Mth46_Adherence_Percent,Mth46_Subsidy_Percent,Mth46_Subsidy_Amt) VALUES(?,?,?,?,?,?,?,?,?,?)");
			$stmt2->execute(array($participant_aid,85,$mth13_adherence_days,$mth13_adherence_percent,$mth13_subsidy_percent,$mth13_subsidy_amt,$mth46_adherence_days,$mth46_adherence_percent,$mth46_subsidy_percent,$mth46_subsidy_amt));		
		}
	}						
	$dbo = null; //Close DB connection
?>	

<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT sa.Study_Arm, p.Participant_ID, a.Days_Monitored, a.Mth13_Adherence_Days, a.Mth13_Adherence_Percent, a.Mth13_Subsidy_Percent, p.Healthcare_Cost_Mth13, a.Mth13_Subsidy_Amt, a.Mth46_Adherence_Days, a.Mth46_Adherence_Percent, a.Mth46_Subsidy_Percent, p.Healthcare_Cost_Mth46, a.Mth46_Subsidy_Amt 
							FROM participants as p 
							LEFT JOIN log_adherence AS a ON p.Participant_AID = a.Participant_AID 
							LEFT JOIN ctbl_study_arm AS sa ON p.Study_Arm_ID = sa.Study_Arm_ID
							ORDER BY p.Study_Arm_ID, Participant_ID');
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$dbo = null; //Close DB connection

	include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/OutputCSV.php");
	download_send_headers("export_adherencereport_" . date("Ymd-His") . ".csv");
	echo outputcsv($result);
	die();
?>		

<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 