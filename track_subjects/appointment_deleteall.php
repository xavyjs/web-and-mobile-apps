<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php PowerUserCheck(); //Check legitimate session ?>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

			<form action="appointment_deleteall_process.php" method="post">
						
			<h1 class='title'>Delete all appointments</h1>
			<p class='title'>Are you sure you want to delete all appointments of this participant?</p>
			
			<table id="tbl_registration">
			<tr>
				<td></td>
				<td>
					<input name="appt_id" size="20" type="hidden" value="<?php echo isset($_POST['appt_id']) ? $_POST['appt_id'] : '' ?>"></input>
					<input name="participant_id" size="20" type="hidden" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : '' ?>"></input>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Delete"></input>&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<a href="appointment.php?participant_id=<?php echo $_POST['participant_id'] ?>">Back to appointment page</a>
				</td>
			</tr>
			</table>
			
			</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>