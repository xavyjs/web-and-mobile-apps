<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="../css/style.css?v=2.14" type="text/css">
	<title>TAKSI</title>
</head>
<?php
/*
 * login_with_fitbit.php
 *
 * @(#) $Id: login_with_fitbit.php,v 1.2 2013/07/31 11:48:04 mlemos Exp $
 *
 */

	/*
	 *  Get the http.php file from http://www.phpclasses.org/httpclient
	 */
	require('http.php');
	require('oauth_client.php');

	$client = new oauth_client_class;
	$client->debug = 1;
	$client->debug_http = 1;
	$client->server = 'Fitbit';
	$client->redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].
		dirname(strtok($_SERVER['REQUEST_URI'],'?')).'/fitbit_connect.php';

	$client->client_id = '2e5313cfc3d44c3e9c20d1d724fdac7a'; $application_line = __LINE__; // Fitbit Consumer Key from dev.fitbit.com | Account: edmund.shen@duke-nus.edu.sg
	$client->client_secret = '4ab38cf55bdb43e9b4cb61be02217421'; // Fitbit Consumer Secret

	if(strlen($client->client_id) == 0
	|| strlen($client->client_secret) == 0)
		die('Please go to Fitbit application registration page https://dev.fitbit.com/apps/new , '.
			'create an application, and in the line '.$application_line.
			' set the client_id to Consumer key and client_secret with Consumer secret. '.
			'The Callback URL must be '.$client->redirect_uri).' Make sure this URL is '.
			'not in a private network and accessible to the Fitbit site.';

	if(($success = $client->Initialize()))
	{
		if(($success = $client->Process()))
		{
			if(strlen($client->access_token))
			{
				$success = $client->CallAPI(
					'https://api.fitbit.com/1/user/-/profile.json', 
					'GET', array(), array('FailOnAccessError'=>true), $user);
					
				include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
				dbConnect(); // Connect to Database		
				$stmt = $dbo->prepare('SELECT * FROM participants WHERE Fitbit_User_ID=:fitbit_user_id AND Participant_ID<>:participant_id'); // Check if the logged in Fitbit User ID exists in other participants
				$stmt->execute(array('fitbit_user_id' => $user->user->encodedId,'participant_id' => $_SESSION['fitbit_participant_id']));
				$row_count = $stmt->rowCount();
				$result = $stmt->fetchAll();
				if ($row_count==0) {	
				} else {	
					foreach ($result as $row){
						echo '<h1>Error connecting to Fitbit</h1>';
						echo "<br><br>Fitbit User ID (" . $user->user->encodedId . ") is already linked with Participant ID (" . $row['Participant_ID'] . ")";
						exit;
					}
				}
				$dbo = null; //Close DB connection					
					
				$success2 = $client->CallAPI(
					'https://api.fitbit.com/1/user/-/activities/steps/date/2013-09-01/today.json',
					'GET', array(), array('FailOnAccessError'=>true), $user2);
				$success3 = $client->CallAPI(
					'https://api.fitbit.com/1/user/-/apiSubscriptions/'.$_SESSION['fitbit_participant_id'].'.json',
					'POST', array(), array('FailOnAccessError'=>true), $user3);	
				$success4 = $client->CallAPI(
					'https://api.fitbit.com/1/user/-/apiSubscriptions.json',
					'GET', array(), array('FailOnAccessError'=>true), $user4);	
				// $result_subscribedel = json_decode($fitbitService->requestdelete('user/-/apiSubscriptions/'.$_SESSION['fitbit_participant_id'].'.json'));
				
			}
		}
		$success = $client->Finalize($success);
	}	
	
	if($client->exit)
		exit;
	if($success) {	
		file_put_contents("log/fitbit_updateparticipant.log",date("dmYHi")."|".$user->user->encodedId."|".$client->access_token."|".$client->access_token_secret."|".$_SESSION['fitbit_participant_id']."\r\n",FILE_APPEND);
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database
		$stmt = $dbo->prepare("UPDATE participants SET Fitbit_User_ID=?, access_token=?, access_token_secret=? WHERE Participant_ID=?");
		$stmt->execute(array($user->user->encodedId, $client->access_token, $client->access_token_secret, $_SESSION['fitbit_participant_id']));							
		$dbo = null; //Close DB connection		
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Fitbit OAuth client results</title>
</head>
<body>
<?php
		echo '<h1>', HtmlSpecialChars($user->user->displayName), 
			', you have connected successfully with Fitbit!</h1>';
		echo 'encodedId: ' . $user->user->encodedId;
		echo '<br>ParticipantID: ' . $_SESSION['fitbit_participant_id'];
		echo '<pre>';
		echo 'access token: ' . $client->access_token;
		echo '</pre>';
		echo '<pre>';
		echo 'access token secret: ' . $client->access_token_secret;
		echo '</pre>';
		echo '<pre>';
		echo $client->access_token_expiry;
		echo '</pre>';
		echo '<pre>';
		echo $client->access_token_type;
		echo '</pre>';
		
		echo 'Subscription List: <pre>';
		print_r($user4);
		echo '</pre>';	
		
		echo '<pre>', HtmlSpecialChars(print_r($user, 1)), '</pre>';
		echo '<pre>', HtmlSpecialChars(print_r($user2, 1)), '</pre>';
?>
</body>
</html>
<?php
	}
	else
	{
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>OAuth client error</title>
</head>
<body>
<h1>OAuth client error</h1>
<p>Error: <?php echo HtmlSpecialChars($client->error); ?></p>
</body>
</html>
<?php
	}
	$_SESSION['fitbit_participant_id'] = null;
?>