<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/comfort_header.php"); ?>

<?php //Title dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_title');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no title yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Title_ID']==$_POST['title_id']) {
				$title_id_row = $title_id_row . "<option value=" . $row['Title_ID'] . " selected='selected'>" . $row['Title'] . "</option>";
			} else {
				$title_id_row = $title_id_row . "<option value=" . $row['Title_ID'] . ">" . $row['Title'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>
		
	<h1 class='title'>Participant</h1>
	<p class='title'>Add New</p>
	<form action="comfort_participant_new_process.php" method="post">			
		<table class='new'>
		<tr>
			<td align='right' width='30%'>Title:</td>
			<td align='left' width='70%'>			
				<select name="title_id">					
					<?php echo $title_id_row; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td align='right'>Last Name (Surname):</td>
			<td align='left'>
				<input name="lastname" type="text" size="40" value="<?php echo isset($_POST['lastname']) ? $_POST['lastname'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>First Name:</td>
			<td align='left'>
				<input name="firstname" type="text" size="40" value="<?php echo isset($_POST['firstname']) ? $_POST['firstname'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>Mobile No:</td>
			<td align='left'>
				<input name="mobile" type="text" size="20" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'></td>
			<td align='left'>
				<br>
			</td>
		</tr>
		<tr>
			<td align='right'>Email:</td>
			<td align='left'>
				<input name="email" type="email" size="40" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'></td>
			<td align='left'>
				OR (Required for Fitbit Account)
			</td>
		</tr>
		<tr>
			<td align='right'>Residential Address:</td>
			<td align='left'>
				<input name="address" type="text" size="40" value="<?php echo isset($_POST['address']) ? $_POST['address'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>Postal Code:</td>
			<td align='left'>
				<input name="postcode" type="text" size="40" value="<?php echo isset($_POST['postcode']) ? $_POST['postcode'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td><br><br><br></td>
			<td align='right'>
				<input type="submit" value="Create Participant"></input>
			</td>
		</tr>
		</table>
	</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
