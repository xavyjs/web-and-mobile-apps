<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once("../resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php				
	$dbo = dbConnect(); // Connect to Database	
	$stmt = $dbo->prepare('SELECT Participant_UID, Title, Firstname, Lastname, Tel, Mobile, Email, Address1, Address2, Address3, Postcode, Note
						FROM participants 
						ORDER BY Participant_UID');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC); // To only retrieve array with associative keys and not number indices		
	include_once("../resources/library/OutputCSV.php");
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=".basename(__FILE__, '.php').".csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	outputCSV($result, array_keys($result[0])); // Pass records and array keys (field names) into output csv function
?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?>