<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php PowerUserCheck(); //Check legitimate session ?>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

			<form action="lottery_clearall_process.php" method="post">
						
			<h1 class='title'>Clear all lottery winners</h1>
			<p class='title'>Are you sure you want to clear all lottery winners of this event?</p>
			
			<table id="tbl_registration">
			<tr>
				<td></td>
				<td>
					<input name="event_id" size="20" type="hidden" value="<?php echo isset($_POST['event_id']) ? $_POST['event_id'] : $_GET['event_id'] ?>"></input>
					<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Clear All"></input>&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<a href="lottery.php">Back to lottery page</a>
				</td>
			</tr>
			</table>
			
			</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>