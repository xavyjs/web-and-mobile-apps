<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;						
		$participant_aid=$_POST['participant_aid'];
		$participant_id=$_POST['participant_id'];
		$age=$_POST['age'];
		$race_id=$_POST['race_id'];
		$gender_id=$_POST['gender_id'];
		$mobile=$_POST['mobile'];
		$study_arm_id=$_POST['study_arm_id'];
		$pstatus_id=$_POST['pstatus_id'];
		$num_daily_doses=$_POST['num_daily_doses'];
		$interval_1to2=$_POST['interval_1to2'];
		$interval_2to3=$_POST['interval_2to3'];
		$interval_3to4=$_POST['interval_3to4'];
		$interval_4to5=$_POST['interval_4to5'];
		$interval_5to6=$_POST['interval_5to6'];
		$interval_6to7=$_POST['interval_6to7'];
		$interval_7to8=$_POST['interval_7to8'];
		$baseline_assessment_date=date('Y-m-d',strtotime($_POST['baseline_assessment_date']));
		$month3_assessment_date=date('Y-m-d',strtotime($_POST['month3_assessment_date']));
		$month6_assessment_date=date('Y-m-d',strtotime($_POST['month6_assessment_date']));
		$healthcare_cost_mth13=$_POST['healthcare_cost_mth13'];
		$healthcare_cost_mth46=$_POST['healthcare_cost_mth46'];
		$pnote=$_POST['pnote'];
		$prev_url=$_POST['prev_url'];

		if ($participant_id=="") {
			$error = true;
			$errormsg = $errormsg . "Participant ID cannot be empty<br>";
		} 
		
		if ($age=="") {
			$error = true;
			$errormsg = $errormsg . "Age cannot be empty<br>";
		} 
		
		if (is_numeric($age)===false & $age!="") {
			$error = true;
			$errormsg = $errormsg . "Age must be a number<br>";
		} 
		
		if ($mobile=="") {
			$error = true;
			$errormsg = $errormsg . "Mobile No cannot be empty<br>";
		} 
		
		if ((is_numeric($mobile)===false || strlen($mobile)!=8) && $mobile!="") {
			$error = true;
			$errormsg = $errormsg . "Mobile No must be 8 numbers<br>";
		} 
		
		if ($num_daily_doses=="") {
			$error = true;
			$errormsg = $errormsg . "No. of Daily Doses cannot be empty<br>";
		} 
		
		if ($num_daily_doses==2 && ($interval_1to2=="" || $interval_2to3!="" || $interval_3to4!="" || $interval_4to5!="" || $interval_5to6!="" || $interval_6to7!="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==3 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4!="" || $interval_4to5!="" || $interval_5to6!="" || $interval_6to7!="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==4 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4=="" || $interval_4to5!="" || $interval_5to6!="" || $interval_6to7!="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==5 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4=="" || $interval_4to5=="" || $interval_5to6!="" || $interval_6to7!="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==6 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4=="" || $interval_4to5=="" || $interval_5to6=="" || $interval_6to7!="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==7 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4=="" || $interval_4to5=="" || $interval_5to6=="" || $interval_6to7=="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==8 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4=="" || $interval_4to5=="" || $interval_5to6=="" || $interval_6to7=="" || $interval_7to8=="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		
		if ($_POST['baseline_assessment_date']=="") {
			$error = true;
			$errormsg = $errormsg . "Date of Baseline Assessment cannot be empty<br>";
		} 
		
		if ($_POST['month3_assessment_date']=="") {
			$error = true;
			$errormsg = $errormsg . "Date of Month 3 Assessment cannot be empty<br>";
		} 
		
		if ($_POST['month6_assessment_date']=="") {
			$error = true;
			$errormsg = $errormsg . "Date of Month 6 Assessment cannot be empty<br>";
		} 
		
		if (is_numeric($healthcare_cost_mth13)===false && $healthcare_cost_mth13!="") {
			$error = true;
			$errormsg = $errormsg . "Healthcare Cost Month 1-3 must be a number<br>";
		} 
		
		if (is_numeric($healthcare_cost_mth46)===false && $healthcare_cost_mth46!="") {
			$error = true;
			$errormsg = $errormsg . "Healthcare Cost Month 4-6 must be a number<br>";
		}
		
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='participant_edit' method='post'>
					<input name='participant_aid' type='hidden' size='20' value='" . $participant_aid . "'></input>
					<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
					<input name='age' type='hidden' size='20' value='" . $age . "'></input>
					<input name='race_id' type='hidden' size='20' value='" . $race_id . "'></input>
					<input name='gender_id' type='hidden' size='20' value='" . $gender_id . "'></input>
					<input name='mobile' type='hidden' size='20' value='" . $mobile . "'></input>
					<input name='study_arm_id' type='hidden' size='20' value='" . $study_arm_id . "'></input>
					<input name='pstatus_id' type='hidden' size='20' value='" . $pstatus_id . "'></input>
					<input name='num_daily_doses' type='hidden' size='20' value='" . $num_daily_doses . "'></input>
					<input name='interval_1to2' type='hidden' size='20' value='" . $interval_1to2 . "'></input>
					<input name='interval_2to3' type='hidden' size='20' value='" . $interval_2to3 . "'></input>
					<input name='interval_3to4' type='hidden' size='20' value='" . $interval_3to4 . "'></input>
					<input name='interval_4to5' type='hidden' size='20' value='" . $interval_4to5 . "'></input>
					<input name='interval_5to6' type='hidden' size='20' value='" . $interval_5to6 . "'></input>
					<input name='interval_6to7' type='hidden' size='20' value='" . $interval_6to7 . "'></input>
					<input name='interval_7to8' type='hidden' size='20' value='" . $interval_7to8 . "'></input>
					<input name='baseline_assessment_date' type='hidden' size='20' value='" . $_POST['baseline_assessment_date'] . "'></input>
					<input name='month3_assessment_date' type='hidden' size='20' value='" . $_POST['month3_assessment_date'] . "'></input>
					<input name='month6_assessment_date' type='hidden' size='20' value='" . $_POST['month6_assessment_date'] . "'></input>
					<input name='healthcare_cost_mth13' type='hidden' size='20' value='" . $healthcare_cost_mth13 . "'></input>
					<input name='healthcare_cost_mth46' type='hidden' size='20' value='" . $healthcare_cost_mth46 . "'></input>
					<input name='pnote' type='hidden' size='20' value='" . $pnote . "'></input>
					<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
					<input type='submit' value='Back'></input></form>";
		} else {					
		
			// if ($fitbit_user_id=="") {
				// $fitbit_user_id = null;
			// } 
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database		
											
			$stmt = $dbo->prepare("UPDATE participants SET Participant_ID=?, Age=?, Race_ID=?, Gender_ID=?, Mobile=?, Study_Arm_ID=?, PStatus_ID=?, Num_Daily_Doses=?, Interval_1to2=?, Interval_2to3=?, Interval_3to4=?, Interval_4to5=?, Interval_5to6=?, Interval_6to7=?, Interval_7to8=?, Baseline_Assessment_Date=?, Month3_Assessment_Date=?, Month6_Assessment_Date=?, Healthcare_Cost_Mth13=?, Healthcare_Cost_Mth46=?, PNote=? WHERE Participant_AID=?");
			$stmt->execute(array($participant_id, $age, $race_id, $gender_id, $mobile, $study_arm_id, $pstatus_id, $num_daily_doses, $interval_1to2, $interval_2to3, $interval_3to4, $interval_4to5, $interval_5to6, $interval_6to7, $interval_7to8, $baseline_assessment_date, $month3_assessment_date, $month6_assessment_date, $healthcare_cost_mth13, $healthcare_cost_mth46, $pnote, $participant_aid));						
			
			// If ($apptstatus_id==1||$apptstatus_id==4) {
				// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/EmailAppointment.php");
				// $email_id = 3; //Template 3: For updated appointment
				// EmailAppointment($participant_id,$event_id,$appointment_date,$appointment_time,$location_id,$email_id);
			// }	
			
			$dbo = null; //Close DB connection			
			header("location:" . $prev_url);							
		}
	} else {
		header("location:participant_edit.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 