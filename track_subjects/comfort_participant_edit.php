<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/comfort_header.php"); ?>

<?php //Retrieving Full Appointment Information
	$participant_id = $_GET['participant_id'];
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM participants WHERE Participant_ID=:participant_id');
	$stmt->execute(array('participant_id' => $participant_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$event_id = $row['Event_ID'];
			$incentive_arm_id = $row['Incentive_Arm_ID'];
			$title_id = $row['Title_ID'];
			$lastname = $row['Lastname'];
			$firstname = $row['Firstname'];
			$mobile = $row['Mobile'];
			$email = $row['Email'];
			$address1 = $row['Address1'];
			$postcode = $row['Postcode'];
			$pnote = $row['PNote'];
			$daily_rental = $row['Daily_Rental'];
		}		
	}						
	$dbo = null; //Close DB connection
?>

<?php //Incentive Arm dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	 
	$stmt = $dbo->prepare('SELECT * FROM ctbl_incentive_arm');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no incentive arm yet";
	} 
	else {
		$incentive_arm_id_row = $incentive_arm_id_row . "<option value='0'>Select one:</option>";
		foreach ($result as $row){
			if ($row['Incentive_Arm_ID']==(isset($_POST['incentive_arm_id']) ? $_POST['incentive_arm_id'] : $incentive_arm_id)) {
				$incentive_arm_id_row = $incentive_arm_id_row . "<option value=" . $row['Incentive_Arm_ID'] . " selected='selected'>" . $row['Incentive_Arm'] . "</option>";
			} else {
				$incentive_arm_id_row = $incentive_arm_id_row . "<option value=" . $row['Incentive_Arm_ID'] . ">" . $row['Incentive_Arm'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Title dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_title');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no title yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Title_ID']==(isset($_POST['title_id']) ? $_POST['title_id'] : $title_id)) {
				$title_id_row = $title_id_row . "<option value=" . $row['Title_ID'] . " selected='selected'>" . $row['Title'] . "</option>";
			} else {
				$title_id_row = $title_id_row . "<option value=" . $row['Title_ID'] . ">" . $row['Title'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

	<h1 class='title'>Participant</h1>
	<p class='title'>Edit</p>
	<?php //echo $appointment_time_t; //To check variable ?>
	<form action="comfort_participant_edit_process.php" method="post">
		<table class='new'>
			<tr>
				<td align='right' width='30%'>Participant ID:</td>
				<td align='left' width='70%'>
					<input name="participant_id2" size="40" type="text" disabled="disabled" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $participant_id ?>"></input>
					<input name="participant_id" size="40" type="hidden" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $participant_id ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Title:</td>
				<td align='left'>			
					<select name="title_id">					
						<?php echo $title_id_row; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td align='right'>Last Name (Surname):</td>
				<td align='left'>
					<input name="lastname" type="text" size="40" value="<?php echo isset($_POST['lastname']) ? htmlspecialchars($_POST['lastname']) : htmlspecialchars($lastname) ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>First Name:</td>
				<td align='left'>
					<input name="firstname" type="text" size="40" value="<?php echo isset($_POST['firstname']) ? htmlspecialchars($_POST['firstname']) : htmlspecialchars($firstname) ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Mobile No.:</td>
				<td align='left'>
					<input name="mobile" type="text" size="40" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : $mobile ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Email:</td>
				<td align='left'>
					<input name="email" type="text" size="40" value="<?php echo isset($_POST['email']) ? $_POST['email'] : $email ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Address:</td>
				<td align='left'>
					<input name="address1" type="text" size="40" value="<?php echo isset($_POST['address1']) ? $_POST['address1'] : $address1 ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Postal Code:</td>
				<td align='left'>
					<input name="postcode" type="text" size="40" value="<?php echo isset($_POST['postcode']) ? $_POST['postcode'] : $postcode ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>PNote:</td>
				<td align='left'>
					<textarea name='pnote' rows='5' cols='42'><?php echo isset($_POST['pnote']) ? $_POST['pnote'] : $pnote ?></textarea>
				</td>
			</tr>
			<tr>
				<td align='right'>Daily Rental (S$):</td>
				<td align='left'>
					<input name="daily_rental" type="text" size="40" value="<?php echo isset($_POST['daily_rental']) ? $_POST['daily_rental'] : $daily_rental ?>"></input>
				</td>
			</tr>
			<tr>
				<td><br><br><br></td>
				<td align='right'>
					<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
					<input type="submit" value="Update"></input>
				</td>
			</tr>
		</table>
	</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
