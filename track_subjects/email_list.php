<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Screener Questionnaire Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT u.Username, e.* FROM log_email AS e 
							LEFT JOIN users AS u ON e.UserID=u.UserID
							WHERE e.Email=:email');
	$stmt->execute(array('email' => $_REQUEST['email']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$userid = "<tr><td align='left'>" . $row['Username'] . "</td>";
			$timestamp = "<td align='left'>" . date("d-m-Y H:i", strtotime($row['Timestamp'])) . "</td>";
			$subject = "<td align='left'>" . $row['Email_Subject'] . "</td>";
			$body = "<td align='left'>" . $row['Email_Body'] . "</td></tr>";
			$sendreceive_row = $sendreceive_row . $userid . $timestamp . $subject . $body;
		}
	}						
	//Close DB connection
	$dbo = null;
?>
			
			<h1 class='title'>Email</h1>
			<p class='title'>List of emails sent to participant</p>
						
			<table class='participantinfo'>
				<tr>
					<th align='left' valign='top' bgcolor="#cdcdcd" colspan='3'>Email Sent</th>
					<td align='right' bgcolor="#cdcdcd" style='border:none;'></td>
				</tr>
				<tr>
					<td align='left' valign='top' width='13%' bgcolor="#cdcdcd">Send By</td>									
					<td align='left' valign='top' width='13%' bgcolor="#cdcdcd">Timestamp</td>
					<td align='left' valign='top' width='25%' bgcolor="#cdcdcd">Subject</td>	
					<td align='left' valign='top' width='49%' bgcolor="#cdcdcd">Body</td>							
				</tr>		
				<?php echo $sendreceive_row; ?>
			</table>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
