<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Retrieving Full Appointment Information
	$participant_id = $_GET['participant_id'];
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM screener
							WHERE Participant_ID=:participant_id ORDER BY Screener_ID');
	$stmt->execute(array('participant_id' => $participant_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "Screener does not exist";
	} 
	else {
		foreach ($result as $row){
			$screener_id = $row['Screener_ID'];
			$scrstatus_id = $row['ScrStatus_ID'];
		}		
	}						
	$dbo = null; //Close DB connection
?>

<?php //Title dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_scrstatus');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no ScrStatus yet";
	} 
	else {
		$scrstatus_id_row = "<option value=''></option>";
		foreach ($result as $row){
			if ($row['ScrStatus_ID']==(isset($_POST['scrstatus_id']) ? $_POST['scrstatus_id'] : $scrstatus_id)) {
				$scrstatus_id_row = $scrstatus_id_row . "<option value=" . $row['ScrStatus_ID'] . " selected='selected'>" . $row['ScrStatus'] . "</option>";
			} else {
				$scrstatus_id_row = $scrstatus_id_row . "<option value=" . $row['ScrStatus_ID'] . ">" . $row['ScrStatus'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

	<h1 class='title'>Screener Questionnaire</h1>
	<p class='title'>Edit</p>
	<?php //echo $appointment_time_t; //To check variable ?>
	<form action="screener_edit_process.php" method="post">
		<table class='new'>
			<tr>
				<td align='right' width='30%'>Participant ID:</td>
				<td align='left' width='70%'>
					<input name="participant_id2" size="40" type="text" disabled="disabled" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $participant_id ?>"></input>
					<input name="participant_id" size="40" type="hidden" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $participant_id ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Status:</td>
				<td align='left'>			
					<select name="scrstatus_id">					
						<?php echo $scrstatus_id_row; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td><br><br><br></td>
				<td align='right'>
					<input name="screener_id" size="40" type="hidden" value="<?php echo isset($_POST['screener_id']) ? $_POST['screener_id'] : $screener_id ?>"></input>
					<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
					<input type="submit" value="Update"></input>
				</td>
			</tr>
		</table>
	</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
