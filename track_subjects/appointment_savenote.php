<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

		<?php
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
											
					$participant_id=$_POST['participant_id'];
					$pnote=$_POST['participant_note'];

					include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
					dbConnect(); // Connect to Database							
				
					$stmt = $dbo->prepare("UPDATE participants SET PNote=? WHERE Participant_UID=?");
					$stmt->execute(array($pnote, $participant_id));
					
					echo "Appointment updated";	
					
					//Close DB connection
					$dbo = null;
					
					//if $_POST['prev_url'] is empty, redirect to appointment.php page
					$prev_url = (empty($_POST['prev_url'])) ? 'appointment.php?participant_id=' . $participant_id . '&status=Note%20saved': $_POST['prev_url']; 	
					header("location:" . $prev_url);																	
				}				
		?>		
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 