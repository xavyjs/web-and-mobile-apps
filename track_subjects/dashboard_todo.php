<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="../css/style.css?v=2.14" type="text/css">
	<title>TAKSI</title>
</head>

<?php //SMS Reply data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	

	$rec_limit=(empty($_GET['rec_limit'])) ? '10' : $_GET['rec_limit']; 
	if (empty($_GET['page'])) {
		$offset=0;
	} else {
		$offset=($_GET['page']-1)*$rec_limit;
	}

	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT * FROM log_todo) AS t			
				WHERE t.Dismissed <> 'y'";		
	$sql .= " ORDER BY t.Timestamp_auto DESC";
	$sql .= " LIMIT :offset, :rec_limit";
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':offset', (int) trim($offset), PDO::PARAM_INT);
	$stmt->bindValue(':rec_limit', (int) trim($rec_limit), PDO::PARAM_INT);										
	$stmt->execute();	
	$found_rows = $dbo->query('SELECT FOUND_ROWS()')->fetchColumn(0);									
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
		
	$todo_header = "<img src='/css/images/todo.png'>To-Do List";
	
	if ($row_count==0) {
		$todolist_row = "<tr><td colspan='5'>No To-Do</td></tr>";	
	} else {	
		foreach ($result as $row){	
			$participant_id_row = "<tr><td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "' target='_parent'>" . $row['Participant_ID'] . "</a></td>";		
			$appointment_id_row = "<td>" . $row['Appointment_ID'] . "</td>";		
			//For note field with hover tip
			if (strlen($row['ToDo'])>35) {
				$todo = "<div style='cursor:help;clear:both;' title='" . str_replace( "'","&#39;",$row['ToDo']) . "'>".substr($row['ToDo'],0,32)."...</div>";	
			} else {
				$todo = $row['ToDo'];
			}
			
			$todo_row = "<td>" . $todo . "</td>";					
			$timestamp_row = "<td>" . date("d-m-Y H:i", strtotime($row['Timestamp_auto'])) . "</td>";	
			$link_dismiss = "<td><a href='todo_dismiss.php?log_todo_id=" . $row['log_ToDo_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Dismiss</a></td></tr>";
			$todolist_row = $todolist_row . $participant_id_row . $appointment_id_row . $todo_row . $timestamp_row . $link_dismiss;			
		}									
	}	
	
	//Pagination ------------------------------------------------
	$participant_id = (empty($_GET['participant_id'])) ? '%' : $_GET['participant_id']; 	
	
	$sql = "SELECT * FROM log_todo AS t 
			WHERE t.Dismissed <> 'y'";					
	$sql .= " ORDER BY t.Timestamp_auto DESC";
	$stmt = $dbo->prepare($sql);					
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
		$todo_pagination = "";
	} 
	else {	
		$total_pages = ceil($row_count/$rec_limit);
		for ($i=1; $i<=$total_pages; $i++) { 
			if ($i==1 && empty($_GET['page'])) {
				$todo_pagination = $todo_pagination . "<strong>".$i."</strong>&nbsp;"; 
			}
			elseif ($i==$_GET['page']) {
				$todo_pagination = $todo_pagination . "<strong>".$i."</strong>&nbsp;"; 
			}
			else {
				$todo_pagination = $todo_pagination . "<a href='dashboard_todo.php?&year=".$_GET['year'].
									"&month=".$_GET['month'].
									"&day=".$_GET['day'].
									"&page=".$i."&rec_limit=".$rec_limit."'>".$i."</a>&nbsp;"; 
			}
		}
	}
	//Pagination ------------------------------------------------			
	$dbo = null; //Close DB connection						
?>

<body id='iframe'>
	<table class='dashboard_left'>	
		<tr>
			<th class='th_header' colspan='5'>
				<?php echo $todo_header ?>
			</th>
		</tr>
		<tr>
			<th width='6%'>PID</th>
			<th width='10%'>AID</th>
			<th width='46%'>To-Do</th>
			<th width='24%'>Timestamp</th>
			<th width='14%'><a href='todo_dismissall.php?prev_url=<?php print urlencode($_SERVER["REQUEST_URI"]) ?>' target='_parent'>Dism All</a></th>
		</tr>
		<?php echo $todolist_row; ?> 
	</table>
	<table width='460px'>
		<tr>
			<td align='left'>	
				<?php echo $todo_pagination; ?> 
			</td>
			<td align='right'>
			</td>
		</tr>
	</table>
</body>