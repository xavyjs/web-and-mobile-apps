<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

			<h1 class='title'>Calendar</h1>
			<p class='title'>Upcoming appointments<p>
				<a href='appointment_toupdate.php' class="classname">Appointment to be Updated</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href='appointment_booked.php' class="classname">Appointment Booked</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href='appointment_toschedule.php' class="classname">Appointment to be Scheduled</a>
				<br><br><br>
				<?php
						include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
						dbConnect(); // Connect to Database									
						
								$stmt = $dbo->prepare('SELECT A.Appointment_ID, A.Participant_UID, A.Event_ID, E.Event, E.Event_Week, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.Appointment_Status, S.ApptStatus, A.Weight, A.BodyFat, A.Note 
														FROM appointments AS A 
														LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
														LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
														LEFT JOIN ctbl_apptstatus AS S ON A.Appointment_Status = S.ApptStatus_ID 
														WHERE A.Appointment_Date= :appointment_date
														ORDER BY A.Participant_UID, A.Event_ID, A.Appointment_ID');
								$stmt->execute(array('appointment_date' => date('Y-m-d')));
								$row_count = $stmt->rowCount();
								$result = $stmt->fetchAll();
								
								echo "<table class='wborder'>	
										<tr bgcolor='#f29966' style='font-weight:bold;'>
											<td colspan='9'>
												Today
											</td></tr>
										<tr>
											<th>PID</th>
											<th>Week</th>
											<th>Event</th>
											<th>Date (dd-mm-yyyy)</th>
											<th>Time</th>
											<th>Location</th>
											<th>Appt Status</th>
											<th>Note</th>
											<th></th></tr>";
								
								if ($row_count==0) {
									echo "<tr><td colspan='9'>
													No appointment
										</td></tr>";									
								} 
								else {	
								
									foreach ($result as $row){
															
										$appt_id=$row['Appointment_ID'];	
										$participant_id=$row['Participant_UID'];	
										$appt_event_id=$row['Event_ID'];								
										$appt_event=$row['Event'];
										$appt_event_week=$row['Event_Week'];
										$appt_date=$row['Appointment_Date'];
										$appt_time=$row['Appointment_Time'];
										$appt_location_id=$row['Location_ID'];
										$appt_location=$row['Location'];
										$Appointment_Status=$row['Appointment_Status'];
										$appt_status=$row['ApptStatus'];
										$weight=$row['Weight'];
										$bodyfat=$row['BodyFat'];
										$note=$row['Note'];
										
										echo "<tr><td><a href='../appointment.php?participant_id=" . $participant_id . "'>" . $participant_id . "</a>";									
										echo "</td><td>" . $appt_event_week ;
										echo "</td><td>" . $appt_event; 
										echo "</td><td>" . date("d-m-Y", strtotime($appt_date)); 
										echo "</td><td>" . date("h:i A", strtotime($appt_time)); 
										echo "</td><td>" . $appt_location; 
										echo "</td><td>" . $appt_status; 
										
										//For note field with hover tip
										echo "</td><td>";
										if ($note != null) {
											echo "<div style='cursor: help;' title='" . $note . "'>&#10004;</div>"; 
										}
										
										echo "</td><td><form action='appointment_edit' method='post'><input name='appt_id' type='hidden' size='20' value='" . $appt_id . "'></input><input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input><input name='appt_event_id' type='hidden' size='20' value='" . $appt_event_id . "'></input></input><input name='appt_location_id' type='hidden' size='20' value='" . $appt_location_id . "'></input></input><input name='Appointment_Status' type='hidden' size='20' value='" . $Appointment_Status . "'></input><input name='weight' type='hidden' size='20' value='" . $weight . "'></input><input name='bodyfat' type='hidden' size='20' value='" . $bodyfat . "'></input><input type='submit' value='Edit'></input></form></td></tr>";
									}								
								}
								echo "</table><br><br>";

								$stmt = $dbo->prepare('SELECT A.Appointment_ID, A.Participant_UID, A.Event_ID, E.Event, E.Event_Week, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.Appointment_Status, S.ApptStatus, A.Weight, A.BodyFat, A.Note 
														FROM appointments AS A 
														LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
														LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
														LEFT JOIN ctbl_apptstatus AS S ON A.Appointment_Status = S.ApptStatus_ID 
														WHERE A.Appointment_Date BETWEEN :today AND :threedays
														ORDER BY A.Participant_UID, A.Event_ID, A.Appointment_ID');
								$stmt->execute(array('today' => date('Y-m-d', strtotime("+1 days")), 'threedays' => date('Y-m-d', strtotime("+3 days"))));
								$row_count = $stmt->rowCount();
								$result = $stmt->fetchAll();
								
									echo "<table class='wborder'>	
										<tr bgcolor='#ffdf85' style='font-weight:bold;'>
											<td colspan='9'>
												Next 3 days
											</td>
										</tr>
										<tr>
											<th>PID</th>
											<th>Week</th>
											<th>Event</th>
											<th>Date (dd-mm-yyyy)</th>
											<th>Time</th>
											<th>Location</th>
											<th>Appt Status</th>
											<th>Note</th>
											<th></th></tr>";
								
								if ($row_count==0) {
									echo "<tr><td colspan='9'>
													No appointment
										</td></tr>";	
								} 
								else {	
								
									foreach ($result as $row){
															
										$appt_id=$row['Appointment_ID'];	
										$participant_id=$row['Participant_UID'];	
										$appt_event_id=$row['Event_ID'];								
										$appt_event=$row['Event'];
										$appt_event_week=$row['Event_Week'];
										$appt_date=$row['Appointment_Date'];
										$appt_time=$row['Appointment_Time'];
										$appt_location_id=$row['Location_ID'];
										$appt_location=$row['Location'];
										$Appointment_Status=$row['Appointment_Status'];
										$appt_status=$row['ApptStatus'];
										$weight=$row['Weight'];
										$bodyfat=$row['BodyFat'];
										$note=$row['Note'];
										
										echo "<tr><td><a href='../appointment.php?participant_id=" . $participant_id . "'>" . $participant_id . "</a>";										
										echo "</td><td>" . $appt_event_week ;
										echo "</td><td>" . $appt_event; 
										echo "</td><td>" . date("d-m-Y", strtotime($appt_date)); 
										echo "</td><td>" . date("h:i A", strtotime($appt_time)); 
										echo "</td><td>" . $appt_location; 
										echo "</td><td>" . $appt_status; 
										
										//For note field with hover tip
										echo "</td><td>";
										if ($note != null) {
											echo "<div style='cursor: help;' title='" . $note . "'>&#10004;</div>"; 
										}
										
										echo "</td><td><form action='appointment_edit' method='post'><input name='appt_id' type='hidden' size='20' value='" . $appt_id . "'></input><input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input><input name='appt_event_id' type='hidden' size='20' value='" . $appt_event_id . "'></input></input><input name='appt_location_id' type='hidden' size='20' value='" . $appt_location_id . "'></input></input><input name='Appointment_Status' type='hidden' size='20' value='" . $Appointment_Status . "'></input><input name='weight' type='hidden' size='20' value='" . $weight . "'></input><input name='bodyfat' type='hidden' size='20' value='" . $bodyfat . "'></input><input type='submit' value='Edit'></input></form></td></tr>";
									}									
								}			
								echo "</table><br><br><br>";
								
					//Close DB connection
					$dbo = null;
				?>		
				
				<table class='searchfilters'>
					<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
					<tr><td colspan=5 style="border-bottom:1px solid; font-weight:bold;">
						Search Appointments
					</td></tr>
					<tr><td>
						&nbsp;
					</td></tr>
					<tr>					
						<td>
							Date From:
						</td>
						<td align="left">
							<select name="date_from_d">
							<?php
								$i = 1;
								if (isset($_POST['date_from_d'])) {
									$var_date_d = $_POST['date_from_d'];
								} else {
									$var_date_d = date("d");
								}
								while ($i<=31) {
									if ($i==$var_date_d) {
										echo "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
									}
									$i++;
								}
							?>
							</select>
							<select name="date_from_m">
							<?php
								$i = 1;
								if (isset($_POST['date_from_m'])) {
									$var_date_m = $_POST['date_from_m'];
								} else {
									$var_date_m = date("n");
								}
								while ($i<=12) {
									if ($i==$var_date_m) {
										echo "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
									}
									$i++;
								}
							?>
							</select>
							<select name="date_from_y">
							<?php
								$i = date("Y")-1;
								if (isset($_POST['date_from_y'])) {
									$var_date_y = $_POST['date_from_y'];
								} else {
									$var_date_y = date("Y");
								}
								while ($i<=date("Y")+2) {
									if ($i==$var_date_y) {
										echo "<option value=" . $i . " selected='selected'>" . $i . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . $i . "</option>";
									}
									$i++;
								}
							?>
							</select>
						</td>
						<td>
							Date To:
						</td>
						<td align="left">
							<select name="date_to_d">
							<?php
								$i = 1;
								if (isset($_POST['date_to_d'])) {
									$var_date_d = $_POST['date_to_d'];
								} else {
									$var_date_d = date("d");
								}
								while ($i<=31) {
									if ($i==$var_date_d) {
										echo "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
									}
									$i++;
								}
							?>
							</select>
							<select name="date_to_m">
							<?php
								$i = 1;
								if (isset($_POST['date_to_m'])) {
									$var_date_m = $_POST['date_to_m'];
								} else {
									$var_date_m = date("n");
								}
								while ($i<=12) {
									if ($i==$var_date_m) {
										echo "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
									}
									$i++;
								}
							?>
							</select>
							<select name="date_to_y">
							<?php
								$i = date("Y")-1;
								if (isset($_POST['date_to_y'])) {
									$var_date_y = $_POST['date_to_y'];
								} else {
									$var_date_y = date("Y");
								}
								while ($i<=date("Y")+2) {
									if ($i==$var_date_y) {
										echo "<option value=" . $i . " selected='selected'>" . $i . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . $i . "</option>";
									}
									$i++;
								}
							?>
							</select>
						</td>
						<td>
							<input type="submit" value="Search"></input>
							</form>
						</td></tr></table><br>
	
	
				<?php
					if ($_SERVER["REQUEST_METHOD"] == "POST") {
						include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
						dbConnect(); // Connect to Database
						
						$date_from=$_POST['date_from_y'] . "-" . $_POST['date_from_m'] . "-" . $_POST['date_from_d'];
						$date_to=$_POST['date_to_y'] . "-" . $_POST['date_to_m'] . "-" . $_POST['date_to_d'];
						
						$stmt = $dbo->prepare('SELECT A.Appointment_ID, A.Participant_UID, A.Event_ID, E.Event, E.Event_Week, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.Appointment_Status, S.ApptStatus, A.Weight, A.BodyFat, A.Note 
														FROM appointments AS A 
														LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
														LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
														LEFT JOIN ctbl_apptstatus AS S ON A.Appointment_Status = S.ApptStatus_ID 
														WHERE A.Appointment_Date BETWEEN :date_from AND :date_to
														ORDER BY A.Participant_UID, A.Event_ID, A.Appointment_ID');
								$stmt->execute(array('date_from' => $date_from, 'date_to' => $date_to));
								$row_count = $stmt->rowCount();
								$result = $stmt->fetchAll();
								
									echo "<table class='wborder'>	
										<tr>
											<th colspan='9'>
												" . date("d-m-Y", strtotime($date_from)) . " to " . date("d-m-Y", strtotime($date_to)) . "
											</th></tr>
										<tr>
											<th>PID</th>
											<th>Week</th>
											<th>Event</th>
											<th>Date (dd-mm-yyyy)</th>
											<th>Time</th>
											<th>Location</th>
											<th>Appt Status</th>
											<th>Note</th>
											<th></th></tr>";
								
								if ($row_count==0) {
									echo "<tr><td colspan='9'>
													No appointment
										</td></tr>";	
								} 
								else {	
								
									foreach ($result as $row){
															
										$appt_id=$row['Appointment_ID'];	
										$participant_id=$row['Participant_UID'];	
										$appt_event_id=$row['Event_ID'];								
										$appt_event=$row['Event'];
										$appt_event_week=$row['Event_Week'];
										$appt_date=$row['Appointment_Date'];
										$appt_time=$row['Appointment_Time'];
										$appt_location_id=$row['Location_ID'];
										$appt_location=$row['Location'];
										$Appointment_Status=$row['Appointment_Status'];
										$appt_status=$row['ApptStatus'];
										$weight=$row['Weight'];
										$bodyfat=$row['BodyFat'];
										$note=$row['Note'];
										
										if ($appt_date . " " . $appt_time < date('Y-m-d H:i:s') && strpos($appt_status,'M,Missed') !== false) {
											echo "<tr style='background-color: #d95252;'>";
										} 
										elseif ($appt_date < date('Y-m-d')) {
											echo "<tr style='color:#999'>";
										}
										elseif ($appt_date > date('Y-m-d') && $appt_date <= date('Y-m-d', strtotime("+3 days"))) {
											echo "<tr style='background-color: #ffdf85;'>";
										} 
										elseif ($appt_date == date('Y-m-d')) {
											echo "<tr style='background-color: #f29966;'>";
										} 
										else {
											echo "<tr>";
										}
										
										echo "<td><a href='../appointment.php?participant_id=" . $participant_id . "'>" . $participant_id . "</a>";										
										echo "</td><td>" . $appt_event_week ;
										echo "</td><td>" . $appt_event; 
										echo "</td><td>" . date("d-m-Y", strtotime($appt_date)); 
										echo "</td><td>" . date("h:i A", strtotime($appt_time)); 
										echo "</td><td>" . $appt_location; 
										echo "</td><td>" . $appt_status; 
										
										//For note field with hover tip
										echo "</td><td>";
										if ($note != null) {
											echo "<div style='cursor: help;' title='" . $note . "'>&#10004;</div>"; 
										}
										
										echo "</td><td><form action='appointment_edit' method='post'><input name='appt_id' type='hidden' size='20' value='" . $appt_id . "'></input><input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input><input name='appt_event_id' type='hidden' size='20' value='" . $appt_event_id . "'></input></input><input name='appt_location_id' type='hidden' size='20' value='" . $appt_location_id . "'></input></input><input name='Appointment_Status' type='hidden' size='20' value='" . $Appointment_Status . "'></input><input name='weight' type='hidden' size='20' value='" . $weight . "'></input><input name='bodyfat' type='hidden' size='20' value='" . $bodyfat . "'></input><input type='submit' value='Edit'></input></form></td></tr>";
									}									
								}			
								echo "</table><br><br>";
								echo "<table align='right'>
									<tr style='font-weight:bold;'><td colspan='2'>Appointment Legend</td></tr>	
									<tr><td style='background-color: #d95252;' width='20px'></td><td>2 consecutive misses</td></tr>
									<tr><td style='background-color: #f29966;' width='20px'></td><td>Today</td></tr>
									<tr><td style='background-color: #ffdf85;' width='20px'></td><td>Next 3 days</td></tr>		
									</table>";
									echo "<div style='clear: both'></div>";
								
					//Close DB connection
					$dbo = null;
					}
				?>		

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>