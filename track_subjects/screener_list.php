<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Screener Questionnaire Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM screener WHERE Participant_ID=:participant_id ORDER BY Screener_ID');
	$stmt->execute(array('participant_id' => $_GET['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		$screener_id = "No screener questionnaire available";
	} 
	else {
		foreach ($result as $row){
			$screener_id = "<tr><td align='left'><a href='screener_view.php?participant_id=" . $_GET['participant_id'] . "&screener_id=" . $row['Screener_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>" . $row['Screener_ID'] . "</a></td>";
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/Eligibility.php");
			$eligibility = cal_eligibility($row['Screener_ID']);
			$eligibility_outcome = "<td align='left'>" . $eligibility[0] . "</td>";
			$eligibility_reason = "<td align='left'>" . $eligibility[1] . "</td>";
			$eligibility_timestamp = "<td align='left'>" . date("d-m-Y H:i", strtotime($row['Timestamp'])) . "</td></tr>";
			$eligibility_row = $eligibility_row . $screener_id . $eligibility_outcome . $eligibility_reason . $eligibility_timestamp;
		}
	}						
	//Close DB connection
	$dbo = null;
?>
			
			<h1 class='title'>Screener Questionnaire</h1>
			<p class='title'>List</p>
						
			<table class='participantinfo'>
				<tr>
					<th align='left' valign='top' bgcolor="#cdcdcd" colspan='3'>Screener Questionnaire</th>
					<td align='right' bgcolor="#cdcdcd" style='border:none;'></td>
				</tr>
				<tr>
					<td align='left' valign='top' width='12%' bgcolor="#cdcdcd">Screener ID</td>									
					<td align='left' valign='top' width='20%' bgcolor="#cdcdcd">Eligibility</td>
					<td align='left' valign='top' width='50%' bgcolor="#cdcdcd">Reason/s</td>		
					<td align='left' valign='top' width='18%' bgcolor="#cdcdcd">Timestamp</td>					
				</tr>		
				<?php echo $eligibility_row; ?>
			</table>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
