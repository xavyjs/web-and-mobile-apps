<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	// $email_txt = "http://" . $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI]; //to test for autoreply url settings
	// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/SendEmail.php");
	// gw_send_email('1','','manual','reply url',$email_txt);	

	$error = false;			
	$log_sms_reply_id=$_GET['log_sms_reply_id'];
	
	if ($log_sms_reply_id=="") {
		$error = true;
	} 
	
	if ($error === true) {
	} else {					
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
		
		$stmt = $dbo->prepare("UPDATE log_sms_reply SET Dismissed='y' WHERE log_SMS_Reply_ID=?");
		$stmt->execute(array($log_sms_reply_id));	
	}
	$dbo = null; //Close DB connection		
	if (isset($_GET['prev_url']) && $_GET['prev_url']<>"") {
		header("location:" . $_GET['prev_url']);	
		//echo "prev:".$_POST['prev_url'];						
	} else {
		header("location:/index.php");
		//echo "index";
	}
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 