<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="../css/style.css?v=2.14" type="text/css">
	<title>TAKSI</title>
</head>
<?php
/*
 * login_with_fitbit.php
 *
 * @(#) $Id: login_with_fitbit.php,v 1.2 2013/07/31 11:48:04 mlemos Exp $
 *
 */

	/*
	 *  Get the http.php file from http://www.phpclasses.org/httpclient
	 */
	require('http.php');
	require('oauth_client.php');

	$client = new oauth_client_class;
	$client->debug = 1;
	$client->debug_http = 1;
	$client->server = 'Fitbit';
	$client->redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].
		dirname(strtok($_SERVER['REQUEST_URI'],'?')).'/fitbit_connectcheck.php';

	$client->client_id = '2e5313cfc3d44c3e9c20d1d724fdac7a'; $application_line = __LINE__; // Fitbit Consumer Key from dev.fitbit.com | Account: edmund.shen@duke-nus.edu.sg
	$client->client_secret = '4ab38cf55bdb43e9b4cb61be02217421'; // Fitbit Consumer Secret

	if(strlen($client->client_id) == 0
	|| strlen($client->client_secret) == 0)
		die('Please go to Fitbit application registration page https://dev.fitbit.com/apps/new , '.
			'create an application, and in the line '.$application_line.
			' set the client_id to Consumer key and client_secret with Consumer secret. '.
			'The Callback URL must be '.$client->redirect_uri).' Make sure this URL is '.
			'not in a private network and accessible to the Fitbit site.';

	if(($success = $client->Initialize()))
	{
		if(($success = $client->Process()))
		{
			if(strlen($client->access_token))
			{
				$success = $client->CallAPI(
					'https://api.fitbit.com/1/user/-/apiSubscriptions.json', // Check Subscription
					'GET', array(), array('FailOnAccessError'=>true), $user);	
				// $result_subscribedel = json_decode($fitbitService->requestdelete('user/-/apiSubscriptions/'.$_SESSION['fitbit_participant_id'].'.json'));
				
			}
		}
		$success = $client->Finalize($success);
	}
	if($client->exit)
		exit;
	if($success) {	
		if ($user->apiSubscriptions) {
			echo "Subscription exists";
		} else {
			echo "Subscription does not exist";
		}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Fitbit OAuth client results</title>
</head>
<body>
<?php
		echo '<h1>', HtmlSpecialChars($user->user->displayName), 
			' you have logged in successfully with Fitbit!</h1>';
		// echo 'encodedId: ' . $user->user->encodedId;
		// echo '<br>ParticipantID: ' . $_SESSION['fitbit_participant_id'];
		echo '<pre>';
		echo 'access token: ' . $client->access_token;
		echo '</pre>';
		echo '<pre>';
		echo 'access token secret: ' . $client->access_token_secret;
		echo '</pre>';
		echo '<pre>';
		echo $client->access_token_expiry;
		echo '</pre>';
		echo '<pre>';
		echo $client->access_token_type;
		echo '</pre>';
		
		echo 'Subscription List: <pre>';
		print_r($user);
		echo '</pre>';	
		
		echo '<pre>', HtmlSpecialChars(print_r($user, 1)), '</pre>';
?>
</body>
</html>
<?php
	}
	else
	{
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>OAuth client error</title>
</head>
<body>
<h1>OAuth client error</h1>
<p>Error: <?php echo HtmlSpecialChars($client->error); ?></p>
</body>
</html>
<?php
	}
	$_SESSION['fitbit_participant_id'] = null;
	$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['value']=null;	
	$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['secret']=null;	
	$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['authorized']=null;	
?>