<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>

<?php 
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$error = false;
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database
			
			//Check if Username is empty
			if (empty($_POST['loginuser'])) {
				$error = true;
				$errormsg = $errormsg . "Username is empty <br>";
			}					
			//Check if Username exists
			else {											
				$stmt = $dbo->prepare('SELECT * FROM users WHERE Username= :username');
				$stmt->execute(array('username' => $_POST['loginuser']));
				$row_count = $stmt->rowCount();
				if (empty($row_count)) {
					$error = true;
					$errormsg = $errormsg . "Username does not exist <br>";
				}						
			}
			
			//Check if Password is empty
			if (empty($_POST['loginpass'])) {
				$error = true;
				$errormsg = $errormsg . "Password is empty <br>";
			}				
			
			//Check if Password is at least 7 characters long
			if (strlen($_POST['loginpass']) < 7) {
				$error = true;
				$errormsg = $errormsg . "Password must be at least 7 characters long <br>";
			}				
			
			//Close DB connection
			$dbo = null;
						
			if ($error === true) {
				//echo $errormsg;
			} else {

				//DB connection string
				include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
				dbConnect(); // Connect to Database
				
				require("resources/library/PasswordHash.php");	
				$hasher = new PasswordHash($hash_cost_log2, $hash_portable);						
				
				$loginuser=$_POST['loginuser'];
				$loginpass=$_POST['loginpass'];
				
				$hash = '*'; // In case the user is not found
				($stmt = $dbo->prepare('select * from users where Username=:username'));
				$stmt->bindParam(':username', $loginuser, PDO::PARAM_STR);
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				//print_r($result);
				$hash = $result[0]['Password'];

				if ($hasher->CheckPassword($loginpass, $hash)) {
					$what = 'Authentication succeeded';
					
					$stmt2 = $dbo->prepare("INSERT INTO log_login(UserID,Username,ip_address) VALUES(:UserID,:Username,:ip_address)");
					$stmt2->execute(array(':UserID' => $result[0]['UserID'], ':Username' => $result[0]['Username'], ':ip_address' => $_SERVER['REMOTE_ADDR']));
					
					session_start();			
					$_SESSION['userid'] = $result[0]['UserID'];		
					$_SESSION['username'] = $result[0]['Username'];
					$_SESSION['email'] = $result[0]['Email'];
					$_SESSION['accesslvl'] = $result[0]['AccessLvl'];
					$_SESSION['loggedin'] = true;
					$_SESSION['signature'] = md5($result[0]['Username'] . $_SERVER['HTTP_USER_AGENT'] . $salt);
					// close the session
					session_write_close();
					if (isset($_POST['prev_url']) && $_POST['prev_url']<>"") {
						header("location:" . $_POST['prev_url']);	
						//echo "prev:".$_POST['prev_url'];						
					} else {
						if (!isset($_SESSION['accesslvl']) || $_SESSION['accesslvl'] == 1) { //Display Delete function if user access level is 3 or higher
							header("location:/comfort_participant_new.php");
						} else {
							header("location:/index.php");
						}
						
						//echo "index";
					}
					//exit(); //Terminate execution of the script
				} else {
					$what = 'Authentication failed';
				}
				unset($hasher);
				
				$errormsg = $what;
				//echo "$what\n";
				
				//echo $_SESSION['username'] . ", " . $_SESSION['email'] . ", " . $_SESSION['loggedin'] . ", " . $_SESSION['signature'];  
				
				//Close DB connection
				$dbo = null;
			}
		}
?>

<?php session_regenerate_id(); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/style.css" type="text/css">
	
	<title>SIGMA</title>
</head>

<body>
<div id="div_login">
	<table id="login_header">
		<tr>
			<td><img src="css/images/login_headerbg.png" alt="TRIO"> 
			</td>

		</tr>
	</table>
</div>	
<body>
<div id="main-wrapper">
	<div id="wrapper_login">
			<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
			<br><br><br>
			<h1 class='title'>Duke-NUS SIGMA Online System</h1>
			<p class='title'>Authorised Access Only</p>
			
			<table class="tbl_registration">
			<tr>
				<td>
					Username:
				</td>
				<td>
					<input name="loginuser" type="text" size="20" value="<?php echo isset($_POST['loginuser']) ? htmlspecialchars($_POST['loginuser']) : '' ?>"></input>
				</td>
			</tr>
			<tr>
				<td>
					Password:
				</td>
				<td>
					<input name="loginpass" type="password" size="20" value="<?php echo isset($_POST['loginpass']) ? htmlspecialchars($_POST['loginpass']) : '' ?>"></input>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td align="right">
					<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
					<input type="submit" value="Log In"></input>
				</td>
			</tr>
			<tr>
				<td colspan='2' style='color:#ff0000;'>
					<br>
					<?php echo $errormsg; ?>
				</td>
			</tr>
			</table>
			</form>
			
	</div>	
</div>
</body>
</html>

<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
