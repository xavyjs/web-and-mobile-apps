<?php
function cal_total_incentive($appointment_id) {      

	//Appointment Data
	$dbo = dbConnect(); // Connect to Database
	$stmt = $dbo->prepare('SELECT a.Event_ID, e.Event, a.Fitbit_Start_Date, a.Appointment_Date, a.ApptStatus_ID, p.* FROM appointments AS a 
							INNER JOIN participants AS p ON a.Participant_ID = p.Participant_ID 
							INNER JOIN ctbl_event AS e ON a.Event_ID = e.Event_ID 
							WHERE a.Appointment_ID=:appointment_id');
	$stmt->execute(array('appointment_id' => $appointment_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "no appointment";
	} 
	else {
		foreach ($result as $row){
			$event_id = $row['Event_ID'];
			$event = $row['Event'];
			$fitbit_start_date=date('Y-m-d',strtotime($row['Fitbit_Start_Date']));
			$appointment_date=date('Y-m-d',strtotime($row['Appointment_Date']));
			$appointment_month = date("m", strtotime($row['Appointment_Date']));
			$appointment_year = date("Y", strtotime($row['Appointment_Date']));
			$apptstatus_id = $row['ApptStatus_ID'];
			$participant_id = $row['Participant_ID'];
			$fitbit_user_id = $row['Fitbit_User_ID'];
			$daily_rental = $row['Daily_Rental'];
	
			switch ($event_id)
			{
			case 3:
				$next_mth_prediction = $row['Prediction_Mth1'];
				break;
			case 4:
				$current_mth_prediction = $row['Prediction_Mth1'];	
				$next_mth_prediction = $row['Prediction_Mth2'];
				$incabsys = $row['IncabSys_Mth1'];
				break;
			case 5: 
				$current_mth_prediction = $row['Prediction_Mth2'];	
				$next_mth_prediction = $row['Prediction_Mth3'];
				$incabsys = $row['IncabSys_Mth2'];
				break;
			case 6:
				$current_mth_prediction = $row['Prediction_Mth3'];	
				$next_mth_prediction = $row['Prediction_Mth4'];
				$incabsys = $row['IncabSys_Mth3'];
				break;
			case 7:  
				$current_mth_prediction = $row['Prediction_Mth4'];	
				$incabsys = $row['IncabSys_Mth4'];
				break;
			case 8:  
				$current_mth_prediction = $row['Prediction_Mth4'];	
				$incabsys = $row['IncabSys_Mth5'];
				break;
			case 9:  
				$current_mth_prediction = $row['Prediction_Mth4'];	
				$incabsys = $row['IncabSys_Mth6'];
				break;
			case 10:  
				$current_mth_prediction = $row['Prediction_Mth4'];	
				$incabsys = $row['IncabSys_Mth7'];
				break;
			default:
				$current_mth_prediction = null;	
				$next_mth_prediction = null;
				$incabsys = null;
			}
			if ($current_mth_prediction==1) {
				$current_mth_prediction='Yes';
			} elseif ($current_mth_prediction==null) {
				$current_mth_prediction='';
			} else {
				$current_mth_prediction='No';
			}
			if ($next_mth_prediction==1) {
				$next_mth_prediction='Yes';
			} elseif ($next_mth_prediction==null) {
				$next_mth_prediction='';
			} else {
				$next_mth_prediction='No';
			}
			
		}		
	} 


	//Steps Data
	$stmt = $dbo->prepare('SELECT * FROM log_steps
				WHERE Steps_Date BETWEEN :date_from AND :date_to 
				AND Fitbit_User_ID=:fitbit_user_id
				ORDER BY Steps_Date ASC');
	$stmt->execute(array('date_from' => $fitbit_start_date,'date_to' => date('Y-m-d', strtotime($appointment_date .' -1 day')),'fitbit_user_id' => $fitbit_user_id));
	$row_count = $stmt->rowCount();
	$days_incentive = $row_count;
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$steps_id = date("d", strtotime($row['Steps_Date']));
			$result_array[$steps_id]['steps_date'] = date("d M Y, D", strtotime($row['Steps_Date']));
			$result_array[$steps_id]['steps_number'] = $row['Steps_Number'];
		}
	}						


	//Calculate Number of days with steps_number
	$days_500=0;
	$days_7000=0;
	$steps_days=0;
	$steps_total=0;
	if (is_array($result_array)) {
		foreach ($result_array as $key => $value) {
			$steps_total=$steps_total+$result_array[$key]['steps_number'];
			$steps_days++;
			if ($result_array[$key]['steps_number']>=500) {
				$days_500++;
			}
			if ($result_array[$key]['steps_number']>=7000) {
				$days_7000++;
			}
		}
		$steps_average=$steps_total/$steps_days;
	}


	//Customized Display rows
	$list= array(4,5,6,7); // Only display prediction ddl for event_id 3,4,5,6
	if (in_array($event_id, $list)){
	
		//Calculate Total Incentive Awarded
		$total_incentive=0;
		if ($days_7000 >= ($days_incentive-8)) {
			$days_7000_eligibility='Yes';
			$total_incentive=$daily_rental;
		} else {
			$days_7000_eligibility='No';
		}
		
		if ($days_7000_eligibility==$current_mth_prediction) {
			$prediction_eligibility='Yes';
			$total_incentive=$total_incentive+10;
		} else {
			$prediction_eligibility='No';
		}
	}


	//Calculate Study Compliance Incentive
	if ($days_500 >= ($days_incentive-8) && $incabsys=='Yes') { // for Study Compliance Incentive Eligibility
		$days_500_eligibility='Yes';
		$total_incentive=$total_incentive+10;
	} else {
		$days_500_eligibility='No';
	}

	$list= array(4,5,6,7,8,9,10); // Only insert into log_incentive if event=array
	if (in_array($event_id, $list)){
		//Insert into log_incentive
		
		if ($apptstatus_id==3){ 
			$stmt = $dbo->prepare('SELECT * FROM log_incentive WHERE Participant_ID=? AND Event_ID=?');
			$stmt->execute(array($participant_id,$event_id));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			
			if ($row_count==0) {
				$stmt = $dbo->prepare("INSERT INTO log_incentive(Participant_ID,Appointment_ID,Event_ID,Appointment_Date,Daily_Rental,Current_Mth_Prediction,Next_Mth_Prediction,Days_7000steps,Days_500steps,Avg_Steps,Step_Incentive_Eligibility,Prediction_Eligibility,Compliance_Incentive_Eligibility,Compliance_Lottery_Eligibility,Total_Incentive) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				$stmt->execute(array($participant_id,$appointment_id,$event_id,$appointment_date,$daily_rental,$current_mth_prediction,$next_mth_prediction,$days_7000,$days_500,$steps_average,$days_7000_eligibility,$prediction_eligibility,$days_500_eligibility,$days_500_eligibility,$total_incentive));
			} 
			else {
				$stmt = $dbo->prepare("UPDATE log_incentive SET Participant_ID=?, Appointment_ID=?, Event_ID=?, Appointment_Date=?, Daily_Rental=?, Current_Mth_Prediction=?, Next_Mth_Prediction=?, Days_7000steps=?, Days_500steps=?, Avg_Steps=?, Step_Incentive_Eligibility=?, Prediction_Eligibility=?, Compliance_Incentive_Eligibility=?, Compliance_Lottery_Eligibility=?, Total_Incentive=? WHERE Participant_ID=? AND Event_ID=?");
				$stmt->execute(array($participant_id,$appointment_id,$event_id,$appointment_date,$daily_rental,$current_mth_prediction,$next_mth_prediction,$days_7000,$days_500,$steps_average,$days_7000_eligibility,$prediction_eligibility,$days_500_eligibility,$days_500_eligibility,$total_incentive,$participant_id,$event_id));	
			}	
		} else {
			$stmt = $dbo->prepare("DELETE FROM log_incentive WHERE Participant_ID=? AND Event_ID=?");
			$stmt->execute(array($participant_id,$event_id));
		}
	}
	Return $total_incentive;
	//Close DB connection
	$dbo = null;
	
	

}		
	
?>