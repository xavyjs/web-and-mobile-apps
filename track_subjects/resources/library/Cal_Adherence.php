<?php
function cal_adherence($participant_aid) {      

//Participant Data
	$dbo = dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT Participant_ID, Num_Daily_Doses, Interval_1to2, Interval_2to3, Interval_3to4, Interval_4to5, Interval_5to6, Interval_6to7, Interval_7to8, Baseline_Assessment_Date, Month3_Assessment_Date, Month6_Assessment_Date 
							FROM participants
							WHERE Participant_AID=:participant_aid');
	$stmt->execute(array('participant_aid' => $participant_aid));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "no participant";
	} 
	else {
		foreach ($result as $row){
			$participant_id = $row['Participant_ID'];
			$num_daily_doses = $row['Num_Daily_Doses'];
			$interval_1to2 = $row['Interval_1to2'];
			$interval_2to3 = $row['Interval_2to3'];
			$interval_3to4 = $row['Interval_3to4'];
			$interval_4to5 = $row['Interval_4to5'];
			$interval_5to6 = $row['Interval_5to6'];
			$interval_6to7 = $row['Interval_6to7'];
			$interval_7to8 = $row['Interval_7to8'];
			$baseline_assessment_date=$row['Baseline_Assessment_Date'];
			$month3_assessment_date=$row['Month3_Assessment_Date'];
			$month6_assessment_date=$row['Month6_Assessment_Date'];	
			$interval_array = array($row['Interval_1to2'],$row['Interval_2to3'],$row['Interval_3to4'],$row['Interval_4to5'],$row['Interval_5to6'],$row['Interval_6to7'],$row['Interval_7to8']);		
		}		
	} 


//Doses Data	
	$stmt = $dbo->prepare('SELECT * FROM import_ecap
			WHERE Participant_Id = :participant_id
			ORDER BY Dose_Timestamp ASC');
	$stmt->execute(array('participant_id' => $participant_id));
	$row_count = $stmt->rowCount();
	$days_incentive = $row_count;
	$result = $stmt->fetchAll();

	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$dose_id = date("YmdHis", strtotime($row['Dose_Timestamp']));
			$result_array[$dose_id]['dose'] = $row['Dose'];
			$result_array[$dose_id]['dose_timestamp'] = date("d M Y, h:i A", strtotime($row['Dose_Timestamp']));
			$row_dose_date = "<td align='left'>" . date("d M Y, D", strtotime($row['Dose_Timestamp'])) . "</td>";
			$row_dose_num = "<td align='left'>" . $row['Dose'] . "</td></tr>";
		}
	}											


//Create windows for month 1-3
	$dose_id="";
	$window_day="";
	$window_day_dose="";
	print "<br>";
	if (is_array($result_array)) {
		foreach ($result_array as $key => $value) {
			
			//Pull out all doses within month 1-3 (+2067hrs = (85 days + the whole of 85th day = 86 days) x 24hrs + 3hrs
			if (strtotime($result_array[$key]['dose_timestamp']) >= strtotime($baseline_assessment_date . "+27 hour") && strtotime($result_array[$key]['dose_timestamp']) <= strtotime($baseline_assessment_date . "+2067 hour")) {
				$incre_hours = ($window_day*24)+27;		
				// Set time of 1st dose for each day after baseline assessment date if time of dose is after 3AM
				if ($window_day_dose=="" && strtotime($result_array[$key]['dose_timestamp']) >= strtotime($baseline_assessment_date . "+$incre_hours hour")) {
				
					$dose_id++;
					$window_day++;
					$window_day_dose++;
					$window_datetime=$result_array[$key]['dose_timestamp'];
					$window_array_13[$dose_id]['window'] = "1-3";
					$window_array_13[$dose_id]['day'] = $window_day;
					$window_array_13[$dose_id]['dose'] = $window_day_dose;
					$window_array_13[$dose_id]['datetime'] = $window_datetime;		
					
					while($window_day_dose<=$num_daily_doses-1) { //loop through the remaining number of daily doses left to create the remaining of the windows for the day	
						$dose_id++;
						$x=$window_day_dose-1; //array starts from [0]
						$window_datetime = date("d M Y, h:i A", strtotime($window_datetime . " +$interval_array[$x] hour"));
						// print $window_datetime . "<br>";
						$window_day_dose++;
						$window_array_13[$dose_id]['window'] = "1-3";
						$window_array_13[$dose_id]['day'] = $window_day;
						$window_array_13[$dose_id]['dose'] = $window_day_dose;
						$window_array_13[$dose_id]['datetime'] = $window_datetime;	
					} 
					$window_day_dose="";
				}		
			}
		}
	}
	// print "<pre>";
	// print_r($window_array_13);
	// print "</pre>";
	
	

//Create windows for month 4-6
	$dose_id="";
	$window_day="";
	$window_day_dose="";
	if (is_array($result_array)) {		
		foreach ($result_array as $key => $value) {			
			//Pull out all doses within month 4-6 (+2379hrs = (98 days + the whole of 98th day = 99 days) x 24hrs + 3hrs
			if (strtotime($result_array[$key]['dose_timestamp']) >= strtotime($month3_assessment_date . "+339 hour") && strtotime($result_array[$key]['dose_timestamp']) <= strtotime($month3_assessment_date . "+2379 hour")) {
				$incre_hours = ($window_day*24)+339;		
				// Set time of 1st dose for each day after baseline assessment date if time of dose is after 3AM
				if ($window_day_dose=="" && strtotime($result_array[$key]['dose_timestamp']) >= strtotime($month3_assessment_date . "+$incre_hours hour")) {
					// print $window_day_dose . " | " . date("d M Y, h:i A", strtotime($result_array[$key]['dose_timestamp'])) . " | " . date("d M Y, h:i A", strtotime($month3_assessment_date . "+$incre_hours hour")) . "<br>";
					$dose_id++;
					$window_day++;
					$window_day_dose++;
					$window_datetime=$result_array[$key]['dose_timestamp'];
					$window_array_46[$dose_id]['window'] = "4-6";
					$window_array_46[$dose_id]['day'] = $window_day;
					$window_array_46[$dose_id]['dose'] = $window_day_dose;
					$window_array_46[$dose_id]['datetime'] = $window_datetime;		
					
					while($window_day_dose<=$num_daily_doses-1) { //loop through the remaining number of daily doses left to create the remaining of the windows for the day	
						$dose_id++;
						$x=$window_day_dose-1; //array starts from [0]
						$window_datetime = date("d M Y, h:i A", strtotime($window_datetime . " +$interval_array[$x] hour"));
						// print $window_datetime . "<br>";
						$window_day_dose++;
						$window_array_46[$dose_id]['window'] = "4-6";
						$window_array_46[$dose_id]['day'] = $window_day;
						$window_array_46[$dose_id]['dose'] = $window_day_dose;
						$window_array_46[$dose_id]['datetime'] = $window_datetime;	
					} 
					$window_day_dose="";
				}		
			}
		}
	}
	// print "<pre>";
	// print_r($window_array_46);
	// print "</pre>";
	
	

//Check doses compliance for month 1-3
	if (is_array($window_array_13)) {
		foreach ($window_array_13 as $key => $value) {			
			$stmt = $dbo->prepare('SELECT * FROM import_ecap
									WHERE Participant_Id =  :participant_id
									AND TIMESTAMPDIFF(SECOND ,:dose_timestamp,Dose_Timestamp) BETWEEN -3600 AND 3600');
			$stmt->execute(array('participant_id' => $participant_id, 'dose_timestamp' => date("Y-m-d H:i:s", strtotime($window_array_13[$key]['datetime']))));
			$row_count = $stmt->rowCount();
			$days_incentive = $row_count;
			$result = $stmt->fetchAll();

			if ($row_count==0) {
				$row_mth13_data =  $row_mth13_data . $window_array_13[$key]['day'] . " | " . $window_array_13[$key]['datetime'] . ": <br>";
				$window_array_13[$key]['comply'] = 0;	
			} else {
				foreach ($result as $row){
					$window_array_13[$key]['comply'] = 1;	
					$comply_days++;
					$row_mth13_data =  $row_mth13_data . $window_array_13[$key]['day'] . " | " . $window_array_13[$key]['datetime'] . ": Comply on " . date("d M Y, h:i A", strtotime($row['Dose_Timestamp'])) . "<br>";				
				}
			}						
		}
	}


//Check doses compliance for month 4-6
	if (is_array($window_array_46)) {
		foreach ($window_array_46 as $key => $value) {			
			$stmt = $dbo->prepare('SELECT * FROM import_ecap
									WHERE Participant_Id =  :participant_id
									AND TIMESTAMPDIFF(SECOND ,:dose_timestamp,Dose_Timestamp) BETWEEN -3600 AND 3600');
			$stmt->execute(array('participant_id' => $participant_id, 'dose_timestamp' => date("Y-m-d H:i:s", strtotime($window_array_46[$key]['datetime']))));
			$row_count = $stmt->rowCount();
			$days_incentive = $row_count;
			$result = $stmt->fetchAll();

			if ($row_count==0) {
				$row_mth46_data =  $row_mth46_data . $window_array_46[$key]['day'] . " " . $window_array_46[$key]['datetime'] . ": <br>";
				$window_array_46[$key]['comply'] = 0;	
			} else {
				foreach ($result as $row){
					$window_array_46[$key]['comply'] = 1;	
					$comply_days++;
					$row_mth46_data =  $row_mth46_data . $window_array_46[$key]['day'] . " " . $window_array_46[$key]['datetime'] . ": Comply on " . date("d M Y, h:i A", strtotime($row['Dose_Timestamp'])) . "<br>";				
				}
			}						
		}
	}


//Count adherence day for month 1-3
	if (is_array($window_array_13)) {

		foreach ($window_array_13 as $key => $value) {
		
			if ($window_array_13[$key]['comply']==1) {
				$count_doses_comply++;
			}	
			
			if ($window_array_13[$key]['dose']==$num_daily_doses) {
				if ($count_doses_comply==$num_daily_doses) {
					$mth13_adherence_days++;
				}
				$count_doses_comply=0;
			}
			
		$total_days = $window_array_13[$key]['day'];
		}
	}
	$mth13_adherence_percent = number_format((float)$mth13_adherence_days / 85, 4);


//Count adherence day for month 4-6
	if (is_array($window_array_46)) {

		foreach ($window_array_46 as $key => $value) {
		
			if ($window_array_46[$key]['comply']==1) {
				$count_doses_comply++;
			}	
			
			if ($window_array_46[$key]['dose']==$num_daily_doses) {
				if ($count_doses_comply==$num_daily_doses) {
					$mth46_adherence_days++;
				}
				$count_doses_comply=0;
			}
			
		$total_days = $window_array_46[$key]['day'];
		}
	}
	$mth46_adherence_percent = number_format((float)$mth46_adherence_days / 85, 4);
	
	return array($mth13_adherence_days, $mth13_adherence_percent, $row_mth13_data, $mth46_adherence_days, $mth46_adherence_percent, $row_mth46_data);

	//Close DB connection
	$dbo = null;
}		



function cal_subsidy_percent($participant_aid,$mth13_adherence_percent,$mth46_adherence_percent) {      

//Participant Data
	$dbo = dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT Participant_ID, Study_Arm_ID, Healthcare_Cost_Mth13, Healthcare_Cost_Mth46
							FROM participants
							WHERE Participant_AID=:participant_aid');
	$stmt->execute(array('participant_aid' => $participant_aid));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "no participant";
	} 
	else {
		foreach ($result as $row){
			$participant_id = $row['Participant_ID'];
			$study_arm_id = $row['Study_Arm_ID'];
		}		
	} 

//Doses Data	
	if ($study_arm_id==2) {

		if ($mth13_adherence_percent>=0.9) {
			$mth13_subsidy_percent=0.5;
		} elseif ($mth13_adherence_percent<0.9 && $mth13_adherence_percent>=0.75) {
			$mth13_subsidy_percent=0.25;
		} else {
			$mth13_subsidy_percent=0;
		}
		
		if ($mth46_adherence_percent>=0.9) {
			$mth46_subsidy_percent=0.5;
		} elseif ($mth46_adherence_percent<0.9 && $mth46_adherence_percent>=0.75) {
			$mth46_subsidy_percent=0.25;
		} else {
			$mth46_subsidy_percent=0;
		}
	}
	return array($mth13_subsidy_percent,$mth46_subsidy_percent);

	//Close DB connection
	$dbo = null;
}		
	
?>