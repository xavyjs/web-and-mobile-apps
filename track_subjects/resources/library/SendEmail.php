<?php
	function gw_send_email($participant_id,$appointment_id,$email_type,$email_subject,$email_text) {  //if $email_type=auto,$email_subject=""$email_text=Email_ID(ctbl_email); elseif $email_type=manual, $email_subject,text=userdefined   
		// include_once("../resources/config.php");
		$dbo = dbConnect(); // Connect to Database
		
		$stmt = $dbo->prepare("SELECT Email
								FROM participants 
								WHERE Participant_ID= :participant_id AND (Email IS NOT NULL OR Email <> '')");
		$stmt->execute(array('participant_id' => $participant_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		
		if ($row_count==0) {
		} 
		else {									
			foreach ($result as $row){	
				if ($email_type=="auto") {				
					$stmt2 = $dbo->prepare('SELECT e.Event, l.Location, s.ApptStatus, a.* FROM appointments AS a 
									LEFT JOIN ctbl_event AS e ON a.Event_ID = e.Event_ID 
									LEFT JOIN ctbl_apptstatus AS s ON a.ApptStatus_id = s.ApptStatus_ID 
									LEFT JOIN ctbl_location AS l on a.Location_ID = l.Location_ID 
									WHERE a.Appointment_ID=:appointment_id AND a.Participant_ID=:participant_id');
					$stmt2->execute(array('appointment_id' => $appointment_id,'participant_id' => $participant_id));
					$row_count2 = $stmt2->rowCount();
					$result2 = $stmt2->fetchAll();			
					if ($row_count2==0) {
					} 
					else {									
						foreach ($result2 as $row2){
							$event=$row2['Event'];	
							$location=$row2['Location'];
							$apptstatus=$row2['ApptStatus'];
							$appointment_date = date("d F Y, D", strtotime($row['Appointment_Date']));
							$appointment_time = date("h:i A", strtotime($row['Appointment_Time']));					
						}
					}

					//Pull SMS Subject from ctbl_sms
					$stmt2 = $dbo->prepare("SELECT *
										FROM ctbl_email 
										WHERE Email_ID = :email_id");
					$stmt2->execute(array('email_id' => $email_text));
					$row_count2 = $stmt2->rowCount();
					$result2 = $stmt2->fetchAll();
					
					if ($row_count2==0) {
					} 
					else {									
						foreach ($result2 as $row2) {				
							$email_subject=$row2['Email_Subject'];
							$email_body=$row2['Email_Body'];
							
							$email_body=str_replace("<Participant_ID>",$participant_id,$email_body);
							$email_body=str_replace("<Event>",$event,$email_body);
							$email_body=str_replace("<Appointment_Date>",$appointment_date,$email_body);
							$email_body=str_replace("<Appointment_Time>",$appointment_time,$email_body);
							$email_body=str_replace("<Location>",$location,$email_body);						          
						}
					}
				} 
				elseif ($email_type=="manual") {
					$email_body=$email_text;
				}
				
				//---Send Email---
				$email_add = $row['Email'];
				$email = "taksi@duke-nus.edu.sg"; //Sender email address
				$subject = $email_subject;
			  
				$boundary = "nextPart";
				
				$headers = "MIME-Version: 1.0\r\n";
				$headers = "From: " . $email . "\r\n";
				$headers .= "BCC: edmund.shen@duke-nus.edu.sg,". $email . "\r\n";
				$headers .= "Reply-To: ". $email . "\r\n";
				$headers .= "Content-Type: multipart/alternative; boundary = $boundary\r\n";

				//text version
				$headers .= "\n--$boundary\n"; // beginning \n added to separate previous content
				$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
				$headers .= "This is the plain version";

				//html version
				$headers .= "\n--$boundary\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				$body = "<html><body><p>";
				$body .= $email_body;
				$body .= "</p></body></html>";

				mail($email_add, $subject,$body,$headers);
				//echo "Thank you for using our mail form";	
				
				$stmt3 = $dbo->prepare("INSERT INTO log_email(Participant_ID,Appointment_ID,Email,Email_Subject,Email_Body,UserID) VALUES(:participant_id,:appointment_id,:email,:email_subject,:email_body,:userid)");
				$stmt3->execute(array(':participant_id' => $participant_id,':appointment_id' => $appointment_id,':email' => $email_add,':email_subject' => $email_subject,':email_body' => $email_body,':userid' => $_SESSION['userid']));	
				$ok = "Email Sent (Email ID: ". $dbo->lastInsertId() . ")";	
			}
			
		$dbo = null; //Close DB connection
		return $ok;  		
		}
	}
?>
