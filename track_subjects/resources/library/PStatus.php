<?php
	function PStatus_Add($participant_id,$pstatus) {
		$dbo = dbConnect(); // Connect to Database
		$stmt = $dbo->prepare("INSERT INTO log_pstatus(Participant_ID,PStatus,Timestamp) VALUES(:participant_id,:pstatus,:timestamp)");
		$stmt->execute(array(':participant_id' => $participant_id,':pstatus' => $pstatus,':timestamp' => date("Y-m-d H:i:s")));
		$dbo = null; //Close DB connection	
	}  
?>