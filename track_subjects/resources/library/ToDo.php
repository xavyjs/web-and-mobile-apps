<?php
	function ToDo_Add($participant_id,$appointment_id,$todo) {
		$dbo = dbConnect(); // Connect to Database
		$stmt = $dbo->prepare("INSERT INTO log_todo(Participant_ID,Appointment_ID,ToDo,Timestamp) 					
								VALUES(:participant_id,:appointment_id,:todo,:timestamp)"); //Insert unconfirmed appointments into To-Do list
		$stmt->execute(array(':participant_id' => $participant_id, ':appointment_id' => $appointment_id, ':todo' => $todo, ':timestamp' => date("Y-m-d H:i:s")));	
		$dbo = null; //Close DB connection	
	}  
?>