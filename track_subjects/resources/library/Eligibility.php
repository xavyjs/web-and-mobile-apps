<?php
function cal_eligibility($screener_id) {      
	// include_once("../resources/config.php");
	$dbo = dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare("SELECT *
							FROM screener 
							WHERE Screener_ID= :screener_id");
	$stmt->execute(array('screener_id' => $screener_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {	
		$eligibility[0] = "Eligible";
		
		foreach ($result as $row) {	

			//Criteria for ineligible	
			if ($row['Q2'] < 50) {
				$eligibility[0] = "Ineligible";
				$eligibility[1] = $eligibility[1] . "Age < 50, ";
			} 
			if ($row['Q2'] > 75) {
				$eligibility[0] = "Ineligible";
				$eligibility[1] = $eligibility[1] . "Age > 75, ";
			} 
			if ($row['Q3'] == 2) {
				$eligibility[0] = "Ineligible";
				$eligibility[1] = $eligibility[1] . "Q3, ";
			} 
			if ($row['Q4'] == 4) {
				$eligibility[0] = "Ineligible";
				$eligibility[1] = $eligibility[1] . "Q4, ";
			} 
			if ($row['Q5'] == 2) {
				$eligibility[0] = "Ineligible";
				$eligibility[1] = $eligibility[1] . "Q5, ";
			} 
			if ($row['Q6'] == 2) {
				$eligibility[0] = "Ineligible";
				$eligibility[1] = $eligibility[1] . "Q6, ";
			} 
				
			//Criteria for conditionally eligible
			if ($eligibility[0] == "Eligible") { 
				if ($row['Q7'] == 1) {
					$eligibility[0] = "Conditionally Eligible";
					$eligibility[1] = $eligibility[1] . "PARQ1, ";
				} 
				if ($row['Q8_1'] == 1 || $row['Q8_2'] == 1 || $row['Q8_3'] == 1 || $row['Q8_4'] == 1) {
					$eligibility[0] = "Conditionally Eligible";
					$eligibility[1] = $eligibility[1] . "PARQ2, ";
				} 
				if ($row['Q9'] == 1) {
					$eligibility[0] = "Conditionally Eligible";
					$eligibility[1] = $eligibility[1] . "PARQ3, ";
				} 
				if ($row['Q10'] == 1) {
					$eligibility[0] = "Conditionally Eligible";
					$eligibility[1] = $eligibility[1] . "PARQ4, ";
				} 
				if ($row['Q11'] == 1) {
					$eligibility[0] = "Conditionally Eligible";
					$eligibility[1] = $eligibility[1] . "PARQ5, ";
				} 
				if ($row['Q12'] == 1) {
					$eligibility[0] = "Conditionally Eligible";
					$eligibility[1] = $eligibility[1] . "PARQ6, ";
				} 
				if ($row['Q13'] == 1) {
					$eligibility[0] = "Conditionally Eligible";
					$eligibility[1] = $eligibility[1] . "PARQ7, ";
				} 			
			}
			
		}
	}
	
	if ($eligibility[0] == "Eligible") {
		$eligibility[2] = "I'm pleased to inform that you are eligible to take part in our study. To contact you for study-related matters we will need to collect your personal contact details. A member of the study team will call you over the next 1-2 weeks to arrange a time for your step counter (i.e. Fitbit Zip) pick-up.";
	} elseif ($eligibility[0] == "Conditionally Eligible") {
		$eligibility[2] = "I'm pleased to inform that you are conditionally eligible to take part in our study. This is based on your responses to select items which may indicate that physical activity may be inappropriate for you, or that you should have medical advice concerning the type of activity most suitable for you. Please collect a copy of the doctor's form from the Comfort Study Coordinator. The form gives details about what participants are to do during the study. You should consult your doctor and show him/her the form. If the doctor deems that you are fit to take part in this study, he/she should give their approval by completing and signing the form. You will only be allowed to take part in this study after you return the completed form to study staff at the following study session.";
	} elseif ($eligibility[0] == "Ineligible") {
		$eligibility[2] = "I'm sorry to inform you that you do not qualify for this study. Thanks again for your time and willingness to participate.";
	}
	
	$eligibility[1] = rtrim($eligibility[1],', '); //Remove comma and space
	return $eligibility;  
	
	//$eligibility[0] = Outcome
	//$eligibility[1] = Reason/s for outcome
	//$eligibility[2] = Response to participant
}  
     //Print("Sending to one way sms " . gw_send_sms("apiusername", "apipassword", "senderid", "61412345678", "test message"));
?>