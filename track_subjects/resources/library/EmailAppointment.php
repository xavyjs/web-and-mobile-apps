<?php

	function EmailAppointment($participant_id,$appt_event,$appt_date,$appt_time,$appt_location,$email_id) {
		// include_once("../resources/config.php");
		$dbo = dbConnect(); // Connect to Database
		
		//$appt_id=$_POST['appt_id'];
		//$participant_id=$_POST['participant_id'];
		
		// print('<pre>');
		// print_r($_POST);
		// print('</pre>');
		
		$stmt = $dbo->prepare("SELECT Email
								FROM participants 
								WHERE Participant_UID= :participant_id AND (Email IS NOT NULL OR Email <> '')");
		$stmt->execute(array('participant_id' => $participant_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		
		if ($row_count==0) {
		} 
		else {									
			foreach ($result as $row){		

				$stmt2 = $dbo->prepare('SELECT Event
											FROM ctbl_event
											WHERE Event_ID= :appt_event');
				$stmt2->execute(array('appt_event' => $appt_event));
				$row_count2 = $stmt2->rowCount();
				$result2 = $stmt2->fetchAll();			
				if ($row_count2==0) {
				} 
				else {									
					foreach ($result2 as $row2){
						$appt_event=$row2['Event'];	
					}
				}
				
				$stmt2 = $dbo->prepare('SELECT Location
											FROM ctbl_location 
											WHERE Location_ID= :appt_location');
				$stmt2->execute(array('appt_location' => $appt_location));
				$row_count2 = $stmt2->rowCount();
				$result2 = $stmt2->fetchAll();			
				if ($row_count2==0) {
				} 
				else {									
					foreach ($result2 as $row2){
						$appt_location=$row2['Location'];	
					}
				}

				$stmt2 = $dbo->prepare("SELECT *
										FROM ctbl_emails 
										WHERE Email_ID = :email_id");
				$stmt2->execute(array('email_id' => $email_id));
				$row_count2 = $stmt2->rowCount();
				$result2 = $stmt2->fetchAll();
				
				if ($row_count2==0) {
				} 
				else {									
					foreach ($result2 as $row2){		
					
						$email_subject=$row2['Email_Subject'];
						$email_body=$row2['Email_Body'];
						
						$email_body=str_replace("<Participant_UID>",$participant_id,$email_body);
						$email_body=str_replace("<Event>",$appt_event,$email_body);
						$email_body=str_replace("<Appointment_Date>",date("d F Y, l", strtotime($appt_date)),$email_body);
						$email_body=str_replace("<Appointment_Time>",date("h:i A", strtotime($appt_time)),$email_body);
						$email_body=str_replace("<Location>",$appt_location,$email_body);				
					}
				}								
				//send email
				$email_add = $row['Email'];
				$email = "trio@duke-nus.edu.sg";
				$subject = $email_subject;
			  
				$boundary = "nextPart";
				
				$headers = "MIME-Version: 1.0\r\n";
				$headers = "From: " . $email . "\r\n";
				$headers .= "BCC: edmund.shen@duke-nus.edu.sg,". $email . "\r\n";
				$headers .= "Reply-To: ". $email . "\r\n";
				$headers .= "Content-Type: multipart/alternative; boundary = $boundary\r\n";

				//text version
				$headers .= "\n--$boundary\n"; // beginning \n added to separate previous content
				$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
				$headers .= "This is the plain version";

				//html version
				$headers .= "\n--$boundary\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				$body = "<html><body><p>";
				$body .= $email_body;
				$body .= "</p></body></html>";

				mail($email_add, $subject,$body,$headers);
				//echo "Thank you for using our mail form";	
			}	
		}	
		//Close DB connection
		$dbo = null;	
	}
?>
