<?php
	function gw_send_sms($participant_aid,$sms_type,$sms_text) {  //if $sms_type = auto, $sms_text = SMS_ID(ctbl_sms); elseif $sms_type = manual, $sms_text = user defined sms text   
		// include_once("../resources/config.php");
		$dbo = dbConnect(); // Connect to Database
		
		$stmt = $dbo->prepare("SELECT p.Mobile, sa.Study_Arm
								FROM participants AS p
								LEFT JOIN ctbl_study_arm AS sa ON p.Study_Arm_ID = sa.Study_Arm_ID 
								WHERE p.Participant_AID= :participant_aid AND (p.Mobile IS NOT NULL OR p.Mobile <> '')");
		$stmt->execute(array('participant_aid' => $participant_aid));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		
		if ($row_count==0) {
		} 
		else {									
			foreach ($result as $row) {	
			
				$user="API933NGD9SB8";
				$pass="API933NGD9SB8933NG";
				$sms_from="SIGMA"; //up to 11 alphanumeric
				$sms_to = "65". $row['Mobile'];	
				$participant_id = $row['Participant_ID'];	
				$study_arm = $row['Study_Arm'];	
			
				if ($sms_type=="auto") {	
					$sms_id=$sms_text;

					//Pull SMS Subject from ctbl_sms
					$stmt2 = $dbo->prepare("SELECT *
											FROM ctbl_sms
											WHERE SMS_ID = :sms_id");
					$stmt2->execute(array('sms_id' => $sms_id));
					$row_count2 = $stmt2->rowCount();
					$result2 = $stmt2->fetchAll();
					
					if ($row_count2==0) {
					} 
					else {									
						foreach ($result2 as $row2) {				
							$sms_msg=$row2['SMS_Subject'];
							$sms_msg=str_replace("<Participant_ID>",$participant_id,$sms_msg);	
							$sms_msg=str_replace("<Study_Arm>",$study_arm,$sms_msg);								
						}
					}
				} 
				elseif ($sms_type=="manual") {
					$sms_msg=$sms_text;
				}
				
				// ---Send SMS--- port 10002
				// $query_string = "api.aspx?apiusername=".$user."&apipassword=".$pass;
				// $query_string .= "&senderid=".rawurlencode($sms_from)."&mobileno=".rawurlencode($sms_to);
				// $query_string .= "&message=".rawurlencode(stripslashes($sms_msg)) . "&languagetype=1";        
				// $url = "http://gateway.onewaysms.sg:10002/".$query_string; 
				
				// ---Send SMS--- port 80
				$query_string = "api2.aspx?apiusername=".$user."&apipassword=".$pass;
				$query_string .= "&senderid=".rawurlencode($sms_from)."&mobileno=".rawurlencode($sms_to);
				$query_string .= "&message=".rawurlencode(stripslashes($sms_msg)) . "&languagetype=1";        
				$url = "http://gateway80.onewaysms.sg/".$query_string; 
				
				$fd = implode('',file($url)); 				
				if ($fd) {                       
					if ($fd > 0) {
						//Print("MT ID : " . $fd);
						$ok = "SMS Sent (MT ID: ". $fd . ")";						
						// Write SMS information to log_sms	
						$stmt3 = $dbo->prepare("INSERT INTO log_sms(MT_ID,Participant_ID,Mobile,Message,UserID,SMS_ID,Timestamp_Send) VALUES(:mt_id,:participant_id,:mobile,:message,:userid,:sms_id,:timestamp_send)");
						$stmt3->execute(array(':mt_id' => $fd,':participant_id' => $participant_aid,':mobile' => $row['Mobile'],':message' => $sms_msg,':userid' => $_SESSION['userid'],':sms_id' => $sms_id,':timestamp_send' => date("Y-m-d H:i:s")));					
					}        
					else {
						//print("Please refer to API on Error : " . $fd);
						$ok = "Failed to send SMS. Please refer to API on Error: " . $fd;
					}
				}           
				else {                       
					// no contact with gateway                      
					$ok = "no fd: " . $url;       
				}
			}
		}
		$dbo = null; //Close DB connection	
		return $ok;  
	}  
		 //Print("Sending to one way sms " . gw_send_sms("apiusername", "apipassword", "senderid", "61412345678", "test message"));
?>