<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="http://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="/resources/library/DateTimePicker/js/jquery-ui-slider-access-addon.js"></script>
<script src="/resources/library/DateTimePicker/js/jquery-ui-timepicker-addon.min.js"></script>

<script>
	$(function() {
		$( "#datepicker" ).datepicker({
			changeMonth: true,  
			changeYear: true,
			dateFormat: 'dd M yy',
			//showOn: 'both',
			//buttonImage: 'resources/library/DateTimePicker/images/calendar.gif',
			//buttonImageOnly: true,
			//buttonText: 'Open calendar'
		});
	});
	
	$(function() {
		$( "#datepicker1" ).datepicker({
			changeMonth: true,  
			changeYear: true,
			dateFormat: 'dd M yy',
			//showOn: 'both',
			//buttonImage: 'resources/library/DateTimePicker/images/calendar.gif',
			//buttonImageOnly: true,
			//buttonText: 'Open calendar'
		});
	});
	
	$(function() {
		$( "#datepicker2" ).datepicker({
			changeMonth: true,  
			changeYear: true,
			dateFormat: 'dd M yy',
			//showOn: 'both',
			//buttonImage: 'resources/library/DateTimePicker/images/calendar.gif',
			//buttonImageOnly: true,
			//buttonText: 'Open calendar'
		});
	});
	
	$(function() {
		$('#timepicker').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker1').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker2').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker3').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker4').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker5').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker6').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker7').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker8').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker9').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker10').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker11').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker12').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker13').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker14').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker15').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker16').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker17').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker18').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker19').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker20').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker21').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker22').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker23').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker24').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker25').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker26').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker27').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker28').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker29').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker30').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker31').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker32').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker33').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker34').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker35').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker36').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker37').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker38').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker39').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker40').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker41').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker42').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker43').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker44').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker45').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker46').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker47').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker48').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker49').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
	
	$(function() {
		$('#timepicker50').timepicker({
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
		
	$(function() {
		$('#datetimepicker').datetimepicker({
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false },
			//controlType: 'select', //Use dropdown list instead of slider
			timeFormat: 'hh:mm TT',
			hourMin: 8,
			hourMax: 18,
			stepMinute: 15,
			addSliderAccess: true,
			sliderAccessArgs: { touchonly: false }
		});
	});
</script>
<style>
.ui-datepicker-trigger { 
	position:relative; 
	top:3px; 
	left:3px;
	cursor:pointer;
}

.ui-timepicker-div .ui-widget-header { 
	margin-bottom: 8px; 
}

.ui-timepicker-div dl { 
	text-align: left; 
}

.ui-timepicker-div dl dt { 
	float: left; 
	clear:left; 
	padding: 0 0 0 5px; 
}

.ui-timepicker-div dl dd { 
	margin: 0 10px 10px 45%; 
}

.ui-timepicker-div td { 
	font-size: 90%; 
}

.ui-tpicker-grid-label { 
	background: none; 
	border: none; 
	margin: 0; 
	padding: 0; 
}

.ui-timepicker-rtl
{ 
	direction: rtl; 
}

.ui-timepicker-rtl dl 
{ 
	text-align: right; 
	padding: 0 5px 0 0; 
}

.ui-timepicker-rtl dl dt{ 
	float: right; 
	clear: right; 
}

.ui-timepicker-rtl dl dd { 
	margin: 0 45% 10px 10px; 
}
</style>