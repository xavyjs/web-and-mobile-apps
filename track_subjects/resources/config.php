<?php


//--------------------------------- phpass configuration START --------------------------------- 
// Base-2 logarithm of the iteration count used for password stretching
$hash_cost_log2 = 8;
// Do we require the hashes to be portable to older systems (less secure)?
$hash_portable = FALSE;
//--------------------------------- phpass configuration END --------------------------------- 



//--------------------------------- mySQL Connection START --------------------------------- 
function dbConnect() {
	global $dbo;
	$conf = array(
		'site_url' => 'http://sigma-study.com/',
		'db_hostname' => 'localhost',
		'db_username' => 'sigma_prod',
		'db_password' => 'HSSRs!gm4',
		'db_type' => 'mysql',
		'db_name' => 'sigma_prod',
		'img_path' => '../images',
		'img_url'  => 'http://sigma-study/images/',
	);

	//mysql_connect("$host", "$username", "$password")or die("cannot connect to database server");
	//mysql_select_db("$db_name")or die("Unable to select database");

	try {
		$dbostr = "mysql:host=" . $conf['db_hostname'] . "; dbname=" . $conf['db_name'];
		$dbo = new PDO($dbostr,$conf['db_username'],$conf['db_password']);
		$dbo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		// Return PDO object
		return $dbo;
		} 
	catch(PDOException $e) {
		echo 'ERROR: ' . $e->getMessage();
		}
}
//--------------------------------- mySQL Connection END --------------------------------- 



//--------------------------------- Legitimate Session Check START --------------------------------- 
function SessionCheck() {
	if (!isset($_SESSION['username']) || !isset($_SESSION['signature']) || !isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true || $_SESSION['signature'] != md5($_SESSION['username'] . $_SERVER['HTTP_USER_AGENT'] . $salt))	{
		session_start();
		session_destroy();
		header("Location: /login.php");
		exit();
	} else	{
		//session_start();

		// set timeout period in seconds
		$inactive = 2400;
		
		// check to see if $_SESSION['timeout'] is set
		if(isset($_SESSION['timeout']) ) {
			$session_life = time() - $_SESSION['timeout'];
			if($session_life > $inactive) { 
				session_destroy(); 
				header("Location: /timeout.php?prev_url=" . urlencode($_SERVER["REQUEST_URI"])); 
				//header("Location: http://" . $_SERVER['SERVER_NAME'] . "/timeout.php?prev_url=" . urlencode($_SERVER["REQUEST_URI"])); 
			}
		}
		$_SESSION['timeout'] = time();
	}
}
//--------------------------------- Legitimate Session Check END --------------------------------- 



//--------------------------------- Admin Session Check START --------------------------------- 
function AdminCheck() {
	if (!isset($_SESSION['accesslvl']) || $_SESSION['accesslvl'] != 5) {
		echo "<h1>Access denied</h1>Only for authorized user with access level 5.";
		exit();
	} 
}
//--------------------------------- Admin Session Check END --------------------------------- 


//--------------------------------- Power User Session Check START --------------------------------- 
function PowerUserCheck() {
	if (!isset($_SESSION['accesslvl']) || $_SESSION['accesslvl'] < 4) {
		echo "<h1>Access denied</h1>Only for authorized user with access level 4 and higher.";
		exit();
	} 
}
//--------------------------------- Power User Session Check END --------------------------------- 

//--------------------------------- Normal User Session Check START --------------------------------- 
function NormalUserCheck() {
	if (!isset($_SESSION['accesslvl']) || $_SESSION['accesslvl'] < 3) {
		echo "<h1>Access denied</h1>Only for authorized user with access level 3 and higher.";
		exit();
	} 
}
//--------------------------------- Power User Session Check END --------------------------------- 



//--------------------------------- Determine Login or Logout at Header START --------------------------------- 
function SessionLogInOut() {
	global $loginout;
	if ($_SESSION['accesslvl'] == 5) {
		print "<a href='../register.php'>Register new account</a>&nbsp;&nbsp;&nbsp;"; 
		print "<a href='../edit_user.php'>Edit User</a>&nbsp;&nbsp;&nbsp;"; 
		print "<a href='../edit_sms.php'>Edit SMS Template</a>&nbsp;&nbsp;&nbsp;";  
	} 
	if ($_SESSION['accesslvl'] >= 3) {
	} 
	if (!isset($_SESSION['username']) || !isset($_SESSION['signature']) || !isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true || $_SESSION['signature'] != md5($_SESSION['username'] . $_SERVER['HTTP_USER_AGENT'] . $salt))	{
		print "<a href='login.php'>Login</a>";
	}
	else {	
		print "<a href='logout.php'>Logout</a>";
	}		
}
//--------------------------------- Determine Login or Logout at Header END --------------------------------- 

?>