<?php
    // php /home/triostud/public_html/uat.trio-study.com/resources/cron/upcoming_appt.php (command for cron job)
	// lynx -dump http://uat.trio-study.com/resources/cron/upcoming_appt.php (command for cron job)
	$sms_type="auto";
	$timestamp=date("Y-m-d H:i:s");
	$todo="Contact participant to confirm appointment";
	//$sms_text=1;
	
	include_once("../../resources/config.php");
	dbConnect(); // Connect to Database
	$sql = 'SELECT * 
				FROM participants										
				WHERE 
				PStatus_ID in (2) AND
				Study_Arm_ID in (1) AND
				TO_DAYS(:today_date)-TO_DAYS(Month3_Assessment_Date)=56';						
	$sql .= ' ORDER BY Month3_Assessment_Date';
	$stmt = $dbo->prepare($sql);
	$stmt->bindValue(':today_date', date('Y-m-d'));							
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	if ($row_count==0) {
	} 
	else {		
		foreach ($result as $row){
			$participant_aid=$row['Participant_AID'];
			$participant_id=$row['Participant_ID'];
			$sms_text=8;
			include_once("../../resources/library/SendSMS.php");
			$sms_result=gw_send_sms($participant_aid,$sms_type,$sms_text);	
			echo $participant_aid . ", ";
		}
	}
	$dbo = null; //Close DB connection	
?>		

