<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang='en'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' href='../css/style.css?v=2.14' type='text/css'>
	<script language='javascript' src='resources/library/calendar/calendar.js'></script>
	<title>SIGMA</title>
</head>

<body>
	<div id='div_header'>
		<table id='tbl_header'>
			<tr>
				<td align='left' width='30%'>
					<a href='../index.php'><img src='../css/images/menu_headerbg.png' alt='SIGMA'></a> 
				</td>
				<td align='right'>
						<?php include_once($_SERVER['DOCUMENT_ROOT'].'/resources/config.php'); ?>
						<?php SessionLogInOut(); //Check legitimate session ?>
				</td>
			</tr>
		</table>
	</div>	
	<div class='main_menu'>
		<ul>
			<li><a href='../index.php'>Dashboard</a></li>
			<li><a href='../participant_list.php'>Participant
				<div class='items_descr'>List | Add New</div></a>
			</li>
			<li><a href='../import_ecap.php'>Import
				<div class='items_descr'>eCAP Data</div></a>
			</li>
			<li><a href='../report_list.php'>Reports
				<div class='items_descr'>Download as csv</div></a>
			</li>
		</ul>
	</div>
<div id='main-wrapper'>
<div id='wrapper'>
<br>