<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/library/DateTimePicker.php");  ?>

<?php //Race dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_race');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no race yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Race_ID']==$_POST['race_id']) {
				$race_id_row = $race_id_row . "<option value=" . $row['Race_ID'] . " selected='selected'>" . $row['Race'] . "</option>";
			} else {
				$race_id_row = $race_id_row . "<option value=" . $row['Race_ID'] . ">" . $row['Race'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Gender dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_gender');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no gender yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Gender_ID']==$_POST['gender_id']) {
				$gender_id_row = $gender_id_row . "<option value=" . $row['Gender_ID'] . " selected='selected'>" . $row['Gender'] . "</option>";
			} else {
				$gender_id_row = $gender_id_row . "<option value=" . $row['Gender_ID'] . ">" . $row['Gender'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Study Arm dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_study_arm');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no study arm yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Study_Arm_ID']==$_POST['study_arm_id']) {
				$study_arm_id_row = $study_arm_id_row . "<option value=" . $row['Study_Arm_ID'] . " selected='selected'>" . $row['Study_Arm'] . "</option>";
			} else {
				$study_arm_id_row = $study_arm_id_row . "<option value=" . $row['Study_Arm_ID'] . ">" . $row['Study_Arm'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>
		
	<h1 class='title'>Participant</h1>
	<p class='title'>Add New</p>
	<form action="participant_new_process.php" method="post">			
		<table class='new'>
		<tr>
			<td align='right'>Participant ID:</td>
			<td align='left'>
				<input name="participant_id" type="text" size="40" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>Age:</td>
			<td align='left'>
				<input name="age" type="text" size="40" value="<?php echo isset($_POST['age']) ? $_POST['age'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right' width='30%'>Race:</td>
			<td align='left' width='70%'>			
				<select name="race_id">					
					<?php echo $race_id_row; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td align='right' width='30%'>Gender:</td>
			<td align='left' width='70%'>			
				<select name="gender_id">					
					<?php echo $gender_id_row; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td align='right'>Mobile No:</td>
			<td align='left'>
				<input name="mobile" type="text" size="20" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'></td>
			<td align='left'>
				<br>
			</td>
		</tr>
		<tr>
			<td align='right' width='30%'>Study Arm:</td>
			<td align='left' width='70%'>			
				<select name="study_arm_id">					
					<?php echo $study_arm_id_row; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td align='right'></td>
			<td align='left'>
				<br>
			</td>
		</tr>
		<tr>
			<td align='right'>No. of Daily Doses (1-8):</td>
			<td align='left'>
				<input name="num_daily_doses" type="number" size="20" value="<?php echo isset($_POST['num_daily_doses']) ? $_POST['num_daily_doses'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'></td>
			<td align='left'>
				Interval between doses (in hours)
			</td>
		</tr>
		<tr>
			<td align='right'>Dose 1 - 2:</td>
			<td align='left'>
				<input name="interval_1to2" type="number" size="10" value="<?php echo isset($_POST['interval_1to2']) ? $_POST['interval_1to2'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>Dose 2 - 3:</td>
			<td align='left'>
				<input name="interval_2to3" type="number" size="10" value="<?php echo isset($_POST['interval_2to3']) ? $_POST['interval_2to3'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>Dose 3 - 4:</td>
			<td align='left'>
				<input name="interval_3to4" type="number" size="10" value="<?php echo isset($_POST['interval_3to4']) ? $_POST['interval_3to4'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>Dose 4 - 5:</td>
			<td align='left'>
				<input name="interval_4to5" type="number" size="10" value="<?php echo isset($_POST['interval_4to5']) ? $_POST['interval_4to5'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>Dose 5 - 6:</td>
			<td align='left'>
				<input name="interval_5to6" type="number" size="10" value="<?php echo isset($_POST['interval_5to6']) ? $_POST['interval_5to6'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>Dose 6 - 7:</td>
			<td align='left'>
				<input name="interval_6to7" type="number" size="10" value="<?php echo isset($_POST['interval_6to7']) ? $_POST['interval_6to7'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>Dose 7 - 8:</td>
			<td align='left'>
				<input name="interval_7to8" type="number" size="10" value="<?php echo isset($_POST['interval_7to8']) ? $_POST['interval_7to8'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'></td>
			<td align='left'>
				<br>
			</td>
		</tr>
		<tr>
			<td align='right'>Date of Baseline Assessment:</td>
			<td align='left'>
				<input type="text" name="baseline_assessment_date" id="datepicker" class="datepicker" readonly="readonly" value="<?php echo isset($_POST['baseline_assessment_date']) ? $_POST['baseline_assessment_date'] : date("d M Y") ?>"/>
			</td>
		</tr>
		<tr>
			<td align='right'>Date of Month 3 Assessment:</td>
			<td align='left'>
				<input type="text" name="month3_assessment_date" id="datepicker1" class="datepicker" readonly="readonly" value="<?php echo isset($_POST['month3_date']) ? $_POST['month3_date'] : date('d M Y',strtotime(date("d M Y") . ' +3 Months')) ?>"/>
			</td>
		</tr>
		<tr>
			<td align='right'>Date of Month 6 Assessment:</td>
			<td align='left'> <!-- To add more datapicker object, go to library/DateTimePicker.php-->
				<input type="text" name="month6_assessment_date" id="datepicker2" class="datepicker" readonly="readonly" value="<?php echo isset($_POST['month6_date']) ? $_POST['month6_date'] : date('d M Y',strtotime(date("d M Y") . ' +6 Months')) ?>"/>
			</td>
		</tr>
		<tr>
			<td align='right'></td>
			<td align='left'>
				<br>
			</td>
		</tr>
		<tr>
			<td align='right'>Healthcare Costs Month 1-3 ($):</td>
			<td align='left'>
				<input name="healthcare_cost_mth13" type="number" step="0.01" min=0 size="20" value="<?php echo isset($_POST['healthcare_cost_mth13']) ? $_POST['healthcare_cost_mth13'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td align='right'>Healthcare Costs Month 4-6 ($):</td>
			<td align='left'>
				<input name="healthcare_cost_mth46" type="number" step="0.01" min=0 size="20" value="<?php echo isset($_POST['healthcare_cost_mth46']) ? $_POST['healthcare_cost_mth46'] : '' ?>"></input>
			</td>
		</tr>
		<tr>
			<td><br><br><br></td>
			<td align='right'>
				<input type="submit" value="Create Participant"></input>
			</td>
		</tr>
		</table>
	</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
