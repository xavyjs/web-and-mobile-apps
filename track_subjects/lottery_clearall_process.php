<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database
			
			$event_id=$_POST['event_id'];
			
			print('<pre>');
			print_r($_POST);
			print('</pre>');
			
			$stmt = $dbo->prepare("DELETE FROM lottery WHERE Event_ID=?");
			$stmt->execute(array($event_id));
			
			echo "Lottery winners deleted";
		}
		
		//Close DB connection
		$dbo = null;
		
		header("location:lottery.php");
	?>
	
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 