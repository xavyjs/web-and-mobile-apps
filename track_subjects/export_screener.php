<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>

<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM screener');
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$dbo = null; //Close DB connection

	include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/OutputCSV.php");
	download_send_headers("export_screener_" . date("Ymd-His") . ".csv");
	echo outputcsv($result);
	die();
?>		

<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 