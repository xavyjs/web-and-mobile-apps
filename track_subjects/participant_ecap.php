<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Steps Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$sql="SELECT * FROM import_ecap
			WHERE Participant_Id = :participant_id
			ORDER BY Dose_Timestamp ASC";
	$stmt = $dbo->prepare($sql);
	$stmt->execute(array('participant_id' => $_GET['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$ecap_id = "<tr><td align='left'>" . $row['eCAP_Id'] . "</td>";
			$dose = "<td align='left'>" . $row['Dose'] . "</td>";
			$dose_date = "<td align='left'>" . date("d M Y, h:i A", strtotime($row['Dose_Timestamp'])) . "</td></tr>";
			$list_row = $list_row . $ecap_id . $dose . $dose_date;
		}
	}						
	//Close DB connection
	$dbo = null;
?>
			
			<h1 class='title'>Doses</h1>
			<p class='title'>Daily doses downloaded from eCAP</p>
						
			<table class='participantinfo'>
				<tr>								
					<th align='left' width='40%'>eCAP ID</th>
					<th align='left' width='20%'>Dose No.</th>		
					<th align='left' width='40%'>Dose Timestamp</th>						
				</tr>		
				<?php echo $list_row; ?>
			</table>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
