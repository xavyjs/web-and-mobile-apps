<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

			<h1 class='title'>Screener Questionnaire</h1>
			<p class='title'>All questions are mandatory</p>

		<?php
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					$error = false;
					$participant_id=$_POST['participant_id'];
					$Q2=$_POST['Q2'];
					$Q3=$_POST['Q3'];
					$Q4=$_POST['Q4'];
					$Q5=$_POST['Q5'];
					$Q6=$_POST['Q6'];
					$Q7=$_POST['Q7'];
					$Q8=$_POST['Q8'];
					if ($_POST['Q8'][1] == "" || $_POST['Q8'][1] == NULL) {
						$Q8_1="";
					} else {
						$Q8_1=1;
					}
					if ($_POST['Q8'][2] == "" || $_POST['Q8'][2] == NULL) {
						$Q8_2="";
					} else {
						$Q8_2=1;
					}
					if ($_POST['Q8'][3] == "" || $_POST['Q8'][3] == NULL) {
						$Q8_3="";
					} else {
						$Q8_3=1;
					}
					if ($_POST['Q8'][4] == "" || $_POST['Q8'][4] == NULL) {
						$Q8_4="";
					} else {
						$Q8_4=1;
					}					
					$Q9=$_POST['Q9'];
					$Q10=$_POST['Q10'];
					$Q11=$_POST['Q11'];
					$Q12=$_POST['Q12'];	
					$Q13=$_POST['Q13'];					
					
					if ($participant_id=="") {
						$error = true;
						$errormsg = $errormsg . "Participant ID is blank<br>";
					} 
					
					if (is_numeric($_POST['Q2'])===false) {
						$error = true;
						$errormsg = $errormsg . "Q2 must be an integer<br>";
					} 
					
					if ($Q2=="") {
						$error = true;
						$errormsg = $errormsg . "Q2 is blank<br>";
					} 
					
					if ($Q3=="") {
						$error = true;
						$errormsg = $errormsg . "Q3 is blank<br>";
					} 
					
					if ($Q4=="") {
						$error = true;
						$errormsg = $errormsg . "Q4 is blank<br>";
					} 
					
					if ($Q5=="") {
						$error = true;
						$errormsg = $errormsg . "Q5 is blank<br>";
					} 
					
					if ($Q6=="") {
						$error = true;
						$errormsg = $errormsg . "Q6 is blank<br>";
					} 
					
					if ($Q7=="") {
						$error = true;
						$errormsg = $errormsg . "Q7 is blank<br>";
					} 
					
					if ($Q9=="") {
						$error = true;
						$errormsg = $errormsg . "Q9 is blank<br>";
					} 
					
					if ($Q10=="") {
						$error = true;
						$errormsg = $errormsg . "Q10 is blank<br>";
					} 
					
					if ($Q11=="") {
						$error = true;
						$errormsg = $errormsg . "Q11 is blank<br>";
					} 
					
					if ($Q12=="") {
						$error = true;
						$errormsg = $errormsg . "Q12 is blank<br>";
					} 	

					if ($Q13=="") {
						$error = true;
						$errormsg = $errormsg . "Q13 is blank<br>";
					} 			
				
					include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
					dbConnect(); // Connect to Database	
					$stmt = $dbo->prepare('SELECT * FROM screener WHERE Participant_ID=:participant_id ORDER BY Screener_ID');
					$stmt->execute(array('participant_id' => $participant_id));
					$row_count = $stmt->rowCount();
					$result = $stmt->fetchAll();				
					if ($row_count==0) {
					} 
					else {
						foreach ($result as $row){
							$error = true;
							$errormsg = $errormsg . "There is an existing screener questionnaire (ID: " . $row['Screener_ID'] . ") for this participant<br>";
						}
					}
					$dbo = null; //Close DB connection
					
					if ($error === true) {
						echo $errormsg;
						echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
								<form action='screener_new' method='post'>
								<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
								<input name='Q2' type='hidden' size='20' value='" . $Q2 . "'></input>
								<input name='Q3' type='hidden' size='20' value='" . $Q3 . "'></input>
								<input name='Q4' type='hidden' size='20' value='" . $Q4 . "'></input>
								<input name='Q5' type='hidden' size='20' value='" . $Q5 . "'></input>
								<input name='Q6' type='hidden' size='20' value='" . $Q6 . "'></input>
								<input name='Q7' type='hidden' size='20' value='" . $Q7 . "'></input>
								<input name='Q8_1' type='hidden' size='20' value='" . $Q8_1 . "'></input>
								<input name='Q8_2' type='hidden' size='20' value='" . $Q8_2 . "'></input>
								<input name='Q8_3' type='hidden' size='20' value='" . $Q8_3 . "'></input>
								<input name='Q8_4' type='hidden' size='20' value='" . $Q8_4 . "'></input>	
								<input name='Q9' type='hidden' size='20' value='" . $Q9 . "'></input>
								<input name='Q10' type='hidden' size='20' value='" . $Q10 . "'></input>
								<input name='Q11' type='hidden' size='20' value='" . $Q11 . "'></input>
								<input name='Q12' type='hidden' size='20' value='" . $Q12 . "'></input>
								<input name='Q13' type='hidden' size='20' value='" . $Q13 . "'></input>
								<div align='right'><input type='submit' value='Back'></input></div></form>";
					} else {					
						include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
						dbConnect(); // Connect to Database								
						$stmt = $dbo->prepare("INSERT INTO screener(Participant_ID,Q2,Q3,Q4,Q5,Q6,Q7,Q8_1,Q8_2,Q8_3,Q8_4,Q9,Q10,Q11,Q12,Q13) VALUES(:participant_id,:Q2,:Q3,:Q4,:Q5,:Q6,:Q7,:Q8_1,:Q8_2,:Q8_3,:Q8_4,:Q9,:Q10,:Q11,:Q12,:Q13)");
						$stmt->execute(array(':participant_id' => $participant_id, ':Q2' => $Q2, ':Q3' => $Q3, ':Q4' => $Q4, ':Q5' => $Q5, ':Q6' => $Q6, ':Q7' => $Q7, ':Q8_1' => $Q8_1, ':Q8_2' => $Q8_2, ':Q8_3' => $Q8_3, ':Q8_4' => $Q8_4, ':Q9' => $Q9, ':Q10' => $Q10, ':Q11' => $Q11, ':Q12' => $Q12, ':Q13' => $Q13));
						$screener_id = $dbo->lastInsertId();
						$dbo = null; //Close DB connection
						
						//Calculating Eligibility
						include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/Eligibility.php");
						$eligibility = cal_eligibility($screener_id);
						$dbo = null; //Close DB connection
						
						print "<div align='center'><br>";
						print "<table class='outcomebox'><tr><th align='left'>Eligibility outcome of participant (ID: " . $participant_id . "):</th></tr><tr><td><h4>" . $eligibility[0] . "</h4></td></tr><tr><td>Reason/s: " . $eligibility[1] . "</td></tr></table></div><br><br>"; 
						print $eligibility[2] . "<br><br>";
						print "<div align='center'><a href='participant_info.php?participant_id=" . $participant_id . "'>Go to information page of Participant ID " . $participant_id . "</a></div>";
						
						include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
						dbConnect(); // Connect to Database			
						$pstatus="Screener outcome: " . $eligibility[0];	//Create PStatus based on Eligibility Outcome
						$stmt = $dbo->prepare("INSERT INTO log_pstatus(Participant_ID,PStatus,Timestamp) VALUES(:participant_id,:pstatus,:timestamp)");
						$stmt->execute(array(':participant_id' => $participant_id,':pstatus' => $pstatus,':timestamp' => date("Y-m-d H:i:s")));
						$dbo = null; //Close DB connection
						
						// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/EmailAppointment.php");
						// $email_id = 1; //Template 1: For newly appointment
						// EmailAppointment($participant_id,$appt_event,$appt_date,$appt_time,$appt_location,$email_id);
						
						// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/SendSMS.php");
						// $sms_id = 1; //Template 1: For newly appointment
						// print gw_send_sms($participant_id,$appt_event,$appt_date,$appt_time,$appt_location,$sms_id);	
						
						
						//if $_POST['prev_url'] is empty, redirect to participant.php page
						$prev_url = (empty($_POST['prev_url'])) ? 'participant.php?participant_id=' . $participant_id : $_POST['prev_url']; 	
						//header("location:" . $prev_url);							
					}
						
				} else {
					header("location:screener_new.php");		
				}
					
		?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 