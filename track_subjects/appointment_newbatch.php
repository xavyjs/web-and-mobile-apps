<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php PowerUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/DateTimePicker.php");  ?>

<?php //Retrieving Full Appointment Information
	$appointment_date=date("d M Y");
	$appointment_time=date("h:i A");
?>

<?php //Event dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_event');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "The database contains no event yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Event_ID']==$_REQUEST['event_id']) {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . " selected='selected'>" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			} elseif ($row['Event_ID']==$event_id) {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . " selected='selected'>" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			}
			else {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . ">" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			}
		}
	}
	$dbo = null; //Close DB connection
?>

<?php //Location dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_location');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no location yet";
	} else {
		foreach ($result as $row){
			if ($row['Location_ID']==$_POST['location_id']) {
				$location_id_row = $location_id_row . "<option value=" . $row['Location_ID'] . " selected='selected'>" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
			} else {
				$location_id_row = $location_id_row . "<option value=" . $row['Location_ID'] . ">" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
			}
		}
	}
	$dbo = null; //Close DB connection
?>

			
<?php 
	if(isset($_POST['selcheckbox'])) {
		// foreach(array_keys($_POST['selcheckbox']) as $i) {
			// $del_id = $del_id . $_POST['selcheckbox'][$i].",";
		// }	
		// $del_id = substr($del_id, 0, -1); 
		
		//implode array
		$inQuery = implode(',', array_fill(0, count($_POST['selcheckbox']), '?'));

		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database
		
		$stmt = $dbo->prepare('SELECT p.Participant_ID, t.Title, p.Title_ID, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote, s1.PStatus 
								FROM participants AS p 
								LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID
								LEFT JOIN log_pstatus AS s1 on p.Participant_ID = s1.Participant_ID
								LEFT OUTER JOIN log_pstatus AS s2 on (p.Participant_ID = s2.Participant_ID AND s1.PStatus_ID < s2.PStatus_ID)
								WHERE s2.PStatus_ID IS NULL
								AND p.Participant_ID in (' . $inQuery . ')
								ORDER BY p.Participant_ID');
			// bindvalue is 1-indexed, so $k+1
			$stmt->execute($_POST['selcheckbox']);
			$row_count = $stmt->rowCount();

			$result = $stmt->fetchAll();
			 
			if ($row_count==0) {
				$appointment_row = "<tr><td colspan='10'>No participant</td></tr>";	
			} else {	
				$j=0;
				foreach ($result as $row){			
					$participant_id_row = "<tr><td><a href='participant_info.php?participant_id=" . $row['Participant_ID'] . "'>" . $row['Participant_ID'] . "</a></td>";		
					$title_row = "<td>" . $row['Title'] . "</td>";
					$lastname_row = "<td>" . $row['Lastname'] . "</td>";
					$firstname_row = "<td>" . $row['Firstname'] . "</td>";
					$mobile_row = "<td>" . $row['Mobile'] . "</td>";
					$email_row = "<td>" . $row['Email'] . "</td>";
					//$address_row = "<td>" . $row['Address1'] . "</td>";
					$postcode_row = "<td>" . $row['Postcode'] . "</td>";
					
					//Calculate Incentive
					$total_incentive=0;
					$incentive_paid=0;
					$incentive_balance=0;
					
					// Get Lottery Incentive
					$stmt2 = $dbo->prepare('SELECT * FROM lottery WHERE Participant_ID=?');
					$stmt2->execute(array($row['Participant_ID']));
					$row_count2 = $stmt2->rowCount();
					$result2 = $stmt2->fetchAll();		
					if ($row_count2==0) {
					} 
					else {
						foreach ($result2 as $row2){
							$total_incentive=$total_incentive+500;
						}	
					}
					
					$stmt2 = $dbo->prepare('SELECT * FROM log_incentive WHERE Participant_ID=?');
					$stmt2->execute(array($row['Participant_ID']));
					$row_count2 = $stmt2->rowCount();
					$result2 = $stmt2->fetchAll();
					
					if ($row_count2==0) {
						$incentive_balance='<td>' . '-' . '</td>';
					} 
					else {
						foreach ($result2 as $row2){
							$total_incentive=$total_incentive+$row2['Total_Incentive'];
							$incentive_paid=$incentive_paid+$row2['Incentive_Paid'];
						}
					}
					$incentive_balance='<td>S$' . number_format(($total_incentive-$incentive_paid),2) . '</td>';
											
					//For note field with hover tip
					if ($row['PNote'] != null) {
						$pnote_row = "<td><div style='cursor:help;clear:both;' title='" . str_replace( "'","&#39;",$row['PNote']) . "'>&#10004;</div></td>";	
					} else {
						$pnote_row = "<td></td>";
					}
					$pstatus_row = "<td>" . $row['PStatus'] . "</td>";
					
					$j++;
					$participant_row = $participant_row . $participant_id_row . $title_row . $lastname_row . $firstname_row . $mobile_row . $email_row . $incentive_balance . $pnote_row . $pstatus_row . '<td><input type="text" name="appointment_time'.$j.'" id="timepicker'.$j.'" readonly="readonly" value="' . (isset($_POST['appointment_time'.$j]) ? $_POST['appointment_time'.$j] : "") . '"></td></tr>';		
					
				}									
			}	
			$dbo = null; //Close DB connection	
	}
?>
											
			<h1 class='title'>Batch Create Appointments</h1>
			<p class='title'>Please fill in the new appointment information</p>
			
			<form action="appointment_newbatch_process.php" method="post">
			<table id="tbl_registration">
			<tr>
				<td>					
				</td>
			</tr>
			
				<table  class='new'>
					<tr>
						<td align='right'>Event:</td>
						<td align='left'>			
							<select name="event_id">					
								<?php echo $event_id_row; ?>	
							</select>
						</td>
					</tr>
					<tr>
						<td align='right'>Fitbit Start Date:</td>
						<td align='left'>
							<input type="text" name="fitbit_start_date" id="datepicker" readonly="readonly" value="<?php echo isset($_POST['fitbit_start_date']) ? $_POST['fitbit_start_date'] : $fitbit_start_date ?>"/>
						</td>
					</tr>
					<tr>
						<td align='right'>Appointment Date:</td>
						<td align='left'>
							<input type="text" name="appointment_date" id="datepicker1" readonly="readonly" value="<?php echo isset($_POST['appointment_date']) ? $_POST['appointment_date'] : $appointment_date ?>"/>
						</td>
					</tr>
					<tr>
						<td align='right'>Location:</td>
						<td align='left'>			
							<select name="location_id">					
								<?php echo $location_id_row; ?>	
							</select>
						</td>
					</tr>
				</table>
				<br><br>
				<table class='wborder' width='960px'>
					<tr>	
						<th width='3%'>PID</th>									
						<th width='6%'>Title</th>
						<th>Last Name</th>		
						<th>First Name</th>	
						<th>Mobile</th>	
						<th>Email</th>	
						<th width='9%'>Balance</th>		
						<th width='5%'>PNote</th>
						<th>PStatus</th>		
						<th>Appt Time</th>		
					</tr>		
					<?php echo $participant_row; ?>
				</table>	
			<tr>
				<td>
					<br>
					<?php 
						if(isset($_POST['selcheckbox'])) {
							foreach($_POST['selcheckbox'] as $value)
							{
							  echo '<input type="hidden" name="selcheckbox[]" value="'. $value. '">';
							}
							echo '<input type="hidden" name="numofparticipant" value="'. $j. '">';
							echo '<input type="submit" value="Create Appointments"></input>&nbsp;&nbsp;&nbsp;';
						}
					?>		
					<a href="participant_select.php">Back to participant page</a>
				</td>
			</tr>
			</table>
			
			</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>