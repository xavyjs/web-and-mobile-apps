<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Screener Questionnaire Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$sql="SELECT * FROM (
			SELECT Participant_ID, Mobile, Message, Timestamp, 'Sent' AS SendReceive FROM log_sms
			UNION ALL
			SELECT Participant_ID, Mobile, Message, Timestamp, 'Received' AS SendReceive FROM log_sms_reply
			) s
			WHERE Participant_ID = :participant_aid
			ORDER BY Timestamp DESC";
	$stmt = $dbo->prepare($sql);
	$stmt->execute(array('participant_aid' => $_GET['participant_aid']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$sendreceive = "<tr><td align='left'>" . $row['SendReceive'] . "</td>";
			$timestamp = "<td align='left'>" . date("d-m-Y H:i", strtotime($row['Timestamp'])) . "</td>";
			$message = "<td align='left'>" . htmlspecialchars($row['Message']) . "</td></tr>";
			$sendreceive_row = $sendreceive_row . $sendreceive . $timestamp . $message;
		}
	}						
	//Close DB connection
	$dbo = null;
?>
			
			<h1 class='title'>SMS</h1>
			<p class='title'>List of SMS sent to and received from participant</p>
						
			<table class='participantinfo'>
				<tr>
					<th align='left' valign='top' bgcolor="#cdcdcd" colspan='2'>SMS Sent/Received</th>
					<td align='right' bgcolor="#cdcdcd" style='border:none;'></td>
				</tr>
				<tr>
					<td align='left' valign='top' width='8%' bgcolor="#cdcdcd"></td>									
					<td align='left' valign='top' width='13%' bgcolor="#cdcdcd">Timestamp</td>
					<td align='left' valign='top' width='79%' bgcolor="#cdcdcd">Message</td>						
				</tr>		
				<?php echo $sendreceive_row; ?>
			</table>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
