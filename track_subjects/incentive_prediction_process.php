<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;	
		$participant_id=$_POST['participant_id'];		
		$event_id=$_POST['event_id'];	
		$next_mth_prediction=$_POST['next_mth_prediction'];
		$prev_url=$_POST['prev_url'];
		echo $prev_url;
		
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='" . $prev_url . "' method='post'>
					<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
					<input name='event_id' type='hidden' size='20' value='" . $event_id . "'></input>
					<input name='next_mth_prediction' type='hidden' size='20' value='" . $next_mth_prediction . "'></input>
					<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
					<input type='submit' value='Back'></input></form>";
		} else {					
		
			if ($next_mth_prediction=='') {
				$next_mth_prediction=null;
			}
		
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database		
			
			switch ($event_id)
			{
			case 3:
				$sql="UPDATE participants SET Prediction_Mth1=? WHERE Participant_ID=?";
				break;
			case 4: 
				$sql="UPDATE participants SET Prediction_Mth2=? WHERE Participant_ID=?";
				break;
			case 5:
				$sql="UPDATE participants SET Prediction_Mth3=? WHERE Participant_ID=?";
				break;
			case 6:  
				$sql="UPDATE participants SET Prediction_Mth4=? WHERE Participant_ID=?";
				break;
			default:
				$sql="UPDATE participants SET Prediction_Mth1=? WHERE Participant_ID=?";
				break;
			}
			
			$stmt = $dbo->prepare($sql);
			$stmt->execute(array($next_mth_prediction, $participant_id));						

			$dbo = null; //Close DB connection			
			header("location:" . $prev_url . "&prediction_status=updated");							
		}
	} else {
		header("location:index.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 