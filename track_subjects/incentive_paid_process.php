<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;	
		$participant_id=$_POST['participant_id'];		
		$event_id=$_POST['event_id'];	
		$incentive_paid=$_POST['incentive_paid'];
		$prev_url=$_POST['prev_url'];
		
		if ($incentive_paid=="") {
			$error = true;
			$errormsg = $errormsg . "Incentive Paid cannot be empty<br>";
		} 
		
		if (is_numeric($incentive_paid)===false) {
						$error = true;
						$errormsg = $errormsg . "Incentive Paid must be numbers<br>";
		} 		
		
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='" . $prev_url . "' method='post'>
					<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
					<input name='event_id' type='hidden' size='20' value='" . $event_id . "'></input>
					<input name='incentive_paid' type='hidden' size='20' value='" . $incentive_paid . "'></input>
					<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
					<input type='submit' value='Back'></input></form>";
		} else {					
		
			if ($incentive_paid=='') {
				$incentive_paid=null;
			}
		
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database		
			
			$stmt = $dbo->prepare("UPDATE log_incentive SET Incentive_Paid=? WHERE Participant_ID=? AND Event_ID=?");
			$stmt->execute(array($incentive_paid, $participant_id, $event_id));						

			$dbo = null; //Close DB connection			
			header("location:" . $prev_url);							
		}
	} else {
		header("location:index.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 