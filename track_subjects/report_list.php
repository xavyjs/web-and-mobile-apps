<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>


<h1 class='title'>Reports</h1>	
<p class='title'>Click on links to download</p>

<table class='participantinfo'> 
	<tr>
		<th width='5%'>S/N</th>
		<th width='30%'>Report</th>
		<th>Description</th>
	</tr>
	<tr>
		<td>1.</td>
		<td><a href="/export_adherencereport.php">Adherence Report</a></td>
		<td></td>
	</tr>
</table>
<br><br>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 