<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;						
		$participant_id=$_POST['participant_id'];
		$appointment_id=$_POST['appointment_id'];
		$event_id=$_POST['event_id'];
		$apptstatus_id=$_POST['apptstatus_id'];
		$newappt_chkbox=$_POST['newappt_chkbox'];
		
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='appointment_edit' method='post'>
					<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
					<input name='appointment_id' type='hidden' size='20' value='" . $appointment_id . "'></input>
					<input name='apptstatus_id' type='hidden' size='20' value='" . $apptstatus_id . "'></input>
					<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
					<input type='submit' value='Back'></input></form>";
		} else {					
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database		
															
			$stmt = $dbo->prepare("UPDATE appointments SET ApptStatus_ID=? WHERE Appointment_ID=?");
			$stmt->execute(array($apptstatus_id, $appointment_id));	

			If ($apptstatus_id==3) { //Create PStatus if Appointment Status = Attended
				$stmt = $dbo->prepare('SELECT Event FROM ctbl_event WHERE Event_ID=:event_id');
				$stmt->execute(array('event_id' => $event_id));
				$row_count = $stmt->rowCount();
				$result = $stmt->fetchAll();
				
				if ($row_count==0) {
				} else {
					foreach ($result as $row){
						$event=$row['Event'];
					}
				}
				$pstatus="Attended " . $event; //Create PStatus AS Attended {Event}	 
			}
			
			if ($apptstatus_id==4) { //Missed		
				$stmt = $dbo->prepare('SELECT Event FROM ctbl_event WHERE Event_ID=:event_id');
				$stmt->execute(array('event_id' => $event_id));
				$row_count = $stmt->rowCount();
				$result = $stmt->fetchAll();
				
				if ($row_count==0) {
				} else {
					foreach ($result as $row){
						$event=$row['Event'];
					}
				}
				$todo="Missed " . $event; //Create PStatus AS Missed {Event}	 
			}
			
			If ($apptstatus_id==6) { //Create PStatus AS Dropped Out if Appointment Status = Closed
				$pstatus="Dropped out";	
			}
								
			if (isset($pstatus)) { //if pstatus is not blank, call function and insert pstatus
				include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/PStatus.php");
				PStatus_Add($participant_id,$pstatus);
			}
			if (isset($todo)) { //if todo is not blank, call function and insert todo
				include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/ToDo.php");
				ToDo_Add($participant_id,$appointment_id,$todo);
			}			
						
			$dbo = null; //Close DB connection			
			if (isset($_POST['newappt_chkbox'])) {
				header("location:/appointment_new.php?participant_id=" . $participant_id . "&appointment_id=" . $appointment_id . "&appt_type=today&prev_url=" . urlencode($_POST['prev_url']));	
				//echo "prev:".$_POST['prev_url'];						
			} else {
				header("location:/index.php");
				//echo "index";
			}						
		}
	} else {
		header("location:appointment_edit.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 