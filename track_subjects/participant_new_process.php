<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

	<h1 class='title'>Create new participant</h1>
	<p class='title'>Adding new driver to screener list</p>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;
		$participant_id=$_POST['participant_id'];
		$age=$_POST['age'];
		$race_id=$_POST['race_id'];
		$gender_id=$_POST['gender_id'];
		$mobile=$_POST['mobile'];
		$study_arm_id=$_POST['study_arm_id'];
		$num_daily_doses=$_POST['num_daily_doses'];
		$interval_1to2=$_POST['interval_1to2'];
		$interval_2to3=$_POST['interval_2to3'];
		$interval_3to4=$_POST['interval_3to4'];
		$interval_4to5=$_POST['interval_4to5'];
		$interval_5to6=$_POST['interval_5to6'];
		$interval_6to7=$_POST['interval_6to7'];
		$interval_7to8=$_POST['interval_7to8'];
		$baseline_assessment_date=date('Y-m-d',strtotime($_POST['baseline_assessment_date']));
		$month3_assessment_date=date('Y-m-d',strtotime($_POST['month3_assessment_date']));
		$month6_assessment_date=date('Y-m-d',strtotime($_POST['month6_assessment_date']));
		$healthcare_cost_mth13=$_POST['healthcare_cost_mth13'];
		$healthcare_cost_mth46=$_POST['healthcare_cost_mth46'];
		
		// Check for race
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
		$stmt = $dbo->prepare('SELECT Race FROM ctbl_race WHERE Race_ID=:race_id');
			$stmt->execute(array('race_id' => $race_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		if ($row_count==0) {
		} 
		else {
			foreach ($result as $row){														
				$race=$row['Race'];	
			}
		}					
		$dbo = null; //Close DB connection
		
		// Check for gender
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
		$stmt = $dbo->prepare('SELECT Gender FROM ctbl_gender WHERE Gender_ID=:gender_id');
			$stmt->execute(array('gender_id' => $gender_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		if ($row_count==0) {
		} 
		else {
			foreach ($result as $row){														
				$gender=$row['Gender'];	
			}
		}					
		$dbo = null; //Close DB connection
		
		// Check for study arm
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
		$stmt = $dbo->prepare('SELECT Study_Arm FROM ctbl_study_arm WHERE Study_Arm_ID=:study_arm_id');
			$stmt->execute(array('study_arm_id' => $study_arm_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		if ($row_count==0) {
		} 
		else {
			foreach ($result as $row){														
				$study_arm=$row['Study_Arm'];	
			}
		}					
		$dbo = null; //Close DB connection
		
		// Check for existing participant
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
		$stmt = $dbo->prepare('SELECT * FROM participants WHERE Participant_ID=:participant_id');
			$stmt->execute(array('participant_id' => $participant_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		if ($row_count==0) {
		} 
		else {
			foreach ($result as $row){														
				$participant_id=$row['Participant_ID'];	
				$error = true;
				$errormsg = $errormsg . "Participant (ID: " . $participant_id . ") already exists<br>";
			}
		}					
		$dbo = null; //Close DB connection
		
		if ($participant_id=="") {
			$error = true;
			$errormsg = $errormsg . "Participant ID cannot be empty<br>";
		} 
		
		if ($age=="") {
			$error = true;
			$errormsg = $errormsg . "Age cannot be empty<br>";
		} 
		
		if (is_numeric($age)===false & $age!="") {
			$error = true;
			$errormsg = $errormsg . "Age must be a number<br>";
		} 
		
		if ($mobile=="") {
			$error = true;
			$errormsg = $errormsg . "Mobile No cannot be empty<br>";
		} 
		
		if ((is_numeric($mobile)===false || strlen($mobile)!=8) && $mobile!="") {
			$error = true;
			$errormsg = $errormsg . "Mobile No must be 8 numbers<br>";
		} 
		
		if ($num_daily_doses=="") {
			$error = true;
			$errormsg = $errormsg . "No. of Daily Doses cannot be empty<br>";
		} 
		
		if ($num_daily_doses==2 && ($interval_1to2=="" || $interval_2to3!="" || $interval_3to4!="" || $interval_4to5!="" || $interval_5to6!="" || $interval_6to7!="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==3 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4!="" || $interval_4to5!="" || $interval_5to6!="" || $interval_6to7!="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==4 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4=="" || $interval_4to5!="" || $interval_5to6!="" || $interval_6to7!="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==5 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4=="" || $interval_4to5=="" || $interval_5to6!="" || $interval_6to7!="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==6 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4=="" || $interval_4to5=="" || $interval_5to6=="" || $interval_6to7!="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==7 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4=="" || $interval_4to5=="" || $interval_5to6=="" || $interval_6to7=="" || $interval_7to8!="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		if ($num_daily_doses==8 && ($interval_1to2=="" || $interval_2to3=="" || $interval_3to4=="" || $interval_4to5=="" || $interval_5to6=="" || $interval_6to7=="" || $interval_7to8=="")) {
			$error = true;
			$errormsg = $errormsg . "Number of Intervals do not match Number of Doses<br>";
		} 
		
		if ($_POST['baseline_assessment_date']=="") {
			$error = true;
			$errormsg = $errormsg . "Date of Baseline Assessment cannot be empty<br>";
		} 
		
		if ($_POST['month3_assessment_date']=="") {
			$error = true;
			$errormsg = $errormsg . "Date of Month 3 Assessment cannot be empty<br>";
		} 
		
		if ($_POST['month6_assessment_date']=="") {
			$error = true;
			$errormsg = $errormsg . "Date of Month 6 Assessment cannot be empty<br>";
		} 
		
		if (is_numeric($healthcare_cost_mth13)===false && $healthcare_cost_mth13!="") {
			$error = true;
			$errormsg = $errormsg . "Healthcare Cost Month 1-3 must be a number<br>";
		} 
		
		if (is_numeric($healthcare_cost_mth46)===false && $healthcare_cost_mth46!="") {
			$error = true;
			$errormsg = $errormsg . "Healthcare Cost Month 4-6 must be a number<br>";
		} 
		
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='participant_new' method='post'>
					<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
					<input name='age' type='hidden' size='20' value='" . $age . "'></input>
					<input name='race_id' type='hidden' size='20' value='" . $race_id . "'></input>
					<input name='gender_id' type='hidden' size='20' value='" . $gender_id . "'></input>
					<input name='mobile' type='hidden' size='20' value='" . $mobile . "'></input>
					<input name='study_arm_id' type='hidden' size='20' value='" . $study_arm_id . "'></input>
					<input name='num_daily_doses' type='hidden' size='20' value='" . $num_daily_doses . "'></input>
					<input name='interval_1to2' type='hidden' size='20' value='" . $interval_1to2 . "'></input>
					<input name='interval_2to3' type='hidden' size='20' value='" . $interval_2to3 . "'></input>
					<input name='interval_3to4' type='hidden' size='20' value='" . $interval_3to4 . "'></input>
					<input name='interval_4to5' type='hidden' size='20' value='" . $interval_4to5 . "'></input>
					<input name='interval_5to6' type='hidden' size='20' value='" . $interval_5to6 . "'></input>
					<input name='interval_6to7' type='hidden' size='20' value='" . $interval_6to7 . "'></input>
					<input name='interval_7to8' type='hidden' size='20' value='" . $interval_7to8 . "'></input>
					<input name='baseline_assessment_date' type='hidden' size='20' value='" . $_POST['baseline_assessment_date'] . "'></input>
					<input name='month3_assessment_date' type='hidden' size='20' value='" . $_POST['month3_assessment_date'] . "'></input>
					<input name='month6_assessment_date' type='hidden' size='20' value='" . $_POST['month6_assessment_date'] . "'></input>
					<input name='healthcare_cost_mth13' type='hidden' size='20' value='" . $healthcare_cost_mth13 . "'></input>
					<input name='healthcare_cost_mth46' type='hidden' size='20' value='" . $healthcare_cost_mth46 . "'></input>
					<div align='right'><input type='submit' value='Back'></input></div></form>";
					
		} else {					
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database								
			$stmt = $dbo->prepare("INSERT INTO participants(Participant_ID,Age,Race_ID,Gender_ID,Mobile,Study_Arm_ID,Num_Daily_Doses,Interval_1to2,Interval_2to3,Interval_3to4,Interval_4to5,Interval_5to6,Interval_6to7,Interval_7to8, Baseline_Assessment_Date, Month3_Assessment_Date, Month6_Assessment_Date, Healthcare_Cost_Mth13, Healthcare_Cost_Mth46) VALUES(:participant_id,:age,:race_id,:gender_id,:mobile,:study_arm_id,:num_daily_doses,:interval_1to2,:interval_2to3,:interval_3to4,:interval_4to5,:interval_5to6,:interval_6to7,:interval_7to8,:baseline_assessment_date,:month3_assessment_date,:month6_assessment_date,:healthcare_cost_mth13,:healthcare_cost_mth46)");
			$stmt->execute(array(':participant_id' => $participant_id, ':age' => $age, ':race_id' => $race_id, ':gender_id' => $gender_id, ':mobile' => $mobile, ':study_arm_id' => $study_arm_id, ':num_daily_doses' => $num_daily_doses, ':interval_1to2' => $interval_1to2, ':interval_2to3' => $interval_2to3, ':interval_3to4' => $interval_3to4, ':interval_4to5' => $interval_4to5, ':interval_5to6' => $interval_5to6, ':interval_6to7' => $interval_6to7, ':interval_7to8' => $interval_7to8, ':baseline_assessment_date' => $baseline_assessment_date, ':month3_assessment_date' => $month3_assessment_date, ':month6_assessment_date' => $month6_assessment_date, ':healthcare_cost_mth13' => $healthcare_cost_mth13, ':healthcare_cost_mth46' => $healthcare_cost_mth46));
			print "<div align='center'>";
			print "<table class='outcomebox'><tr><th>Participant ID (" . $participant_id . ") has been created.</th></tr></table>"; 
			print "<a href='participant_info.php?participant_aid=" . $dbo->lastInsertId() . "'>Proceed to Participant Information page</a></div>";
			
			$dbo = null; //Close DB connection
			
			// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/EmailAppointment.php");
			// $email_id = 1; //Template 1: For newly appointment
			// EmailAppointment($participant_id,$appt_event,$appt_date,$appt_time,$appt_location,$email_id);
			
			// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/SendSMS.php");
			// $sms_id = 1; //Template 1: For newly appointment
			// print gw_send_sms($participant_id,$appt_event,$appt_date,$appt_time,$appt_location,$sms_id);	
							
			//header("location:" . $prev_url);							
		}
			
	} else {
		header("location:participant_new.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 