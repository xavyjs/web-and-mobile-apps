<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;						
		$participant_id=$_POST['participant_id'];
		$appointment_id=$_POST['appointment_id'];
		$event_id=$_POST['event_id'];
		$fitbit_start_date=date('Y-m-d',strtotime($_POST['fitbit_start_date']));
		$appointment_date=date('Y-m-d',strtotime($_POST['appointment_date']));
		$appointment_time=date('H:i:s',strtotime($_POST['appointment_time']));
		$location_id=$_POST['location_id'];
		$apptstatus_id=$_POST['apptstatus_id'];
		$note=$_POST['note'];
		$disable_sms=$_POST['disable_sms'];
		
		if ($_POST['fitbit_start_date']=="" && in_array($_POST['event_id'], array('3', '4', '5', '6', '7', '8', '9', '10'))) {
			$error = true;
			$errormsg = $errormsg . "Fitbit Start Date cannot be empty for this event<br>";
		} 
		if ($_POST['appointment_date']=="") {
			$error = true;
			$errormsg = $errormsg . "Appointment Date cannot be empty<br>";
		} 
		if ($_POST['appointment_time']=="") {
			$error = true;
			$errormsg = $errormsg . "Appointment Time cannot be empty<br>";
		} 
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='appointment_edit' method='post'>
					<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
					<input name='appointment_id' type='hidden' size='20' value='" . $appointment_id . "'></input>
					<input name='event_id' type='hidden' size='20' value='" . $event_id . "'></input>
					<input name='fitbit_start_date' type='hidden' size='20' value='" . $_POST['fitbit_start_date'] . "'></input>
					<input name='appointment_date' type='hidden' size='20' value='" . $_POST['appointment_date'] . "'></input>
					<input name='appointment_time' type='hidden' size='20' value='" . $_POST['appointment_time'] . "'></input>
					<input name='location_id' type='hidden' size='20' value='" . $location_id . "'></input>
					<input name='apptstatus_id' type='hidden' size='20' value='" . $apptstatus_id . "'></input>
					<input name='note' type='hidden' size='20' value='" . $note . "'></input>
					<input name='disable_sms' type='hidden' size='20' value='" . $disable_sms . "'></input>
					<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
					<input type='submit' value='Back'></input></form>";
		} else {					
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database		
			
			if ($_POST['fitbit_start_date']=="") {
				$fitbit_start_date = null;
			} 
				
			$stmt = $dbo->prepare('SELECT ApptStatus_ID FROM appointments WHERE Appointment_ID=:appointment_id');
			$stmt->execute(array('appointment_id' => $appointment_id));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			
			if ($row_count==0) {
			} 
			else {
				foreach ($result as $row){
					$prev_apptstatus_id=$row['ApptStatus_ID'];
				}
			}
											
			$stmt = $dbo->prepare("UPDATE appointments SET Event_ID=?, Fitbit_Start_Date=?, Appointment_Date=?, Appointment_Time=?, Location_ID=?, ApptStatus_ID=?, Note=?, Disable_SMS=? WHERE Appointment_ID=?");
			$stmt->execute(array($event_id, $fitbit_start_date, $appointment_date, $appointment_time, $location_id, $apptstatus_id, $note, $disable_sms, $appointment_id));	

			If ($apptstatus_id==3) { //Create PStatus if Appointment Status = Attended
				$stmt = $dbo->prepare('SELECT Event FROM ctbl_event WHERE Event_ID=:event_id');
				$stmt->execute(array('event_id' => $event_id));
				$row_count = $stmt->rowCount();
				$result = $stmt->fetchAll();
				
				if ($row_count==0) {
				} 
				else {
					foreach ($result as $row){
						$event=$row['Event'];
					}
				}
				$pstatus="Attended " . $event;	
				$stmt = $dbo->prepare("INSERT INTO log_pstatus(Participant_ID,PStatus,Timestamp) VALUES(:participant_id,:pstatus,:timestamp)");
				$stmt->execute(array(':participant_id' => $participant_id,':pstatus' => $pstatus,':timestamp' => date("Y-m-d H:i:s")));
			}
			
			If ($apptstatus_id==6) { //Create PStatus AS Dropped Out if Appointment Status = Closed
				$pstatus="Dropped out";	
				$stmt = $dbo->prepare("INSERT INTO log_pstatus(Participant_ID,PStatus,Timestamp) VALUES(:participant_id,:pstatus,:timestamp)");
				$stmt->execute(array(':participant_id' => $participant_id,':pstatus' => $pstatus,':timestamp' => date("Y-m-d H:i:s")));
			}
			
			// If ($prev_apptstatus_id==1||$prev_apptstatus_id==2) {
				// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/EmailAppointment.php");
				// $email_id = 3; //Template 3: For updated appointment
				// EmailAppointment($participant_id,$event_id,$appointment_date,$appointment_time,$location_id,$email_id);
			// }	
			
			$dbo = null; //Close DB connection			
			if (isset($_POST['prev_url']) && $_POST['prev_url']<>"") {
				header("location:" . $_POST['prev_url']);				
			} else {
				header("location:/index.php");
				//echo "index";
			}						
		}
	} else {
		header("location:appointment_edit.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 