<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Personal Details Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT p.Fitbit_User_ID, p.access_token, p.access_token_secret, t.Title, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote FROM participants AS p LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID WHERE p.Participant_ID=:participant_id');
	$stmt->execute(array('participant_id' => $_GET['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "<input name='name' size='80' type='text' disabled='disabled' value='No participant selected'></input>";
	} 
	else {
		foreach ($result as $row){
			$fitbit_user_id = $row['Fitbit_User_ID'];
			$access_token = $row['access_token'];
			$access_token_secret = $row['access_token_secret'];
			$title = $row['Title'];
			$lastname = $row['Lastname'];
			$firstname = $row['Firstname'];
			$mobile = $row['Mobile'];
			$email = $row['Email'];
			$address1 = $row['Address1'];
			$postcode = $row['Postcode'];
			$pnote = $row['PNote'];
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Fitbit Connection Check
	if (isset($fitbit_user_id)) {	
		$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['value']=$access_token;	
		$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['secret']=$access_token_secret;	
		$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['authorized']=true;		
		require('http.php');
		require('oauth_client.php');

		$client = new oauth_client_class;
		$client->debug = 1;
		$client->debug_http = 1;
		$client->server = 'Fitbit';
		$client->redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].
			dirname(strtok($_SERVER['REQUEST_URI'],'?')).'/fitbit_connectcheck.php';

		$client->client_id = '2e5313cfc3d44c3e9c20d1d724fdac7a'; $application_line = __LINE__; // Fitbit Consumer Key from dev.fitbit.com | Account: edmund.shen@duke-nus.edu.sg
		$client->client_secret = '4ab38cf55bdb43e9b4cb61be02217421'; // Fitbit Consumer Secret

		if(strlen($client->client_id) == 0
		|| strlen($client->client_secret) == 0)
			die('Please go to Fitbit application registration page https://dev.fitbit.com/apps/new , '.
				'create an application, and in the line '.$application_line.
				' set the client_id to Consumer key and client_secret with Consumer secret. '.
				'The Callback URL must be '.$client->redirect_uri).' Make sure this URL is '.
				'not in a private network and accessible to the Fitbit site.';

		if(($success = $client->Initialize()))
		{
			if(($success = $client->Process()))
			{
				if(strlen($client->access_token))
				{
					$success = $client->CallAPI(
						'https://api.fitbit.com/1/user/-/apiSubscriptions.json', // Check Subscription
						'GET', array(), array('FailOnAccessError'=>true), $user);					
				}
			}
			$success = $client->Finalize($success);
		}
		if($client->exit)
			exit;
		if($success) {	
			if ($user->apiSubscriptions) {
			} else {
				$fitbit_msg = "Subscription does not exist; reason/s: User revokes permission or application changed between read and read/write. Solution: Reconnect to Fitbit API ";
			}
		} else {
			$fitbit_msg = "Fitbit connection error; possibly due to wrong access token/secret. Solution: Reconnect to Fitbit API";
		}
		$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['value']=null;	
		$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['secret']=null;	
		$_SESSION['OAUTH_ACCESS_TOKEN']['http://api.fitbit.com/oauth/access_token']['authorized']=null;	
	}
	$link_fitbit_user_id = '<a href="participant_fitbit_connect.php?participant_id=' . $_REQUEST['participant_id'] . '&fitbit_user_id=' . $fitbit_user_id . '">' . $fitbit_user_id . '</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="participant_fitbit_delete.php?participant_id=' . $_REQUEST['participant_id'] . '&fitbit_user_id=' . $fitbit_user_id . '&prev_url=' . urlencode($_SERVER["REQUEST_URI"]) . '">Del</a>';
	if ($fitbit_msg) {
		$link_fitbit_user_id = $link_fitbit_user_id . ' <div style="font-weight:bold;color:#ff0000;cursor:help;display:inline-block;clear:both;" title="' . str_replace( '"','&#39;',$fitbit_msg) . '">!</div>';
	}
?>

<?php //Fitbit Steps Data

	if (isset($fitbit_user_id)) {
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database
		
		$stmt = $dbo->prepare('SELECT MAX(Steps_Date), Auto_Timestamp FROM log_steps WHERE Fitbit_User_ID=:fitbit_user_id AND Auto_Timestamp IS NOT NULL GROUP BY Auto_Timestamp');
		$stmt->execute(array('fitbit_user_id' => $fitbit_user_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		
		if ($row_count==0) {
			$row_fitbit = "Sync Now";
		} 
		else {
			foreach ($result as $row){
				$row_fitbit = date("d-m-Y H:i", strtotime($row['Auto_Timestamp']));
			}
		}						
		$dbo = null; //Close DB connection
	}
?>

<?php //Log PStatus Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM log_pstatus WHERE Participant_ID=:participant_id ORDER BY PStatus_ID');
	$stmt->execute(array('participant_id' => $_GET['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$pstatus = $row['PStatus'];
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Screener Questionnaire Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT s.*, c.ScrStatus FROM screener as s 
							LEFT JOIN ctbl_scrstatus as c ON s.ScrStatus_ID = c.ScrStatus_ID
							WHERE s.Participant_ID=:participant_id ORDER BY s.Screener_ID');
	$stmt->execute(array('participant_id' => $_GET['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		$link_screener = "<a href='screener_new.php?participant_id=" . $_GET['participant_id'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>New</a>";
	} 
	else {
		foreach ($result as $row){
			$screener_id = $row['Screener_ID'];
			$link_screener = "&nbsp;<a href='screener_edit.php?participant_id=" . $_GET['participant_id'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a>
								&nbsp;<a href='screener_view.php?participant_id=" . $_GET['participant_id'] . "&screener_id=" . $screener_id . "'>View</a>";
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/Eligibility.php");
			$eligibility = cal_eligibility($row['Screener_ID']);
			$eligibility_outcome = $eligibility[0];
			$eligibility_reason = $eligibility[1];
			$eligibility_scrstatus = $row['ScrStatus'];
			$eligibility_timestamp = date("d-m-Y H:i", strtotime($row['Timestamp']));
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Get Event Name into dynamic variable
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT Event FROM ctbl_event ORDER BY Event_ID');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result_event = $stmt->fetchAll();				
	$dbo = null; //Close DB connection
?>

<?php //Event Data with dynamic variables
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	$stmt = $dbo->prepare('SELECT l.Location, s.ApptStatus, a.* FROM appointments AS a 
							LEFT JOIN ctbl_apptstatus AS s ON a.ApptStatus_ID = s.ApptStatus_ID 
							LEFT JOIN ctbl_location AS l on a.Location_ID = l.Location_ID 
							WHERE a.Participant_ID=:participant_id 
							ORDER BY Appointment_ID');
	$stmt->execute(array('participant_id' => $_REQUEST['participant_id']));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		//$event1_date = "<a href='appointment_new.php?participant_id=" . $_REQUEST['participant_id'] . "&event_id=1'>Make appointment</a>";
	} 
	else {
		foreach ($result as $row){
			$event_id = $row['Event_ID'];
			$result_array[$event_id]['apptid'] = $row['Appointment_ID']; //Dynamic variable by concatenating
			$result_array[$event_id]['date'] = date("d M Y, D", strtotime($row['Appointment_Date']));
			$result_array[$event_id]['time'] = date("h:i A", strtotime($row['Appointment_Time']));
			$result_array[$event_id]['location'] = $row['Location'];
			$result_array[$event_id]['status'] = $row['ApptStatus'];
			// $result_array[$event_id]['reply'] = $row['Reply_chkbox'];
			$result_array[$event_id]['note'] = $row['Note'];
			
			$result_array[$event_id]['link_new'] = "&nbsp;<a href='appointment_new.php?participant_id=" . $row['Participant_ID'] . "&event_id=" . $row['Event_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>New</a>";
			if ($result_array[$event_id]['apptid'] <> "" || $result_array[$event_id]['apptid'] <> NULL) { //Display Edit link if event has appointment/s
				$result_array[$event_id]['link_history'] = "&nbsp;<a href='appointment_list.php?participant_id=" . $row['Participant_ID'] . "&event_id=" . $row['Event_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>History</a>";
				$result_array[$event_id]['link_edit'] = "&nbsp;<a href='appointment_edit.php?appointment_id=" . $row['Appointment_ID'] . "&prev_url=" . urlencode($_SERVER["REQUEST_URI"]) . "'>Edit</a>";
			}		
		}
	}	
	
	if ($result_array != NULL) {
		foreach ($result_array as $key => $value) {
			//SMS Log records
			$stmt = $dbo->prepare('SELECT * FROM log_sms WHERE Participant_ID=:participant_id AND Appointment_ID=:appointment_id ORDER BY log_SMS_ID');
			$stmt->execute(array('participant_id' => $_REQUEST['participant_id'],'appointment_id' => $result_array[$key]['apptid']));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			
			if ($row_count==0) {
			} 
			else {
				$i=1;	
				foreach ($result as $row){			
					$stmt = $dbo->prepare('SELECT * FROM log_sms WHERE Participant_ID=:participant_id AND Appointment_ID=:appointment_id ORDER BY log_SMS_ID');
					$stmt->execute(array('participant_id' => $_REQUEST['participant_id'],'appointment_id' => $result_array[$key]['apptid']));
					$row_count = $stmt->rowCount();
					$result = $stmt->fetchAll();
					
					if ($row_count==0) { 						
					} 
					else {//Get data from log_sms_reply	
						$stmt2 = $dbo->prepare('SELECT * FROM log_sms_reply WHERE Participant_ID=:participant_id AND MT_ID=:mt_id ORDER BY Timestamp');
						$stmt2->execute(array('participant_id' => $_REQUEST['participant_id'],'mt_id' => $row['MT_ID']));
						$row_count2 = $stmt2->rowCount();
						$result2 = $stmt2->fetchAll();
						if ($row_count2==0) {	
							$reply_txt="";
						} 
						else {	
							$reply_txt = "&nbsp;&nbsp;&nbsp;Reply: ";
							foreach ($result2 as $row2){
								$reply_txt = $reply_txt . " (" . date("dmYHi", strtotime($row2['Timestamp'])) . " " . $row2['Message'] . ")&nbsp;&nbsp;&nbsp;";
							}
						}
						$result_array[$key]['sms'] = $result_array[$key]['sms'] . $i . ". " . date("dmYHi", strtotime($row['Timestamp'])) . $reply_txt . "<br>";
						$i=$i+1;
					}						
				}
				$result_array[$key]['sms'] = rtrim($result_array[$key]['sms'],'<br>'); //Remove comma and space				
			}
		}
	
		$result_array_sort=$result_array; //Sort the array based on key 'date' and 'time'
		foreach ($result_array_sort as $row) {
			$appointment_date[] = strtotime($row['date']);
			$appointment_time[] = strtotime($row['time']);
		}
		array_multisort($appointment_date, SORT_DESC, $appointment_time, SORT_DESC, $result_array_sort); //Multiple sort with array_multisort
		
		foreach($result_array_sort as $elementkey => $element) { //Loop through the arrow and unset all records with key 'date' before today
			foreach($element as $valuekey => $value) {
				if($valuekey == 'date' && strtotime($value) < strtotime('today')){
					//delete this particular object from the $array
					unset($result_array_sort[$elementkey]);	
				} 
			}
		}
		foreach ($result_array_sort as $key => $value) {
				$next_appointment_id = $result_array_sort[$key]['apptid'];
		}
		foreach ($result_array as $key => $value) {
			if ($result_array[$key]['apptid']==$next_appointment_id){
				$result_array[$key]['color']="#f29966";
			} else {
				$result_array[$key]['color']="#cdcdcd";		
			}
		}
	}
	$dbo = null; //Close DB connection
?>
			
	<h1 class='title'>Participant</h1>
	<p class='title'>Information</p>
	<?php //echo $event1_sms; //To test variable ?>
	<table class='participantinfo'>
		<tr>
			<th align='left' bgcolor="#cdcdcd" colspan='3'>Personal Details</th>
			<td align='right' bgcolor="#cdcdcd" style='border:none;' colspan='3'>
				<a href="sms_list.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&mobile=<?php echo $mobile; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">SMS History</a>&nbsp;
				<a href="email_list.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&email=<?php echo urlencode($email); ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">Email History</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="send_email.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&email_type=manual&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">Email</a>&nbsp;
				<a href="send_sms.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&sms_type=auto&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">Send SMS</a>&nbsp;
				<a href="participant_edit.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">Edit</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='12%' bgcolor="#cdcdcd">Participant ID:</td>
			<td align='left' width='10%'>
				<?php echo isset($_REQUEST['participant_id']) ? $_REQUEST['participant_id'] : '' ?>
				<input name="participant_id" size="15" type="hidden" value="<?php echo isset($_REQUEST['participant_id']) ? $_REQUEST['participant_id'] : '' ?>"></input>
			</td>
			<td align='right' width='8%' bgcolor="#cdcdcd">Name:</td>
			<td align='left' width='44%'>							
				<?php echo $title . ' ' . $lastname . ' ' . $firstname; ?>
			</td>
			<td align='right' width='12%' bgcolor="#cdcdcd">Fitbit User ID:</td>
			<td align='left' width='14%'>							
				<?php echo isset($fitbit_user_id) ? $link_fitbit_user_id : '<a href="participant_fitbit_connect.php?participant_id=' . $_REQUEST['participant_id'] . '">Connect to Fitbit</a>' ?>
			</td>			
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">Mobile No.:</td>
			<td align='left'>
				<?php echo $mobile; ?>			
			</td>
			<td align='right' bgcolor="#cdcdcd">Email:</td>
			<td align='left'>							
				<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
			</td>
			<td align='right' bgcolor="#cdcdcd">Fitbit Sync on:</td>
			<td align='left'>							
				<?php echo '<a href="participant_fitbit_sync.php?participant_id=' . $_REQUEST['participant_id'] . '&fitbit_user_id=' . $fitbit_user_id . '">' . $row_fitbit . '</a>'; ?>
			</td>	
		</tr>
		<tr>	
			<td align='right' bgcolor="#cdcdcd">Address:</td>
			<td align='left' colspan='3'>							
				<?php echo $address1; ?>
			</td>
			<td align='right' bgcolor="#cdcdcd">Postal Code:</td>
			<td align='left'>
				<?php echo $postcode; ?>
			</td>
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">PNote:</td>
			<td align='left' colspan='5'>
				<?php echo $pnote; ?>
			</td>
		</tr>	
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">PStatus:</td>
			<td align='left' colspan='5'>
				<?php echo $pstatus; ?>
			</td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'>
		<tr>
			<th align='left' bgcolor="#cdcdcd" colspan='4'>Most Recent Screener Questionnaire</th>
			<td align='right' bgcolor="#cdcdcd" style='border:none;' colspan='3'>
				<?php echo $link_screener; ?>
			</td>
		</tr>
		<tr>
			<td align='right' width='3%' bgcolor="#cdcdcd">ID:</td>
			<td align='left' width='7%'>
				<?php echo $screener_id; ?>
			</td>
			<td align='left' width='16%'>							
				<?php echo $eligibility_outcome; ?>
			</td>
			<td align='right' width='8%' bgcolor="#cdcdcd">Reason/s:</td>
			<td align='left' width='35%'>							
				<?php echo $eligibility_reason; ?>
			</td>
			<td align='right' width='6%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='25%'>							
				<?php echo $eligibility_scrstatus; ?>
			</td>
		</tr>	
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=1; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=2; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=3; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=4; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=5; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=6; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=7; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=8; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=9; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
		<br>
	<table class='participantinfo'> <?php $event_num=10; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=20; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'> <?php $event_num=21; ?> <!-- Change Event_ID here to cascade down -->
		<tr>
			<th align='left' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" colspan='5'><?php echo $result_event[$event_num-1][0]; ?></th> <!--[Array Num][Field Num]; Change Array Num according to Event_ID-->
			<td align='right' bgcolor="<?php print isset($result_array[$event_num]['color']) ? $result_array[$event_num]['color'] : '#cdcdcd' ?>" style='border:none;' colspan='5'>
				<?php echo $result_array[$event_num]['link_history']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $result_array[$event_num]['link_edit']; ?>
				<a href="appointment_new.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&event_id=<?php echo $event_num; ?>&prev_url=<?php echo urlencode($_SERVER["REQUEST_URI"]); ?>">New</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt ID:</td>
			<td align='left' width='8%'><?php echo $result_array[$event_num]['apptid']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Date:</td>
			<td align='left' width='14%'><?php echo $result_array[$event_num]['date']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Appt Time:</td>
			<td align='left' width='9%'><?php echo $result_array[$event_num]['time']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Location:</td>
			<td align='left' width='13%'><?php echo $result_array[$event_num]['location']; ?></td>
			<td align='right' width='9%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='11%'><?php echo $result_array[$event_num]['status']; ?></td>
		</tr>
		<tr>
			<td align='right' bgcolor="#cdcdcd">SMS Sent:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['sms']; ?></td>					
		</tr>
		<tr>
			<td align='right' valign='top' bgcolor="#cdcdcd">Note:</td>
			<td align='left' colspan='9'><?php echo $result_array[$event_num]['note']; ?></td>
		</tr>
	</table>
	<br>
	<table class='participantinfo'>
		<tr>
			<th align='left' bgcolor="#cdcdcd" colspan='4'>Steps & Incentives</th>
			<td align='right' bgcolor="#cdcdcd" style='border:none;' colspan='3'>
				<a href='participant_steps.php?participant_id=<?php echo $_REQUEST['participant_id']; ?>&prev_url=<?php echo urlencode($_SERVER['REQUEST_URI']); ?>'>Steps</a>
			</td>
		</tr>
		<tr>
			<td align='right' width='3%' bgcolor="#cdcdcd">ID:</td>
			<td align='left' width='7%'>
				<?php echo $screener_id; ?>
			</td>
			<td align='left' width='16%'>							
				<?php echo $eligibility_outcome; ?>
			</td>
			<td align='right' width='8%' bgcolor="#cdcdcd">Reason/s:</td>
			<td align='left' width='35%'>							
				<?php echo $eligibility_reason; ?>
			</td>
			<td align='right' width='6%' bgcolor="#cdcdcd">Status:</td>
			<td align='left' width='25%'>							
				<?php echo $eligibility_scrstatus; ?>
			</td>
		</tr>	
	</table>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
