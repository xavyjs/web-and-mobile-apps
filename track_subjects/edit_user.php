<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php AdminCheck(); //Check legitimate session ?>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>
						
			<h1 class='title'>Edit User</h1>
			<p class='title'>please input the registration details to update an account here</p>
			
			<p style='color:red;'><b>Warning!</b><br>Making changes may have serious consequences to the system process.<br>Proceed with caution.<br><br>To delete an user, select the user and enter <b>DELETE</b> for username and <b>DELETEUSER</b> for both passwords.<br><br></p>
			
			<?php
			
				include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
				dbConnect(); // Connect to Database
				
				$stmt = $dbo->prepare("SELECT *	FROM users ORDER BY AccessLvl DESC,Username");
					$stmt->execute();
					$row_count = $stmt->rowCount();
					$result = $stmt->fetchAll();
					
					if ($row_count==0) {
						echo "No user available.";
					} 
					else {		
						echo "<table><tr><td>Select User: </td>
								<td>
									<form action=" . htmlentities($_SERVER['PHP_SELF']) . " method='get'>
										<select name='userid'>";
						foreach ($result as $row){	
							if ($row['UserID']==$_GET['userid']||$row['UserID']==$_POST['userid']) {
								echo "<option value=" . $row['UserID'] . " selected='selected'>Access Level " . $row['AccessLvl'] . " - " . htmlspecialchars($row['Username']) . "</option>";	
							}
							else {
								echo "<option value=" . $row['UserID'] . ">Access Level " . $row['AccessLvl'] . " - " . htmlspecialchars($row['Username']) . "</option>";	
							}
						}
					echo "</select>&nbsp;&nbsp;&nbsp;<input type='submit' value='Edit'></input></form></td></tr></table><br>";		
					}							
					
				if ($_SERVER["REQUEST_METHOD"] == "GET") {
					include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
					dbConnect(); // Connect to Database
					
					$userid=$_GET['userid'];
					
					$stmt = $dbo->prepare("SELECT *
											FROM users 
											WHERE UserID =?");
					$stmt->execute(array($userid));
					$row_count = $stmt->rowCount();
					$result = $stmt->fetchAll();
					
					if ($row_count==0) {
					} 
					else {									
						foreach ($result as $row){		
						
							$username=$row['Username'];
							$email=$row['Email'];
							
							echo "<form action=" . htmlentities($_SERVER['PHP_SELF']) . " method='post'>
									<table>
										<tr>
											<td valign='top' align='right'>
												Username: 
											</td>
											<td align='left'>
												<input name='reguser' size='80' type='text' value='" . htmlspecialchars($username) . "'></input><br>
											</td>
										</tr>
										<tr>
											<td valign='top' align='right'>Email: 
											</td>
											<td align='left'>
												<input name='regemail' size='80' type='text' value='" . htmlspecialchars($email) . "'></input><br>
											</td>
										</tr>
										<tr>
											<td valign='top' align='right'>Access Level: 
											</td>
											<td align='left'>
												<select name='accesslvl'>";
								
							$stmt2 = $dbo->prepare('SELECT * FROM ctbl_accesslvl ORDER BY AccessLvl_ID');
							$stmt2->execute();
							$row_count2 = $stmt2->rowCount();
							$result2 = $stmt2->fetchAll();
							
							if ($row_count2==0) {
							} 
							else {
								foreach ($result2 as $row2){							
									if ($row2['AccessLvl_ID']==$row['AccessLvl']) {
									echo "<option value=" . $row2['AccessLvl_ID'] . " selected='selected'>" . $row2['AccessLvl_ID'] . ". " . $row2['AccessLvl'] . "</option>";
									}
									else {
										echo "<option value=" . $row2['AccessLvl_ID'] . ">" . $row2['AccessLvl_ID'] . ". " . $row2['AccessLvl'] . "</option>";
									}
								}
							}
																			
								echo			"</select><br>
											</td>
										</tr>								
										<tr>
											<td valign='top' align='right'>New Password: 
											</td>
											<td align='left'>
												<input name='regpass1' size='80' type='text' value='" . $_POST['regpass1'] . "'></input><br>
											</td>
										</tr>
										<tr>
											<td valign='top' align='right'>Repeat New Password: 
											</td>
											<td align='left'>
												<input name='regpass2' size='80' type='text' value='" . $_POST['regpass2'] . "'></input><br>
											</td>
										</tr>
										<tr>
											<td></td>
											<td align='right'>
												<input name='userid' type='hidden' size='20' value='" . $userid . "'><input type='submit' value='Proceed' name='button_post'></input>
											</td>
										</tr>
									</table></form>";
						}
					}
					if ($_GET['status']=='updated') {
						echo '<span style="color:#ff0000;">User updated</span>';
					} else if ($_GET['status']=='deleted') {
						echo '<span style="color:#ff0000;">User deleted</span>';
					}
				}	

				if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['button_post'] == "Proceed") {
					$error = false;

					//Check if Username is empty
					if (empty($_POST['reguser'])) {
						$error = true;
						$errormsg = $errormsg . "Username is empty <br>";
					}
					//Check if Username exists
					else {											
						$stmt = $dbo->prepare('SELECT * FROM users WHERE Username= :username AND UserID<>:userid');
						$stmt->execute(array('username' => $_POST['reguser'],'userid' => $_POST['userid']));
						$row_count = $stmt->rowCount();
						if ($row_count > 0) {
							$error = true;
							$errormsg = $errormsg . "This username is already taken <br>";
						}						
					}
					
					//Check if Email is empty
					if (empty($_POST['regemail'])) {
						$error = true;
						$errormsg = $errormsg . "Email is empty <br>";
					}
					//Check if Email exists
					else {
						$stmt = $dbo->prepare('SELECT * FROM users WHERE Email= :email AND UserID<>:userid');
						$stmt->execute(array('email' => $_POST['regemail'],'userid' => $_POST['userid']));
						$row_count = $stmt->rowCount();
						if ($row_count > 0) {
							$error = true;
							$errormsg = $errormsg . "This email address is already taken <br>";
						}
					}
					
					//Check if Password is empty
					if (empty($_POST['regpass1'])) {
						$error = true;
						$errormsg = $errormsg . "Password is empty <br>";
					}
					
					//Check if Repeat Password is empty
					if (empty($_POST['regpass2'])) {
						$error = true;
						$errormsg = $errormsg . "Repeat Password is empty <br>";
					}
					
					//Check if Password is at least 7 characters long
					if (strlen($_POST['regpass1']) < 7) {
						$error = true;
						$errormsg = $errormsg . "Password must be at least 7 characters long <br>";
					}
					
					//Check if Repeat Password is at least 7 characters long
					if (strlen($_POST['regpass2']) < 7) {
						$error = true;
						$errormsg = $errormsg . "Repeat Password must be at least 7 characters long <br>";
					}
					
					//Check if Password and Repeat Password match
					if (!empty($_POST['regpass1']) && !empty($_POST['regpass2']) && $_POST['regpass1'] != $_POST['regpass2']) {
						$error = true;
						$errormsg = $errormsg . "Repeat Password does not match <br>";
					}
					
					//Check if Email format is valid
					if (!empty($_POST['regemail']) && !filter_var($_POST['regemail'], FILTER_VALIDATE_EMAIL)) {
						$error = true;
						$errormsg = $errormsg . "Email format is not valid <br>";
					}
												
					if ($error === true) {
						echo $errormsg;
					} else {
										
						require("resources/library/PasswordHash.php");	
						$hasher = new PasswordHash($hash_cost_log2, $hash_portable);
						
						$username=$_POST['reguser'];
						$email=$_POST['regemail'];
						$password=$_POST['regpass1'];
						$accesslvl=$_POST['accesslvl'];
						$userid=$_POST['userid'];
						
						$hash = $hasher->HashPassword($password);
						if (strlen($hash) < 20)
							fail('Failed to hash new password');
						unset($hasher);
						
						if (strtolower($username) == 'delete' && strtolower($password) == 'deleteuser') { //Delete user if Username is set to DELETE
							$stmt = $dbo->prepare("DELETE FROM users WHERE UserID=?");
							$stmt->execute(array($userid));
							header("location:edit_user.php?userid=".$userid."&status=deleted");
						} else {
							$stmt = $dbo->prepare("UPDATE users SET Username=?, Email=?, Password=?, AccessLvl=? WHERE UserID=?");
							$stmt->execute(array($username,$email,$hash,$accesslvl,$userid));	
							header("location:edit_user.php?userid=".$userid."&status=updated");
						}
						
					}					
				}
									
				//Close DB connection
				$dbo = null;					
			?>				
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>