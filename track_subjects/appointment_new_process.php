<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$error = false;
		$participant_id=$_POST['participant_id'];
		$event_id=$_POST['event_id'];
		$fitbit_start_date=date('Y-m-d',strtotime($_POST['fitbit_start_date']));
		$appointment_date=date('Y-m-d',strtotime($_POST['appointment_date']));
		$appointment_time=date('H:i:s',strtotime($_POST['appointment_time']));
		$location_id=$_POST['location_id'];
		$apptstatus_id=1; //Status as Booked
		
		if (is_numeric($_POST['participant_id'])===false) {
			$error = true;
			$errormsg = $errormsg . "Participant ID must be an integer<br>";
		} 
		if ($_POST['fitbit_start_date']=="" && in_array($_POST['event_id'], array('3', '4', '5', '6', '7', '8', '9', '10'))) {
			$error = true;
			$errormsg = $errormsg . "Fitbit Start Date cannot be empty for this event<br>";
		} 
		if ($_POST['appointment_date']=="") {
			$error = true;
			$errormsg = $errormsg . "Appointment Date cannot be empty<br>";
		} 
		if ($_POST['appointment_time']=="") {
			$error = true;
			$errormsg = $errormsg . "Appointment Time cannot be empty<br>";
		} 
		if ($error === true) {
			echo $errormsg;
			echo "<br><br>Click on the BACK button below to go back to the previous page.<br><br><br>
					<form action='appointment_new' method='post'>
					<input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input>
					<input name='event_id' type='hidden' size='20' value='" . $event_id . "'></input>
					<input name='fitbit_start_date' type='hidden' size='20' value='" . $_POST['fitbit_start_date'] . "'></input>
					<input name='appointment_date' type='hidden' size='20' value='" . $_POST['appointment_date'] . "'></input>
					<input name='appointment_time' type='hidden' size='20' value='" . $_POST['appointment_time'] . "'></input>
					<input name='location_id' type='hidden' size='20' value='" . $location_id . "'></input>
					<input name='prev_url' type='hidden' size='20' value='" . $prev_url . "'></input>
					<div align='right'><input type='submit' value='Back'></input></div></form>";
		} else {					
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database	
			
			if ($_POST['fitbit_start_date']=="") {
				$fitbit_start_date = null;
			} 

			$stmt = $dbo->prepare('SELECT MAX(Appointment_ID) AS Appointment_ID, ApptStatus_ID FROM appointments WHERE Participant_ID=:participant_id AND event_id=:event_id HAVING Appointment_ID IS NOT NULL'); //Update prev appointment status to Rescheduled if status = Booked or Confirmed
			$stmt->execute(array('participant_id' => $participant_id, 'event_id' => $event_id));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();

			if ($row_count==0) {
				if ($event_id==2) {
					$stmt2 = $dbo->prepare("INSERT INTO appointments(Participant_ID,Event_ID,Appointment_Date,Appointment_Time,Location_ID,ApptStatus_ID) 					VALUES(:participant_id,:event_id,:appointment_date,:appointment_time,:location_id,:apptstatus_id)");
					$stmt2->execute(array(':participant_id' => $participant_id, ':event_id' => 1, ':appointment_date' => date('Y-m-d',strtotime($appointment_date . ' -3 Weekdays')), ':appointment_time' => $appointment_time, ':location_id' => 2, ':apptstatus_id' => 1));
				}
			} 
			else {
				foreach ($result as $row){
					if ($row['ApptStatus_ID'] == 1 || $row['ApptStatus_ID'] == 2) {
						$stmt2 = $dbo->prepare("UPDATE appointments SET ApptStatus_ID=5 WHERE Appointment_ID=?");
						$stmt2->execute(array($row['Appointment_ID']));		
					}
				}
			}
			
			$stmt = $dbo->prepare("INSERT INTO appointments(Participant_ID,Event_ID,Fitbit_Start_Date,Appointment_Date,Appointment_Time,Location_ID,ApptStatus_ID) 					VALUES(:participant_id,:event_id,:fitbit_start_date,:appointment_date,:appointment_time,:location_id,:apptstatus_id)");
			$stmt->execute(array(':participant_id' => $participant_id, ':event_id' => $event_id, ':fitbit_start_date' => $fitbit_start_date, ':appointment_date' => $appointment_date, ':appointment_time' => $appointment_time, ':location_id' => $location_id, ':apptstatus_id' => $apptstatus_id));

			// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/EmailAppointment.php");
			// $email_id = 1; //Template 1: For newly appointment
			// EmailAppointment($participant_id,$event_id,$appointment_date,$appointment_time,$location_id,$email_id);
			
			// include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/SendSMS.php");
			// $sms_id = 1; //Template 1: For newly appointment
			// print gw_send_sms($participant_id,$event_id,$appointment_date,$appointment_time,$location_id,$sms_id);

			if (isset($_POST['prev_url']) && $_POST['prev_url']<>"") {
				print "Appointment (ID: " . $dbo->lastInsertId() . ") created<br><br>";
				print "<a href='" . $_POST['prev_url'] . "'>Back to previous page</a>";							
			} else {
				header("location:participant_info.php?participant_id=" . $participant_id);	
				//echo "index";
			}
			$dbo = null; //Close DB connection
		}						
	} else {
		header("location:appointment_new.php");		
	}
			
?>
		
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 