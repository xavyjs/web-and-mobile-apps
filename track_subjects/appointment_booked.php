<?php session_start(); ?>
<?php include_once("resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include("resources/templates/header.php"); ?>


			<h1 class='title'>Calendar</h1>
			<p class='title'>Appointments to be Updated<p>
				
				<table width="800px" cellpadding="3px">
					<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
					<tr><td colspan=6 style="border-bottom:1px solid; font-weight:bold;">
						Search Filters
					</td></tr>
					<tr><td>
						&nbsp;
					</td></tr>
					<tr>				
						<th align="right">Participant ID:</th>
						<td align="left">
							<input name="participant_id" type="number" size="10" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : '' ?>"></input>
						</td>
							<?php
							
								//--------------Event Field Start
								include_once("resources/config.php");
								dbConnect(); // Connect to Database
								
								echo "<th align='right' width='120px'>Event:</th><td align='left' colspan='3'><select name='appt_event'>";
								$stmt = $dbo->prepare('SELECT * FROM ctbl_event');
								$stmt->execute();
								$row_count = $stmt->rowCount();
								$result = $stmt->fetchAll();
								
								if ($row_count==0) {
									echo "The database contains no event yet";
								} 
								else {
									echo "<option value=''> </option>";
									foreach ($result as $row){
										if ($row['Event_ID']==$_POST['appt_event']) {
											echo "<option value=" . $row['Event_ID'] . " selected='selected'>" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
										}
										else {
												echo "<option value=" . $row['Event_ID'] . ">" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
										}			
									}
								}
								echo "</select></td></tr>";								
								//--------------Event Field End		
								

								//--------------Location Field Start
								echo "<tr><th align='right'>Location:</th><td align='left'><select name='appt_location'>";
								$stmt = $dbo->prepare('SELECT * FROM ctbl_location');
								$stmt->execute();
								$row_count = $stmt->rowCount();
								$result = $stmt->fetchAll();
														
								if ($row_count==0) {
									echo "The database contains no location yet";
								} 
								else {
									echo "<option value=''> </option>";
									foreach ($result as $row){
										if ($row['Location_ID']==$_POST['appt_location']) {
											echo "<option value=" . $row['Location_ID'] . " selected='selected'>" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
										}
										else {
												echo "<option value=" . $row['Location_ID'] . ">" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
										}		
									}
								}
								echo "</select></td>";
								//--------------Location Field End

								//Close DB connection
								$dbo = null;
							?>

						<th align="right">
							Date From: 
						</th>
						<td align="left">
							<select name="date_from_d">
							<?php
								$i = 1;
								echo "<option value=''> </option>";
								while ($i<=31) {
									if ($i==$_POST['date_from_d']) {
									echo "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
									}
									$i++;
								}
							?>
							</select>
							<select name="date_from_m">
							<?php
								$i = 1;
								echo "<option value=''> </option>";
								while ($i<=12) {
									if ($i==$_POST['date_from_m']) {
									echo "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
									}
									$i++;
								}
							?>
							</select>
							<select name="date_from_y">
							<?php
								$i = date("Y")-1;
								echo "<option value=''> </option>";
								while ($i<=date("Y")+2) {
									if ($i==$_POST['date_from_y']) {
									echo "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
									}
									$i++;
								}
							?>
							</select>
							
							<?php
								// get class into the page
								// require_once('resources/library/calendar/tc_calendar.php');

								// instantiate class and set properties
								// $myCalendar = new tc_calendar("date1", true);
								// $myCalendar->setIcon("resources/library/calendar/images/iconCalendar.gif");
								// $myCalendar->setDate(01, 03, 1960);
								// $myCalendar->setPath("resources/library/calendar/");
								// $myCalendar->setYearInterval(1960, 2015);
								// $myCalendar->dateAllow('1960-01-01', '2015-03-01');
								// $myCalendar->setSpecificDate(array("2011-04-01", "2011-04-13", "2011-04-25"), 0, 'month');
								// $myCalendar->setOnChange("myChanged('test')");

								// output the calendar
								// $myCalendar->writeScript();	  
							?>

						</td>
						<th align="right">
							Date To: 
						</th>
						<td align="left">
							<select name="date_to_d">
							<?php
								$i = 1;
								echo "<option value=''> </option>";
								while ($i<=31) {
									if ($i==$_POST['date_to_d']) {
									echo "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
									}
									$i++;
								}
							?>
							</select>
							<select name="date_to_m">
							<?php
								$i = 1;
								echo "<option value=''> </option>";
								while ($i<=12) {
									if ($i==$_POST['date_to_m']) {
									echo "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
									}
									$i++;
								}
							?>
							</select>
							<select name="date_to_y">
							<?php
								$i = date("Y")-1;
								echo "<option value=''> </option>";
								while ($i<=date("Y")+2) {
									if ($i==$_POST['date_to_y']) {
									echo "<option value=" . $i . " selected='selected'>" . sprintf('%02d', $i) . "</option>";
									}
									else {
										echo "<option value=" . $i . ">" . sprintf('%02d', $i) . "</option>";
									}
									$i++;
								}
							?>
							</select>
						</td>
						</tr>
						<tr>
							<td></td><td></td><td></td><td></td>
							<td align="right">
								<input name="filter" type="hidden" value="1"></input>
								<input type="submit" value="Filter"></input>
								</form>
							</td>
							<td align="right">
								<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
								<input name="clearall" type="hidden" value="1"></input>
								<input type="submit" value="Clear All"></input>
								</form>
							</td>

						</tr></table><br>
				
				
	
				<?php
					//Clear all fields and show all records
					if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['clearall'] == 1) {					
					}
								
					//Run filter script						
						$participant_id = (empty($_POST['participant_id'])) ? '%' : $_POST['participant_id']; 	
						$appt_event = (empty($_POST['appt_event'])) ? '%' : $_POST['appt_event']; 
						$appt_location = (empty($_POST['appt_location'])) ? '%' : $_POST['appt_location']; 
						// The above is identical to this if/else statement
						// if (empty($_POST['participant_id'])) {
							// $participant_id = '%';
						// } else {
							// $participant_id = $_POST['participant_id'];
						// }						
						
						include_once("resources/config.php");
						dbConnect(); // Connect to Database
						
						$date_from=$_POST['date_from_y'] . "-" . $_POST['date_from_m'] . "-" . $_POST['date_from_d'];
						$date_to=$_POST['date_to_y'] . "-" . $_POST['date_to_m'] . "-" . $_POST['date_to_d'];
						
						if ($date_from=='--') {
							$date_from = null;
						}
						if ($date_to=='--') {
							$date_to = null;
						}
						
						if (empty($date_from)) {
							$date_from = "1900-01-01";
						}
						if (empty($date_to)) {
							$date_to = "3000-01-01";
						}

						$sql = 'SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, E.Event, A.Appointment_Date, A.Appointment_Time, A.Location_ID, L.Location, A.ApptStatus_ID, S.ApptStatus
														FROM appointments AS A 
														LEFT JOIN ctbl_event AS E ON A.Event_ID = E.Event_ID 
														LEFT JOIN ctbl_location AS L ON A.Location_ID = L.Location_ID 
														LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID 
														WHERE (A.ApptStatus_ID = 1 OR A.ApptStatus_ID = 4) AND	
														ADDTIME(A.Appointment_Date, A.Appointment_Time)< :appointment_datetime';					

									$sql .= ' AND A.Participant_ID LIKE :participant_id';
									$sql .= ' AND A.Event_ID LIKE :appt_event';
									$sql .= ' AND A.Location_ID LIKE :appt_location';
									$sql .= ' AND A.Appointment_Date BETWEEN :date_from AND :date_to';
									$sql .= ' ORDER BY A.Participant_ID, A.Event_ID, A.Appointment_Date, A.Appointment_Time';
									$stmt = $dbo->prepare($sql);
									$stmt->bindValue(':appointment_datetime', date('Y-m-d H:i:s'));
									$stmt->bindValue(':participant_id', $participant_id);
									$stmt->bindValue(':appt_event', $appt_event);
									$stmt->bindValue(':appt_location', $appt_location);
									$stmt->bindValue(':date_from', $date_from);
									$stmt->bindValue(':date_to', $date_to);								
	

								$stmt->execute();						
								$row_count = $stmt->rowCount();
								$result = $stmt->fetchAll();
								
								if ($participant_id=='%') {
									$participant_id="";
								}
								if ($appt_event=='%') {
									$appt_event="";
								}
								if ($appt_location=='%') {
									$appt_location="";
								}
								$date_from = date("d-m-Y", strtotime($date_from));
								if ($date_from=='01-01-1900') {
									$date_from="";
								}
								$date_to = date("d-m-Y", strtotime($date_to));
								if ($date_to=='01-01-3000') {
									$date_to="";
								}

									echo "<table class='wborder'>	
										<tr>
											<th colspan='13'>
												Filter (Participant ID=" . $participant_id . ", Event=" . $appt_event . ", Location=" . $appt_location . ", Date From=" . $date_from . ", Date To=" . $date_to . ")
											</th></tr>
										<tr>
											<th>PID</th>
											<th>Week</th>
											<th>Event</th>
											<th>Recommended Date (dd-mm-yyyy)</th>
											<th>Booked Date (dd-mm-yyyy)</th>
											<th>Time</th>
											<th>Location</th>
											<th>Appt Status</th>
											<th>Weight (kg)</th>
											<th>NM</th>
											<th>Body Fat (%)</th>
											<th>NM</th>
											<th> </th>
										</tr>";
								
								if ($row_count==0) {
									echo "<tr><td colspan='12'>
													No appointment
										</td></tr>";	
								} 
								else {	
								
									foreach ($result as $row){
															
										$appt_id=$row['Appointment_ID'];	
										$participant_id=$row['Participant_ID'];	
										$appt_event_id=$row['Event_ID'];								
										$appt_event=$row['Event'];
										$appt_event_week=$row['Event_Week'];
										$appt_date_auto=$row['Appointment_Date_Recommended'];	
										$appt_date=$row['Appointment_Date'];
										$appt_time=$row['Appointment_Time'];
										$appt_location_id=$row['Location_ID'];
										$appt_location=$row['Location'];
										$Appointment_Status=$row['Appointment_Status'];
										$appt_status=$row['ApptStatus'];
										$weight=$row['Weight'];
										$weight_chkbox=$row['Weight_chkbox'];
										$bodyfat=$row['BodyFat'];
										$bodyfat_chkbox=$row['BodyFat_chkbox'];
										
										//--------------------------Add code to determine previous appointment status
										$appt_status2 = "";
										if (!empty($Appointment_Status)) {
											$stmt2 = $dbo->prepare('SELECT A.Appointment_ID, A.Participant_ID, A.Event_ID, A.Appointment_Date, A.Appointment_Time, A.ApptStatus_ID, S.ApptStatus
															FROM appointments AS A
															LEFT JOIN ctbl_apptstatus AS S ON A.ApptStatus_ID = S.ApptStatus_ID 
															WHERE A.Participant_ID= :participant_id AND
															A.Appointment_ID!= :appt_id AND															
															A.Event_ID= :appt_event_id AND
															(Appointment_Date < :appt_date OR (Appointment_Date = :appt_date AND Appointment_Time < :appt_time)) AND
															(A.ApptStatus_ID IS NOT NULL AND A.ApptStatus_ID!=0)
															ORDER BY A.Event_ID, A.Appointment_Date DESC, A.Appointment_Time DESC');
											$stmt2->execute(array('participant_id' => $participant_id, 'appt_id' => $appt_id, 'appt_event_id' => $appt_event_id, 'appt_date' => $appt_date, 'appt_time' => $appt_time));
											$row_count2 = $stmt2->rowCount();
											$result2 = $stmt2->fetchAll();
											
											if ($row_count2==0) {
											} 
											else {	
												foreach ($result2 as $row2){
													
													$appt_status2=$row2['ApptStatus'];
													$appt_status= $appt_status2[0] . "," . $appt_status;
												}
											}
										}
										//--------------------------Add code to determine previous appointment status
										
										// if ($appt_date . " " . $appt_time < date('Y-m-d H:i:s') && $Appointment_Status == 4) {
											// echo "<tr style='background-color: #d95252;'>";
										// } 
										if ($appt_date < date('Y-m-d')) {
											echo "<tr style='color:#999'>";
										}
										elseif ($appt_date > date('Y-m-d') && $appt_date <= date('Y-m-d', strtotime("+3 days"))) {
											echo "<tr style='background-color: #ffdf85;'>";
										} 
										elseif ($appt_date == date('Y-m-d')) {
											echo "<tr style='background-color: #f29966;'>";
										} 
										else {
											echo "<tr>";
										}
										
										if ($appt_date_auto != null) {
											$appt_date_auto=date("d-m-Y", strtotime($appt_date_auto)); 
										}
										
										if ($appt_date != null) {
											$appt_date=date("d-m-Y", strtotime($appt_date)); 
										}
										
										if ($appt_time != null) {
											$appt_time=date("h:i A", strtotime($appt_time)); 
										}
										
										echo "<td><a href='../appointment.php?participant_id=" . $participant_id . "'>" . $participant_id . "</a>";		
										echo "</td><td>" . $appt_event_week; 
										echo "</td><td>" . $appt_event; 
										echo "</td><td>" . $appt_date_auto; 
										echo "</td><td>" . $appt_date; 
										echo "</td><td>" . $appt_time; 
										echo "</td><td>" . $appt_location; 
										echo "</td><td>" . $appt_status; 
										echo "</td><td>" . $weight; 
										if ($weight_chkbox==1) {
											echo "</td><td><input name='weight_chkbox' type='checkbox' disabled=disabled' size='20' value='" . $weight_chkbox . "' checked></input>"; 
										} else {
											echo "</td><td><input name='weight_chkbox' type='checkbox' disabled=disabled' size='20' value='" . $weight_chkbox . "'></input>"; 
										}
										echo "</td><td>" . $bodyfat;
										if ($bodyfat_chkbox==1) {
											echo "</td><td><input name='bodyfat_chkbox' type='checkbox' disabled=disabled' size='20' value='" . $bodyfat_chkbox . "' checked></input>"; 
										} else {
											echo "</td><td><input name='bodyfat_chkbox' type='checkbox' disabled=disabled' size='20' value='" . $bodyfat_chkbox . "'></input>"; 
										}
										echo "</td><td><form action='appointment_edit' method='post'><input name='appt_id' type='hidden' size='20' value='" . $appt_id . "'></input><input name='participant_id' type='hidden' size='20' value='" . $participant_id . "'></input><input name='appt_event_id' type='hidden' size='20' value='" . $appt_event_id . "'></input></input><input name='appt_location_id' type='hidden' size='20' value='" . $appt_location_id . "'></input></input><input name='Appointment_Status' type='hidden' size='20' value='" . $Appointment_Status . "'></input><input name='weight' type='hidden' size='20' value='" . $weight . "'></input><input name='bodyfat' type='hidden' size='20' value='" . $bodyfat . "'></input><input name='prev_url' type='hidden' size='20' value='" . $_SERVER["REQUEST_URI"] . "'></input><input type='submit' value='Edit'></input></form></td></tr>";
									}									
								}			
								echo "</table><br><br>";

								echo "<table align='right'>
								<tr style='font-weight:bold;'><td colspan='2'>Appointment Legend</td></tr>	
								<tr><td style='background-color: #d95252;' width='20px'></td><td>2 consecutive misses</td></tr>
								<tr><td style='background-color: #f29966;' width='20px'></td><td>Today</td></tr>
								<tr><td style='background-color: #ffdf85;' width='20px'></td><td>Next 3 days</td></tr>		
								</table>";
								echo "<div style='clear: both'></div>";
								
					//Close DB connection
					$dbo = null;
					
					
				?>		

<?php include("resources/templates/footer.php"); ?>