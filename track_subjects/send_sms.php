<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<script>
function showMessage(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","send_sms_getmessage.php?sms_id="+str,true);
xmlhttp.send();
}
</script>

<?php //Retrieving Participant's mobile
	$participant_aid = $_GET['participant_aid'];
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM participants WHERE Participant_AID=:participant_aid');
	$stmt->execute(array('participant_aid' => $participant_aid));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "Appointment does not exist";
	} 
	else {
		foreach ($result as $row){
			$participant_id = $row['Participant_ID'];
			$event_id = $row['Event_ID'];
			$title_id = $row['Title_ID'];
			$lastname = $row['Lastname'];
			$firstname = $row['Firstname'];
			$mobile = $row['Mobile'];
			$email = $row['Email'];
			$address1 = $row['Address1'];
			$postcode = $row['Postcode'];
			$pnote = $row['PNote'];
		}		
	}						
	$dbo = null; //Close DB connection
?>

<?php //Retrieving SMS Templates
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_sms ORDER BY SMS_ID');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
	} 
	else {
		$sms_text_row = "<option value=''></option>";
		foreach ($result as $row){
			if ($row['SMS_ID']==(isset($_POST['sms_text']) ? $_POST['sms_text'] : $sms_text)) {
				$sms_text_row = $sms_text_row . "<option value=" . $row['SMS_ID'] . " selected='selected'>" . $row['SMS_ID'] . ". " . htmlspecialchars($row['Template_Name']) . "</option>";
			} else {
				$sms_text_row = $sms_text_row . "<option value=" . $row['SMS_ID'] . ">" . $row['SMS_ID'] . ". " . htmlspecialchars($row['Template_Name']) . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

	<h1 class='title'>SMS</h1>
	<p class='title'>Send Template SMS to participant</p>
	<?php //echo $appointment_time_t; //To check variable ?>
	<form action="send_sms_process.php" method="post">
		<table class='new'>
			<tr>
				<td align='right' width='30%'>Participant ID:</td>
				<td align='left' width='70%'>
					<input name="participant_aid2" size="40" type="text" disabled="disabled" value=<?php echo $participant_id; ?>></input>
					<input name="participant_aid" size="40" type="hidden" value="<?php echo isset($_POST['participant_aid']) ? $_POST['participant_aid'] : $_GET['participant_aid'] ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Mobile No.:</td>
				<td align='left'>
					<input name="mobile" type="text" size="40" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : $mobile ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Subject:</td>
				<td align='left'>
					<select name="sms_text" onchange="showMessage(this.value)"><?php echo $sms_text_row; ?></select>
				</td>
			</tr>
			<tr>
				<td align='right'>Message:</td>
				<td align='left'>
					<div id="txtHint">Select a subject above</div>
				</td>
			</tr>
			<tr>
				<td><br><br><br></td>
				<td align='right'>
					<a href="send_sms_manual.php?participant_aid=<?php echo $_GET['participant_aid']; ?>&sms_type=manual&prev_url=<?php echo urlencode($_GET['prev_url']); ?>">Send Manual SMS</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input name="sms_type" size="40" type="hidden" value="auto"></input>
					<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
					<input type="submit" value="Send"></input>
				</td>
			</tr>
		</table>
	</form>
	
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
