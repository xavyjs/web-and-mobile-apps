<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php AdminCheck(); //Check legitimate session ?>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>
						
			<h1 class='title'>Create a new Account</h1>
			<p class='title'>please input the registration details to create an account here</p>
			
			<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
			<table id="tbl_registration">
			<tr>
				<td>Username: </td><td><input name="reguser" type="text" size="40" value="<?php echo isset($_POST['reguser']) ? htmlspecialchars($_POST['reguser']) : '' ?>"></input></td>
			</tr>
			<tr>
				<td>Email: </td><td><input name="regemail" type="text" size="40" value="<?php echo isset($_POST['regemail']) ? htmlspecialchars($_POST['regemail']) : '' ?>"></input></td>
			</tr>
			<tr>
				<td>Password: </td><td><input name="regpass1" type="password" size="40" value="<?php echo isset($_POST['regpass1']) ? htmlspecialchars($_POST['regpass1']) : '' ?>"></input></td>
			</tr>
			<tr>
				<td>Repeat Password: </td><td><input name="regpass2" type="password" size="40" value="<?php echo isset($_POST['regpass2']) ? htmlspecialchars($_POST['regpass2']) : '' ?>"></input></td>
			</tr>
			<tr>
				<?php
					//--------------Access Level Field Start
					include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
					dbConnect(); // Connect to Database
					echo "<td align='right'>Access Level: </td><td align='left'><select name='accesslvl'>";
					$stmt = $dbo->prepare('SELECT * FROM ctbl_accesslvl ORDER BY AccessLvl_ID');
							$stmt->execute();
							$row_count = $stmt->rowCount();
							$result = $stmt->fetchAll();
							
							if ($row_count==0) {
								echo "The database contains no access level yet";
							} 
							else {
								echo "yes";
								foreach ($result as $row){							
									if ($row['AccessLvl_ID']==$_POST['accesslvl']) {
									echo "<option value=" . $row['AccessLvl_ID'] . " selected='selected'>" . $row['AccessLvl_ID'] . ". " . $row['AccessLvl'] . "</option>";
									}
									else {
										echo "<option value=" . $row['AccessLvl_ID'] . ">" . $row['AccessLvl_ID'] . ". " . $row['AccessLvl'] . "</option>";
									}
								}
							}
					echo "</select></td>";		

					//Close DB connection
					$dbo = null;					
					//--------------Access Level Field End
				?>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Register"></input>
				</td>
			</tr>
			</table>
			
			</form>
			
		<?php 
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					$error = false;
					include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
					dbConnect(); // Connect to Database
					
					echo $conf['test'];
					
					//Check if Username is empty
					if (empty($_POST['reguser'])) {
						$error = true;
						$errormsg = $errormsg . "Username is empty <br>";
					}
					//Check if Username exists
					else {											
						$stmt = $dbo->prepare('SELECT * FROM users WHERE Username= :username');
						$stmt->execute(array('username' => $_POST['reguser']));
						$row_count = $stmt->rowCount();
						if ($row_count > 0) {
							$error = true;
							$errormsg = $errormsg . "This username is already taken <br>";
						}						
					}
					
					//Check if Email is empty
					if (empty($_POST['regemail'])) {
						$error = true;
						$errormsg = $errormsg . "Email is empty <br>";
					}
					//Check if Email exists
					else {
						$stmt = $dbo->prepare('SELECT * FROM users WHERE Email= :email');
						$stmt->execute(array('email' => $_POST['regemail']));
						$row_count = $stmt->rowCount();
						if ($row_count > 0) {
							$error = true;
							$errormsg = $errormsg . "This email address is already taken <br>";
						}
					}
					
					//Check if Password is empty
					if (empty($_POST['regpass1'])) {
						$error = true;
						$errormsg = $errormsg . "Password is empty <br>";
					}
					
					//Check if Repeat Password is empty
					if (empty($_POST['regpass2'])) {
						$error = true;
						$errormsg = $errormsg . "Repeat Password is empty <br>";
					}
					
					//Check if Password is at least 7 characters long
					if (strlen($_POST['regpass1']) < 7) {
						$error = true;
						$errormsg = $errormsg . "Password must be at least 7 characters long <br>";
					}
					
					//Check if Repeat Password is at least 7 characters long
					if (strlen($_POST['regpass2']) < 7) {
						$error = true;
						$errormsg = $errormsg . "Repeat Password must be at least 7 characters long <br>";
					}
					
					//Check if Password and Repeat Password match
					if (!empty($_POST['regpass1']) && !empty($_POST['regpass2']) && $_POST['regpass1'] != $_POST['regpass2']) {
						$error = true;
						$errormsg = $errormsg . "Repeat Password does not match <br>";
					}
					
					//Check if Email format is valid
					if (!empty($_POST['regemail']) && !filter_var($_POST['regemail'], FILTER_VALIDATE_EMAIL)) {
						$error = true;
						$errormsg = $errormsg . "Email format is not valid <br>";
					}
					
					//Close DB connection
					$dbo = null;
								
					if ($error === true) {
						echo '<br><br><span style="color:#ff0000;">' . $errormsg . '</span>';
					} else {
						
						//DB connection string
						include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
						dbConnect(); // Connect to Database
						
						require("resources/library/PasswordHash.php");	
						$hasher = new PasswordHash($hash_cost_log2, $hash_portable);
						
						$username=$_POST['reguser'];
						$email=$_POST['regemail'];
						$password=$_POST['regpass1'];
						$accesslvl=$_POST['accesslvl'];
						
						$hash = $hasher->HashPassword($password);
						if (strlen($hash) < 20)
							fail('Failed to hash new password');
						unset($hasher);
									
						$stmt = $dbo->prepare("INSERT INTO users(Username,Email,Password,AccessLvl) VALUES(:username,:email,:password,:accesslvl)");
						$stmt->execute(array(':username' => $username, ':email' => $email, ':password' => $hash, ':accesslvl' => $accesslvl));

						echo "Account created";
						$_POST['reguser'] = "123";
						
						//Close DB connection
						$dbo = null;
					}
				}
		?>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>