<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php PowerUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>
		
	<h1 class='title'>Delete appointment</h1>
	<p class='title'>Deletion is permanent</p>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
			dbConnect(); // Connect to Database
			
			$appointment_id=$_POST['appointment_id'];
			$prev_url=$_POST['prev_url'];
			
			$stmt = $dbo->prepare("DELETE FROM appointments WHERE Appointment_ID=?");
			$stmt->execute(array($appointment_id));
			
			$dbo = null; //Close DB connection
			print "Appointment (ID: " . $appointment_id . ") deleted<br><br>";
			print "<a href='" . $_POST['prev_url'] . "'>Back to previous page</a>";
		} else {
			header("location:appointment_delete.php");		
		}
	?>
	
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 