<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Retrieving Full Appointment Information
	$participant_id = $_GET['participant_id'];
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT Fitbit_Device FROM participants WHERE Participant_ID=:participant_id');
	$stmt->execute(array('participant_id' => $participant_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
	} 
	else {
		foreach ($result as $row){
			$fitbit_device = $row['Fitbit_Device'];
		}		
	}						
	$dbo = null; //Close DB connection
?>


	<h1 class='title'>Participant</h1>
	<p class='title'>Edit</p>
	<?php //echo $appointment_time_t; //To check variable ?>
	<form action="participant_fitbit_device_process.php" method="post">
		<table class='new'>
			<tr>
				<td align='right' width='30%'>Participant ID:</td>
				<td align='left' width='70%'>
					<input name="participant_id2" size="40" type="text" disabled="disabled" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $participant_id ?>"></input>
					<input name="participant_id" size="40" type="hidden" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $participant_id ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Fitbit Device:</td>
				<td align='left'>
					<input name="fitbit_device" type="text" size="40" value="<?php echo isset($_POST['fitbit_device']) ? $_POST['fitbit_device'] : $fitbit_device ?>"></input>
				</td>
			</tr>
			<tr>
				<td><br><br><br></td>
				<td align='right'>
					<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
					<input type="submit" value="Update"></input>
				</td>
			</tr>
		</table>
	</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
