<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php //Readme
// Incentives are calculated when accessing incentive.php and participant_info.php with function resources/library/incentive.php
// Once calculated, the results will be inserted into log_incentive
// So any changes made to this page has to be made to resources/library/incentive.php
?>

<?php //Participant Data
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$participant_aid=$_GET['participant_aid'];
	$participant_id=$_GET['participant_id'];
	
	$stmt = $dbo->prepare('SELECT Participant_ID, Num_Daily_Doses, Interval_1to2, Interval_2to3, Interval_3to4, Interval_4to5, Interval_5to6, Interval_6to7, Interval_7to8, Baseline_Assessment_Date, Month3_Assessment_Date, Month6_Assessment_Date 
							FROM participants
							WHERE Participant_AID=:participant_aid');
	$stmt->execute(array('participant_aid' => $participant_aid));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "no participant";
	} 
	else {
		foreach ($result as $row){
			$participant_id = $row['Participant_ID'];
			$num_daily_doses = $row['Num_Daily_Doses'];
			$interval_1to2 = $row['Interval_1to2'];
			$interval_2to3 = $row['Interval_2to3'];
			$interval_3to4 = $row['Interval_3to4'];
			$interval_4to5 = $row['Interval_4to5'];
			$interval_5to6 = $row['Interval_5to6'];
			$interval_6to7 = $row['Interval_6to7'];
			$interval_7to8 = $row['Interval_7to8'];
			$baseline_assessment_date=$row['Baseline_Assessment_Date'];
			$month3_assessment_date=$row['Month3_Assessment_Date'];
			$month6_assessment_date=$row['Month6_Assessment_Date'];	
			$interval_array = array($row['Interval_1to2'],$row['Interval_2to3'],$row['Interval_3to4'],$row['Interval_4to5'],$row['Interval_5to6'],$row['Interval_6to7'],$row['Interval_7to8']);		
		}		
	} 
	$dbo = null; //Close DB connection
?>

<?php 
// Calculate Adherence
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/Cal_Adherence.php");
	list($mth13_adherence_days, $mth13_adherence_percent, $row_mth13_data, $mth46_adherence_days, $mth46_adherence_percent, $row_mth46_data) = cal_adherence($participant_aid);
?>


<?php //Customized Display rows

	$list= array(4,5,6,7); // Only display prediction ddl for event_id 3,4,5,6
	if (in_array($event_id, $list)){
	
		//Calculate Total Incentive Awarded
		$total_incentive=0;
		if ($days_7000 >= ($days_incentive-8)) {
			$days_7000_eligibility='Yes';
			$total_incentive=$daily_rental;
		} else {
			$days_7000_eligibility='No';
		}
		
		if ($days_7000_eligibility==$current_mth_prediction) {
			$prediction_eligibility='Yes';
			$total_incentive=$total_incentive+10;
		} else {
			$prediction_eligibility='No';
		}
		
		$current_mth_prediction_tr = '	<tr>								
											<th>Current Month Prediction</th>
											<td>'.$current_mth_prediction.'</td>						
										</tr>';
		$days_7000_eligibility_tr = '	<tr>								
											<th>Step Incentive Eligibility (>= ' . ($days_incentive-8) . ' days with 7000 steps)</th>
											<td>'.$days_7000_eligibility.'</td>						
										</tr>';							
		$prediction_eligibility_tr = '	<tr>								
											<th>Prediction Task Incentive Eligibility</th>
											<td>'.$prediction_eligibility.'</td>						
										</tr>';
	}
	
	
	$list= array(3,4,5,6); // Only display prediction ddl for event_id 3,4,5,6
	if (in_array($event_id, $list)){
		$next_mth_prediction_tr = '	<tr>								
										<th>Next Month Prediction</th>
										<td>'.$next_mth_prediction.'</td>						
									</tr>	';
		$next_mth_prediction_status=isset($_GET['prediction_status']) ? '&#10004;' : '';							
		$next_mth_prediction_form='<table width="700px" align="left">
										<tr>			
											<form action="incentive_prediction_process.php" method="post">
											<td width="70%" style="font-weight:bold;" align="right">Predict if you will attain next month steps target?</td>
											<td width="20%" align="left">
												<input name="participant_id" size="40" type="hidden" value="'.$participant_id.'"></input>
												<input name="event_id" size="40" type="hidden" value="'.$event_id.'"></input>
												<input name="prev_url" size="40" type="hidden" value="'.$_SERVER["REQUEST_URI"].'"></input>
												<select name="next_mth_prediction">' . $next_mth_prediction_row . '</select>
												' . $next_mth_prediction_status . '
											</td>		
											<td width="10%" align="right"><input type="submit" value="Update"></input></td>
											</form>
										</tr>			
									</table>';
	}
	 
	if ($days_500 >= ($days_incentive-8) && $incabsys=='Yes') { // for Study Compliance Incentive Eligibility
		$days_500_eligibility='Yes';
		$total_incentive=$total_incentive+10;
	} else {
		$days_500_eligibility='No';
	}
	
	$list= array(4,5,6,7,8,9,10); // Only display prediction ddl for event_id 3,4,5,6
	if (in_array($event_id, $list)){
		$incabsys_status=isset($_GET['incabsys_status']) ? '&#10004;' : '';
		$incabsys_form='<table width="700px" align="left">
								<tr>			
									<form action="incentive_incabsys_process.php" method="post">
									<td width="70%" style="font-weight:bold;" align="right">Log in and out of in-cab driving system for >=' . ($days_incentive-8) . ' of ' . $days_incentive . ' days?</td>
									<td width="20%" align="left">
										<input name="participant_id" size="40" type="hidden" value="'.$participant_id.'"></input>
										<input name="event_id" size="40" type="hidden" value="'.$event_id.'"></input>
										<input name="prev_url" size="40" type="hidden" value="'.$_SERVER["REQUEST_URI"].'"></input>
										<select name="incabsys">' . $incabsys_row . '</select>
										' . $incabsys_status . '
									</td>		
									<td width="10%" align="right"><input type="submit" value="Update"></input></td>
									</form>
								</tr>			
							</table>';
		$total_incentive_tr = '<table width="760px" ><tr><td><br></td></tr></table><table class="wborder" width="700px" align="left">
									<tr>								
										<th width="70%" style="font-weight:bold;">Total Incentive Awarded</th>
										<td width="30%">S$' . number_format($total_incentive,2) . '</td>						
									</tr>
									'.$lottery_incentive_tr.'
								</table>';
		
		include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
		dbConnect(); // Connect to Database		
		$stmt = $dbo->prepare('SELECT Incentive_Paid FROM log_incentive WHERE Participant_ID=? AND Event_ID=?');
		$stmt->execute(array($participant_id,$event_id));
		$row_count = $stmt->rowCount();
		$result = $stmt->fetchAll();
		
		if ($row_count==0) {
		} 
		else {
			foreach ($result as $row){
				$incentive_paid=$row['Incentive_Paid'];
			}
		}							
		$dbo = null;
		if ($incentive_paid==null){
			$incentive_paid=0;
		}
		$incentive_paid_tr = '<table class="wborder" width="700px" align="left">
									<tr>								
										<th width="70%" style="font-weight:bold;">Incentive Paid</th>
										<td width="30%">S$' . number_format($incentive_paid,2) . '</td>						
									</tr>			
								</table>';
		$incentive_paid_form='<table width="700px" align="left">
										<tr>			
											<form action="incentive_paid_process.php" method="post">
											<td width="70%" style="font-weight:bold;" align="right">Amount to pay (S$):</td>
											<td width="20%" align="left">
												<input name="participant_id" size="40" type="hidden" value="'.$participant_id.'"></input>
												<input name="event_id" size="40" type="hidden" value="'.$event_id.'"></input>
												<input name="prev_url" size="40" type="hidden" value="'.$_SERVER["REQUEST_URI"].'"></input>
												<input name="incentive_paid" size="10" type="text" value="'.$_POST['incentive_paid'].'"></input>
											</td>		
											<td width="10%" align="right"><input type="submit" value="Pay"></input></td>
											</form>
										</tr>			
									</table>';
		
	}
?>

<?php //Insert into log_incentive
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$list= array(4,5,6,7,8,9,10); // Only insert into log_incentive if event=array
	if (in_array($event_id, $list)){
		//Insert into log_incentive
		
		if ($apptstatus_id==3){
			$stmt = $dbo->prepare('SELECT * FROM log_incentive WHERE Participant_ID=? AND Event_ID=?');
			$stmt->execute(array($participant_id,$event_id));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			
			if ($row_count==0) {
				$stmt = $dbo->prepare("INSERT INTO log_incentive(Participant_ID,Appointment_ID,Event_ID,Appointment_Date,Daily_Rental,Current_Mth_Prediction,Next_Mth_Prediction,Days_7000steps,Days_500steps,Avg_Steps,Step_Incentive_Eligibility,Prediction_Eligibility,Compliance_Incentive_Eligibility,Compliance_Lottery_Eligibility,Total_Incentive) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				$stmt->execute(array($participant_id,$appointment_id,$event_id,$appointment_date,$daily_rental,$current_mth_prediction,$next_mth_prediction,$days_7000,$days_500,$steps_average,$days_7000_eligibility,$prediction_eligibility,$days_500_eligibility,$days_500_eligibility,$total_incentive));
			} 
			else {
				$stmt = $dbo->prepare("UPDATE log_incentive SET Participant_ID=?, Appointment_ID=?, Event_ID=?, Appointment_Date=?, Daily_Rental=?, Current_Mth_Prediction=?, Next_Mth_Prediction=?, Days_7000steps=?, Days_500steps=?, Avg_Steps=?, Step_Incentive_Eligibility=?, Prediction_Eligibility=?, Compliance_Incentive_Eligibility=?, Compliance_Lottery_Eligibility=?, Total_Incentive=? WHERE Participant_ID=? AND Event_ID=?");
				$stmt->execute(array($participant_id,$appointment_id,$event_id,$appointment_date,$daily_rental,$current_mth_prediction,$next_mth_prediction,$days_7000,$days_500,$steps_average,$days_7000_eligibility,$prediction_eligibility,$days_500_eligibility,$days_500_eligibility,$total_incentive,$participant_id,$event_id));	
			}	
		} else {
			$stmt = $dbo->prepare("DELETE FROM log_incentive WHERE Participant_ID=? AND Event_ID=?");
			$stmt->execute(array($participant_id,$event_id));
		}	
	}
	//Close DB connection
	$dbo = null;
?>


			
			<h1 class='title'>Doses</h1>
			<p class='title'>Daily doses downloaded from eCAP</p>
			
			<div style='float:left;width:760px;'>	
				<table class='wborder' width='700px' align='left'>
					<tr>								
						<th width='70%'>Participant ID</th>
						<td width='30%'><a href='participant_info.php?participant_aid=<?php echo $participant_aid; ?>'><?php echo $participant_id; ?></a></td>						
					</tr>	
					<tr>								
						<th width='70%'>Date of Baseline Assessment</th>
						<td width='30%'><?php echo date("d M Y, D", strtotime($baseline_assessment_date)); ?></td>						
					</tr>
					<tr>								
						<th width='70%'>Date of Month 3 Assessment</th>
						<td width='30%'><?php echo date("d M Y, D", strtotime($month3_assessment_date)); ?></td>						
					</tr>
					<tr>								
						<th width='70%'>Date of Month 6 Assessment</th>
						<td width='30%'><?php echo date("d M Y, D", strtotime($month6_assessment_date)); ?></td>						
					</tr>
					<tr>								
						<th width='70%'>Number of Daily Doses</th>
						<td width='30%'><?php echo $num_daily_doses; ?></td>						
					</tr>
					<tr>								
						<th width='70%'>Dose Intervals</th>
						<td width='30%'><?php echo $interval_1to2 . ' ' . $interval_2to3 . ' ' . $interval_3to4 . ' ' . $interval_4to5 . ' ' . $interval_5to6 . ' ' . $interval_6to7 . ' ' . $interval_7to8; ?></td>						
					</tr>
				</table>
				<table width='760px' ><tr><td><br></td></tr></table>
				<table class='wborder' width='700px' align='left'>
					<tr>								
						<th width='70%'>Month 1-3 Starts From</th>
						<td width='30%'><?php echo date("d M Y, h:i A", strtotime($baseline_assessment_date . "+27 hour")); ?></td>						
					</tr>
					<tr>								
						<th width='70%'>Month 1-3 Ends At</th>
						<td width='30%'><?php echo date("d M Y, h:i A", strtotime($baseline_assessment_date . "+2067 hour")); ?></td>						
					</tr>
					<tr>								
						<th width='70%'>Adherent Days</th>
						<td width='30%'><?php echo $mth13_adherence_days; ?></td>						
					</tr>
					<tr>								
						<th width='70%'>% of Days Found Adherent (Adherent Days / 85)</th>
						<td width='30%'><?php echo $mth13_adherence_percent*100 . "%"; ?></td>						
					</tr>
				</table>
				<table width='760px' ><tr><td><br></td></tr></table>
				<table class='wborder' width='700px' align='left'>
					<tr>								
						<th width='70%'>Month 4-6 Starts From</th>
						<td width='30%'><?php echo date("d M Y, h:i A", strtotime($month3_assessment_date . "+339 hour")); ?></td>						
					</tr>
					<tr>								
						<th width='70%'>Month 4-6 Ends At</th>
						<td width='30%'><?php echo date("d M Y, h:i A", strtotime($month3_assessment_date . "+2379 hour")); ?></td>						
					</tr>
					<tr>								
						<th width='70%'>Adherent Days</th>
						<td width='30%'><?php echo $mth46_adherence_days; ?></td>						
					</tr>
					<tr>								
						<th width='70%'>% of Days Found Adherent (Adherent Days / 85)</th>
						<td width='30%'><?php echo $mth46_adherence_percent*100 . "%"; ?></td>						
					</tr>
				</table>
				<table width='760px' ><tr><td><br></td></tr></table>
				<table width='760px' ><tr><td><br></td></tr></table>
			</div>
				
				<table class='wborder' width='960px'>
					<tr>								
						<th align='left' width='50%'>Mth 1-3</th>
						<th align='left' width='50%'>Mth 4-6</th>						
					</tr>		
					<tr>
						<td align='left' valign='top'>
							<?php echo $row_mth13_data; ?>
						</td>
						<td align='left' valign='top'>
							<?php echo $row_mth46_data; ?>
						</td>
					</tr>
				</table>


<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
