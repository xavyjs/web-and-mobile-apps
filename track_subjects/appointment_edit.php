<?php ob_start(); //Turning on the output buffer. So any output is kept in the buffer. (Put code at top of page) ?>
<?php session_start(); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php"); ?>
<?php SessionCheck(); //Check legitimate session ?>
<?php NormalUserCheck(); //Check legitimate session ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/header.php"); ?>

<?php include_once($_SERVER['DOCUMENT_ROOT']."/resources/library/DateTimePicker.php");  ?>

<?php //Retrieving Full Appointment Information
	$appointment_id = isset($_POST['appointment_id']) ? $_POST['appointment_id'] : $_GET['appointment_id'];
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM appointments 
							WHERE Appointment_ID=:appointment_id');
	$stmt->execute(array('appointment_id' => $appointment_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "Appointment does not exist";
	} 
	else {
		foreach ($result as $row){
			$participant_id = $row['Participant_ID'];
			$event_id = $row['Event_ID'];
			$fitbit_start_date = $row['Fitbit_Start_Date'];
			$appointment_date = $row['Appointment_Date'];
			$appointment_time = $row['Appointment_Time'];
			$location_id = $row['Location_ID'];
			$apptstatus_id = $row['ApptStatus_ID'];
			$note = $row['Note'];
			$disable_sms = isset($_POST['disable_sms']) ? $_POST['disable_sms'] : $row['Disable_SMS'];
			
			if ($disable_sms==1) {
				$disable_sms_checked="checked";
			} else { 
				$disable_sms_checked="";
			}
		}
		if (isset($fitbit_start_date)) {
			$fitbit_start_date=date("d M Y", strtotime($fitbit_start_date));
		}
		$appointment_date=date("d M Y", strtotime($appointment_date));
		$appointment_time=date("h:i A", strtotime($appointment_time));
		
	}						
	$dbo = null; //Close DB connection
?>

<?php //Retrieving Full Participant Information
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT t.Title, p.Lastname, p.Firstname, p.Mobile, p.Email, p.Address1, p.Postcode, p.PNote FROM participants AS p LEFT JOIN ctbl_title AS t ON p.Title_ID = t.Title_ID WHERE p.Participant_ID=:participant_id');
	$stmt->execute(array('participant_id' => $participant_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "Participant does not exist";
	} 
	else {
		foreach ($result as $row){
			$title = $row['Title'];
			$lastname = $row['Lastname'];
			$firstname = $row['Firstname'];
		}		
	}						
	$dbo = null; //Close DB connection
?>

<?php //Event dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_event');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();

	if ($row_count==0) {
		echo "The database contains no event yet";
	} 
	else {
		foreach ($result as $row){
			if ($row['Event_ID']==(isset($_POST['event_id']) ? $_POST['event_id'] : $event_id)) {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . " selected='selected'>" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			} else {
				$event_id_row = $event_id_row . "<option value=" . $row['Event_ID'] . ">" . $row['Event_ID'] . ". " . $row['Event'] . "</option>";
			}
		}
	}						
	$dbo = null; //Close DB connection
?>

<?php //Location dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_location');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no location yet";
	} else {
		foreach ($result as $row){
			if ($row['Location_ID']==(isset($_POST['location_id']) ? $_POST['location_id'] : $location_id)) {
				$location_id_row = $location_id_row . "<option value=" . $row['Location_ID'] . " selected='selected'>" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
			} else {
				$location_id_row = $location_id_row . "<option value=" . $row['Location_ID'] . ">" . $row['Location_ID'] . ". " . $row['Location'] . "</option>";
			}
		}
	}
	$dbo = null; //Close DB connection
?>

<?php //Status dropdown list
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database
	
	$stmt = $dbo->prepare('SELECT * FROM ctbl_apptstatus');
	$stmt->execute();
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
		echo "The database contains no status yet";
	} else {
		foreach ($result as $row){
			if ($row['ApptStatus_ID']==(isset($_POST['apptstatus_id']) ? $_POST['apptstatus_id'] : $apptstatus_id)) {
				$apptstatus_id_row = $apptstatus_id_row . "<option value=" . $row['ApptStatus_ID'] . " selected='selected'>" . $row['ApptStatus_ID'] . ". " . $row['ApptStatus'] . "</option>";
			} else {
				$apptstatus_id_row = $apptstatus_id_row . "<option value=" . $row['ApptStatus_ID'] . ">" . $row['ApptStatus_ID'] . ". " . $row['ApptStatus'] . "</option>";
			}
		}
	}
	$dbo = null; //Close DB connection
?>

<?php //SMS Log
	include_once($_SERVER['DOCUMENT_ROOT']."/resources/config.php");
	dbConnect(); // Connect to Database

	$stmt = $dbo->prepare('SELECT * FROM log_sms WHERE Participant_ID=:participant_id AND Appointment_ID=:appointment_id ORDER BY log_SMS_ID');
	$stmt->execute(array('participant_id' => $participant_id,'appointment_id' => $appointment_id));
	$row_count = $stmt->rowCount();
	$result = $stmt->fetchAll();
	
	if ($row_count==0) {
	} 
	else {
		$i=1;	
		foreach ($result as $row){			
			$stmt = $dbo->prepare('SELECT * FROM log_sms WHERE Participant_ID=:participant_id AND Appointment_ID=:appointment_id ORDER BY log_SMS_ID');
			$stmt->execute(array('participant_id' => $participant_id,'appointment_id' => $appointment_id));
			$row_count = $stmt->rowCount();
			$result = $stmt->fetchAll();
			
			if ($row_count==0) { 						
			} 
			else {//Get data from log_sms_reply	
				$stmt2 = $dbo->prepare('SELECT * FROM log_sms_reply WHERE Participant_ID=:participant_id AND MT_ID=:mt_id ORDER BY Timestamp');
				$stmt2->execute(array('participant_id' => $participant_id,'mt_id' => $row['MT_ID']));
				$row_count2 = $stmt2->rowCount();
				$result2 = $stmt2->fetchAll();
				if ($row_count2==0) {	
					$reply_txt="";
				} 
				else {	
					$reply_txt = "&nbsp;&nbsp;&nbsp;Reply&#013;&#010;";
					foreach ($result2 as $row2){
						$reply_txt = $reply_txt . " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . date("dmYHi", strtotime($row2['Timestamp'])) . " " . $row2['Message'] . "&#013;&#010;";
					}
				}
				$sms = $sms . $i . ". " . date("dmYHi", strtotime($row['Timestamp'])) . "&#013;&#010;" . $reply_txt . "&#013;&#010;"; // Line break &#013;&#010;
				$i=$i+1;
			}						
		}
	}
	$dbo = null; //Close DB connection
?>

	<h1 class='title'>Appointment</h1>
	<p class='title'>Edit</p>
	<?php //echo $appointment_time_t; //To check variable ?>
	<form action="appointment_edit_process.php" method="post">
		<table class='new'>
			<tr>
				<td align='right' width='30%'>Participant:</td>
				<td align='left' width='70%'>
					<?php echo "(<a href='participant_info.php?participant_id=" . $participant_id . "'>ID: " . $participant_id . "</a>) " .$title . " " . htmlspecialchars($lastname) . " " . htmlspecialchars($firstname); ?> 
					<input name="participant_id" size="40" type="hidden" value="<?php echo isset($_POST['participant_id']) ? $_POST['participant_id'] : $participant_id ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right' width='30%'>Appointment ID:</td>
				<td align='left' width='70%'>
					<input name="appointment_id2" size="40" type="text" disabled="disabled" value="<?php echo isset($_POST['appointment_id']) ? $_POST['appointment_id'] : $appointment_id ?>"></input>
					<input name="appointment_id" size="40" type="hidden" value="<?php echo isset($_POST['appointment_id']) ? $_POST['appointment_id'] : $_GET['appointment_id'] ?>"></input>
				</td>
			</tr>
			<tr>
				<td align='right'>Event:</td>
				<td align='left'>			
					<select name="event_id"><?php echo $event_id_row; ?></select>
				</td>
			</tr>
			<tr>
				<td align='right'>Fitbit Start Date:</td>
				<td align='left'>
					<input type="text" name="fitbit_start_date" id="datepicker" class="datepicker" readonly="readonly" value="<?php echo isset($_POST['fitbit_start_date']) ? $_POST['fitbit_start_date'] : '' ?><?php echo isset($fitbit_start_date) ? $fitbit_start_date : '' ?>"/>
				</td>
			</tr>
			<tr>
				<td align='right'>Appointment Date:</td>
				<td align='left'>
					<input type="text" name="appointment_date" id="datepicker2" class="datepicker" readonly="readonly" value="<?php echo isset($_POST['appointment_date']) ? $_POST['appointment_date'] : $appointment_date ?>"/>
				</td>
			</tr>
			<tr>
				<td align='right'>Appointment Time:</td>
				<td align='left'>
					<input type="text" name="appointment_time" id="timepicker" readonly="readonly" value="<?php echo isset($_POST['appointment_time']) ? $_POST['appointment_time'] : $appointment_time ?>"/>
				</td>
			</tr>
			<tr>
				<td align='right'>Location:</td>
				<td align='left'>			
					<select name="location_id"><?php echo $location_id_row; ?></select>
				</td>
			</tr>
			<tr>
				<td align='right'>Status:</td>
				<td align='left'>			
					<select name="apptstatus_id"><?php echo $apptstatus_id_row; ?></select>
				</td>
			</tr>
			<tr>
				<td align='right' style='vertical-align:top;'>Note:</td>
				<td align='left'>			
					<textarea name='note' rows='5' cols='50'><?php echo isset($_POST['note']) ? $_POST['note'] : $note ?></textarea>
				</td>
			</tr>
			<tr>
				<td align='right' style='vertical-align:top;'>SMS Sent:</td>
				<td align='left'>			
					<textarea name='sms' rows='10' cols='50' disabled><?php echo $sms; ?></textarea>
				</td>
			</tr>
			<?php if ($event_id==2 || $event_id==3) {?>
			<tr>
				<td align='right' style='vertical-align:top;'>Disable SMS?</td>
				<td align='left'>	
					<input type="checkbox" name="disable_sms" value="1" <?php echo $disable_sms_checked; ?>>

				</td>
			</tr>
			<?php } ?>
			<tr>
				<td><br><br><br></td>
				<td align='right'>
					<input name="prev_url" size="40" type="hidden" value="<?php echo isset($_POST['prev_url']) ? $_POST['prev_url'] : $_GET['prev_url'] ?>"></input>
					<input type="submit" value="Update"></input>
				</td>
			</tr>
		</table>
	</form>

<?php include($_SERVER['DOCUMENT_ROOT']."/resources/templates/footer.php"); ?>
<?php //ob_flush(); //Flush the buffer. (Put code at end of page) ?> 
