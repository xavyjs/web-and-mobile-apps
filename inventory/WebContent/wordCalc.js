function words(amount) {

		var word = calc(amount / 100000);
		var words = "";
		if (!word.equals("")) {
			words += word + "Lakh ";
		}
		amount %= 100000;
		// System.out.println(amount);
		word = calc(amount / 1000);
		if (!(word=="")) {
			words += word + "thousand ";
		}
		amount %= 1000;
		word="";
		if (amount / 100 != 0) {
			word = ones[(amount / 100) - 1] + " hundred ";
		}
		words += word;
		amount %= 100;
		word = calc(amount);
		if (!(word=="")) {
			if(words=="")
				words += word ;
			else
				words += "and " + word ;
		} else {
			words=words.replace("thousand ", "thousand and ");
			words += "Rupees";
		}
		return words;
	}

	function calc(x) {
		String word = "";
		if (x / 10 == 0) {
			word += "";
			if (x % 10 != 0) {
				word += ones[x % 10 - 1] + " ";
			}
		}

		else {
			if (x / 10 == 1) {

				if (x % 10 == 0) {
					word += "ten ";
				} else {
					word += ten[x % 10 - 1] + " ";
				}
			} else {
				word += tens[x / 10 - 1] + " ";

				if (x % 10 != 0) {
					word += ones[x % 10 - 1] + " ";
				}
			}

		}
		return word;

	}
	
	var ones = { "one", "two", "three", "four", "five", "six", "seven",	"eight", "nine" };
	var tens = { "ten", "twenty", "thirty", "forty", "fifty", "sixty",
			"seventy", "eighty", "ninty" };
	var ten = { "eleven", "twelve", "thirteen", "forteen", "fifteen",
			"sixteen", "seventeen", "eighteen", "nineteen" };