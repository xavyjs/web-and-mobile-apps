var inventory = angular.module("inventory",[]);
inventory.controller("mainController",function($scope){
	
	
	$scope.page="/sooryoll/jsp/order/order.html";
	$scope.setMenu = function(x){
		clickMenu(x);
		switch(x)
		{
			case 1:
				$scope.page="/sooryoll/jsp/order/order.html";
				break;
			case 2:
				$scope.page="/sooryoll/jsp/order/supply.html";
				break;
			case 3:
				$scope.page="/sooryoll/jsp/order/stock.html";
				break;
			case 4:
				$scope.page="/sooryoll/jsp/order/finance.html";
				break;
			case 5:
				$scope.page="/sooryoll/jsp/order/settings.html";
				break;
		}
			
	}
	$scope.init=function(){
		header();
	};
	$scope.init();
});
inventory.controller("orderController",function($scope,$http){
		$scope.addForm = false;
		$scope.addDeliveryForm=false;
		$scope.delivery = {

				
				deliveryId:null,
				orderId:"",
				quantity:0,
				packagingCharge:0.0,
				transportType:"",
				parcelServiceId:"",
				refNumber:"",
				description:"",
				dateString:""
		};
		$scope.transports;
		$scope.buttonName = "ADD Order";
		$scope.formToggle = function(){
			if($scope.addForm){
				$scope.addForm = false;
				$scope.addDeliveryForm = false;
				$scope.buttonName="Add Order";
			} else {
				$scope.addForm = true;
				$scope.buttonName="Cancel";
			}
		}
		$scope.switchDeliveryForm = function(x){
			if($scope.addDeliveryForm){
				$scope.addDeliveryForm = false;
				
			} else {
				$scope.delivery.orderId = x.orderId;
				$scope.delivery.quantity = x.quantity;
				$scope.addDeliveryForm = true;
			}
		}
		$scope.init = function(){
			$http.post('/sooryoll/order/load.action', {orderRequest:$scope.order}).
			  success(function(data, status, headers, config) {
					$scope.orders = data.orders;
					$scope.customers = data.customers;
					$scope.products=data.products;
					$scope.productTotal = data.productTotal;
					$scope.customerWiseProduct = data.customerWiseProduct;
					$scope.transports = data.transports;
			  });
		};
		$scope.add = function(){
			//alert("adding");
			$http.post('/sooryoll/order/add.action', {orderRequest:$scope.order}).
			  success(function(data, status, headers, config) {
					$scope.orders = data.orders;
					$scope.productTotal = data.productTotal;
					$scope.customerWiseProduct = data.customerWiseProduct;
					$scope.order="";
					$scope.sort="";
			  });
		};
		$scope.deliver = function(){
			$http.post('/sooryoll/delivery/add.action', {delivery:$scope.delivery}).
			  success(function(data, status, headers, config) {
				  if(data.result){
					$scope.orders = data.orders;
					$scope.productTotal = data.productTotal;
					$scope.customerWiseProduct = data.customerWiseProduct;
					$scope.order="";
					$scope.sort="";
					$scope.addDeliveryForm=false;
					$scope.delivery="";
					alert("delivery successfully recorder");
					
				  }else{
					  alert(data.messages[0]);
				  }
			  });
		}
		$scope.init();
		$scope.customers = {
		};
		$scope.products = {
		};
		$scope.orders={
		};
		$scope.order= {
			
			productId:"",
			customer:"",
			quantity:"",
			description:""
		};
		$scope.sort;
		$scope.sorter = function(x){
			
			switch(x){
			case 0:
				
				$scope.orders.sort(function(a,b){
					var i ;
					if(new Date(a.date) > new Date(b.date)){
						i=-1;
					}else if(new Date(a.date) < new Date(b.date)){
						i=1;
					}else{
						i=0;
					}
					return i;
					});
				break;
			case 1:
				$scope.orders.sort(function(a,b){return a.customer.localeCompare(b.customer)});
				break;
			case 2:
				$scope.orders.sort(function(a,b){return a.productId.localeCompare(b.productId)});
				break;
			case 3:
				$scope.orders.sort(function(a,b){return a.quantity-b.quantity});
				break;
			} 
		};
		
		$scope.customerWiseProduct=[];
		$scope.productTotal=[];
		
		
		
});
inventory.controller("productController",function($scope,$http){
	$scope.editProduct=false;
	$scope.addForm = false;
	$scope.buttonName = "ADD Product";
	$scope.products;
	$scope.sort;
	$scope.formToggle = function(){
		if($scope.addForm){
			$scope.addForm = false;
			$scope.buttonName="Add Product";
			$scope.editProduct=false;
		} else {
			$scope.addForm = true;
			$scope.buttonName="Cancel";
		}
	};
	$scope.remove = function(id){
		alert(id);
	};
	$scope.edit = function(id){
		for(var i=0;i<$scope.products.length;i++){
			if($scope.products[i].productId==id){
				$scope.product = $scope.products[i];
				break;
			}
		}
		$scope.addForm = true;
		$scope.buttonName="Cancel";
		$scope.editProduct=true;
		
	};
	
	$scope.sorter = function(x){
		
		switch(x){
		case 0:
			$scope.products.sort(function(a,b){return a.productId.localeCompare(b.produtId);});
			break;
		case 1:
			$scope.products.sort(function(a,b){return a.name.localeCompare(b.name);});
			break;
		case 2:
			$scope.products.sort(function(a,b){return a.grade-b.grade;});
			break;
		case 3:
			$scope.products.sort(function(a,b){return a.pricePerLit-b.pricePerLit;});
			break;
		} 
	};
	$scope.add = function(){
		
		
		$http.post('/sooryoll/product/add.action', {product:$scope.product}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("User Successfully added");
					$scope.product="";
					$scope.init();
				}
		  });
	};
	$scope.update = function(){
		$http.post('/sooryoll/product/update.action', {product:$scope.product}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("User Successfully updated");
					$scope.product="";
					$scope.init();
				}
		  });
	};
	$scope.init = function(){
		$http.post('/sooryoll/product/load.action', {orderRequest:$scope.order}).
		  success(function(data, status, headers, config) {
				$scope.products = data.products;
		  });
		
	};
	$scope.init();
	
	
});

inventory.controller("materialController",function($scope,$http){
	$scope.editMaterial=false;
	$scope.addForm = false;
	$scope.buttonName = "ADD Material";
	$scope.materials;
	$scope.sort;
	$scope.formToggle = function(){
		if($scope.addForm){
			$scope.addForm = false;
			$scope.buttonName="Add Material";
			$scope.editMaterial=false;
		} else {
			$scope.addForm = true;
			$scope.buttonName="Cancel";
		}
	};
	$scope.remove = function(id){
		alert(id);
	};
	$scope.edit = function(id){
		for(var i=0;i<$scope.materials.length;i++){
			if($scope.materials[i].materialId==id){
				$scope.material = $scope.materials[i];
				break;
			}
		}
		$scope.addForm = true;
		$scope.buttonName="Cancel";
		$scope.editMaterial=true;
		
	};
	
	$scope.sorter = function(x){
		
		switch(x){
		case 0:
			
			$scope.materials.sort(function(a,b){return a.materialId.localeCompare(b.materialId);});
			break;
		case 1:
			$scope.materials.sort(function(a,b){return a.name.localeCompare(b.name);});
			break;
		case 2:
			$scope.materials.sort(function(a,b){return a.grade-b.grade;});
			break;
		case 3:
			$scope.materials.sort(function(a,b){return a.pricePerLit-b.pricePerLit;});
			break;
		} 
	};
	$scope.add = function(){
		
		
		$http.post('/sooryoll/material/add.action', {material:$scope.material}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("User Successfully added");
					$scope.material="";
					$scope.initM();
				}
		  });
	};
	$scope.update = function(){
		$http.post('/sooryoll/material/update.action', {material:$scope.material}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("Material Successfully updated");
					$scope.material="";
					$scope.initM();
				}
		  });
	};
	$scope.initM = function(){
		$http.post('/sooryoll/material/load.action', {orderRequest:$scope.order}).
		  success(function(data, status, headers, config) {
				$scope.materials = data.materials;
		  });
		
	};
	$scope.initM();
	
	
});

inventory.controller("supplierController",function($scope,$http){
	$scope.editSupplier=false;
	$scope.addForm = false;
	$scope.buttonName = "ADD Supplier";
	$scope.suppliers;
	$scope.sort;
	$scope.formToggle = function(){
		$scope.supplier = "";
		if($scope.addForm){
			$scope.addForm = false;
			$scope.buttonName="Add Supplier";
			$scope.editSupplier=false;
		} else {
			$scope.supplier={name:"",company:"",address1:"",address2:"",address3:"",pincode:"",mobile:"",eMail:"",accounts:[]};
			$scope.addForm = true;
			$scope.buttonName="Cancel";
		}
	};
	$scope.remove = function(id){
		alert(id);
	};
	$scope.edit = function(id){
		
		for(var i=0;i<$scope.suppliers.length;i++){
			if($scope.suppliers[i].name==id){
				$scope.supplier = $scope.suppliers[i];
				break;
			}
		}
		$scope.addForm = true;
		$scope.buttonName="Cancel";
		$scope.editSupplier=true;
		
	};
	$scope.addAccount = function(){
		$scope.supplier.accounts.push({accountNumber:"",accountName:"",ifsc:"",accountType:"",description:""});
		
	};
	$scope.removeAccount = function(id){
		$scope.supplier.accounts =$scope.supplier.accounts.filter(
				function(el){
					return el.accountNumber != id;
				}
		);	
	};
	$scope.sorter = function(x){
		
		switch(x){
		case 0:
			
			$scope.suppliers.sort(function(a,b){return a.name.localeCompare(b.produtId)});
			break;
		case 1:
			$scope.suppliers.sort(function(a,b){return a.name.localeCompare(b.name)});
			break;
		case 2:
			$scope.suppliers.sort(function(a,b){return a.rade.localeCompare(b.grade)});
			break;
		case 3:
			$scope.suppliers.sort(function(a,b){return a.pricePerLitre-b.pricePerLitre});
			break;
		} 
	};
	$scope.add = function(){
		$http.post('/sooryoll/supplier/add.action', {supplier:$scope.supplier}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("User Successfully added");
					$scope.supplier="";
					$scope.initS();
				}
		  });
	};
	$scope.update = function(){
		$http.post('/sooryoll/supplier/update.action', {supplier:$scope.supplier}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("Supplier Successfully updated");
					$scope.supplier="";
					$scope.initS();
				}
		  });
	};
	$scope.initS = function(){
		
		$http.post('/sooryoll/supplier/load.action', {orderRequest:$scope.order}).
		  success(function(data, status, headers, config) {
				$scope.suppliers = data.suppliers;
		  });
		$scope.editSupplier=false;
		$scope.addForm = false;
		$scope.buttonName = "ADD Supplier";
	};
	$scope.initS();
	
	
});


inventory.controller("customerController",function($scope,$http){
	$scope.editCustomer=false;
	$scope.addForm = false;
	$scope.buttonName = "ADD Customer";
	$scope.customers;
	$scope.sort;
	
	$scope.formToggle = function(){
		$scope.customer = "";
		if($scope.addForm){
			$scope.addForm = false;
			$scope.buttonName="Add Customer";
			$scope.editCustomer=false;
		} else {
			$scope.customer={name:"",company:"",address1:"",address2:"",address3:"",pincode:"",mobile:"",eMail:"",accounts:[]};
			$scope.addForm = true;
			$scope.buttonName="Cancel";
		}
	};
	$scope.remove = function(id){
		alert(id);
	};
	$scope.edit = function(id){
		
		for(var i=0;i<$scope.customers.length;i++){
			if($scope.customers[i].name==id){
				$scope.customer = $scope.customers[i];
				break;
			}
		}
		$scope.addForm = true;
		$scope.buttonName="Cancel";
		$scope.editCustomer=true;
		
	};
	$scope.addAccount = function(){
		$scope.customer.accounts.push({accountNumber:"",accountName:"",ifsc:"",accountType:"",description:""});
		
	};
	$scope.removeAccount = function(id){
		$scope.customer.accounts =$scope.customer.accounts.filter(
				function(el){
					return el.accountNumber != id;
				}
		);	
	};
	$scope.sorter = function(x){
		
		switch(x){
		case 0:
			
			$scope.customers.sort(function(a,b){return a.name.localeCompare(b.produtId)});
			break;
		case 1:
			$scope.customers.sort(function(a,b){return a.name.localeCompare(b.name)});
			break;
		case 2:
			$scope.customers.sort(function(a,b){return a.rade.localeCompare(b.grade)});
			break;
		case 3:
			$scope.customers.sort(function(a,b){return a.pricePerLitre-b.pricePerLitre});
			break;
		} 
	};
	$scope.add = function(){
		//alert();
		$http.post('/sooryoll/customer/add.action', {customer:$scope.customer}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("User Successfully added");
					$scope.customer="";
					$scope.initS();
				}
		  });
	};
	$scope.update = function(){
		$http.post('/sooryoll/customer/update.action', {customer:$scope.customer}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("Customer Successfully updated");
					$scope.customer="";
					$scope.initS();
				}
		  });
	};
	$scope.initS = function(){
		
		$http.post('/sooryoll/customer/load.action', {orderRequest:$scope.order}).
		  success(function(data, status, headers, config) {
				$scope.customers = data.customers;
				
		  });
		$scope.editCustomer=false;
		$scope.addForm = false;
		$scope.buttonName = "ADD Customer";
	};
	$scope.initS();
	
	
});

inventory.controller("supplyController",function($scope,$http){
	$scope.addForm = false;
	$scope.editSupply=false;
	$scope.buttonName = "ADD Supply";
	$scope.supply;
	$scope.sort;
	$scope.supplies;
	$scope.materials;
	$scope.suppliers;
	$scope.transports;
	
	$scope.formToggle = function(){
		$scope.supply = "";
		if($scope.addForm){
			$scope.addForm = false;
			$scope.buttonName="Add Supply";
			$scope.editSupply=false;
		} else {
			$scope.supply="";
			$scope.addForm = true;
			$scope.buttonName="Cancel";
		}
	};
	$scope.remove = function(id){
		//alert(id.supplyId);
		$http.post('/sooryoll/supply/remove.action', {id:id.supplyId}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("Supply Successfully removed");
					$scope.supply="";
					$scope.initSu();
				}else{
					alert(data.messages[0]);
				}
		  });
	};
	$scope.edit = function(id){
		
		for(var i=0;i<$scope.supplies.length;i++){
			if($scope.supplies[i].name==id){
				$scope.supply = $scope.supplies[i];
				break;
			}
		}
		$scope.addForm = true;
		$scope.buttonName="Cancel";
		$scope.editSupply=true;
		
	};
	$scope.addAccount = function(){
		$scope.supply.accounts.push({accountNumber:"",accountName:"",ifsc:"",accountType:"",description:""});
		
	};
	$scope.removeAccount = function(id){
		$scope.supply.accounts =$scope.supply.accounts.filter(
				function(el){
					return el.accountNumber != id;
				}
		);	
	};
	$scope.sorter = function(x){
		
		switch(x){
		case 0:
			
			$scope.supplies.sort(function(a,b){return a.name.localeCompare(b.produtId)});
			break;
		case 1:
			$scope.supplies.sort(function(a,b){return a.name.localeCompare(b.name)});
			break;
		case 2:
			$scope.supplies.sort(function(a,b){return a.rade.localeCompare(b.grade)});
			break;
		case 3:
			$scope.supplies.sort(function(a,b){return a.pricePerLitre-b.pricePerLitre});
			break;
		} 
	};
	$scope.add = function(){
		//alert();
		$http.post('/sooryoll/supply/add.action', {supply:$scope.supply}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("User Successfully added");
					$scope.supply="";
					$scope.initSu();
				}
		  });
	};
	$scope.update = function(){
		$http.post('/sooryoll/supply/update.action', {supply:$scope.supply}).
		  success(function(data, status, headers, config) {
				if(data.result){
					alert("Supply Successfully updated");
					$scope.supply="";
					$scope.initSu();
				}
		  });
	};
	$scope.initSu = function(){
		
		$http.post('/sooryoll/supply/load.action', {orderRequest:$scope.order}).
		  success(function(data, status, headers, config) {
			  	$scope.supplies = data.supplies;
				$scope.materials = data.materials;
				$scope.transports = data.transports;
				$scope.suppliers = data.suppliers;
		  });
		$scope.editSupply=false;
		$scope.addForm = false;
		$scope.buttonName = "ADD Supply";
	};
	$scope.initSu();
	
	
});


inventory.controller("stockController",function($scope,$http){
	
	$scope.stocks;
	$scope.inventory;
	$scope.products;
	$scope.useForm = false;
	$scope.formLable = "Produce";
	$scope.materialToUse=null;
	$scope.quantityToUse;
	$scope.materialsToUse=[];
	$scope.quantityToProduce;
	$scope.productToProduce;
	
	$scope.setUseForm = function(){
		if($scope.useForm){
			$scope.useForm = false;
			$scope.formLable = "Produce";
		}else{
			$scope.useForm = true;
			$scope.formLable = "Cancel";
		}
	};
	$scope.addMaterial = function(){
		if($scope.materialToUse==null){
			alert("please choose material");
			return;
		}
		if($scope.quantityToUse==""||$scope.quantityToUse==0){
			alert("please specify quantity");
			return;
		}
		for(var p=0;p<$scope.materialsToUse.length;p++){
			if($scope.materialsToUse[p].material.materialId==$scope.materialToUse){
				var avail;
				for(var t=0;t<$scope.stocks.length;t++){
					if($scope.stocks[t].material.materialId==$scope.materialToUse){
						avail = $scope.stocks[t].quantity;
					}
				}
				
				if(avail<(+$scope.materialsToUse[p].quantity)+(+$scope.quantityToUse)){
					alert("given quantity is not available for the chosen Material");
				}else{
					$scope.materialsToUse[p].quantity=(+$scope.materialsToUse[p].quantity)+(+$scope.quantityToUse);
				}
				return;
			}
		}
		var tObj = null;
		for(var v=0;v<$scope.stocks.length;v++){
			if($scope.stocks[v].material.materialId==$scope.materialToUse){
				if($scope.stocks[v].quantity<$scope.quantityToUse){
					alert("given quantity is not available for the chosen Material");
					return;
				}else{	
					tObj = $scope.stocks[v].material; 
				}
				break;
			}
		}
		var obj = {material:tObj,quantity:$scope.quantityToUse};
		$scope.materialsToUse.push(obj);
	};
	$scope.removeLastMaterial = function(){
		//alert('here'+$scope.materialsToUse.length);
		$scope.materialsToUse.pop($scope.materialsToUse.length-1);
		
	};
	$scope.initSu = function(){	
		$http.post('/sooryoll/stock/load.action', {orderRequest:$scope.order}).
		  success(function(data, status, headers, config) {
			  	$scope.stocks = data.stocks;
			  	$scope.inventory = data.inventory;
			  	$scope.products = data.products;
			  	$scope.materials = data.materials;
		  });
	};
	$scope.produce = function(){
		//alert($scope.productToProduce+"    "+$scope.quantityToProduce);
		var materials = [];
		for(var p=0;p<$scope.materialsToUse.length;p++){
			//alert($scope.materialsToUse[p].material.materialId+"  "+$scope.materialsToUse[p].quantity)
			var mat={material:$scope.materialsToUse[p].material.materialId,quantity:$scope.materialsToUse[p].quantity};
			//alert(mat);
			try{materials.push(mat);}
			catch(e){alert(e);}
			
		}
		$http.post('/sooryoll/stock/produce.action', {request:{materials:materials,product:$scope.productToProduce,quantity:$scope.quantityToProduce}}).
		  success(function(data, status, headers, config) {
			  	if(data.result){
			  		alert("Product Successfully Produced");
			  		$scope.initSu();
			  	}else{
			  		alert(data.messages[0]);
			  	}
			  	
		  });
		
	};
	$scope.initSu();
	
	
});

inventory.controller("financeController",function($scope,$http){
	
	$scope.dues;
	$scope.balances;
	
	$scope.customers;
	$scope.customerSort = function(){
		$scope.balances.sort(function(a,b){return a.customer.name.localeCompare(b.customer.name)});
	}
	$scope.balanceSort = function(){
		$scope.balances.sort(function(a,b){return a.amount-b.amount});
	}
	$scope.supplierSort = function(){
		$scope.dues.sort(function(a,b){return a.supplier.name.localeCompare(b.supplier.name)});
	}
	$scope.dueSort = function(){
		$scope.dues.sort(function(a,b){return a.amount-b.amount});
	}
	$scope.collection={
			
			customerId:"",
			amount:0,
			collectionId:0
	};
	$scope.dCollection={
			
			customerId:"",
			amount:0,
			collectionId:0
	};
	$scope.collectionAccounts;
	$scope.collectionForm = false;
	
	$scope.suppliers;
	$scope.payment;
	$scope.dPayment={
		paymentId:0,
		supplierId:"",
		amount:0
		
	};
	$scope.paymentAccounts;
	$scope.paymentForm = false;
	
	
	
	
	$scope.initFi = function(){	
		$http.post('/sooryoll/finance/load.action', {orderRequest:$scope.order}).
		  success(function(data, status, headers, config) {
			  	$scope.dues = data.dues;
			  	$scope.balances = data.balances;
			  	
			  	//$scope.customers = data.customers;
			  	//$scope.suppliers = data.suppliers;
		  });
	};
	
	$scope.addCollection = function(){
		if($scope.collectionForm){
			$scope.collectionForm = false;
		}else{
			$scope.collectionForm = true;
		}
	};
	$scope.addPayment = function(){
		if($scope.paymentForm){
			$scope.paymentForm = false;
		}else{
			$scope.paymentForm = true;
		}
	};
	
	$scope.getCustomerAccounts = function(){
		
		$http.post('/sooryoll/finance/accounts_load.action', {customerId:$scope.collection.customerId}).
		  success(function(data, status, headers, config) {
			  	$scope.collectionAccounts = data.accounts;
		  });
	};
	$scope.getAccounts = function(i){
		
		var obj;
		if(i==1){
			obj = {id:$scope.collection.customerId,type:i};
		}else{
			obj = {id:$scope.payment.supplierId,type:i};
		}
		$http.post('/sooryoll/finance/accounts_load.action',obj ).
		  success(function(data, status, headers, config) {
			  	if(i==1)
			  		$scope.collectionAccounts = data.accounts;
			  	else
			  		$scope.paymentAccounts = data.accounts;
			  
		  });
	};
	
	$scope.saveCollection = function(){
		$http.post('/sooryoll/finance/add_collection.action', {collection:$scope.collection}).
		  success(function(data, status, headers, config) {
			  	if(data.result){
			  		alert("collection added successfully");
			  		$scope.collection = $scope.dCollection;
			  		$scope.collectionForm = false;
			  		$scope.initFi();
			  	}else{
			  		alert("operation failed\n"+data.messages[0]);
			  	}
		  });
	};
	$scope.savePayment = function(){
		$http.post('/sooryoll/finance/add_payment.action', {payment:$scope.payment}).
		  success(function(data, status, headers, config) {
			  	if(data.result){
			  		alert("payment added successfully");
			  		$scope.payment = $scope.dPayment;
			  		$scope.paymentForm = false;
			  		$scope.initFi();
			  	}else{
			  		alert("operation failed\n"+data.messages[0]);
			  	}
		  });
	};
	
	$scope.initFi();
});
inventory.controller("settingsController",function($scope,$http){
	$scope.innerPage="/sooryoll/jsp/order/product.html";
	$scope.setInnerMenu = function(x){
		clickInnerMenu(x);
		switch(x)
		{
			
			case 1:
				$scope.innerPage="/sooryoll/jsp/order/product.html";
				break;
			case 2:
				$scope.innerPage="/sooryoll/jsp/order/material.html";
				break;
			case 3:
				$scope.innerPage="/sooryoll/jsp/order/supplier.html";
				break;
			case 4:
				$scope.innerPage="/sooryoll/jsp/order/customer.html";
				break;
			
		}
	}
});
inventory.controller("billController",function($scope,$http){
	$scope.rows = [];
	$scope.addRow = function(){
		var row = {
			slNo:"",
			item:"",
			packaging:0,
			units:0,
			quantity:0,
			priceRs:0.0,
			pricePs:0.0,
			AmountRs:0.0,
			AmountPs:0.0
		}
		$scope.rows.push(row);
	};
	$scope.initBill();
});



