<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HIGH CARE</title>
<link href="/sooryoll/layout.css" rel="stylesheet" type="text/css">
<script	src="/sooryoll/js/layout.js"></script>
<script	src="/sooryoll/js/jquery.min.js"></script>
<script src="/sooryoll/js/plugins/angular.min.js"></script>
<script src="/sooryoll/jsp/order/inventory.js"></script>
<link href="/sooryoll/form.css" rel="stylesheet" type="text/css">
</head>
<style>
	.menu_bar{
		width:100%;
		height:35px;
	}
	.menu_bar div{
		float:left;
		height:100%
	}
	
	.menu_bar{
		width:100%;
		height:35px;
		background:inherit;
		border: 2px solid gray;
		text-align: center;
	}
	.menu_bar div{
		width:12.5%;
		height:100%;
		color:#1f7ec7;
		float:left;
	}
	.menu_bar > .active{
		background:#1f7ec7;
		color:white;
	}
	
	.menu_bar > div >input{
		border: 0px solid white;
		background: inherit;
		1width: 100%;
		height: 100%;
	}
	.menu_bar > div:hover{
		background:#1f7ec7;
		color:white;
		border-right: 1px solid white;
		border-left: 1px solid white;
	}
	div{
		-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
		-moz-box-sizing: border-box;    /* Firefox, other Gecko */
		box-sizing: border-box;/* Opera/IE 8+ */
	}
</style>
<body>
<div class="top">
</div>
<div class="content" ng-app="inventory" ng-controller="mainController">
<div class="menu_bar">
<div class="active"><input type="button" ng-click="setMenu(1)" onClick="clickMenu(1)" value="Orders"/></div>
<div><input type="button" ng-click="setMenu(2)" value="Supply"/></div>
<div><input type="button" ng-click="setMenu(3)" value="Stock"/></div>
<div><input type="button" ng-click="setMenu(4)" value="Finance"/></div>
<div><input type="button" ng-click="setMenu(5)" value="Settings"/></div>
</div>
<div ng-include="page">
</div>
</div>
<div class="footer"></div>
</body>
<script>
	function clickMenu(i){
		var v=1;
		//alert(i);
		$(".menu_bar div").each(function(){
			if(v==i){
				$(this).addClass("active");
			}else{
				$(this).removeClass("active");
			}
			v++;
		});
	}
	function clickInnerMenu(i){
		clickMenu(5);
		var v=1;
		//alert(i);
		$(".menu_bar div").each(function(){
			if(v>5){
				if(v==i+5){
					$(this).addClass("active");
				}else{
					$(this).removeClass("active");
				}
			}
			v++;
		});
	}
</script>
<style>
	
	label {
	float:left;
	text-align:left;
	margin-right:40px;
	color:#0066CC;
	line-height:23px;	 
	}
	.contact_info{
		width:90%;
		-webkit-border-radius: 15px;
		-moz-border-radius: 15px;
		border-radius: 15px;
		background-color:#E2F3FD;
		-webkit-box-shadow: #B3B3B3 10px 10px 10px;
		-moz-box-shadow: #B3B3B3 10px 10px 10px;
		 box-shadow: #B3B3B3 10px 10px 10px;
		font-size:small;
		color:#334C4C;
		font-weight: 900;
		 
	}
	.key_info{
		width:90%;
		-webkit-border-radius: 15px;
		-moz-border-radius: 15px;
		border-radius: 15px;
		background-color:#E2F3FD;
		margin-left:3%;
		padding:2%;
		-webkit-box-shadow: #B3B3B3 8px 8px 8px;
		-moz-box-shadow: #B3B3B3 8px 8px 8px;
		 box-shadow: #B3B3B3 8px 8px 8px;
		 font-family: "Verdana";
		font-size:small;
		color:#334C4C;
		font-weight: 900;
	}
	.det_info{
		
		width:90%;
		-webkit-border-radius: 15px;
		-moz-border-radius: 15px;
		border-radius: 15px;
		background-color:#E2F3FD;
		margin-left:3%;
		 
	}
	.det_info table {
		font-family: "Verdana";
		font-size:small;
		color:#334C4C;
		font-weight: 900;
		
	}

	.info{
	background-color: #ffffff;
	-moz-box-shadow: inset 0px 0px 14px 3px #75b6ff;
	-webkit-box-shadow: inset 0px 0px 14px 3px #75b6ff;
	box-shadow: inset 0px 0px 14px 3px #75b6ff;
	}
	.heading{
		width:80%;
		margin-left:10%;
		height:30px;
		-webkit-border-radius: 15px 15px 0px 0px;
		-moz-border-radius: 15px 15px 0px 0px;
		border-radius: 15px 15px 0px 0px;
		color:navy;
		font:menu bold;
		border-bottom:2px solid #75b6ff;
		padding:0px;
		margin-bottom:15px;
	}
	
	
.style_table{
	width:100%;
	border-collapse:collapse;
	table-layout:auto;
	vertical-align:top;
	margin-bottom:15px;
	border:1px solid #CCCCCC;
	}

.style_table th{
	color:#FFFFFF;
	background: #5985EC;
	border:1px solid #CCCCCC;
	border-collapse:collapse;
	text-align:center;
	table-layout:auto;
	vertical-align:middle;
	}

.style_table tbody td{
	vertical-align:top;
	border-collapse:collapse;
	border-left:1px solid #CCCCCC;
	border-right:1px solid #CCCCCC;
	}
	
.style_table thead th, .style_table tbody td{
	padding:5px;
	border-collapse:collapse;
	}

.style_table tbody tr{
	color:#666666;
	background-color:#f2f5fd;
	}
	
.style_table tbody tr.light{
	color:#666666;
	background-color:#f2f5fd;
	}

.style_table tbody tr.dark{
	color:#666666;
	background-color:#E2F3FD;
	}
</style>

</html>