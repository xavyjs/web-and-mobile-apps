<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Customer</title>
<script src="/sooryoll/jquery.min.js"></script>
<link href="/sooryoll/layout.css" rel="stylesheet" type="text/css">
<link href="/sooryoll/form.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="header">

<ul style="list-style: none; padding-top: 15px; padding-bottom: 15px;">
	<li style="padding-bottom: 0; font-size: 14px;">HIGH CARE PRODUCTS</li>
	<li style="padding-up: 0; font-size: 26px;">SOORYOLL LUBRICANTS</li>
	<li style="padding-up: 0; font-size: 12px">High Performance
	Lubricants for Motors</li>
</ul>

</div>
<div id='cssmenu'>
<ul>

	<li class='active'><a href='/Form1c'><span>Home</span></a></li>
	<li class='has-sub'><a href='#'><span>Office</span></a></li>
	<li class='has-sub'><a href='#'><span>Citizen</span></a></li>
	<li class='last'><a href='#'><span>Site Map</span></a></li>
</ul>

<div style="float: right; width: 26%" id="welcome_note">
<div class="welome_note" style="float: left; padding-top: 2%"
	id="welcome_note_id"></div>
<div id="log_out_id" style="float: right;"><a
	href="logout.action">logout</a></div>
</div>
</div>
<div class="content">

<table width="100%" border="2">
<tr style="width:100%">
<td style="width:50%">
<b style="height:10%">Customer Code No.</b><br>
<textarea style="width:99%;height:85%"></textarea>
</td>
<td style="width:50%">
	<table border="2" style="width:100%">
		<tr style="width:100%">
			<td style="width:25%">D.M.No.:</td>
			<td style="width:25%"><input type="text"></td>
			<td style="width:25%">Date:</td>
			<td style="width:25%"><input type="text"></td>
		</tr>
		<tr style="width:100%">
			<td colspan="2" style="width:25%">Transporter</td>
			<td colspan="2" style="width:25%"><input type="text"></td>
		</tr>
		<tr style="width:100%">
			<td style="width:25%">Batch No.</td>
			<td style="width:25%"><input type="text"></td>
			<td style="width:25%">Date</td>
			<td style="width:25%"><input type="text"></td>
		</tr>
		<tr style="width:100%">
			<td style="width:25%">Lab Report No.</td>
			<td style="width:25%"><input type="text"></td>
			<td style="width:25%">Date</td>
			<td style="width:25%"><input type="text"></td>
		</tr>
	</table>
</td>
</tr>
</table>
<table border="2" style="width:100%;" id="billTable">
<tr>
	<th>S/No</th>
	<th>ITEM CODE/decription</th>
	<th>Packing</th>
	<th>units</th>
	<th>Quantity</th>
	<th colspan="2">Rate Per unit<br>Rs.</th>
	<th colspan="2">Amount<br>Rs.</th>
	
</tr>
</table>
<div align="right"><input type="button" id="addItem" value="Add" onClick="addItem();"></div><br>
<table width="100%" border="2">
<tr style="width:100%">
<td style="width:50%">
<b style="height:10%">Amount in words</b><div id="amountWords"></div><br>
</td>
<td style="width:50%">
	<table border="2" style="width:100%">
		<tr style="width:100%">
			<td style="width:25%">Net sales value:</td>
			<td style="width:25%"><div id="sales_value"></div></td>	
		</tr>
		<tr style="width:100%">
			<td style="width:25%">Vat</td>
			<td style="width:25%"><input type="text"></td>	
		</tr>
		<tr style="width:100%">
			<td style="width:25%">Cst</td>
			<td style="width:25%"><input type="text"></td>
		</tr>
		<tr style="width:100%">
			<td style="width:25%">Total</td>
			<td style="width:25%"></td>
		</tr>
	</table>
</td>
</tr>
</table>
</div>
<div class="footer"></div>
</body>
<script type="text/javascript">



function addItem() {

	var table = document.getElementById('billTable');
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	var cell1 = row.insertCell(0);
	var element1 = document.createElement("input");
	element1.type = "text";
	element1.name = "slNo";
	element1.disabled=true;
	$count++;
	element1.value = $count;
	//.text("1");
	cell1.appendChild(element1);
	//cell1.text="
	var cell2 = row.insertCell(1);
	var element2 = document.createElement("input");
	element2.type = "text";
	element2.name = "item";
//	element2.id = "occupierName_id";
	cell2.appendChild(element2);
	var cell3 = row.insertCell(2);
	var element3 = document.createElement("input");
	element3.type = "text";
	element3.name = "packaging";
	cell3.appendChild(element3);
	var cell4 = row.insertCell(3);
	var element4 = document.createElement("input");
	element4.type = "text";
	element4.name = "units";
	element4.className="units_id";
	cell4.appendChild(element4);
	var cell5 = row.insertCell(4);
	var element5 = document.createElement("input");
	element5.type = "text";
	element5.name = "quantity";
	element5.className="quantity_id";
	cell5.appendChild(element5);
	var cell6 = row.insertCell(5);
	var element6 = document.createElement("input");
	element6.type = "text";
	element6.name = "priceRs";
	element6.className="priceRs_id";
	cell6.appendChild(element6);
	var cell7 = row.insertCell(6);
	var element7 = document.createElement("input");
	element7.type = "text";
	element7.name = "pricePs";
	element7.className="pricePs_id";
	cell7.appendChild(element7);
	var cell8 = row.insertCell(7);
	var element8 = document.createElement("input");
	element8.type = "text";
	element8.name = "AmountRs";
	element8.className="amountRs_id";
	cell8.appendChild(element8);
	var cell9 = row.insertCell(8);
	var element9 = document.createElement("input");
	element9.type = "text";
	element9.name = "AmountPs";
	element9.className="amountPs_id";
	cell9.appendChild(element9);

	//alert(words(100));
	$(".units_id").bind("change",unitUpdate);
	$(".priceRs_id").bind("change",priceRUpdate);
	$(".pricePs_id").bind("change",pricePUpdate);
}
var $count=0;
$(document).ready(function(){
	
	//alert(getWords(2354541));
});

function unitUpdate(){

	
	var unitsS=$(this).val();
	var priceR=$(this).parent().next().next().children("input").val();
	var priceP=$(this).parent().next().next().next().children("input").val();
	if(priceR+""==""){
		return;
	}
	var price=parseFloat(priceR+"."+priceP);
	var units=parseFloat(unitsS);
	var amount=price*units;
	alert((amount+"").indexOf("."));
	if((amount+"").indexOf(".")!=-1){
		alert(amount);
	var amountR=(amount+"").split("\.")[0];
	var amountP=(amount+"").split("\.")[1];
	alert(amount+" "+amountR+" "+amountP);
	amountP=amountP.substring(0,2);
	$(this).parent().next().next().next().next().children("input").val(amountR);
	$(this).parent().next().next().next().next().next().children("input").val(amountP);
	}
	else{
	$(this).parent().next().next().next().next().children("input").val(amount);
	$(this).parent().next().next().next().next().next().children("input").val("00");
	}
	var totalR=0;
	var totalP=0;
	$(".amountRs_id").each(function(){
		totalR+=Math.floor($(this).val());
	});
	$(".amountPs_id").each(function(){
		totalP+=Math.floor($(this).val());
	});
	totalP/=100;
	totalR+=(totalP);
	if(Math.floor((totalP+"").split("\.")[1])<10)
	$("#sales_value").html(totalR+""+0);
	else
	$("#sales_value").html(totalR);	

	var amwrd=getWords(Math.floor((totalR+"").split("\.")[0]));
	amwrd+=" Rupees";
	if(Math.floor((totalR+"").split("\.")[1])>0)
	amwrd+=(" and "+getWords(Math.floor((totalR+"").split("\.")[1])))+" Paise";	
	$("#amountWords").html(amwrd);
}
function priceRUpdate(){

	var unitsS=$(this).parent().prev().prev().children("input").val();
	var priceR=$(this).val();
	var priceP=$(this).parent().next().children("input").val();
}
function pricePUpdate(){

	var unitsS=$(this).parent().prev().prev().prev().children("input").val();
	var priceR=$(this).parent().prev().children("input").val();
	var priceP=$(this).val();
}
</script>
<script>

var ones = [ 'one', 'two', 'three', 'four', 'five', 'six', 'seven',	'eight', 'nine' ];
var tens = [ "ten", "twenty", "thirty", "forty", "fifty", "sixty",
		"seventy", "eighty", "ninty" ];
var ten = [ "eleven", "twelve", "thirteen", "forteen", "fifteen",
		"sixteen", "seventeen", "eighteen", "nineteen" ];

function getWords(amount) {
	//alert("ss");
	//alert(amount / 100000);
	var word = calc(Math.floor(amount / 100000));
	
	var words = "";
	if (word!="") {
		words += word + "Lakh ";
	}
	amount =Math.floor(amount% 100000);
	// System.out.println(amount);
	word = calc(Math.floor(amount / 1000));
	if (word!="") {
		words += word + "thousand ";
	}
	amount %= 1000;
	word="";
	if (Math.floor(amount / 100) != 0) {
		word = ones[Math.floor((amount / 100) - 1)] + " hundred ";
	}
	words += word;
	amount %= 100;
	word = calc(Math.floor(amount));
	if (word!="") {
		if(words=="")
			words += word ;
		else
			words += "and " + word ;
	} else {
		words=words.replace("thousand ", "thousand and ");
		words += "Rupees";
	}
	return words;
}

function calc(x) {
	var word = "";
	if (Math.floor(x / 10) == 0) {
		word += "";
		if (Math.floor(x % 10) != 0) {
			word += ones[Math.floor(x % 10 - 1)] + " ";
		}
		//alert(word);
	}
	

	else {
		if (Math.floor(x / 10) == 1) {

			if (Math.floor(x % 10) == 0) {
				word += "ten ";
			} else {
				word += ten[Math.floor(x % 10 - 1)] + " ";
			}
		} else {
			word += tens[Math.floor(x / 10 - 1)] + " ";

			if (x % 10 != 0) {
				word += ones[Math.floor(x % 10 - 1)] + " ";
			}
		}

	}
	return word;

}



</script>
<style>
	input {
		width:100%;
	}
</style>
</html>