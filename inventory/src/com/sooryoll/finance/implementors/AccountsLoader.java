package com.sooryoll.finance.implementors;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.sooryoll.finance.interfaces.AccountsLoadable;
import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Account;
import com.sooryoll.pojo.Customer;
import com.sooryoll.pojo.Supplier;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;

public class AccountsLoader extends FIMImplementor implements AccountsLoadable{

	private String id;
	private List<Account> accounts = new ArrayList<Account>();
	private boolean result = true;
	private int type;
	
	
	public void setType(int type) {
		this.type = type;
	}
	public List<Account> getAccounts() {
		return accounts;
	}
	public void setId(String customerId) {
		this.id = customerId;
	}
	public List<String> getMessages(){
		return messages;
	}
	
	public boolean isResult() {
		return result;
	}
	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			if(type==1){
				DataAccessLayer<Customer> customerAccessLayer = new DataAccessLayer<Customer>(Customer.class,hbSession);
				Customer customer = customerAccessLayer.retrive(id);
				accounts = customer.getAccounts();
			}else{
				DataAccessLayer<Supplier> supplierAccessLayer = new DataAccessLayer<Supplier>(Supplier.class,hbSession);
				Supplier supplier= supplierAccessLayer.retrive(id);
				accounts = supplier.getAccounts();
			}
			if(accounts == null){
				result = false;
				messages.add(DEFAULT_ERROR);
			
			}
		}catch (Exception e) {
			// TODO: handle exception
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		
		return super.execute();
	}

	
}
