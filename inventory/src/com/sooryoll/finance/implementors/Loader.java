package com.sooryoll.finance.implementors;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.sooryoll.finance.interfaces.Loadable;
import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Balance;
import com.sooryoll.pojo.Customer;
import com.sooryoll.pojo.Due;
import com.sooryoll.pojo.Supplier;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;

public class Loader extends FIMImplementor implements Loadable{

	private List<Balance> balances = new ArrayList<Balance>();
	private List<Due> dues = new ArrayList<Due>();
	private List<Customer> customers = new ArrayList<Customer>();
	private List<Supplier> suppliers = new ArrayList<Supplier>();

	
	public List<Balance> getBalances() {
		return balances;
	}
	public List<Due> getDues() {
		return dues;
	}
	
	
	public List<Customer> getCustomers() {
		return customers;
	}
	public List<Supplier> getSuppliers() {
		return suppliers;
	}
	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Balance> balanceAccessLayer = new DataAccessLayer<Balance>(Balance.class,hbSession);
			DataAccessLayer<Due> dueAccessLayer = new DataAccessLayer<Due>(Due.class,hbSession);
			DataAccessLayer<Customer> customerAccessLayer = new DataAccessLayer<Customer>(Customer.class,hbSession);
			DataAccessLayer<Supplier> supplierAccessLayer = new DataAccessLayer<Supplier>(Supplier.class,hbSession);
			//customers = customerAccessLayer.list();
			//suppliers = supplierAccessLayer.list();
			balances =balanceAccessLayer.list();
			dues = dueAccessLayer.list();
		}catch (Exception e) {
			
		}finally{
			if(hbSession.isOpen()){
				hbSession.close(); 
			}
		}
		return true;
	}
}

