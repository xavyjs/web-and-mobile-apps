package com.sooryoll.finance.implementors;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;

import com.sooryoll.finance.interfaces.CollectionAddable;
import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Balance;
import com.sooryoll.pojo.Collection;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;

public class CollectionAdder extends FIMImplementor implements CollectionAddable{

	private Collection collection;
	private boolean result;
	
	public Collection getCollection() {
		return collection;
	}
	public void setCollection(Collection collection) {
		this.collection = collection;
	}
	public boolean isResult() {
		return result;
	}
	public List<String> getMessages(){
		return messages;
	}
    
	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		result = true;
		try{
			hbSession.beginTransaction();
			Balance balance = (Balance) hbSession.load(Balance.class, collection.getCustomerId());
			try{
				if(balance.getAmount()<collection.getAmount()){
					throw new ObjectNotFoundException(hbSession, null);
				}
			}catch (ObjectNotFoundException e) {
				result = false;
				messages.add("collections amount greater than customer's due");
			}
			if(result){
				balance.setAmount(balance.getAmount()-collection.getAmount());
				hbSession.update(balance);
				collection.setDate(new Date());
				hbSession.save(collection);
				hbSession.getTransaction().commit();
			}
		}catch (HibernateException e) {
			hbSession.getTransaction().rollback();
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		collection = null;
		return super.execute();
	}
}
