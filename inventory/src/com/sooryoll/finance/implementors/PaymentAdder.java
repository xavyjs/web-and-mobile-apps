package com.sooryoll.finance.implementors;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;

import com.sooryoll.finance.interfaces.PaymentAddable;
import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Due;
import com.sooryoll.pojo.Payment;
import com.sooryoll.struts.FIMImplementor;

public class PaymentAdder extends FIMImplementor implements PaymentAddable{

	private Payment payment;
	private boolean result;
	
	
	public boolean isResult() {
		return result;
	}
	public List<String> getMessages(){
		return messages;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	public Payment getPayment() {
		return payment;
	}
	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		result = true;
		try{
			hbSession.beginTransaction();
			Due due = (Due) hbSession.load(Due.class, payment.getSupplierId());
			try{
				if(due.getAmount()<payment.getAmount()){
					throw new ObjectNotFoundException(hbSession, null);
				}
			}catch (ObjectNotFoundException e) {
				result = false;
				messages.add("collections amount greater than customer's due");
			}
			if(result){
				due.setAmount(due.getAmount()-payment.getAmount());
				hbSession.update(due);
				payment.setDate(new Date());
				hbSession.save(payment);
				hbSession.getTransaction().commit();
			}
		}catch (HibernateException e) {
			hbSession.getTransaction().rollback();
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		payment = null;
		return super.execute();
	}
	
}
