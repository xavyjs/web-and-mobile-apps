package com.sooryoll.finance.actions;

import com.sooryoll.finance.implementors.AccountsLoader;
import com.sooryoll.finance.interfaces.AccountsLoadable;
import com.sooryoll.struts.FIMAction;

public class LoadAccounts extends FIMAction<AccountsLoadable>{

	@Override
	protected AccountsLoadable getLogic() {
		return new AccountsLoader();
	}

}
