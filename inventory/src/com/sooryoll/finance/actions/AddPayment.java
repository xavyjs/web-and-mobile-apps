package com.sooryoll.finance.actions;

import com.sooryoll.finance.implementors.PaymentAdder;
import com.sooryoll.finance.interfaces.PaymentAddable;
import com.sooryoll.struts.FIMAction;

public class AddPayment extends FIMAction<PaymentAddable>{

	@Override
	protected PaymentAddable getLogic() {
		return new PaymentAdder();
	}

}
