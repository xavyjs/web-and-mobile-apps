package com.sooryoll.finance.actions;

import com.sooryoll.finance.implementors.Loader;
import com.sooryoll.finance.interfaces.Loadable;
import com.sooryoll.struts.FIMAction;

public class Load extends FIMAction<Loadable>{

	@Override
	protected Loadable getLogic() {
		return new Loader();
	}

}
