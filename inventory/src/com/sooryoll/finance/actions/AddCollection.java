package com.sooryoll.finance.actions;

import com.sooryoll.finance.implementors.CollectionAdder;
import com.sooryoll.finance.interfaces.CollectionAddable;
import com.sooryoll.struts.FIMAction;

public class AddCollection extends FIMAction<CollectionAddable>{

	@Override
	protected CollectionAddable getLogic() {
		return new CollectionAdder();
	}

}
