package com.sooryoll.finance.interfaces;

import java.util.List;

import com.sooryoll.pojo.Balance;
import com.sooryoll.pojo.Customer;
import com.sooryoll.pojo.Due;
import com.sooryoll.pojo.Supplier;
import com.sooryoll.struts.FIMInterface;

public interface Loadable extends FIMInterface{

	public List<Balance> getBalances();
	public List<Due> getDues();
	public List<Customer> getCustomers();
	public List<Supplier> getSuppliers(); 
}
