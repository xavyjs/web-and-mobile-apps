package com.sooryoll.finance.interfaces;

import java.util.List;

import com.sooryoll.pojo.Payment;
import com.sooryoll.struts.FIMInterface;

public interface PaymentAddable extends FIMInterface{

	public boolean isResult();
	public List<String> getMessages();
	public void setPayment(Payment payment);
	public Payment getPayment();
}
