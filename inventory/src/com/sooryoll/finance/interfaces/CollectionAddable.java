package com.sooryoll.finance.interfaces;

import com.sooryoll.pojo.Collection;
import com.sooryoll.struts.FIMInterface;

public interface CollectionAddable extends FIMInterface{

	public Collection getCollection();
	public void setCollection(Collection collection);
	public boolean isResult();
}
