package com.sooryoll.finance.interfaces;

import java.util.List;

import com.sooryoll.pojo.Account;
import com.sooryoll.struts.FIMInterface;

public interface AccountsLoadable extends FIMInterface {

	public List<Account> getAccounts();
	public void setId(String customerId);
	public List<String> getMessages();
	public boolean isResult();
	public void setType(int type);
}
