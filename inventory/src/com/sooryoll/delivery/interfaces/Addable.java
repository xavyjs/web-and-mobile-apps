package com.sooryoll.delivery.interfaces;

import java.util.List;

import com.sooryoll.orders.implementors.OrderRequest;
import com.sooryoll.pojo.Order;
import com.sooryoll.struts.FIMInterface;

public interface Addable extends FIMInterface{

	
	public List<Object[]> getProductTotal();
	public List<Object[]> getCustomerWiseProduct();
	public List<Order> getOrders();
	public List<String> getMessages();
	
}
