package com.sooryoll.delivery.implementors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.delivery.interfaces.Addable;
import com.sooryoll.pojo.Balance;
import com.sooryoll.pojo.Delivery;
import com.sooryoll.pojo.Inventory;
import com.sooryoll.pojo.Order;
import com.sooryoll.pojo.Product;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.pojo.dataaccess.OrdersDataAccess;
import com.sooryoll.struts.FIMImplementor;

public class Adder extends FIMImplementor implements Addable{

	
	private Delivery delivery;
	private String message;
	private boolean result = true;
	private List<Order> orders = new ArrayList<Order>();
	private List<Object[]> productTotal;
	private List<Object[]> customerWiseProduct;
	
	
	
	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	public String getMessage() {
		return message;
	}
	public List<String> getMessages(){
		return messages;
	}
	public List<Object[]> getProductTotal() {
		return productTotal;
	}

	public List<Object[]> getCustomerWiseProduct() {
		return customerWiseProduct;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public boolean isResult() {
		return result;
	}

	
	public boolean execute(){
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		DataAccessLayer<Order> orderAccessLayer = new DataAccessLayer<Order>(Order.class,hbSession);
		DataAccessLayer<Balance> balanceAccessLayer = new DataAccessLayer<Balance>(Balance.class,hbSession);
		DataAccessLayer<Product> productAccessLayer = new DataAccessLayer<Product>(Product.class,hbSession);
		DataAccessLayer<Inventory> inventoryAccessLayer = new DataAccessLayer<Inventory>(Inventory.class,hbSession);
		
		Order order = orderAccessLayer.retrive(delivery.getOrderId());
		Product product = productAccessLayer.retrive(order.getProductId());
		Inventory inventory = inventoryAccessLayer.retrive(product.getProductId());
		try{
			if(inventory.getQuantity()<delivery.getQuantity())
				throw new HibernateException("");
			else
				inventory.setQuantity(inventory.getQuantity()-delivery.getQuantity());
		}catch (HibernateException e) {
			messages.add("Sorry delivery cannot be made!!!\nRequired quantity not available in the inventory");
			result = false;
			return true;
		}
		Balance balance = balanceAccessLayer.retrive(order.getCustomer());
		if(order.getQuantity()<delivery.getQuantity()){
			message = "quantity larger than the order";
			result = false;
			return true;
		}else if(delivery.getQuantity()<=0){
			message = "Invalid Quantity";
			result = false;
			return true;
		}
		if(delivery.getDate()==null){
			delivery.setDate(new Date());
		}
		try{
			order.setQuantity(order.getQuantity()-delivery.getQuantity());
			if(order.getQuantity()==0){
				order.setStatus(2);
			}else{
				order.setStatus(1);
			}
			try{
				System.out.println(balance);
			}catch(HibernateException e){
				balance = new Balance();
				balance.setCustomerId(order.getCustomer());
				balance.setAmount(0);
				balanceAccessLayer.add(balance);
			}
			balance.setAmount(balance.getAmount()+(delivery.getQuantity()*product.getPricePerLit())+delivery.getPackagingCharge());
			try{
				hbSession.beginTransaction();
				hbSession.update(order);
				hbSession.save(delivery);
				hbSession.update(balance);
				hbSession.update(inventory);
				hbSession.getTransaction().commit();
				result=true;
			}catch (Exception e) {
				e.printStackTrace();
				hbSession.getTransaction().rollback();
				message="Some thing went wrong!!!";
				result = false;
			}
			if(!result)
				throw new HibernateException("");
			OrdersDataAccess oDa = new OrdersDataAccess();
			orders = oDa.getPendingOrders();
			productTotal = oDa.getProductTotal();
			customerWiseProduct = oDa.getCustomerWiseProduct();
			
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		return true;
	}

	
}
