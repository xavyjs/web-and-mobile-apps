package com.sooryoll.delivery.actions;

import com.sooryoll.delivery.implementors.Adder;
import com.sooryoll.delivery.interfaces.Addable;
import com.sooryoll.struts.FIMAction;

public class Add extends FIMAction<Addable>{

	@Override
	protected Addable getLogic() {
		return new Adder();
	}

}
