package com.sooryoll.supplier.implementors;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Supplier;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;
import com.sooryoll.supplier.interfaces.Loadable;

public class Loader extends FIMImplementor implements Loadable{

	private List<Supplier> suppliers = new ArrayList<Supplier>();

	public List<Supplier> getSuppliers() {
		return suppliers;
	}
	
	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Supplier> supplierDataAccessLayer = new DataAccessLayer<Supplier>(Supplier.class,hbSession);
			suppliers = supplierDataAccessLayer.list();
		}catch (Exception e) {
			suppliers = null;
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		return true;
	}
}
