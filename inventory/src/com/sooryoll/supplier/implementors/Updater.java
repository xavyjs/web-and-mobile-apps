package com.sooryoll.supplier.implementors;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Product;
import com.sooryoll.pojo.Supplier;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;
import com.sooryoll.struts.FIMInterface;
import com.sooryoll.supplier.interfaces.Updatable;

public class Updater extends FIMImplementor implements Updatable{

	private Supplier supplier;
	private boolean result;


	
	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public boolean isResult() {
		return result;
	}

	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Supplier> sDa = new DataAccessLayer<Supplier>(Supplier.class,hbSession);
			sDa.update(supplier);
			result = true;
		}catch (Exception e) {
			result=false;
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		supplier = null;
		return true;
	}
}
