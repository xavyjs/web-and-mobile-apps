package com.sooryoll.supplier.interfaces;

import com.sooryoll.pojo.Supplier;
import com.sooryoll.struts.FIMInterface;

public interface Updatable extends FIMInterface{

	public Supplier getSupplier();
	public void setSupplier(Supplier supplier);
	public boolean isResult();
}
