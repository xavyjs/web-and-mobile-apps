package com.sooryoll.supplier.interfaces;

import com.sooryoll.pojo.Supplier;
import com.sooryoll.struts.FIMInterface;

public interface Addable extends FIMInterface{

	public Supplier getSupplier();
	public void setSupplier(Supplier supplier);
	public boolean isResult();
}
