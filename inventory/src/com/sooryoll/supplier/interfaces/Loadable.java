package com.sooryoll.supplier.interfaces;

import java.util.List;

import com.sooryoll.pojo.Supplier;
import com.sooryoll.struts.FIMInterface;

public interface Loadable extends FIMInterface{

	public List<Supplier> getSuppliers();
}
