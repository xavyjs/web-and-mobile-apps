package com.sooryoll.supplier.actions;

import com.sooryoll.struts.FIMAction;
import com.sooryoll.supplier.implementors.Updater;
import com.sooryoll.supplier.interfaces.Updatable;

public class Update extends FIMAction<Updatable>{

	@Override
	protected Updatable getLogic() {
		return new Updater();
	}

}
