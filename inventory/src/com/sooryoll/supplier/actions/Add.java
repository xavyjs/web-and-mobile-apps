package com.sooryoll.supplier.actions;

import com.sooryoll.struts.FIMAction;
import com.sooryoll.supplier.implementors.Adder;
import com.sooryoll.supplier.implementors.Loader;
import com.sooryoll.supplier.interfaces.Addable;
import com.sooryoll.supplier.interfaces.Loadable;

public class Add extends FIMAction<Addable>{

	@Override
	protected Addable getLogic() {
		return new Adder();
	}

}
