package com.sooryoll.supplier.actions;

import com.sooryoll.struts.FIMAction;
import com.sooryoll.supplier.implementors.Loader;
import com.sooryoll.supplier.interfaces.Loadable;

public class Load extends FIMAction<Loadable>{

	@Override
	protected Loadable getLogic() {
		return new Loader();
	}

}
