package com.sooryoll.stock.implementors;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Inventory;
import com.sooryoll.pojo.Product;
import com.sooryoll.pojo.Stock;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.stock.interfaces.Loadable;
import com.sooryoll.struts.FIMImplementor;

public class Loader extends FIMImplementor implements Loadable{

	private List<Stock> stocks = new ArrayList<Stock>();
	private List<Inventory> inventory = new ArrayList<Inventory>();
	private List<Product> products = new ArrayList<Product>();

	
	public List<Inventory> getInventory() {
		return inventory;
	}
	public List<Stock> getStocks() {
		return stocks;
	}
	
	public List<Product> getProducts() {
		return products;
	}
	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		DataAccessLayer<Stock> stockAccessLayer = new DataAccessLayer<Stock>(Stock.class,hbSession);
		DataAccessLayer<Inventory> inventoryAccessLayer = new DataAccessLayer<Inventory>(Inventory.class,hbSession);
		DataAccessLayer<Product> productAccessLayer = new DataAccessLayer<Product>(Product.class,hbSession);
		stocks = stockAccessLayer.list();
		inventory = inventoryAccessLayer.list();
		products = productAccessLayer.list();
		hbSession.close();
		return super.execute();
		
	}
	
}
