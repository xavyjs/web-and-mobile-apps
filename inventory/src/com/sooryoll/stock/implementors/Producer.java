package com.sooryoll.stock.implementors;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Inventory;
import com.sooryoll.pojo.Stock;
import com.sooryoll.stock.interfaces.Producable;
import com.sooryoll.struts.FIMImplementor;

public class Producer extends FIMImplementor implements Producable{

	private Request request;
	private boolean result;

	public List<String> getMessages(){
		return messages;
	}
	public boolean isResult() {
		return result;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	public boolean execute(){
		Session session = FactoryGenerator.sessionFactory.openSession();
		result = true;
		try{
			session.beginTransaction();
			for(int i=0;i<request.getMaterials().size();i++){
				Stock materialStock = (Stock) session.load(Stock.class, request.getMaterials().get(i).getMaterial());
				materialStock.setQuantity(materialStock.getQuantity()-request.getMaterials().get(i).getQuantity());
				if(materialStock.getQuantity()<0)
					throw new HibernateException("Required Quantity of material not available in stock");
				session.update(materialStock);
				
			}
			Inventory inventory = (Inventory) session.load(Inventory.class, request.getProduct());
			try{
				inventory.setQuantity(inventory.getQuantity()+request.getQuantity());
				session.update(inventory);
			}catch (ObjectNotFoundException e) {
				inventory = new Inventory();
				inventory.setProductId(request.getProduct());
				inventory.setQuantity(request.getQuantity());
				session.save(inventory);
			}
			session.getTransaction().commit();
		}catch (Exception e) {
			session.getTransaction().rollback();
			messages.add(DEFAULT_ERROR);
			result = false;
			e.printStackTrace();
		}finally{
			if(session.isOpen())
				session.close();
		}
		
		return true;
	}
	
}
