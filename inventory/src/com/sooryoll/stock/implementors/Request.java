package com.sooryoll.stock.implementors;

import java.util.ArrayList;

public class Request {

	private ArrayList<Material> materials = new ArrayList<Material>();
	private String product;
	private int quantity;
	
	public ArrayList<Material> getMaterials() {
		return materials;
	}
	public void setMaterials(ArrayList<Material> materials) {
		this.materials = materials;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}

