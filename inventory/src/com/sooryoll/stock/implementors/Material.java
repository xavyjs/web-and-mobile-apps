package com.sooryoll.stock.implementors;

public class Material {
	private String material;
	private int quantity;
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String materialId) {
		this.material = materialId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String toString(){
		return material+":"+quantity; 
	}
}
