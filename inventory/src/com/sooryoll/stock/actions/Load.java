package com.sooryoll.stock.actions;

import com.sooryoll.stock.implementors.Loader;
import com.sooryoll.stock.interfaces.Loadable;
import com.sooryoll.struts.FIMAction;

public class Load extends FIMAction<Loadable>{

	@Override
	protected Loadable getLogic() {
		return new Loader();
	}

}
