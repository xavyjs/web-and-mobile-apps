package com.sooryoll.stock.actions;

import com.sooryoll.stock.implementors.Producer;
import com.sooryoll.stock.interfaces.Producable;
import com.sooryoll.struts.FIMAction;

public class Produce extends FIMAction<Producable>{

	@Override
	protected Producable getLogic() {
		return new Producer();
	}

}
