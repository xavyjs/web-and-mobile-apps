package com.sooryoll.stock.interfaces;

import java.util.List;

import com.sooryoll.stock.implementors.Request;
import com.sooryoll.struts.FIMInterface;

public interface Producable extends FIMInterface{

	public List<String> getMessages();
	public boolean isResult();
	public void setRequest(Request request);
}
