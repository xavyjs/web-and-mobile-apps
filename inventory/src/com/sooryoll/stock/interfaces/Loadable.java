package com.sooryoll.stock.interfaces;

import java.util.List;

import com.sooryoll.pojo.Inventory;
import com.sooryoll.pojo.Product;
import com.sooryoll.pojo.Stock;
import com.sooryoll.struts.FIMInterface;

public interface Loadable extends FIMInterface{

	public List<Inventory> getInventory();
	public List<Stock> getStocks();
	public List<Product> getProducts();
}
