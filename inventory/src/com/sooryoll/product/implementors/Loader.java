package com.sooryoll.product.implementors;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Product;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.product.interfaces.Loadable;
import com.sooryoll.struts.FIMImplementor;

public class Loader extends FIMImplementor implements Loadable {

	private List<Product> products = new ArrayList<Product>();
	
	
	public List<Product> getProducts() {
		return products;
	}


	@Override
	public boolean execute() {
		Session hbSession=FactoryGenerator.sessionFactory.openSession();
		DataAccessLayer<Product> pDa = new DataAccessLayer<Product>(Product.class,hbSession);
		products = pDa.list();
		hbSession.close();
		return true;
	}
}
