package com.sooryoll.product.implementors;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Product;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.product.interfaces.Updatable;
import com.sooryoll.struts.FIMImplementor;

public class Updater extends FIMImplementor implements Updatable{

	private Product product;
	private boolean result;

	public void setProduct(Product product) {
		this.product = product;
	}

	public Product getProduct() {
		return product;
	}
	
	public boolean isResult() {
		return result;
	}

	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Product> pDa = new DataAccessLayer<Product>(Product.class,hbSession);
			pDa.update(product);
			result = true;
		}catch (Exception e) {
			result=false;
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		return true;
	}
}
