package com.sooryoll.product.actions;

import com.sooryoll.product.implementors.Updater;
import com.sooryoll.product.interfaces.Updatable;
import com.sooryoll.struts.FIMAction;

public class Update extends FIMAction<Updatable>{

	@Override
	protected Updatable getLogic() {
		return new Updater();
	}

}
