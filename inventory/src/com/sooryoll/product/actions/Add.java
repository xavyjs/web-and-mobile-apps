package com.sooryoll.product.actions;

import com.sooryoll.product.implementors.Adder;
import com.sooryoll.product.interfaces.Addable;
import com.sooryoll.struts.FIMAction;

public class Add extends FIMAction<Addable>{

	@Override
	protected Addable getLogic() {
		return new Adder();
	}

}
