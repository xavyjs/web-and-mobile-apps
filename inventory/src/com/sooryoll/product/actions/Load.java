package com.sooryoll.product.actions;

import com.sooryoll.product.implementors.Loader;
import com.sooryoll.product.interfaces.Loadable;
import com.sooryoll.struts.FIMAction;

public class Load extends FIMAction<Loadable>{

	@Override
	protected Loadable getLogic() {
		return new Loader();
	}

}
