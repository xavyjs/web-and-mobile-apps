package com.sooryoll.product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;


import com.opensymphony.xwork2.ActionSupport;
import com.sooryoll.pojo.*;
import com.sooryoll.pojo.dataaccess.*;

public class Action extends ActionSupport implements SessionAware{

	private Map<String,Object> session=new HashMap<String,Object>();
	private Product product;
	private List<Product> products;
	public static final String FAILURE="failure";
	private String productId;
	public void setSession(Map<String, Object> arg0) {
		session=arg0;
		
	}
	public String execute()
	{
		return SUCCESS;
	}
	public String save(){
		if(new ProductDataAccess().add(product)){
			products = new ProductDataAccess().list();
			return SUCCESS;
		}
		else
			return FAILURE;
	}
	public String view(){
		products=new ProductDataAccess().list();
		return SUCCESS;
	}
	public String manage(){
		
		return SUCCESS;
	}

	public String remove(){
		
		if(new ProductDataAccess().removeProduct(new ProductDataAccess().getProduct(productId))){
			products=new ProductDataAccess().list();
			return SUCCESS;
		}
		else{
			products=new ProductDataAccess().list();
			return FAILURE;
		}
	}
	
	public String edit(){
		System.out.println(productId);
		product= new ProductDataAccess().getProduct(productId);
		if(product==null)
			return FAILURE;
		return SUCCESS;
	}
	
public String update(){
		
		if(new ProductDataAccess().updateProduct(product))
		return SUCCESS;
		else
			products=new ProductDataAccess().list();
			return FAILURE;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	public Product getProduct() {
	
		return product;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductId() {
		return productId;
	}

}
