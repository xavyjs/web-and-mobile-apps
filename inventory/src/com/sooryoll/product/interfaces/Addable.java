package com.sooryoll.product.interfaces;

import com.sooryoll.pojo.Product;
import com.sooryoll.struts.FIMInterface;

public interface Addable extends FIMInterface {

	public void setProduct(Product product);
	public Product getProduct();
	public boolean isResult();
}
