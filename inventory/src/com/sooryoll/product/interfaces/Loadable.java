package com.sooryoll.product.interfaces;

import java.util.List;

import com.sooryoll.pojo.Product;
import com.sooryoll.struts.FIMInterface;

public interface Loadable extends FIMInterface{
	public List<Product> getProducts();

}
