package com.sooryoll.customer.interfaces;

import java.util.List;

import com.sooryoll.pojo.Customer;
import com.sooryoll.struts.FIMInterface;

public interface Loadable extends FIMInterface{

	public List<Customer> getCustomers();
}
