package com.sooryoll.customer.interfaces;

import com.sooryoll.pojo.Customer;
import com.sooryoll.struts.FIMInterface;

public interface Addable extends FIMInterface{

	public Customer getCustomer();
	public void setCustomer(Customer supplier);
	public boolean isResult();
}
