package com.sooryoll.customer.actions;

import com.sooryoll.struts.FIMAction;
import com.sooryoll.customer.implementors.Loader;
import com.sooryoll.customer.interfaces.Loadable;

public class Load extends FIMAction<Loadable>{

	@Override
	protected Loadable getLogic() {
		return new Loader();
	}

}
