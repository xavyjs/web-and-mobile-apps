package com.sooryoll.customer.actions;

import com.sooryoll.struts.FIMAction;
import com.sooryoll.customer.implementors.Adder;
import com.sooryoll.customer.interfaces.Addable;

public class Add extends FIMAction<Addable>{

	@Override
	protected Addable getLogic() {
		return new Adder();
	}

}
