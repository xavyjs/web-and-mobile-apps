package com.sooryoll.customer.actions;

import com.sooryoll.struts.FIMAction;
import com.sooryoll.customer.implementors.Updater;
import com.sooryoll.customer.interfaces.Updatable;

public class Update extends FIMAction<Updatable>{

	@Override
	protected Updatable getLogic() {
		return new Updater();
	}

}
