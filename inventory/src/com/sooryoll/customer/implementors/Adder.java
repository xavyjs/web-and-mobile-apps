package com.sooryoll.customer.implementors;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Customer;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;
import com.sooryoll.customer.interfaces.Addable;

public class Adder extends FIMImplementor implements Addable{

	private Customer customer;
	private boolean result;


	
	
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public boolean isResult() {
		return result;
	}

	@Override
	public boolean execute() {
		System.out.println("here");
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Customer> sDa = new DataAccessLayer<Customer>(Customer.class,hbSession);
			sDa.add(customer);
			result = true;
		}catch (Exception e) {
			result=false;
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		customer = null;
		return true;
	}
}
