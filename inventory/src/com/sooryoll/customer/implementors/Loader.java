package com.sooryoll.customer.implementors;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Customer;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;
import com.sooryoll.customer.interfaces.Loadable;

public class Loader extends FIMImplementor implements Loadable{

	private List<Customer> customers = new ArrayList<Customer>();

	public List<Customer> getCustomers() {
		return customers;
	}
	
	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Customer> customerDataAccessLayer = new DataAccessLayer<Customer>(Customer.class,hbSession);
			customers = customerDataAccessLayer.list();
		}catch (Exception e) {
			customers = null;
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		return true;
	}
}
