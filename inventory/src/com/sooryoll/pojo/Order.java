package com.sooryoll.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sooryoll.utils.Utilities;

@Entity
@Table(name="orders")
public class Order implements Comparable<Order>{

	@Id
	@GeneratedValue
	@Column(name="order_id")
	private long orderId;
	@Column(name="product_id")
	private String productId;
	@Column(name="customer")
	private String customer;
	private long quantity;
	private Date date;
	@Column(name="fulfilled_date")
	private Date fulfilledDate;
	private int status;
	private String description;
	
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=RefOrderStatus.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "status", referencedColumnName="status_code" ,insertable = false, updatable = false)
	private RefOrderStatus refOrderStatus;
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=Product.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "product_id", referencedColumnName="product_id" ,insertable = false, updatable = false)
	private Product product;
	
	
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getFulfilledDate() {
		return fulfilledDate;
	}
	public void setFulfilledDate(Date fulfilledDate) {
		this.fulfilledDate = fulfilledDate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDateString(){
		return Utilities.dateToString(date);
	}
	public void setRefOrderStatus(RefOrderStatus refOrderStatus) {
		this.refOrderStatus = refOrderStatus;
	}
	public RefOrderStatus getRefOrderStatus() {
		return refOrderStatus;
	}
	public int compareTo(Order o) {
		return this.getCustomer().compareTo(o.getCustomer());
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Product getProduct() {
		return product;
	}
	
	//@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL,targetEntity=Customer.class)
	//@JoinColumn(name="order_id",referencedColumnName="name")
	//private Customer customer;
	
	//@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL,targetEntity=Product.class)
	//@JoinColumn(name="order_id",referencedColumnName="name")
	//private Product product;
	
	//@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL,targetEntity=Delivery.class)
	//@JoinColumn(name="order_id",referencedColumnName="delivery_id")
	//private List<Delivery> deliveries = new ArrayList<Delivery>();
	
	
	
	
}
