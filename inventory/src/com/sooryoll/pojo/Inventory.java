package com.sooryoll.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="inventory")
public class Inventory {

	@Id
	@Column(name="product_id")
	private String productId;
	private long quantity;
	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER,targetEntity=Product.class)
	@JoinColumn(name="product_id",referencedColumnName="product_id",insertable=false,updatable=false)
	private Product product;
	
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductId() {
		return productId;
	}
	
	
}
