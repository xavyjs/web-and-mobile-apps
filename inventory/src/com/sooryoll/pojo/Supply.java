package com.sooryoll.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.sooryoll.utils.Utilities;
@Entity
public class Supply {

	@Id
	@GeneratedValue
	@Column(name="supply_id")
	private long supplyId;
	@Column(name="material_id")
	private String materialId;
	@Column(name="supplier_id")
	private String supplierId;
	private int quantity;
	@Column(name="parcel_service_id")
	private String parcelServiceId;
	@Column(name="ref_number")
	private String refNumber;
	@Column(name="transport_charges")
	private double transportCharge;
	private String description;
	private Date date;
	
	@OneToOne(fetch=FetchType.EAGER,targetEntity=Material.class)
	@JoinColumn(name="material_id",referencedColumnName="material_id",insertable=false,updatable=false)
	private Material material;
	@OneToOne(fetch=FetchType.EAGER,targetEntity=Supplier.class)
	@JoinColumn(name="supplier_id",referencedColumnName="name",insertable=false,updatable=false)
	private Supplier supplier;
	@OneToOne(fetch=FetchType.EAGER,targetEntity=ParcelService.class)
	@JoinColumn(name="parcel_service_id",referencedColumnName="name",insertable=false,updatable=false)
	private ParcelService parcelService;
	public long getSupplyId() {
		return supplyId;
	}
	public void setSupplyId(long supplyId) {
		this.supplyId = supplyId;
	}
	
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getParcelServiceId() {
		return parcelServiceId;
	}
	public void setParcelServiceId(String parcelServiceId) {
		this.parcelServiceId = parcelServiceId;
	}
	public String getRefNumber() {
		return refNumber;
	}
	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}
	public double getTransportCharge() {
		return transportCharge;
	}
	public void setTransportCharge(double transportCharge) {
		this.transportCharge = transportCharge;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public ParcelService getParcelService() {
		return parcelService;
	}
	public void setParcelService(ParcelService parcelService) {
		this.parcelService = parcelService;
	}
	public void setDateString(String date){
		this.date=Utilities.stringToDate(date);
	}
	public String getDateString(){
		return Utilities.dateToString(date);
	}
	
	
	
}
