package com.sooryoll.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Collection {
	
	@Id
	@GeneratedValue
	@Column(name="collection_id")
	private long collectionId;
	@Column(name="customer")
	private String customerId;
	private double amount;
	@Column(name="payment_type")
	private String paymentType;
	@Column(name="account")
	private String accountId;
	@Column(name="ref_numbet")
	private String refNumber;
	private String description;
	private Date date;
	
	@ManyToOne(targetEntity=Customer.class,cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name="customer",referencedColumnName="name",insertable=false,updatable=false)
	private Customer customer;
	@ManyToOne(targetEntity=Account.class,cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name="account",referencedColumnName="account_number",insertable=false,updatable=false)
	private Account account;
	public long getCollectionId() {
		return collectionId;
	}
	public void setCollectionId(long collectionId) {
		this.collectionId = collectionId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getRefNumber() {
		return refNumber;
	}
	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getDate() {
		return date;
	}
	
	

}
