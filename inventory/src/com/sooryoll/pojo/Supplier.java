package com.sooryoll.pojo;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table (name="supplier")
public class Supplier {

	
	@Id
	@Column (name="name")
	private String name;
	@Column (name="company")
	private String company;
	@Column (name="email")
	private String eMail;
	@Column (name="address1")
	private String address1;
	@Column (name="address2")
	private String address2;
	@Column (name="address3")
	private String address3;
	@Column (name="pincode")
	private long pincode;
	@Column (name="mobile")
	private long mobile;
	
	@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinTable(name="supplier_account_link",joinColumns=@JoinColumn(name="name"),inverseJoinColumns=@JoinColumn(name="account_number"))
	private List<Account> accounts=new ArrayList<Account>();
	
	public String getName()
	{
		return name;
	}
	public String getEMail()
	{
		return eMail;
	}
	public String getAddress1()
	{
		return address1;
	}
	public String getAddress2()
	{
		return address2;
	}
	public String getAddress3()
	{
		return address3;
	}
	public long getPincode()
	{
		return pincode;
	}
	public long getMobile()
	{
		return mobile;
	}
	
	
	public void setName(String s)
	{
		 name=s;
	}
	public void setEMail(String s)
	{
		 eMail=s;
	}
	public void setAddress1(String s)
	{
		 address1=s;
	}
	public void setAddress2(String s)
	{
		 address2=s;
	}
	public void setAddress3(String s)
	{
		 address3=s;
	}
	public void setPincode(long s)
	{
		 pincode=s;
	}
	public void setMobile(long s)
	{
		 mobile=s;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCompany() {
		return company;
	}
	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	public List<Account> getAccounts() {
		return accounts;
	}
	
}
