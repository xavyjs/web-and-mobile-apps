package com.sooryoll.pojo;
import javax.persistence.*;
@Entity
@Table (name="product")
public class Product {
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public double getPricePerLit() {
		return pricePerLit;
	}
	public void setPricePerLit(double pricePerLit) {
		this.pricePerLit = pricePerLit;
	}
	@Id
	@Column (name="product_id")
	private String productId;
	@Column (name="name")
	private String name;
	@Column (name="grade")
	private int grade;
	@Column (name="price_per_lit")
	private double pricePerLit;
	
	

}
