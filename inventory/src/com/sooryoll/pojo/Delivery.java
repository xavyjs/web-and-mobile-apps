package com.sooryoll.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sooryoll.utils.Utilities;

@Entity
@Table(name = "delivery")
public class Delivery {

	@Id
	@GeneratedValue
	@Column(name="delivery_id")
	private long deliveryId;
	@Column(name="order_id")
	private long orderId;
	private int quantity;
	@Column(name="packaging_charge")
	private double packagingCharge;
	@Column(name="transport_type")
	private String transportType;
	@Column(name="parcel_service_id")
	private String parcelServiceId;
	@Column(name="ref_number")
	private String refNumber;
	private String description;
	private Date date;
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER,targetEntity=Order.class)
	@JoinColumn(name="order_id",referencedColumnName="order_id",insertable=false,updatable=false)
	private Order order;
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER,targetEntity=ParcelService.class)
	@JoinColumn(name="parcel_service",referencedColumnName="name",insertable=false,updatable=false)
	private ParcelService parcelService;
	
	
	public long getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(long deliveryId) {
		this.deliveryId = deliveryId;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPackagingCharge() {
		return packagingCharge;
	}
	public void setPackagingCharge(double packagingCharge) {
		this.packagingCharge = packagingCharge;
	}
	public String getTransportType() {
		return transportType;
	}
	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}
	
	public String getRefNumber() {
		return refNumber;
	}
	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public void setParcelService(ParcelService parcelService) {
		this.parcelService = parcelService;
	}
	public ParcelService getParcelService() {
		return parcelService;
	}
	public void setParcelServiceId(String parcelServiceId) {
		this.parcelServiceId = parcelServiceId;
	}
	public String getParcelServiceId() {
		return parcelServiceId;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Order getOrder() {
		return order;
	}
	public String getDateString() {
		return Utilities.dateToString(date);
	}
	public void setDateString(String date) {
		this.date=Utilities.stringToDate(date);
	}
	
	
	
	
	
	
}
