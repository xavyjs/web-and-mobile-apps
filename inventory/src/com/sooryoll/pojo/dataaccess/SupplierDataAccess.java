package com.sooryoll.pojo.dataaccess;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Customer;
import com.sooryoll.pojo.Product;
import com.sooryoll.pojo.Supplier;


public class SupplierDataAccess {
	
	
	public boolean add(Supplier supplier){
		SessionFactory factory=FactoryGenerator.sessionFactory;
		try{
		Session session=factory.openSession();
		session.beginTransaction();
		session.save(supplier);
		session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			
			return false;
			
		}
		
		return true;
	}
	
	public boolean updateSupplier(Supplier supplier){
		
		Session session=null;
		try{
		SessionFactory factory=FactoryGenerator.sessionFactory;
		session=factory.openSession();
		session.beginTransaction();
		System.out.println(supplier.getName());
		session.update(supplier);
		session.getTransaction().commit();
		}
		catch(Exception e){
			session.getTransaction().rollback();
			e.printStackTrace();
			
			return false;
			
		}
		
		return true;
	}
	
	public List<Supplier> list(){
		
		List<Supplier> suppliers;
		
		try{
		SessionFactory factory=FactoryGenerator.sessionFactory;
		Session session=factory.openSession();
		session.beginTransaction();
		suppliers=session.createQuery("from Supplier").list();
		session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			
			return null;
			
		}
		return suppliers;
	}
	public Supplier getSupplier(String name){
		
		Supplier supplier;
		
		try{
		SessionFactory factory=FactoryGenerator.sessionFactory;
		Session session=factory.openSession();
		session.beginTransaction();
		supplier=(Supplier) session.load(Supplier.class,name);
		session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			
			return null;
			
		}
		return supplier;
	}
	
	public boolean removeSupplier(String supplier){
		
			
		SessionFactory factory=FactoryGenerator.sessionFactory;
		Session session=null;
		session=factory.openSession();
		try{
			session.beginTransaction();
			Supplier supp = (Supplier) session.load(Supplier.class, supplier);
			session.delete(supp);
			session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		} finally{
			if(session.isOpen())
				session.close();
		}
		return true;
	}
	
	
}
