package com.sooryoll.pojo.dataaccess;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Order;

public class OrdersDataAccess {

	public List<Order> getPendingOrders(){
		List orders= null;
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		hbSession.beginTransaction();
		try{
			Criteria cr = hbSession.createCriteria(Order.class);
			cr.add(Restrictions.between("status", 0, 1));
			orders = cr.list();
		}catch (Exception e) {
			e.printStackTrace();
			orders=null;
		}finally{
			hbSession.getTransaction().commit();
			hbSession.close();
		}
		return orders;
	}
	public List<Object[]> getProductTotal(){
		List orders= null;
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		hbSession.beginTransaction();
		try{
			Criteria cr = hbSession.createCriteria(Order.class);
			cr.add(Restrictions.between("status", 0, 1));
			cr.setProjection(
					Projections.projectionList().add(
							Projections.groupProperty("productId")
					).add(
							Projections.sum("quantity")
					)
			);
			orders = cr.list();
		}catch (Exception e) {
			e.printStackTrace();
			orders=null;
		}finally{
			hbSession.getTransaction().commit();
			hbSession.close();
		}
		return orders;
	}
	public List<Object[]> getCustomerWiseProduct(){
		List orders= new ArrayList<Object>();
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		hbSession.beginTransaction();
		try{
			Criteria cr = hbSession.createCriteria(Order.class);
			cr.add(Restrictions.between("status", 0, 1));
			cr.setProjection(
					Projections.projectionList().add(
							Projections.groupProperty("customer")
					)
			);
			List<Object> obs = cr.list();
			Iterator<Object> obIt = obs.iterator();
			while(obIt.hasNext()){
				Object ob = obIt.next();
				System.out.println(ob+"");
				cr = hbSession.createCriteria(Order.class);
				cr.add(Restrictions.between("status", 0, 1));
				cr.add(Restrictions.eq("customer", ob+""));
				cr.setProjection(
						Projections.projectionList().add(
								Projections.groupProperty("productId")
						).add(
								Projections.sum("quantity")
						)
				);
				List ordersT= new ArrayList<Object>();
				ordersT = cr.list();
				Object[] obj = new Object[]{ob,ordersT};
				                         
				orders.add(obj);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			orders=null;
		}finally{
			hbSession.getTransaction().commit();
			hbSession.close();
		}
		return orders;
	}
	
	
}
