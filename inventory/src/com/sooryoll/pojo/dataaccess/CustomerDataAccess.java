package com.sooryoll.pojo.dataaccess;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Account;
import com.sooryoll.pojo.Customer;


public class CustomerDataAccess {
	
	public boolean add(Customer customer){
		SessionFactory factory=FactoryGenerator.sessionFactory;
		Session session=factory.openSession();
		try{
		
		session.beginTransaction();
		session.save(customer);
		session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		} finally{
			if(session.isOpen())
				session.close();
		}
		
		return true;
	}
	
	public boolean updateCustomer(Customer customer){
		SessionFactory factory=FactoryGenerator.sessionFactory;
		Session session=null;
		session=factory.openSession();
		try{
			
			session.beginTransaction();
			session.update(customer);
			session.getTransaction().commit();
		}
		catch(Exception e){
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
			
		} finally{
			if(session.isOpen())
				session.close();
		}
		
		return true;
	}
	
	public List<Customer> list(){
		
		List<Customer> customers;
		SessionFactory factory=FactoryGenerator.sessionFactory;
		Session session=factory.openSession();
		try{
		
		
		session.beginTransaction();
		Criteria cr = session.createCriteria(Customer.class);
		customers=cr.list();
		session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();	
			return null;
			
		} finally{
			if(session.isOpen())
				session.close();
		}
		return customers;
	}
	public Customer getCustomer(String name){
		
		Customer cust = new Customer();
		SessionFactory factory=FactoryGenerator.sessionFactory;
		Session session=factory.openSession();
		try{
		session.beginTransaction();
		Customer customer = (Customer) session.load(Customer.class,name);
		cust.setName(customer.getName());
		cust.setAddress1(customer.getAddress1());
		cust.setAddress2(customer.getAddress2());
		cust.setAddress3(customer.getAddress3());
		cust.setCompany(customer.getCompany());
		cust.setEMail(customer.getEMail());
		cust.setMobile(customer.getMobile());
		cust.setPincode(customer.getPincode());
		session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			return null;	
		} finally{
			if(session.isOpen())
				session.close();
		}
		return cust;
	}
	
	public boolean removeCustomer(String customerName){
		
		
		SessionFactory factory=FactoryGenerator.sessionFactory;
		Session session=null;
		session=factory.openSession();
		try{
			session.beginTransaction();
			Customer cust = (Customer) session.load(Customer.class, customerName);
			session.delete(cust);
			session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		} finally{
			if(session.isOpen())
				session.close();
		}
		return true;
	}
	
	
	
	
}
