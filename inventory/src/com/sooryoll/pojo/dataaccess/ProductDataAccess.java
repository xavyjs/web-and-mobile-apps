package com.sooryoll.pojo.dataaccess;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.sooryoll.hibernateutil.FactoryGenerator;

import com.sooryoll.pojo.Product;


public class ProductDataAccess {
	
	
	public boolean add(Product product){
		SessionFactory factory=FactoryGenerator.sessionFactory;

		try{
		
		Session session=factory.openSession();
		session.beginTransaction();
		session.save(product);
		session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			
			return false;
			
		}
		
		return true;
	}
	
	public boolean updateProduct(Product product){
		SessionFactory factory=FactoryGenerator.sessionFactory;
		Session session=null;
		try{
		
		session=factory.openSession();
		session.beginTransaction();
		session.update(product);
		session.getTransaction().commit();
		}
		catch(Exception e){
			session.getTransaction().rollback();
			e.printStackTrace();
			
			return false;
			
		}
		
		return true;
	}
	
	public List<Product> list(){
		
		List<Product> products;
		SessionFactory factory=FactoryGenerator.sessionFactory;
		try{
		
		Session session=factory.openSession();
		session.beginTransaction();
		products=session.createQuery("from Product").list();
		session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			
			return null;
			
		}
		return products;
	}
	public Product getProduct(String name){
		
		Product product;
		
		SessionFactory factory=FactoryGenerator.sessionFactory;
		try{
		
		Session session=factory.openSession();
		session.beginTransaction();
		product=(Product) session.load(Product.class,name);
		
		session.getTransaction().commit();
		}
		catch(Exception e){
			e.printStackTrace();
			
			return null;
			
		}
		System.out.println("sss"+product.getName());
		return product;
	}
	
	public boolean removeProduct(Product product){
		
		
		SessionFactory factory=FactoryGenerator.sessionFactory;
		Session session=null;
		try{
		
		session=factory.openSession();
		session.beginTransaction();
		session.createSQLQuery("delete from product where name = '"+product.getName()+"'").executeUpdate();
		session.getTransaction().commit();
		
		}
		catch(Exception e){
			e.printStackTrace();
			session.getTransaction().rollback();
			
			return false;
			
		}
		return true;
	}
	
	
}
