package com.sooryoll.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="due")
public class Due {

	@Id
	@Column(name="supplier_id")
	private String supplierId;
	private double amount;
	@OneToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL,targetEntity=Supplier.class)
	@JoinColumn(name="supplier_id",referencedColumnName="name",insertable=false,updatable=false)
	private Supplier supplier;
	
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	
	
	
}
