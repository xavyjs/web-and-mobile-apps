package com.sooryoll.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Payment {
	
	@Id
	@GeneratedValue
	@Column(name="payment_id")
	private long paymentId;
	@Column(name="supplier_id")
	private String supplierId;
	private double amount;
	@Column(name="transfer_type")
	private String transferType;
	@Column(name="account_id")
	private String accountId;
	@Column(name="ref_number")
	private String refNumner;
	private String description;
	private Date date;
	@ManyToOne(cascade=CascadeType.ALL,targetEntity=Supplier.class,fetch=FetchType.EAGER)
	@JoinColumn(name="supplier_id",referencedColumnName="name",insertable=false,updatable=false)
	private Supplier supplier;
	@ManyToOne(cascade=CascadeType.ALL,targetEntity=Account.class,fetch=FetchType.EAGER)
	@JoinColumn(name="account_id",referencedColumnName="account_number",insertable=false,updatable=false)
	private Account account;
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getTransferType() {
		return transferType;
	}
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getRefNumner() {
		return refNumner;
	}
	public void setRefNumner(String refNumner) {
		this.refNumner = refNumner;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}
	public long getPaymentId() {
		return paymentId;
	}
	
	
}
