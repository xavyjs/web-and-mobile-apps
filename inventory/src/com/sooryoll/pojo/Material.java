package com.sooryoll.pojo;
import javax.persistence.*;
@Entity
@Table (name="material")
public class Material {

	@Id
	@Column (name="material_id")
	private String materialId;
	@Column (name="grade")
	private int grade;
	@Column (name="priceperlit")
	private double pricePerLit;
	@Column (name="name")
	private String name;
	
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public double getPricePerLit() {
		return pricePerLit;
	}
	public void setPricePerLit(double pricePerLit) {
		this.pricePerLit = pricePerLit;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	
	
}
