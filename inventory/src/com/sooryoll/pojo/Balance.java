package com.sooryoll.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name="balance")
public class Balance {

	@Id
	@Column(name="customer")
	private String customerId;
	private double amount;

	@OneToOne(fetch=FetchType.EAGER, targetEntity=Customer.class, cascade=CascadeType.ALL)
	@JoinColumn(name = "customer", referencedColumnName="name" ,insertable = false, updatable = false)
	private Customer customer;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	} 
	
	
}
