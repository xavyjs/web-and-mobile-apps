package com.sooryoll.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity 
@Table(name="stock")
public class Stock {

	@Id
	@Column(name="material_id")
	private String materialId;
	private long quantity;
	@OneToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL,targetEntity=Material.class)
	@JoinColumn(name="material_id",referencedColumnName="material_id",insertable=false,updatable=false)
	private Material material;
	
	
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	
	
	
}
