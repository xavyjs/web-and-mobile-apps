package com.sooryoll.pojo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Spending{

	@Id
	@GeneratedValue
	@Column(name="spending_id")
	private long spendingId;
	private String category;
	private double amount;
	private String description;
	@Column(name="ref_number")
	private String refNumner;
	private Date date;
	public long getSpendingId() {
		return spendingId;
	}
	public void setSpendingId(long spendingId) {
		this.spendingId = spendingId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRefNumner() {
		return refNumner;
	}
	public void setRefNumner(String refNumner) {
		this.refNumner = refNumner;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
}
