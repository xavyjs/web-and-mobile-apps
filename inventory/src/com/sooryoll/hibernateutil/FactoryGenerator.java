package com.sooryoll.hibernateutil;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;

import sun.org.mozilla.javascript.internal.Ref;

import com.sooryoll.pojo.*;

public class FactoryGenerator {
	
	public static final SessionFactory sessionFactory;
	static{
		sessionFactory=getSessionFactory();
	}
	private static SessionFactory getSessionFactory()
	{
		Configuration configuration = new Configuration();
		
		configuration.addAnnotatedClass(ParcelService.class);
		configuration.addAnnotatedClass(Account.class);
		configuration.addAnnotatedClass(RefOrderStatus.class);
		configuration.addAnnotatedClass(Supplier.class);
		configuration.addAnnotatedClass(Material.class);
		configuration.addAnnotatedClass(Customer.class);
		configuration.addAnnotatedClass(Product.class);
		configuration.addAnnotatedClass(Order.class);
		configuration.addAnnotatedClass(Delivery.class);
		configuration.addAnnotatedClass(Supply.class);
		configuration.addAnnotatedClass(Balance.class);
		configuration.addAnnotatedClass(Due.class);
		configuration.addAnnotatedClass(Collection.class);
		configuration.addAnnotatedClass(Payment.class);
		configuration.addAnnotatedClass(Inventory.class);
		configuration.addAnnotatedClass(Stock.class);
		configuration.addAnnotatedClass(Spending.class);
		configuration.addAnnotatedClass(Login.class);
		configuration.configure();
	    return  configuration.buildSessionFactory(new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry());
	}
	

}
