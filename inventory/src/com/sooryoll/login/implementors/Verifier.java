package com.sooryoll.login.implementors;

import java.util.List;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.login.interfaces.Verifiable;
import com.sooryoll.pojo.Login;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;
import com.sooryoll.utils.Utilities;

public class Verifier extends FIMImplementor implements Verifiable{

	private String userId;
	private String password;
	private boolean result;
	private String welcomeName;
	
	
	public List<String> getMessages(){
		return messages;
	}
	public String getWelcomeName() {
		return welcomeName;
	}
	public boolean isResult() {
		return result;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		result =true;
		try{
			DataAccessLayer<Login> loginAccessLayer = new DataAccessLayer<Login>(Login.class,hbSession);
			Login login = loginAccessLayer.retrive(userId);
			if(login.getPassword().equals(Utilities.encrypt(password))){
				welcomeName="Welcome";
			}else{
				result = false;
			}
		}catch (Exception e) {
			result = false;
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		return super.execute();
	}
	
}
