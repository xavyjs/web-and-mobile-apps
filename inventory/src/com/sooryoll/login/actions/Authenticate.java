package com.sooryoll.login.actions;

import com.sooryoll.login.implementors.Verifier;
import com.sooryoll.login.interfaces.Verifiable;
import com.sooryoll.struts.FIMAction;

public class Authenticate extends FIMAction<Verifiable>{

	@Override
	protected Verifiable getLogic() {
		return new Verifier();
	}

}
