package com.sooryoll.login.interfaces;

import java.util.List;

import com.sooryoll.struts.FIMInterface;

public interface Verifiable extends FIMInterface{

	public String getWelcomeName();
	public boolean isResult();
	public void setUserId(String userId);
	public void setPassword(String password);
	public List<String> getMessages();
}
