package com.sooryoll.orders.implementors;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.orders.interfaces.Loadable;
import com.sooryoll.pojo.Customer;
import com.sooryoll.pojo.Order;
import com.sooryoll.pojo.ParcelService;
import com.sooryoll.pojo.Product;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.pojo.dataaccess.OrdersDataAccess;
import com.sooryoll.struts.FIMImplementor;
import java.util.*;

import org.hibernate.Session;

public class Loader extends FIMImplementor implements Loadable{
	
	private List<Customer> customers = new ArrayList<Customer>();
	private List<Product> products = new ArrayList<Product>();
	private List<Order> orders = new ArrayList<Order>();
	private List<Object[]> productTotal;
	private List<Object[]> customerWiseProduct;
	private List<ParcelService> transports = new ArrayList<ParcelService>();
	private boolean result = true;
	
	
	
	public List<ParcelService> getTransports() {
		return transports;
	}
	public void setTransports(List<ParcelService> transports) {
		this.transports = transports;
	}
	public List<Object[]> getCustomerWiseProduct() {
		return customerWiseProduct;
	}
	public boolean isResult() {
		return result;
	}
	public List<Customer> getCustomers() {
		return customers;
	}
	public List<Product> getProducts() {
		return products;
	}
	
	public List<Order> getOrders() {
		return orders;
	}
	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Customer> dcC = new DataAccessLayer<Customer>(Customer.class,hbSession);
			DataAccessLayer<Product> dcP = new DataAccessLayer<Product>(Product.class,hbSession);
			DataAccessLayer<ParcelService> pDa = new DataAccessLayer<ParcelService>(ParcelService.class,hbSession);
			OrdersDataAccess oDa = new OrdersDataAccess();
			customers = dcC.list();
			products = dcP.list();
			orders = oDa.getPendingOrders();
			productTotal = oDa.getProductTotal();
			customerWiseProduct = oDa.getCustomerWiseProduct();
			transports = pDa.list();
			
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			customers = null;
			products = null;
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		 
		return true;
	}
	public List<Object[]> getProductTotal() {
		return productTotal;
	}
	

}
