package com.sooryoll.orders.implementors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.orders.interfaces.Addable;
import com.sooryoll.pojo.Order;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.pojo.dataaccess.OrdersDataAccess;
import com.sooryoll.struts.FIMImplementor;

public class Adder extends FIMImplementor implements Addable{

	private OrderRequest orderRequest = new OrderRequest();
	private boolean result = true;
	private List<Order> orders = new ArrayList<Order>();
	private List<Object[]> productTotal;
	private List<Object[]> customerWiseProduct;
	
	
	
	public List<Object[]> getProductTotal() {
		return productTotal;
	}

	public List<Object[]> getCustomerWiseProduct() {
		return customerWiseProduct;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public boolean isResult() {
		return result;
	}

	
	public boolean execute(){
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		Order order = new Order();
		try{
			order.setCustomer(orderRequest.getCustomer());
			order.setProductId(orderRequest.getProductId());
			order.setQuantity(Long.parseLong(orderRequest.getQuantity()));
			order.setStatus(0);
			order.setDate(new Date());
			order.setDescription(orderRequest.getDescription());
			order.setFulfilledDate(null);
			DataAccessLayer<Order> oDc = new DataAccessLayer<Order>(Order.class,hbSession);
			oDc.add(order);
			OrdersDataAccess oDa = new OrdersDataAccess();
			orders = oDa.getPendingOrders();
			productTotal = oDa.getProductTotal();
			customerWiseProduct = oDa.getCustomerWiseProduct();
			
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		return true;
	}

	public void setOrderRequest(OrderRequest orderRequest) {
		this.orderRequest = orderRequest;
	}

	public OrderRequest getOrderRequest() {
		return orderRequest;
	}
}
