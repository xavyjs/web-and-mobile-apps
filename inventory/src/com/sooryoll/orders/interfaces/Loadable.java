package com.sooryoll.orders.interfaces;

import java.util.List;

import com.sooryoll.pojo.Customer;
import com.sooryoll.pojo.Order;
import com.sooryoll.pojo.ParcelService;
import com.sooryoll.pojo.Product;
import com.sooryoll.struts.FIMInterface;

public interface Loadable extends FIMInterface{

	public List<Customer> getCustomers();
	public List<Product> getProducts();
	public List<Order> getOrders();
	public boolean isResult();
	public List<Object[]> getProductTotal();
	public List<Object[]> getCustomerWiseProduct();
	public List<ParcelService> getTransports();
}
