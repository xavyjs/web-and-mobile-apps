package com.sooryoll.orders.interfaces;

import java.util.List;

import com.sooryoll.orders.implementors.OrderRequest;
import com.sooryoll.pojo.Delivery;
import com.sooryoll.pojo.Order;
import com.sooryoll.struts.FIMInterface;

public interface Addable extends FIMInterface{

	
	public List<Object[]> getProductTotal();
	public List<Object[]> getCustomerWiseProduct();
	
	public void setOrderRequest(OrderRequest orderRequest);

	public OrderRequest getOrderRequest();
	public List<Order> getOrders();
	
}
