package com.sooryoll.orders.actions;

import com.sooryoll.orders.implementors.Loader;
import com.sooryoll.orders.interfaces.Loadable;
import com.sooryoll.struts.FIMAction;

public class Load extends FIMAction<Loadable>{

	@Override
	protected Loadable getLogic() {
		return new Loader();
	}

}
