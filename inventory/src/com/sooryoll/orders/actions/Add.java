package com.sooryoll.orders.actions;

import com.sooryoll.orders.implementors.Adder;
import com.sooryoll.orders.interfaces.Addable;
import com.sooryoll.struts.FIMAction;

public class Add extends FIMAction<Addable>{

	@Override
	protected Addable getLogic() {
		return new Adder();
	}

}
