package com.sooryoll.materials.interfaces;

import java.util.List;

import com.sooryoll.pojo.Material;
import com.sooryoll.struts.FIMInterface;

public interface Loadable extends FIMInterface {

	public List<Material> getMaterials();
}
