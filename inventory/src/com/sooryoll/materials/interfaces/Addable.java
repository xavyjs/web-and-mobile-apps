package com.sooryoll.materials.interfaces;

import com.sooryoll.pojo.Material;
import com.sooryoll.struts.FIMInterface;

public interface Addable extends FIMInterface{

	public Material getMaterial();
	public void setMaterial(Material material);

	public boolean isResult();
}
