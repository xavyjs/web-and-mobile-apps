package com.sooryoll.materials.implementors;


import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.materials.interfaces.Updatable;
import com.sooryoll.pojo.Material;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;

public class Updater extends FIMImplementor implements Updatable{

	private Material material;
	private boolean result;

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	

	public boolean isResult() {
		return result;
	}

	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Material> mDa = new DataAccessLayer<Material>(Material.class,hbSession);
			result = mDa.update(material);
		}catch (Exception e) {
			result = false;
		}finally{
			if(hbSession.isOpen())
				hbSession.close();
		}
		return true;
	}
	
	
}
