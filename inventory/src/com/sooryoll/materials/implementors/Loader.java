package com.sooryoll.materials.implementors;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.materials.interfaces.Loadable;
import com.sooryoll.pojo.Material;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;

public class Loader extends FIMImplementor implements Loadable{

	List<Material> materials = new ArrayList<Material>();
	
	public List<Material> getMaterials() {
		return materials;
	}

	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Material> mDa = new DataAccessLayer<Material>(Material.class,hbSession);
			materials = mDa.list();
		}catch (Exception e) {
			
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		return true;
	}
}
