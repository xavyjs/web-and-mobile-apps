package com.sooryoll.materials.actions;

import com.sooryoll.materials.implementors.Adder;
import com.sooryoll.materials.interfaces.Addable;
import com.sooryoll.struts.FIMAction;

public class Add extends FIMAction<Addable>{

	@Override
	protected Addable getLogic() {
		return new Adder();
	}

}
