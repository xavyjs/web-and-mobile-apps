package com.sooryoll.materials.actions;

import com.sooryoll.materials.implementors.Updater;
import com.sooryoll.materials.interfaces.Updatable;
import com.sooryoll.struts.FIMAction;

public class Update extends FIMAction<Updatable>{

	@Override
	protected Updatable getLogic() {
		return new Updater();
	}

}
