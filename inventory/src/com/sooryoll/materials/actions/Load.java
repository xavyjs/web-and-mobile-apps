package com.sooryoll.materials.actions;

import com.sooryoll.materials.implementors.Loader;
import com.sooryoll.materials.interfaces.Loadable;
import com.sooryoll.struts.FIMAction;

public class Load extends FIMAction<Loadable>{

	@Override
	protected Loadable getLogic() {
		return new Loader();
	}

}
