package com.sooryoll.supply.implementors;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Supply;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;
import com.sooryoll.supply.interfaces.Updatable;

public class Updater extends FIMImplementor implements Updatable{

	private Supply supply;
	private boolean result;

	public void setSupply(Supply supply) {
		this.supply = supply;
	}

	public Supply getSupply() {
		return supply;
	}

	
	

	public boolean isResult() {
		return result;
	}

	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Supply> sDa = new DataAccessLayer<Supply>(Supply.class,hbSession);
			sDa.update(supply);
			result = true;
		}catch (Exception e) {
			result=false;
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		supply = null;
		return true;
	}

	
}
