package com.sooryoll.supply.implementors;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Due;
import com.sooryoll.pojo.Material;
import com.sooryoll.pojo.Spending;
import com.sooryoll.pojo.Stock;
import com.sooryoll.pojo.Supply;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;
import com.sooryoll.supply.interfaces.Addable;

public class Adder extends FIMImplementor implements Addable{

	private Supply supply;
	private boolean result;
	private String message;


	
	
	
	public String getMessage() {
		return message;
	}

	public Supply getSupply() {
		return supply;
	}

	public void setSupply(Supply supply) {
		this.supply = supply;
	}

	public boolean isResult() {
		return result;
	}

	@Override
	public boolean execute() {
		result = true;
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		System.out.println(supply.getMaterialId());
		System.out.println(supply.getSupplierId());
		System.out.println(supply.getTransportCharge());
		System.out.println(supply.getParcelServiceId());
		try{
			DataAccessLayer<Material> mDa = new DataAccessLayer<Material>(Material.class,hbSession);
			DataAccessLayer<Due> dueAccessLayer = new DataAccessLayer<Due>(Due.class,hbSession);
			DataAccessLayer<Stock> stockAccessLayer = new DataAccessLayer<Stock>(Stock.class,hbSession);
			Due due;
			try{
				due = dueAccessLayer.retrive(supply.getSupplierId());
				due.getSupplierId();
			}catch (HibernateException e) {
				due = new Due();
				due.setSupplierId(supply.getSupplierId());
				due.setAmount(0);
				dueAccessLayer.add(due);
			}
			Stock stock;
			try{
				stock = stockAccessLayer.retrive(supply.getMaterialId());
				stock.getMaterial();
			}catch (HibernateException e) {
				stock = new Stock();
				stock.setMaterialId(supply.getMaterialId());
				stock.setQuantity(0);
				stockAccessLayer.add(stock);
			}
			try{
				System.out.println("try");
				Spending spending= new Spending();
				spending.setAmount(supply.getTransportCharge());
				spending.setDate(new Date());
				spending.setCategory("parcel charge");
				spending.setRefNumner(supply.getRefNumber());
				hbSession.beginTransaction();
				Material m = (Material) hbSession.load(Material.class,supply.getMaterialId());
				stock.setQuantity(stock.getQuantity()+supply.getQuantity());
				due.setAmount(due.getAmount()+(supply.getQuantity()*m.getPricePerLit()));
				hbSession.save(supply);
				hbSession.update(stock);
				hbSession.update(due);
				hbSession.save(spending);
				hbSession.getTransaction().commit();
			}catch (HibernateException e) {
				System.out.println("catch");
				e.printStackTrace();
				result = false;
				hbSession.getTransaction().rollback();
				message = "some thing went wrong!!!";
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		supply = null;
		return true;
	}
}
