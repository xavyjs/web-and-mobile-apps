package com.sooryoll.supply.implementors;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Due;
import com.sooryoll.pojo.Stock;
import com.sooryoll.pojo.Supply;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;
import com.sooryoll.supply.interfaces.Removable;

public class Remover extends FIMImplementor implements Removable{

	private long id;
	private boolean result;

	public void setId(long id) {
		this.id = id;
	}

	public boolean isResult() {
		return result;
	}
	public List<String> getMessages(){
		return messages;
	}
	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		DataAccessLayer<Supply> supplyaAccessLayer = new DataAccessLayer<Supply>(Supply.class,hbSession);
		DataAccessLayer<Due> dueAccessLayer = new DataAccessLayer<Due>(Due.class,hbSession);
		DataAccessLayer<Stock> stockAccessLayer = new DataAccessLayer<Stock>(Stock.class,hbSession);
		Supply supply = supplyaAccessLayer.retrive(id);
		Due due = dueAccessLayer.retrive(supply.getSupplierId());
		Stock stock = stockAccessLayer.retrive(supply.getMaterialId());
		due.setAmount(due.getAmount()-(supply.getMaterial().getPricePerLit()*supply.getQuantity()));
		stock.setQuantity(stock.getQuantity()-supply.getQuantity());
		hbSession.beginTransaction();
		try{
			
			hbSession.update(due);
			hbSession.update(stock);
			hbSession.delete(supply);
			hbSession.getTransaction().commit();
			result = true;
		}catch (HibernateException e) {
			e.printStackTrace();
			hbSession.getTransaction().rollback();
			messages.add(DEFAULT_ERROR);
			result= false;
		}finally{
			if(hbSession.isOpen())
				hbSession.close();
		}
		
		return true;
	}
}
