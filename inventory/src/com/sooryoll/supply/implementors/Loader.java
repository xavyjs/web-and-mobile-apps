package com.sooryoll.supply.implementors;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.sooryoll.hibernateutil.FactoryGenerator;
import com.sooryoll.pojo.Material;
import com.sooryoll.pojo.ParcelService;
import com.sooryoll.pojo.Supplier;
import com.sooryoll.pojo.Supply;
import com.sooryoll.pojo.dataaccess.DataAccessLayer;
import com.sooryoll.struts.FIMImplementor;
import com.sooryoll.supply.interfaces.Loadable;

public class Loader extends FIMImplementor implements Loadable{

	private List<Supply> supplies = new ArrayList<Supply>();
	private List<Supplier> suppliers = new ArrayList<Supplier>();
	private List<Material> materials = new ArrayList<Material>();
	private List<ParcelService> transports = new ArrayList<ParcelService>();

	
	
	
	public List<ParcelService> getTransports() {
		return transports;
	}
	public List<Supply> getSupplies() {
		return supplies;
	}
	public List<Supplier> getSuppliers() {
		return suppliers;
	}
	public List<Material> getMaterials() {
		return materials;
	}



	@Override
	public boolean execute() {
		Session hbSession = FactoryGenerator.sessionFactory.openSession();
		try{
			DataAccessLayer<Supply> supplyDataAccessLayer = new DataAccessLayer<Supply>(Supply.class,hbSession);
			DataAccessLayer<Supplier> supplierAccessLayer = new DataAccessLayer<Supplier>(Supplier.class,hbSession);
			DataAccessLayer<Material> materialAccessLayer = new DataAccessLayer<Material>(Material.class,hbSession);
			DataAccessLayer<ParcelService> parcelServiseAccessLayer = new DataAccessLayer<ParcelService>(ParcelService.class,hbSession);
			suppliers = supplierAccessLayer.list();
			materials = materialAccessLayer.list();
			supplies = supplyDataAccessLayer.list();
			System.out.println(supplies.size());
			transports = parcelServiseAccessLayer.list();
		}catch (Exception e) {
			supplies = null;
		}finally{
			if(hbSession.isOpen()){
				hbSession.close();
			}
		}
		return true;
	}
}
