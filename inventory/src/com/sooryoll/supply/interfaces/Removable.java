package com.sooryoll.supply.interfaces;

import java.util.List;

import com.sooryoll.struts.FIMInterface;

public interface Removable extends FIMInterface{

	public void setId(long id);
	public boolean isResult();
	public List<String> getMessages();
}
