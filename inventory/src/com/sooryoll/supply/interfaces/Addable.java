package com.sooryoll.supply.interfaces;

import com.sooryoll.pojo.Supply;
import com.sooryoll.struts.FIMInterface;

public interface Addable extends FIMInterface{

	public Supply getSupply();
	public void setSupply(Supply supplier);
	public boolean isResult();
}
