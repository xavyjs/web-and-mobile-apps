package com.sooryoll.supply.interfaces;

import java.util.List;

import com.sooryoll.pojo.Supply;
import com.sooryoll.struts.FIMInterface;

public interface Loadable extends FIMInterface{

	public List<Supply> getSupplies();
}
