package com.sooryoll.supply.interfaces;

import com.sooryoll.pojo.Supply;
import com.sooryoll.struts.FIMInterface;

public interface Updatable extends FIMInterface{

	public void setSupply(Supply supply) ;
	public Supply getSupply();
	public boolean isResult();
}
