package com.sooryoll.supply.actions;

import com.sooryoll.struts.FIMAction;
import com.sooryoll.supply.implementors.Remover;
import com.sooryoll.supply.interfaces.Removable;

public class Remove extends FIMAction<Removable>{

	@Override
	protected Removable getLogic() {
		return new Remover();
	}

}
