package com.sooryoll.supply.actions;

import com.sooryoll.struts.FIMAction;
import com.sooryoll.supply.implementors.Updater;
import com.sooryoll.supply.interfaces.Updatable;

public class Update extends FIMAction<Updatable>{

	@Override
	protected Updatable getLogic() {
		return new Updater();
	}

}
