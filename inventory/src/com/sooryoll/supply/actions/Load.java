package com.sooryoll.supply.actions;

import com.sooryoll.struts.FIMAction;
import com.sooryoll.supply.implementors.Loader;
import com.sooryoll.supply.interfaces.Loadable;

public class Load extends FIMAction<Loadable>{

	@Override
	protected Loadable getLogic() {
		return new Loader();
	}

}
