package com.sooryoll.supply.actions;

import com.sooryoll.struts.FIMAction;
import com.sooryoll.supply.implementors.Adder;
import com.sooryoll.supply.interfaces.Addable;

public class Add extends FIMAction<Addable>{

	@Override
	protected Addable getLogic() {
		return new Adder();
	}

}
